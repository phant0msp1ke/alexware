main_dir = '/media/alex/5FC39EAD5A6AA312/Neuropixels_DATA/Sorted/Normal';
t_bin_ms = 20;
stim_type = 'normal';

animal_list = dir(main_dir);
animal_list = animal_list(~ismember({animal_list.name},{'.','..'}));
for animal = 1:numel(animal_list)
    fprintf('== Processing animal %s ==\n',animal_list(animal).name);
    pen_dir = dir([fullfile(animal_list(animal).folder, animal_list(animal).name),'/**/*ap.bin']);
    for pen = 1:length(pen_dir)
        sorted_dir = pen_dir(pen).folder;
        fprintf('== Working on: %s ==\n',sorted_dir);
        sahani_slow(sorted_dir, t_bin_ms, stim_type);
    end
end