function psth_final = psth_func_switch(Y,start_time_ms,psth_end_time_s,t_bin_ms,grid_root)
%psth_final = psth_func(Y,start_time_ms,grid_root)
%Add a normal description


%% Initial parameters
n_stim = 8; %The number of switching sound stimuli
n_reps = 10; %The number of repeats for each echoic condition

psth_end_time_ms = psth_end_time_s*1000; %Convert to ms 
edges_ms = (0:t_bin_ms:psth_end_time_ms); %The edges of the histogram

%% Get PSTHs
spike_times_ms = Y(:,1).*1000; % Get the spike times in ms
clusters = Y(:,2);
ix_clusters = unique(clusters); %Find all the units in the input file
n_clusters = length(ix_clusters);

dir_info = dir([grid_root '/*Info.mat']); %Get the names of all files with this extension
grid_filename = fullfile(dir_info.folder, dir_info.name); %Form the name of the file to be opened
grid_ben = load(grid_filename);

%Initialize the struct array
psth_final(n_clusters).data = [];
psth_final(n_clusters).cluster_id = [];


fprintf('== Generating PSTHs ==\n');tic;
parfor cluster = 1:n_clusters
    fprintf('== Processing cluster %.0f/%.0f ==\n',cluster,n_clusters);
    psth_temp = cell(1,n_stim); %Initialize an empty cell to contain the stimuli
    cluster_id = ix_clusters(cluster); %Load the current unit
    
    for s = 1:n_stim
        ixs = [];
        ixs = find(grid_ben.grid.randomisedGridSetIdx==s); %Find the indices of the repeats for a given stimulus
        
        for r = 1:n_reps
            ix_stim_ms = start_time_ms(ixs(r)); %Get the appropritate start time
            psth_temp{s}(r,:) = histc(spike_times_ms(clusters == cluster_id),ix_stim_ms + edges_ms); %Make a histogram and store it
        end
        
    end
    
    psth_final(cluster).data = cell2mat(psth_temp); %Combine the histograms from all the stimuli for a given cluster
    psth_final(cluster).cluster_id = cluster_id;
%     imagesc(psth_final(cluster).data);caxis([0 1]); %Plot to check the
%     clusters
%     pause(0.5);
end

fprintf('== Done! Processing took %0.fs ==\n',toc);
end