% function match_immuno_to_roi(lbl_path, mask_path)
%function match_immuno_to_roi(lbl_path, mask_path)
%This function takes the immuno label coordinates and assigns a ROI number
%to them based on the ROI mask


lbl_path = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction/Leela/results/area04/slice5/cell_label.mat';

mask_path = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction/Leela/img_mask/area04/ROIMask.mat';

%Load the immuno cell labels
temp_lbl = load(lbl_path);
cell_label = temp_lbl.cell_label;
cell_types = fieldnames(cell_label);

temp_roi = load(mask_path);
mask = temp_roi.ROIMask; %Load the mask
roi_nums = unique(mask(:)); %Get the unique ROI numbers
roi_nums(roi_nums==0) = []; %Remove the zero from them
n_rois = length(roi_nums); %Get number of unique rois
sz_mask = size(mask); %Get the size of the mask

cell_label.mask = mask; %Append the ROI mask to cell_label
cell_label.bimask = logical(mask); %Append also a binary version of the ROI mask to cell_label
cell_label.roi_nums = roi_nums; %Append the actual ROI numbers 
cell_label.n_rois = n_rois; %Append the total number of ROIs

%Find the coordiantes of every roi
roi_lin_ix = cell(n_rois,1);
roi_xy_ix = cell(n_rois,1);

for j = 1:n_rois
    cur_roi_n = roi_nums(j);
    roi_lin_ix{j} = find(mask==cur_roi_n);
    [roi_xy_ix{j}(:,2), roi_xy_ix{j}(:,1)] = ind2sub(size(mask), roi_lin_ix{j});
end

%Loop over the cell types to assign a ROI number
for c = 1:length(cell_types)
    ct = cell_types{c};
    n_cell = length(cell_label.(ct).imjz);
    %Initialise
    cell_label.(ct).roi_match = []; 
    cell_label.(ct).roi_match_d = []; 
    
    %Loop over each cell from a given type to find its ROI number 
    for k = 1:n_cell
        matxy = round(cell_label.(ct).matxy(k,:)); %Get the xy values for every immuno cell
        row = matxy(2); col = matxy(1);
        lin_ix = sub2ind(sz_mask, row, col);
        
        %Search over the ROI numbers to find which one it belongs to
        roi_ix = find(cell2mat(cellfun(@(x) ismember(lin_ix,x), roi_lin_ix, 'UniformOutput', false))==1);
        roi_no = roi_nums(roi_ix); %Find the corresponding ROI number
        cell_label.(ct).roi_match(k,1) = roi_no; %Assign it to the cell_label var
        
        %Compute using the Eclidean distance between the clicked immuno
        %cell and the ROI with the closest pixel
        [~, roi_ix_d] = min(cell2mat(cellfun(@(x) min(sqrt(sum((matxy-x).^2,2))), roi_xy_ix, 'UniformOutput', false))); %Find the ix of the ROI which has the closest pixel to the clicked immuno cell
        roi_no_d = roi_nums(roi_ix_d); %Find the corresponding ROI number
        cell_label.(ct).roi_match_d(k,1) = roi_no_d; %Assign it to the cell_label var

    end
end

%Save the results
save(lbl_path,'cell_label');

        