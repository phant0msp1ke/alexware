%This is a script based on tpti_matchingexample_alexdata and
%tpti_matchingexample where I want to clean up the code and make it more
%readable and accessible

animal_name = 'Leela'; %Name of the animal
area_name = 'area01'; %Name of the area
plane_name = 'slice6'; %Name of the plane
 
%Ask the user whether the images have been transformed yet. Setup a logical
%flag for this
prompt = 'Have the images been transformed? y/n [y]: ';
resp = input(prompt,'s');

if  strcmp(resp,'n')
    already_transformed = 0;
elseif isempty(resp) || strcmp(resp,'y')
    already_transformed = 1; 
else
    error('Invalid input. This should be yes/no')
end


base_dir = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction/'; %The base dir for the Reconstruction
full_name = [animal_name,'_', area_name]; %The full name
conf_full_name = [animal_name,'_', area_name,'_', plane_name]; %The full name for the confocal files
tmp_folder = fullfile(base_dir,'tmp/'); %The temporary folder where to store the ImageJ macro scripts which are created nascently
ROOTNEW = fullfile(base_dir, animal_name, 'results', area_name, plane_name, filesep); %The name of the folder with the binary files to be transformed

%Load the ROI mask, make a red mask and a transperancy mask for later use
transperancy = 0.2;
roi_mask_path = fullfile(ROOTNEW,[full_name, '.mat']);
temp_roi_mask = load(roi_mask_path); %Load the ROI mask
ROImask = temp_roi_mask.SEGMENT.bwcells;
mask = double(ROImask); %Convert mask to double
red_mask = cat(3, ones(size(mask)), zeros(size(mask)), zeros(size(mask))); %Make a red mask
transp_mask = transperancy*mask; %Make a transperancy mask

dbstop if error;     
     
javaaddpath '/home/alex/java/jar/mij.jar'; 

try 
    MIJ.exit; 
catch
    fprintf('IMageJ already closed\n');
end
close all;


tocopy = 0;
RSG.savedir       = ROOTNEW;
RSG.rootdir2      = ROOTNEW;
RSG.rootdir3      = ROOTNEW;
RSG.savedirSEG{1} = ROOTNEW;
RSG.savedirSEG{4} = ROOTNEW;
ImmunoDir         = ROOTNEW;
 

%--------------------------------------------------------------------------
% logbook info about recordings

clear info;

ID = 'M99_20190101'; % Alex data
info.(ID).site = 'a';
bix=1;
info.(ID).block_segment= bix;
info.(ID).fnamePC1{bix}= full_name;
info.(ID).fnamePC2{bix}= full_name;
info.(ID).fnamePC3{bix}= full_name;


%--------------------------------------------------------------------------

rix = 1;
ROIseslist{rix} = {'M99_20190101'};
ROIsesblks{rix} = {[1]    [1]    [1]};
ROIsitenam{rix} = 'M99a';

ALLERROR={};
for rix=1
    for layer=2
     
        fprintf('%d:%s\n',rix,ROIseslist{rix}{1});
        
        do_review = 1; % leave as 1
        if do_review
            
            % review correspondence in GUI
            clear global mystackA mystackU;
            
            cfg = [];
            cfg.ROIseslist=ROIseslist{rix};
            cfg.ROIsesblks=ROIsesblks{rix};
            cfg.ROIsitenam=ROIsitenam{rix};
            cfg.rootdir1 = RSG.rootdir2;
            cfg.rootdir2 = RSG.savedir;
            for NID=1:length(cfg.ROIseslist) %NID=10
                RSG.info = info.(cfg.ROIseslist{NID});
                blockix=cfg.ROIsesblks{NID};
                BLK=1;cfg.fnamePC2{NID} = RSG.info.fnamePC2{blockix(BLK)};
                cfg.fname{NID} = sprintf('%s%s_ch-525.bin',RSG.rootdir2,cfg.fnamePC2{NID} );
                [x,y,z]=fileparts(cfg.fname{NID});
                cfg.fname{NID} = sprintf('%s%s_pp4seg_layer%d_nofilt.bin',RSG.savedirSEG{4},y,layer);
                
                if tocopy
                    of = cfg.fname{NID};
                    [x,y,z]=fileparts(of);
                    nf = [ROOTNEW,y,z];
                    S = copyfile(of,nf);
                    if ~S, error('here'); end
                end
            end
            
            for NID=1:length(cfg.ROIseslist) %NID=11 i=3
                cfg.CRSP{NID} = [];
            end
            cfg.miscrsp = 1;
            
            % refresh mask names if necessary
            [x,y,z]=fileparts(RSG.savedirSEG{1});
            [x1,y,z]=fileparts(x);
            for i=1:length(cfg.CRSP)
                
                if not(isempty(cfg.CRSP{i}))
                    [x,y,z]=fileparts(cfg.CRSP{i}.segname);
                    [x2,y,z]=fileparts(x);
                    if ~isequal(x1,x2)
                        cfg.CRSP{i}.segname=strrep(cfg.CRSP{i}.segname,x2,x1);
                    end
                end
                
            end
            
            COMBI=[[1:length(ROIseslist{rix})-1]', [2:length(ROIseslist{rix})]'];
            COMBIcfg = cell([size(COMBI,1),1]);
            for cmb=1:size(COMBI,1)
                of = sprintf('%s%s_%s_ROICcfg_layer%d.mat',RSG.savedir,ROIseslist{rix}{COMBI(cmb,1)},ROIseslist{rix}{COMBI(cmb,2)},layer);
                tmp=load(of);
                COMBIcfg{cmb}=tmp.cfg;
                if tocopy
                    [x,y,z]=fileparts(of);
                    nf = [ROOTNEW,y,z];
                    S = copyfile(of,nf);
                    if ~S, error('here'); end
                end
            end
            cfg.COMBI    = COMBI;
            cfg.COMBIcfg = COMBIcfg;
            
            close all;
            
            % find correspondence only for session with empty CRSPR (sessions mentioned in miscrsp)
            if ~isfield(cfg,'miscrsp')
                cfg.miscrsp=[];
            end
            
            cfg.CRSP{1}.segname = sprintf('%s%s.mat',RSG.savedir,full_name);
            
            clear global mystackA;

            cfg.ROImask = ROImask; % Assign the ROImask for display
            cfg.red_mask = red_mask; %Assign the Red mask
            cfg.transp_mask = transp_mask; %Assign the Transperancy mask
            mystackviewer_CRSP(cfg); % Calls the main function
            
            % info about cell in confocal:
            global mystackA;
            
            if ~already_transformed % This is where the transformation happens
                
                %------------------------
                % open corresponding immunostacks in matlab viewer
                % mystackviewer_segment
                % winopen(ImmunoDir)
                
                fnameS = dir(sprintf('%s%s*%s*.bin',ImmunoDir,'*_GCAMP.bin'));
                filename = fnameS(1).name;
                pathname = ImmunoDir;
                
                % these are the bin files (not yet transformed)
                cfgI=[];
                cfgI.fname{1} = [pathname,filename];
                cfgI.fname{2} = [pathname,strrep(filename,'GCAMP','PV')];
                cfgI.fname{3} = [pathname,strrep(filename,'GCAMP','SOM')];
                cfgI.fname{4} = [pathname,strrep(filename,'GCAMP','GAD')];
                
                %mystackviewer_immunoview(cfgI);
                
                clear global myIview;
                %profile on
                cfgI.MA = 1024; %Set the max value for the confocal viewer
                myimmunoview(cfgI);
                %tpti_viewer_confocal(cfgI);
                %profile viewer;
                
                %--------------------------------------------------------------
                % also load the non transformed tiff stack in image j
                clear fn;
                temp_name = [conf_full_name,'_GCAMP_orig.tif'];
                fn  = strrep([ImmunoDir, temp_name],'\','/');
                fid = fopen([tmp_folder,'/tmp.ijm'],'wt');
                fprintf(fid,'run("Bio-Formats Macro Extensions");\n');
                fprintf(fid,'Ext.openImagePlus("%s");\n',fn);
                fclose(fid);
                print_st = ['notepad "',tmp_folder,'tmp.ijm" &'];
                system(print_st);
                Miji; %temp_name Alex change 19/08/20
                MIJ.run('Install...', ['install=',tmp_folder,'/tmp.ijm']);
                MIJ.run('tmp');
            end
            
            if already_transformed %After transformation is done, load the transformed images and click the immunolabels
                %--------------------------------------------------------------
                % also load the transformed tiff stack
                % edit tpti_demo_alex.m
                Miji;
                %Get all the .tif files in the immunofolder and remove the original non-transformed one or any other random .tif files
                fnameS = dir([ImmunoDir,'*.tif']);
                ix_del = [];
                for jj = 1:length(fnameS)
                    if ~isempty(strfind(fnameS(jj).name,'original')) || isempty(strfind(fnameS(jj).name,'GCAMP')) &&...
                            isempty(strfind(fnameS(jj).name,'PV')) && isempty(strfind(fnameS(jj).name,'SOM')) && ...
                            isempty(strfind(fnameS(jj).name,'GAD')),
                        ix_del = [jj,ix_del];
                    end
                end
                fnameS(ix_del) = [];
                
                clear fn;
                for ii = 1:4
                    fn{ii} = fullfile(fnameS(ii).folder,fnameS(ii).name);
                    fn0{ii} = fnameS(ii).name;
                end
                
                fid = fopen([tmp_folder,'/tmp2.ijm'],'wt');
                fprintf(fid,'run("Bio-Formats Macro Extensions");\n');
                
                for jj=1:4
                    fprintf(fid,'Ext.openImagePlus("%s");\n',fn{jj});
                end
                
                ix = [find(cellfun(@(x) contains(x,'GCAMP'),fn0)),find(cellfun(@(x) contains(x,'GAD'),fn0)),...
                    find(cellfun(@(x) contains(x,'PV'),fn0)),find(cellfun(@(x) contains(x,'SOM'),fn0))];
                
                fprintf(fid,'run("Merge Channels...", "c2=%s c4=%s c5=%s c1=%s create ignore");\n',fn0{ix(1)},fn0{ix(2)},fn0{ix(3)},fn0{ix(4)});
                %                 fprintf(fid,'run("Channels Tool...");\n');
                fprintf(fid,'Stack.setDisplayMode("color");\n');
                fclose(fid);
                print_st = ['notepad "',tmp_folder,'tmp.ijm2" &'];
                system(print_st)
                
                MIJ.run('Install...',['install=',tmp_folder,'/tmp2.ijm']);
                MIJ.run('tmp2');
                
                %----------------------------------------------------------
            end
            
 
        end % if do_review,
        
    end % for layer=1:4,

end % for rix=1:length(ROIseslist)

ready = 0; %Logical flag whether done with clicking the immunolabels
%% This piece of code will close the windows and save the output of the immuno labeling
DEFAULT = 'No'; 
cell_type{1} = 'GAD'; cell_type{2} = 'PV'; cell_type{3} = 'SOM';
f_name{1} = 'matxy'; f_name{2} = 'imjxy'; f_name{3} = 'imjz';

if already_transformed && ready
    
    Button = questdlg('Are you sure you want to close everything?', ...
        'Exit', ...
        'Yes', 'No',DEFAULT);
    
    switch Button
        case 'Yes'
            %Initialise variables
            for c = 1:length(cell_type)
                for f = 1:length(f_name)
                    cell_label.(cell_type{c}).(f_name{f}) = [];
                end
            end
            
            global mystackA; %Update the global variable to collect the clicked info
            CELLLABEL = mystackA.cfg.CL{1}.CELLLABEL;
            n_cell = length(CELLLABEL);
            
            for j = 1:n_cell
                lbl = CELLLABEL{j}{2}; %Get the cell type
                cell_label.(lbl).matxy(end+1,:) = CELLLABEL{j}{1}; %Get the matlab xy coordinates for ROI mask
                cell_label.(lbl).imjxy(end+1,:) = CELLLABEL{j}{3}(1:2); %Get the imagej xy coordinates for clicked cells
                cell_label.(lbl).imjz(end+1,1) = CELLLABEL{j}{3}(3); %Get the imagej z coordinate for clicked cells
            end
            sv_name = fullfile(ROOTNEW,'cell_label.mat');
            save(sv_name,'cell_label');
            MIJ.run('Close All'); % this crashes matlab if no ImageJ open...(at least on Beast)
            MIJ.exit;
            close all;
        case 'No'
            disp('Resume your work.')
    end

    
elseif ready
    
        Button = questdlg('Are you sure you want to close everything?', ...
        'Exit', ...
        'Yes', 'No',DEFAULT);
    
    switch Button
        case 'Yes'
            MIJ.run('Close All'); % this crashes matlab if no ImageJ open...(at least on Beast)
            MIJ.exit;
            close all;
        case 'No'
            disp('Resume your work.')
    end

    
end