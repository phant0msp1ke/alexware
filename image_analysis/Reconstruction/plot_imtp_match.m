%This function will plot the correspondence between the clicked cells in
%the 2p in vivo image vs the histological confocal pictures.

results_dir = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction/Leela/results/area04/slice5'; %The folder with the results
transp_2p = 0.2; %Set the transperancy value for plotting 2p images
transp_conf = 0.15; %Set the transperancy value for plotting 2p images
type = 'full';
square_sz = 40; %The size of the squares

% gad_col = [0.159, 0.159, 0.095]; %Set the GAD color

%Get the animal and area names
[temp, plane] = fileparts(results_dir);
[temp, area] = fileparts(temp);
[temp, ~] = fileparts(temp);
[~, animal] = fileparts(temp);

%Load the cell labels and mask
load(fullfile(results_dir,'cell_label.mat'),'cell_label');
z_keep = mode(cell_label.GAD.imjz); %Keep the z-plane with the most cells for plotting
mask = cell_label.mask;
bimask = double(cell_label.bimask);
sz_mask = size(mask);

%Check if there are empty entries
som_empty = isempty(cell_label.SOM.roi_match);
pv_empty = isempty(cell_label.PV.roi_match);

%Make 2p immuno masks
pv_mask = cat(3, zeros(sz_mask), zeros(sz_mask), ones(sz_mask)); %Make a blue PV mask
som_mask = cat(3, ones(sz_mask), zeros(sz_mask), zeros(sz_mask)); %Make a red SOM mask
gad_mask = cat(3, zeros(sz_mask), ones(sz_mask), zeros(sz_mask)); %Make a red SOM mask

if ~pv_empty
    pv_transp_mask = ismember(mask, cell_label.PV.roi_match)*transp_2p;
else
    pv_transp_mask = zeros(sz_mask);
end

if ~som_empty
    som_transp_mask = ismember(mask, cell_label.SOM.roi_match)*transp_2p;
else
    som_transp_mask = zeros(sz_mask);
end

gad_transp_mask = ismember(mask, cell_label.GAD.roi_match)*transp_2p;

%Load the mean 2p image
mean_img_name = fullfile(results_dir, [animal,'_',area,'_mean_green.tif']);
[mean_img, ~] = tiff_loader(mean_img_name);

%Load the confocal immuno images
pv_name = fullfile(results_dir, strjoin({animal, area, plane, 'PV.tif'},'_'));
som_name = fullfile(results_dir, strjoin({animal, area, plane, 'SOM.tif'},'_'));
gad_name = fullfile(results_dir, strjoin({animal, area, plane, 'GAD.tif'},'_'));
[pv_img_xyz, ~] = tiff_loader(pv_name);
pv_img = pv_img_xyz(:,:,z_keep);
[som_img_xyz, ~] = tiff_loader(som_name);
som_img = som_img_xyz(:,:,z_keep);
[gad_img_xyz, ~] = tiff_loader(gad_name);
gad_img = gad_img_xyz(:,:,z_keep);

%Make the confocal immuno masks
sz_confmask = size(pv_img); %The size of the ImageJ images

if strcmp(type, 'outline')
    transp_conf = 1;
end
        
pv_confmask = cat(3, zeros(sz_confmask), zeros(sz_confmask), ones(sz_confmask)); %Make a blue PV mask
som_confmask = cat(3, ones(sz_confmask), zeros(sz_confmask), zeros(sz_confmask)); %Make a red SOM mask
gad_confmask = cat(3, zeros(sz_confmask), ones(sz_confmask), zeros(sz_confmask)); %Make a red SOM mask
center_pv = cell_label.PV.imjxy; center_som = cell_label.SOM.imjxy; center_gad = cell_label.GAD.imjxy; %Get the center of ImageJ ROIs

pv_transp_confmask = zeros(sz_confmask); pv_transp_confmask = drawsquare(pv_transp_confmask, center_pv, square_sz, type)*transp_conf;
som_transp_confmask = zeros(sz_confmask); som_transp_confmask = drawsquare(som_transp_confmask, center_som, square_sz, type)*transp_conf;
gad_transp_confmask = zeros(sz_confmask); gad_transp_confmask = drawsquare(gad_transp_confmask, center_gad, square_sz, type)*transp_conf;

%% Plot the immuno pictures together with the 2p matches
tp_max = 200;
conf_max = 800;

row = 2;
col = 3;
per = 0.005;
edgel = per; edger = per; edgeh = 0.025; edgeb = per; space_h = 0.001; space_v = 0.02;
[pos]=subplot_pos(row,col,edgel,edger,edgeh,edgeb,space_h,space_v);
figure('units','normalized','outerposition',[0 0 1 1]);
set(gcf,'color','w');

%Plot PV 2p
ax(1) = subplot('position',pos{1});
image(mean_img);
colormap(ax(1),gray(tp_max));
hold on;
image(pv_mask, 'AlphaData', pv_transp_mask);
title('\color{blue}PV');
axis equal;
axis off;
hold off;

%Plot SOM 2p
ax(2) = subplot('position',pos{2});
image(mean_img);
colormap(ax(2), gray(tp_max));
hold on;
image(som_mask, 'AlphaData', som_transp_mask);
title('\color{red}SOM');
axis equal;
axis off;
hold off;

%Plot GAD 2p
ax(3) = subplot('position',pos{3});
image(mean_img);
colormap(ax(3), gray(tp_max));
hold on;
image(gad_mask, 'AlphaData', gad_transp_mask);
title('\color{green}GAD');
axis equal;
axis off;
hold off;

%Plot PV confocal
ax(4) = subplot('position',pos{4});
image(pv_img);
colormap(ax(4), gray(conf_max));
hold on;
image(pv_confmask, 'AlphaData', pv_transp_confmask);
axis equal;
axis off;


%Plot SOM confocal
ax(5) = subplot('position',pos{5});
image(som_img);
colormap(ax(5), gray(conf_max));
hold on;
image(som_confmask, 'AlphaData', som_transp_confmask);
axis equal;
axis off;

%Plot GAD confocal
ax(6) = subplot('position',pos{6});
image(gad_img);
colormap(ax(6), gray(conf_max+200));
hold on;
image(gad_confmask, 'AlphaData', gad_transp_confmask);
axis equal;
axis off;

%Save the result
save_name = fullfile(results_dir, [animal,'_',area,'_',plane,'_immunocheck.jpg']);
saveas(gcf, save_name);
close all;