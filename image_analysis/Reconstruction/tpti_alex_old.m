%This is a script based on tpti_matchingexample_alexdata and
%tpti_matchingexample where I want to clean up the code and make it more
%readable and accessible 

dbstop if error;
javaaddpath '/home/alex/java/jar/mij.jar'; 

try 
    MIJ.exit; 
catch
    fprintf('IMageJ already closed\n');
end
close all;

area_name = 'Area13';
ROOTNEW = '/home/alex/Desktop/Data/Reconstruction_data/Conv_bin_files/';


tocopy = 0;
RSG.savedir       = ROOTNEW;
RSG.rootdir2      = ROOTNEW;
RSG.rootdir3      = ROOTNEW;
RSG.savedirSEG{1} = ROOTNEW;
RSG.savedirSEG{4} = ROOTNEW;
ImmunoDir         = ROOTNEW;


%--------------------------------------------------------------------------
% logbook info about recordings

clear info;

ID = 'M99_20190101'; % Alex data
info.(ID).site = 'a';
bix=1;
info.(ID).block_segment= bix;
info.(ID).fnamePC1{bix}= area_name;
info.(ID).fnamePC2{bix}= area_name;
info.(ID).fnamePC3{bix}= area_name;


%--------------------------------------------------------------------------

rix = 1;
ROIseslist{rix} = {'M99_20190101'};
ROIsesblks{rix} = {[1]    [1]    [1]};
ROIsitenam{rix} = 'M99a';

ALLERROR={};
for rix=1%[2,13,19,22,27,30,36]%5%:17%2%2%[2,10,19,23,28,36]%18%:length(ROIseslist), still missing 15,31
    for layer=2
        
        fprintf('%d:%s\n',rix,ROIseslist{rix}{1});
        
        do_review = 1; % leave as 1
        if do_review
            
            % review correspondence in GUI
            clear global mystackA mystackU;
            
            cfg = [];
            cfg.ROIseslist=ROIseslist{rix};
            cfg.ROIsesblks=ROIsesblks{rix};
            cfg.ROIsitenam=ROIsitenam{rix};
            cfg.rootdir1 = RSG.rootdir2;
            cfg.rootdir2 = RSG.savedir;
            for NID=1:length(cfg.ROIseslist) %NID=10
                RSG.info = info.(cfg.ROIseslist{NID});
                blockix=cfg.ROIsesblks{NID};
                BLK=1;cfg.fnamePC2{NID} = RSG.info.fnamePC2{blockix(BLK)};
                cfg.fname{NID} = sprintf('%s%s_ch-525.bin',RSG.rootdir2,cfg.fnamePC2{NID} );
                [x,y,z]=fileparts(cfg.fname{NID});
                cfg.fname{NID} = sprintf('%s%s_pp4seg_layer%d_nofilt.bin',RSG.savedirSEG{4},y,layer);
                
                if tocopy
                    of = cfg.fname{NID};
                    [x,y,z]=fileparts(of);
                    nf = [ROOTNEW,y,z];
                    S = copyfile(of,nf);
                    if ~S, error('here'); end
                end
            end
            
            for NID=1:length(cfg.ROIseslist) %NID=11 i=3
                cfg.CRSP{NID} = [];
            end
            cfg.miscrsp = 1;
            
            % refresh mask names if necessary
            [x,y,z]=fileparts(RSG.savedirSEG{1});
            [x1,y,z]=fileparts(x);
            for i=1:length(cfg.CRSP)

                if not(isempty(cfg.CRSP{i}))
                    [x,y,z]=fileparts(cfg.CRSP{i}.segname);
                    [x2,y,z]=fileparts(x);
                    if ~isequal(x1,x2)
                        cfg.CRSP{i}.segname=strrep(cfg.CRSP{i}.segname,x2,x1);
                    end
                end

            end
            
            COMBI=[[1:length(ROIseslist{rix})-1]', [2:length(ROIseslist{rix})]'];
            COMBIcfg = cell([size(COMBI,1),1]);
            for cmb=1:size(COMBI,1)
                of = sprintf('%s%s_%s_ROICcfg_layer%d.mat',RSG.savedir,ROIseslist{rix}{COMBI(cmb,1)},ROIseslist{rix}{COMBI(cmb,2)},layer);
                tmp=load(of);
                COMBIcfg{cmb}=tmp.cfg;
                if tocopy                    
                    [x,y,z]=fileparts(of);
                    nf = [ROOTNEW,y,z];
                    S = copyfile(of,nf);
                    if ~S, error('here'); end
                end
            end
            cfg.COMBI    = COMBI;
            cfg.COMBIcfg = COMBIcfg;
            
            close all;
            
            % find correspondence only for session with empty CRSPR (sessions mentioned in miscrsp)
            if ~isfield(cfg,'miscrsp')
                cfg.miscrsp=[];
            end
            
            cfg.CRSP{1}.segname = sprintf('%s%s.mat',RSG.savedir,area_name);
            
            clear global mystackA;
            mystackviewer_CRSP(cfg) % edit mystackviewer_correspondence
            
            % info about cell in confocal:
            global mystackA;
                  
            if true % testing part
              
                %------------------------
                % open corresponding immunostacks in matlab viewer
                % mystackviewer_segment
                % winopen(ImmunoDir)

                fnameS = dir(sprintf('%s%s*%s*.bin',ImmunoDir,'*_GCAMP.bin'));
                filename = fnameS(1).name;
                pathname = ImmunoDir;
                
                % these are the bin files (not yet transformed)
                cfgI=[];
                cfgI.fname{1} = [pathname,filename];
                cfgI.fname{2} = [pathname,strrep(filename,'GCAMP','PV')];
                cfgI.fname{3} = [pathname,strrep(filename,'GCAMP','SOM')];
                cfgI.fname{4} = [pathname,strrep(filename,'GCAMP','GAD')];
                
                %mystackviewer_immunoview(cfgI);
                
                clear global myIview;
                %profile on
                myimmunoview(cfgI);
                %tpti_viewer_confocal(cfgI);
                %profile viewer;
                
                %--------------------------------------------------------------
                % also load the non transformed tiff stack in image j
              
                clear fn;
                fn  = strrep([ImmunoDir,'All_colors_GCAMP_original.tif'],'\','/'); 
                fid = fopen('/home/alex/Desktop/Data/Reconstruction_data/temp/tmp.ijm','wt');
                
                fprintf(fid,'run("Bio-Formats Macro Extensions");\n');
                fprintf(fid,'Ext.openImagePlus("%s");\n',fn);
                fclose(fid);
                
                system('notepad "/home/alex/Desktop/Data/Reconstruction_data/temp/tmp.ijm" &')
                Miji;
                MIJ.run('Install...', ['install=/home/alex/Desktop/Data/Reconstruction_data/temp/tmp.ijm']);
                MIJ.run('tmp');
                
            end
            
            set(gcf,'CloseRequestFcn','uiresume(gcbf);closereq;');
            interactwithGUI=1; % !! if 0 automatically saves CRSPR files
        
            if interactwithGUI,
                choice = questdlg('Continue?', ...
                    'resscn_ROI_correspondence_sessionlist', ...
                    'Yes','No','No');
            else
                choice = 'Yes';
            end
            % Handle response
            switch choice
                case 'Yes'
                    NID
                case 'No'
                    NID
                    break;
            end
 
        end % if do_review,
        
    end % for layer=1:4,
    try
        if do_immuno
            MIJ.run('Close All'); % this crashes matlab if no ImageJ open...(at least on Beast)
            MIJ.exit;
        end
    end
end % for rix=1:length(ROIseslist)