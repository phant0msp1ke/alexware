%This script combines the results from the two planes which were
%reconstructed separately 

results_dir{1} = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction/Leela/results/area04/slice4'; %The folder with the results from plane 1
results_dir{2} = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction/Leela/results/area04/slice5'; %The folder with the results from plane 2

c_type{1} = 'GAD'; c_type{2} = 'PV'; c_type{3} = 'SOM';

%Load the data
for j = 1:2
    data{j} = load(fullfile(results_dir{j},'cell_label.mat'),'cell_label');
end


%Get the field names of the cell types
fnames = fieldnames(data{1}.cell_label.GAD);

%Combine the results together
for j = 1:length(c_type)
    c_name = c_type{j};
    
    for i = 1:length(fnames)
        fname = fnames{i};
        rez.(c_name).(fname) = [data{1}.cell_label.(c_name).(fname); data{2}.cell_label.(c_name).(fname)] ;
    end
    
end

%Copy the common fields
rez.mask = data{1}.cell_label.mask;
rez.bimask = data{1}.cell_label.bimask;
rez.roi_nums = data{1}.cell_label.roi_nums;
rez.n_rois = data{1}.cell_label.n_rois;

%Save the results
[base_dir, ~] = fileparts(results_dir{1});
save_dir = fullfile(base_dir,'combined');
if ~exist(save_dir,'dir')
    mkdir(save_dir);
end
save_name = fullfile(save_dir,'rez.mat');
save(save_name,'rez');

%Copy the pictures from the plane folders to the results folder
temp1 = dir(fullfile(results_dir{1},'*.jpg'));
temp2 = dir(fullfile(results_dir{2},'*.jpg'));
name1 = fullfile(results_dir{1}, temp1.name);
name2 = fullfile(results_dir{2}, temp2.name);
copyfile(name1, save_dir);
copyfile(name2, save_dir);
    