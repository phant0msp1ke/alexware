function img = drawsquare(img, center_xy, sz, type)

if ~exist('type', 'var') || isempty(type)
    type = 'full';
end

n_points = size(center_xy,1);
center_xy = round(center_xy);

for j = 1:n_points
    x = center_xy(j,1); y = center_xy(j,2);
    hsz = round(sz/2);
    ybounds = [y-hsz; y+hsz];
    xbounds = [x-hsz; x+hsz];
    switch type
        case 'outline'
            img(ybounds, xbounds(1):xbounds(2)) = 1;
            img(ybounds(1):ybounds(2), xbounds) = 1;
        case 'full'
            img(ybounds(1):ybounds(2), xbounds(1):xbounds(2)) = 1;
    end
end




