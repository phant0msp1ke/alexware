function fix_scanlines_test(img_xyz,shift_pix,save_dir)

if ~exist(save_dir,'dir')
    mkdir(save_dir)
end


shifts = [-shift_pix:1:shift_pix];
num_shifts = length(shifts);

row = 2;
col = num_shifts;
per = 0.005;
edgel = per; edger = per; edgeh = 0.02; edgeb = per; space_h = per; space_v = 0.02;
[pos]=subplot_pos(row,col,edgel,edger,edgeh,edgeb,space_h,space_v);
figure('units','normalized','outerposition',[0 0 1 1]);
set(gcf,'color','w');

for j = 1:num_shifts
    
    curr_shift = shifts(j);
    %Shift in the cols
    img_cols = img_xyz;
    img_cols(:,1:2:end) = circshift(img_cols(:,1:2:end),curr_shift);
    
    %Shift in the rows
    img_rows = img_xyz;
    img_rows(1:2:end,:) = circshift(img_rows(1:2:end,:),curr_shift);
    
    %Plot the shifts in columns
    subplot('position',pos{j});
    imagesc(img_cols);
    axis off;
    title(['Cols shift ',num2str(curr_shift)]);
    
    %Plot the shifts in rows
    subplot('position',pos{num_shifts+j});
    imagesc(img_rows);
    axis off;
    title(['Rows shift ',num2str(curr_shift)]);
end


file_name = ['Different shifts of cols and rows'];
save_name = fullfile(save_dir,[file_name,'.jpg']);
saveas(gcf,save_name);