function plot_SN_histograms(CC_rez, params)

sel_types = fieldnames(CC_rez);
data_types = fieldnames(CC_rez.(sel_types{1}));
cc_types = fieldnames(CC_rez.(sel_types{1}).(data_types{1}));
int_types = fieldnames(CC_rez.(sel_types{1}).(data_types{1}).(cc_types{1}));
int_names = {'EXC-EXC','EXC-GADother','EXC-PV','EXC-SOM','GADother-GADother','GADother-PV','GADother-SOM','PV-PV','PV-SOM','SOM-SOM'}';

save_dir = params.save_dir;
font_sz = 22;
lw = 5;
edge_width = 0.075;
edges = [-1:edge_width:1];

edge_width2 = 0.025;
edges2 = [-1:edge_width2:1];

row = 10;
col = 1;
per = 0.005;
edgel = 0.075; edger = per; edgeh = 0.05; edgeb = 0.09; space_h = 0.05; space_v = 0.015;
[pos]=subplot_pos(row,col,edgel,edger,edgeh,edgeb,space_h,space_v);

for j = 1:length(sel_types)
    sel_type = sel_types{j};
    
    save_dir_sel_type = fullfile(save_dir, sel_type);
    if ~exist(save_dir_sel_type,'dir')
        mkdir(save_dir_sel_type);
    end
    
    for i = 1:length(data_types)
        data_type = data_types{i};
        
        save_dir_data_type = fullfile(save_dir_sel_type, data_type);
        if ~exist(save_dir_data_type,'dir')
            mkdir(save_dir_data_type);
        end
        
        cc_types = fieldnames(CC_rez.(sel_type).(data_type));
        
        for k = 1:length(cc_types)
            cc_type = cc_types{k};
            
            %Plot the histograms together in the same plot
            
            figure('units','normalized','outerposition',[0 0 1 1]);
            hold on;
            for l = 1:length(int_types)
                int_type = int_types{l};
                data = CC_rez.(sel_type).(data_type).(cc_type).(int_type);
                [values, edges] = histcounts(data, edges, 'Normalization', 'probability');
                centers = (edges(1:end-1)+edges(2:end))/2;
                plot(centers, values, 'LineWidth', lw);
                title(strjoin({sel_type, data_type,'CC', cc_type},' '));
                xlabel(['CC ',cc_type]);
                ylabel('Proportion');
                set(gcf,'color','w');
                set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
            end
            hold off;
            legend(int_names);
            xlim([-0.7 0.7]);
            xline(0,'--','LineWidth',2,'Color', 'k','FontSize',font_sz);
            save_name = fullfile(save_dir_data_type, strjoin({'Histogram',sel_type, data_type, cc_type,'.svg'},'_'));
            saveas(gcf, save_name);
            close;
            
            %Plot the histograms individually in one column
            
            figure('units','normalized','outerposition',[0 0 1 1]);
            for m = 1:length(int_types)
                int_type = int_types{m};
                data = CC_rez.(sel_type).(data_type).(cc_type).(int_type);
                subplot('position',pos{m});
                counts = histcounts(data, edges2);
                if ismember(m,[1:4])
                    face_col = 'r'; edge_col = 'r';
                else
                    face_col = 'b'; edge_col = 'b';
                end
                histogram('BinEdges', edges2,'BinCounts', counts,'Normalization','probability', 'FaceColor',face_col,'EdgeColor',edge_col);
                if m==10
                    xlabel(['CC ',cc_type]);
                    ylabel('Proportion');
                end
                if m==1
                    title(strjoin({sel_type, data_type,'CC', cc_type},' '));
                end
                set(gcf,'color','w');
                set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
                xlim([-0.65 0.65]);
                xline(0,'--','LineWidth',2,'Color', 'k','FontSize',font_sz);
                legend(int_names{m});
            end
            save_name = fullfile(save_dir_data_type, strjoin({'Individiual','Histogram',sel_type, data_type, cc_type,'.svg'},'_'));
            saveas(gcf, save_name);
            close;
            
        end
        
    end
end