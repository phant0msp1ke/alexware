function plot_BF_areas(BFs_area, params)

%% Define plotting parameters

sel_type = fieldnames(BFs_area);
cell_types = fieldnames(BFs_area.(sel_type{1}));
font_sz = 25;
row = 1;
col = 3;
per = 0.005;
edgel = 0.075; edger = per; edgeh = 0.1; edgeb = 0.08; space_h = 0.05; space_v = 0.015;
[pos]=subplot_pos(row,col,edgel,edger,edgeh,edgeb,space_h,space_v);

freqs = params.freqs;
save_dir = params.save_dir;
save_dir_all = params.save_dir_all;
area_name = params.area_name;
sel_names = params.sel_names;
analysis_type = params.analysis_type;
skip_f = 2;


num_freqs = numel(freqs);
y_labels = cell(0);
for jj = 1:skip_f:numel(freqs)
    y_labels{end+1} = num2str(freqs(jj),'%.1f');
end

%% Plot a violin plot of the BFs
figure('units','normalized','outerposition',[0 0 1 1]);
transp = 0.25;
violin_width = 0.4;

for j = 1:length(sel_type)
    
    sel_curr = sel_type{j};
    sel_name = sel_names{j};
    subplot('position',pos{j});
    violins = violinplot(BFs_area.(sel_curr),cell_types, 'ShowMean', true, 'ViolinAlpha', transp, 'Width', violin_width);
    switch analysis_type
        case 'ephys'
            violins(1).ViolinColor = [1, 0, 0];
            violins(2).ViolinColor = [0, 0, 1];
            
            annotation('textbox',[(j-1)*0.3+0.335 0.8 0.1 0.1],'String', sprintf('EXC=%s',num2str(length(BFs_area.(sel_curr).EXC))),'LineStyle','none','Color','r','FontSize',font_sz-10,'FontWeight','bold');
            annotation('textbox',[(j-1)*0.3+0.335 0.77 0.1 0.1],'String', sprintf('GAD=%s',num2str(length(BFs_area.(sel_curr).INH))),'LineStyle','none','Color','b','FontSize',font_sz-10,'FontWeight','bold');
        case '2p'
            violins(1).ViolinColor = [1, 0, 0];
            violins(2).ViolinColor = [0, 0, 1];
            violins(3).ViolinColor = [0, 1, 1];
            violins(4).ViolinColor = [1, 0, 1];
            
            annotation('textbox',[(j-1)*0.3+0.335 0.8 0.1 0.1],'String', sprintf('EXC=%s',num2str(length(BFs_area.(sel_curr).EXC))),'LineStyle','none','Color','r','FontSize',font_sz-10,'FontWeight','bold');
            annotation('textbox',[(j-1)*0.3+0.335 0.77 0.1 0.1],'String', sprintf('GAD=%s',num2str(length(BFs_area.(sel_curr).GAD))),'LineStyle','none','Color','b','FontSize',font_sz-10,'FontWeight','bold');
            annotation('textbox',[(j-1)*0.3+0.335 0.74 0.1 0.1],'String', sprintf('PV=%s',num2str(length(BFs_area.(sel_curr).PV))),'LineStyle','none','Color','c','FontSize',font_sz-10,'FontWeight','bold');
            annotation('textbox',[(j-1)*0.3+0.335 0.71 0.1 0.1],'String', sprintf('SOM=%s',num2str(length(BFs_area.(sel_curr).SOM))),'LineStyle','none','Color','m','FontSize',font_sz-10,'FontWeight','bold');
    end

    
    set(findall(gca, 'Type', 'Line'),'LineWidth',1.5);
    set(gca, 'YScale', 'log');
    set(gca,'Ytick',[]);
    set(gca,'YminorTick','off');
    yticks(freqs(1:skip_f:num_freqs));
    if j == 1
        ylabel('Frequency (kHz)');
        yticklabels(y_labels);
    else
        set(gca,'Yticklabel',[]);
    end
    ylim([0.2 35]);
    title(['BF ', sel_name]);
    set(gcf,'color','w');
    set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
end

save_name = fullfile(save_dir,['BF variability ', area_name,'.svg']);
saveas(gcf, save_name);
save_name = fullfile(save_dir_all,['BF variability ', area_name,'.svg']);
saveas(gcf, save_name);
close;
