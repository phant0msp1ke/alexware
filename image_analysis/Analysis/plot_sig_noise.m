function CC_int = plot_sig_noise(data, params)

%% Define plotting parameters

sel_type = fieldnames(data);
data.(sel_type{1}) = rmfield(data.(sel_type{1}), 'GAD'); %Remove the GAD field from the names of cells
cell_types = fieldnames(data.(sel_type{1}));

save_dir = params.save_dir;
area_name = params.area_name;
sel_names = params.sel_names;
plot_on = params.plot;
data_type = params.data_type;
font_sz = 25;

colors = {'b', [0,0.54,0.54], 'm'};
%% Analyse the data

for j = 1:length(sel_type)
    
    sel_curr = sel_type{j};
    sel_name = sel_names{j};
    save_dir_full = fullfile(save_dir,sel_curr);
    
    if ~exist(save_dir_full,'dir')
        mkdir(save_dir_full)
    end
    
    %Get the indices of PV and SOM and find where they overlap with GAD
%     ix_remove = ismember(params.ids.(sel_curr).GAD, [params.ids.(sel_curr).PV; params.ids.(sel_curr).SOM]);
%     params.ids.(sel_curr).GAD(ix_remove) = []; %Remove the indices for duplicated GAD also from the params variable
%     
%     %Remove the PV and SOM ffrom the GAD data
%     data.(sel_curr).GAD(ix_remove) = [];
    
    %Define borders of the different cell types
    n_exc = length(params.ids.(sel_curr).EXC); n_gadother = length(params.ids.(sel_curr).GADother);
    n_pv = length(params.ids.(sel_curr).PV); n_som = length(params.ids.(sel_curr).SOM);
    border.GADother = n_exc + 0.5; border.PV = border.GADother + n_gadother; border.SOM = border.PV + n_pv;
    
    %Initialise variables
    temp_all = cell(0);
    temp_mean = cell(0);
    ix_rep = [];
    data_nelken = cell(0);
    
    for k = 1:length(cell_types)
        cell_curr = cell_types{k};
        temp_all = cellfun(@(x) cellfun(@(y) mean(y,2),x,'UniformOutput',false),data.(sel_curr).(cell_curr), 'UniformOutput', false); %Take the mean across time for every trial for every cell
        temp_all = cellfun(@cell2mat, temp_all, 'UniformOutput',false); %Convert the data to a matrix for each ROI which is Trials X Stimuli
        temp_mean = cellfun(@mean, temp_all, 'UniformOutput',false); %Compute the mean across trials to get 1 X Stimuli
        
        data_total.(cell_curr) = cell2mat(temp_mean); %Store the data necessary to compute the Total correlation of dimensions nROIS X Stimuli
        data_noise.(cell_curr) = cellfun(@(x) x - mean(x), temp_all, 'UniformOutput',false); %Remove the mean response for each stimulus for every trial
        data_noise.(cell_curr)  = cellfun(@(x) x(:)', data_noise.(cell_curr), 'UniformOutput',false); %Vectorize the responses in each cell
        data_noise.(cell_curr) = cell2mat(data_noise.(cell_curr)); %Convert to matrix of dimensions nROIS X (Stimuli*Reps)
        
        %Fitzpatrick NC
        data_noise_fitz.(cell_curr) = cellfun(@(x) (x - mean(x))./std(x), temp_all, 'UniformOutput',false); %Z-score (remove mean, divide by std) the data to compute NC like Fitzpatrick paper
        data_noise_fitz.(cell_curr)  = cellfun(@(x) x(:)', data_noise_fitz.(cell_curr), 'UniformOutput',false); %Vectorize the responses in each cell
        data_noise_fitz.(cell_curr) = cell2mat(data_noise_fitz.(cell_curr)); %Convert to matrix of dimensions nROIS X (Stimuli*Reps)
        
        %Nelken Signal correlation data
        data_nelken = [data_nelken; temp_all]; %Store the cells in the order EXC, GADother, PV, SOM
    end
    
    %Find the repeating entries between PV and SOM
    ids = [params.ids.(sel_curr).EXC; params.ids.(sel_curr).GADother; params.ids.(sel_curr).PV; params.ids.(sel_curr).SOM]; %Get all the indices together
    [~,~,ib]=unique(ids,'rows','stable');
    ix_rep = find(hist(ib,unique(ib))>1)';
    
    for i = 1:length(ix_rep)
        temp_ix = find(ids==ids(ix_rep(i)));
        ix_rep(i,2) = temp_ix(end);
    end
    
    %Combine the data together
    data_total_CC = [data_total.EXC; data_total.GADother; data_total.PV; data_total.SOM]; %Gather the data from all cell types together in order for the Total CC
    data_noise_CC = [data_noise.EXC; data_noise.GADother; data_noise.PV; data_noise.SOM]; %Gather the data from all cell types together in order for the Noise CC
    data_noise_fitz_CC = [data_noise_fitz.EXC; data_noise_fitz.GADother; data_noise_fitz.PV; data_noise_fitz.SOM]; %Gather the data from all cell types together in order for the Noise CC
    
    %Noise correlation
    CC.Noise = corr(data_noise_CC'); %Compute the correlation matrix for the Noise CC
    CC.Noise(logical(eye(size(CC.Noise)))) = NaN; %Set the diagonal entries to NaN
    
    %Noise correlation Fitzpatrick
    CC.Noise_Fitzpatrick = corr(data_noise_fitz_CC'); %Compute the correlation matrix for the Noise CC
    CC.Noise_Fitzpatrick(logical(eye(size(CC.Noise_Fitzpatrick)))) = NaN; %Set the diagonal entries to NaN
    
    %Compute the Signal Correlations if using the stim period, otherwise
    %not (pre-stim has no signal)
    
    if strcmp(data_type, 'stim')
        %Total correlation
        CC_Total = corr(data_total_CC'); %Compute the correlation matrix for the Total CC
        CC_Total(logical(eye(size(CC_Total)))) = NaN; %Set the diagonal entries to NaN
        
        %Signal correlation according to eLife paper
        CC.SignalPaper = CC_Total - CC.Noise; %Compute the difference to find the signal correlation
        
        %Signal correlation according to the Nelken paper
        CC.SignalNelken = compute_SC_corrected_fast(data_nelken);
        CC.SignalNelken(logical(eye(size(CC.SignalNelken)))) = NaN; %Set the diagonal entries to NaN
    end
    
    cc_types = fieldnames(CC);
    
    %Set the reepating cells CC to NaN
    for m = 1:size(ix_rep,1)
        for c = 1:length(cc_types)
            CC.(cc_types{c})(ix_rep(m,1),ix_rep(m,2)) = NaN;
            CC.(cc_types{c})(ix_rep(m,2),ix_rep(m,1)) = NaN;
        end
    end
    
    
    %Collect all the different interactions, there are 10 posibilities:
    %E-E, E-G, E-P, E-S;
    %G-G, G-P, G-S
    %P-P, P-S
    %S-S
    
    for c = 1:length(cc_types)
        cc_type = cc_types{c};
        
        %EXC interactions
        CC_int_EE = CC.(cc_type)(1:n_exc,1:n_exc); %Get the part from CC matrix corresponding to EXC neurons only
        CC_int_EE = CC_int_EE(logical(tril(CC_int_EE,-1))); %Get only the lower triangular part w/o diagonal
        CC_int.(sel_curr).(cc_type).EE = CC_int_EE(:);
        
        CC_int_E_all = CC.(cc_type)(n_exc+1:end,1:n_exc); %Get the part from CC matrix corresponding to the interaction of EXC with GADother,PV and SOM
        CC_int_EG = CC_int_E_all(1:n_gadother,:); %Get the interaction EXC-GADother
        CC_int_EP = CC_int_E_all(n_gadother+1:n_gadother + n_pv,:); %Get the interaction EXC-GADother
        CC_int_ES = CC_int_E_all((end - n_som)+1:end,:); %Get the interaction EXC-GADother
        
        CC_int.(sel_curr).(cc_type).EG = CC_int_EG(:);
        CC_int.(sel_curr).(cc_type).EP = CC_int_EP(:);
        CC_int.(sel_curr).(cc_type).ES = CC_int_ES(:);
        
        %GADother interactions
        CC_int_GG = CC.(cc_type)(n_exc+1:n_exc+n_gadother, n_exc+1:n_exc+n_gadother); %Get the part from CC matrix corresponding to GADother neurons only
        CC_int_GG = CC_int_GG(logical(tril(CC_int_GG,-1))); %Get only the lower triangular part w/o diagonal
        CC_int.(sel_curr).(cc_type).GG = CC_int_GG;
        
        CC_int_G_all = CC.(cc_type)(n_exc+n_gadother+1:end,n_exc+1:n_exc+n_gadother); %Get the part from CC matrix corresponding to the interaction of GADotGADher with PV and SOM
        CC_int_GP = CC_int_G_all(1:n_pv,:); %Get the interaction GADother-PV
        CC_int_GS = CC_int_G_all((end - n_som)+1:end,:); %Get the interaction GADother-SOM
        
        CC_int.(sel_curr).(cc_type).GP = CC_int_GP(:);
        CC_int.(sel_curr).(cc_type).GS = CC_int_GS(:);
        
        %PV interactions
        CC_int_PP = CC.(cc_type)(n_exc+n_gadother+1:n_exc+n_gadother+n_pv, n_exc+n_gadother+1:n_exc+n_gadother+n_pv); %Get the part from CC matrix corresponding to PV neurons only
        CC_int_PP = CC_int_PP(logical(tril(CC_int_PP,-1))); %Get only the lower triangular part w/o diagonal
        CC_int.(sel_curr).(cc_type).PP = CC_int_PP;
        
        CC_int_PS = CC.(cc_type)(n_exc+n_gadother+n_pv+1:end, n_exc+n_gadother+1:n_exc+n_gadother+n_pv); %Get the part from CC matrix corresponding to the interaction of GADother with PV and SOM
        
        CC_int.(sel_curr).(cc_type).PS = CC_int_PS(:);
        
        %SOM interactions
        CC_int_SS = CC.(cc_type)((end-n_som)+1:end, (end-n_som)+1:end); %Get the part from CC matrix corresponding to SOM neurons only
        CC_int_SS = CC_int_SS(logical(tril(CC_int_SS,-1))); %Get only the lower triangular part w/o diagonal
        CC_int.(sel_curr).(cc_type).SS = CC_int_SS;
    end
    
    if plot_on
        %Plot the data
        for c = 1:length(cc_types)
            figure('units','normalized','outerposition',[0 0 1 1]);
            cc_type = cc_types{c};
            imagesc(CC.(cc_type));
            colormap('redblue');
            colorbar;
            max_val = max(abs(CC.(cc_type)(:)));
            caxis([-max_val max_val]);
            names = fieldnames(border);
            
            for l = 1:length(names)
                name = names{l};
                yline(border.(name),'--',{name},'LabelHorizontalAlignment','left','LabelVerticalAlignment','middle','LineWidth',2,'Color', colors{l},'FontSize',font_sz);
                xline(border.(name),'--',{name},'LabelHorizontalAlignment','center','LineWidth',2,'Color', colors{l},'FontSize',font_sz);
            end
            
            title([cc_type,' Correlation ', sel_name,' ', area_name]);
            xlabel('ROI #');
            ylabel('ROI #');
            set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
            set(gcf,'color','w');
            axis square;
            save_name = fullfile(save_dir_full,strjoin({area_name,sel_name,'CC',cc_type,'.svg'},'_'));
            saveas(gcf,save_name);
            close;
        end
    end

end
    