function [fra_prop] = compute_fra_prop_2p(fra, base_rate, params)
%[clust_info] = compute_fra_stats(fra_psth)
%This function computes characteristic frequency (CF), best frequency (BF), Q20
%and Q40. We first smooth the FRA with a smoothing window. Then we
%threshold the resulting FRA with the spontaneous rate of each neuron + 20%
%of the maximum activity from the smoothed FRA. We then define FRA 'bounds'
%as the lowest level at each freqeuncy that is significantaly higher above this threshold 
%>>INPUT>>
%fra_psth
%<<OUTPUT<<

freqs = params.freqs;
db_lvls = params.dB_lvls;

%% First perform smoothing of the FRA

s_win=[0.25 0.5 0.25; 0.5 1 0.5; 0.25 0.5 0.25]; %This is the Hamming window used for the smoothing
s_win = s_win/sum(s_win(:)); % Normalise so it sums to 1
fra_pad = [fra(:,1), fra, fra(:,end)]; %Pad the FRA by replicating the 1st and last columns
fra_pad = [fra_pad(1,:); fra_pad; fra_pad(end,:)]; %Replicate the 1st and last rows of the resulting matrix to finish the padding with 2 entries in every direction

fra_smooth = conv2(fra_pad, s_win, 'same'); %Apply 2D convolution with zero padding
fra_smooth(:, [1,end]) = []; %Delete the first and last columns which were added for padding
fra_smooth([1,end], :) = []; %Delete the first and last rows which were added for padding

%% Define base rate and compute bounds

%Define the threshold firing rate. This will be the spontaneous (base) rate
% + 20% of the max smoothed FRA rate
th_rate = base_rate + 1/5*max(fra_smooth(:));
fra_th = fra_smooth - th_rate; %Subtract the threshold from all FRA entries
ix_sig = find(fra_th>0); %Find the index of the freq-level combinations that are significantly higher than the threshold
[lvl_ix, f_ix] = ind2sub(size(fra),ix_sig); %Convert the linear ix to rows (levels) and cols (frequencies) indices
sig_f_ix = unique(f_ix); %Find all the freqeuncies that have at least one significant bin in the fra

%For every significant freqeuncy find the lowest dB level that is
%significant
if ~isempty(sig_f_ix)
    for f = 1:numel(sig_f_ix)
        min_lvl_ixs(f) = max(lvl_ix(f_ix==sig_f_ix(f))); %Note because lvls go high->low but indexing is 1->n we use the max
    end
else
    min_lvl_ixs = [];
end

%% Find the BF, CF, Q20 and Q40 (if possible)

%BF
[~,bf_ix] = max(mean(fra));
bf = freqs(bf_ix);

%CF
[cf_lvl_ix,~] = max(min_lvl_ixs); %Find the maximum value for the index of the dB level at the lowest threshold
if ~isempty(cf_lvl_ix)
    cf_ix = sig_f_ix(min_lvl_ixs==cf_lvl_ix);
    cf = freqs(cf_ix); %Get the freqeuncy/ies that correspond to this lowest level/s
else
    cf = NaN;
    cf_ix = [];
end


if length(cf)>1
    cf = 2^mean(log2(cf)); %If there is more than one freq at the lowest threshold find the log-weighted mean
    cf_ix = 2^mean(log2(sig_f_ix(min_lvl_ixs==cf_lvl_ix))); %Get the index of cf in the same way for plotting purposes
end

%Q20
q20_lvl_ix = cf_lvl_ix - 1; %Find the dB level which is one-up to the CF level (+20dB)
if q20_lvl_ix>0 
    q20_f_ix = unique(f_ix(lvl_ix==q20_lvl_ix));
    q20_f = freqs(q20_f_ix);
    bw_20 = max(q20_f) - min(q20_f); %First find the bandwidth at 20dB louder than the lowest threshold level
    q20 = cf/bw_20;
    
    if isempty(q20) || isinf(q20)
        q20 = NaN;
    end
    
    if isempty(bw_20) || bw_20==0
        bw_20 = NaN;
    end
else
    q20 = NaN;
    bw_20 = NaN;
    q20_f_ix = NaN;
end

%Q40
q40_lvl_ix = cf_lvl_ix - 2; %Find the dB level which is two-up to the CF level (+40dB)
if q40_lvl_ix>0
    q40_f_ix = unique(f_ix(lvl_ix==q40_lvl_ix));
    q40_f = freqs(q40_f_ix);
    bw_40 = max(q40_f) - min(q40_f); %Then find the bandwidth at 40dB louder than the lowest threshold level
    q40 = cf/bw_40;
    
    if isempty(q40) || isinf(q40)
        q40 = NaN;
    end
    
    if isempty(bw_40) || bw_40==0
        bw_40 = NaN;
    end
else
    q40 = NaN;
    bw_40 = NaN;
    q40_f_ix = NaN;
end

%% Save the results
lvl_plot_ix = zeros(length(freqs),1);
lvl_plot_ix(sig_f_ix) = min_lvl_ixs;

fra_prop.lvl_plot_ix = lvl_plot_ix;
fra_prop.f_plot_ix = [1:length(freqs)]';
fra_prop.bf = bf; 
fra_prop.cf = cf; 
fra_prop.bf_ix = bf_ix; 
fra_prop.cf_ix = cf_ix; 
fra_prop.q20 = q20; 
fra_prop.q40 = q40; 
fra_prop.bw_20 = bw_20; 
fra_prop.bw_40 = bw_40;
fra_prop.q20_f_ix = q20_f_ix;
fra_prop.q20_lvl_ix = q20_lvl_ix*ones(length(q20_f_ix));
fra_prop.q40_f_ix = q40_f_ix;
fra_prop.q40_lvl_ix = q40_lvl_ix*ones(length(q40_f_ix));
db_th = db_lvls(cf_lvl_ix);
if isempty(db_th)
    db_th = NaN;
end
fra_prop.db_th = db_th;
fra_prop.fra_smooth = fra_smooth;