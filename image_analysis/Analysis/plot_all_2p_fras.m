function plot_all_2p_fras(fras, params)

%% Define plotting parameters


row = 4;
col = 3;
per = 0.005;
edgel = 0.05; edger = per; edgeh = per; edgeb = 0.08; space_h = 0.015; space_v = 0.015;
[pos]=subplot_pos(row,col,edgel,edger,edgeh,edgeb,space_h,space_v);
num_clust = numel(fras);
freqs = params.freqs;
save_dir = params.save_dir;
cell_type = params.cell_type;
skip_f = 2;
font_sz = 16;

num_freqs = numel(freqs);
x_labels = cell(0);
for jj = 1:skip_f:numel(freqs)
    x_labels{end+1} = num2str(freqs(jj),'%.1f');
end

dB_lvls = flipud(params.dB_lvls);
num_dB_lvls = numel(dB_lvls);

%% Plot the correct FRAs
clust_spacing = row*col - 1;
num_groups = ceil(num_clust/(clust_spacing+1));
last_spacing = num_clust - (num_groups-1)*(clust_spacing+1) - 1;
first_clust_ix = [1:clust_spacing+1:(num_groups)*(clust_spacing+1)];

cl = 0;
for group = 1:num_groups
    
    first_clust = first_clust_ix(group);
    last_clust = first_clust + clust_spacing;
    
    if group == num_groups
        last_clust = first_clust + last_spacing;
    end
    num_subplots = numel(first_clust:last_clust);
    plot_ix = [first_clust:last_clust];
    
    figure('units','normalized','outerposition',[0 0 1 1]);
    for ii = 1:num_subplots
        cl = cl+1;
        subplot('position',pos{ii});
        imagesc(fras{cl});
        colormap('fake_parula');

        if ii == (row - 1)*col + 1
            xticks([1:skip_f:num_freqs]);
            xticklabels(x_labels);
            yticks(1:num_dB_lvls);
            y_labels = string(dB_lvls);
            yticklabels(y_labels);
            set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
            xlabel('Frequency (kHz)','FontSize',font_sz+10,'FontWeight','normal');
            ylabel('Sound Level (dB)','FontSize',font_sz+10,'FontWeight','normal');
        elseif ii > (row - 1)*col + 1 && ii <= row*col
            xticks([1:skip_f:num_freqs]);
            xticklabels(x_labels);
            set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
%             xlabel('Frequency [kHz]','FontSize',font_sz+10,'FontWeight','normal');
            set(gca,'ytick',[]);
        else
            set(gca,'Xticklabel',[]);
            set(gca,'Yticklabel',[]);
        end
        set(gcf,'color','w');
    end

    if exist('save_dir','var')
        file_name = ['FRAs_',cell_type,'_batch',num2str(group)];
        save_name = fullfile(save_dir,[file_name,'.svg']);
        saveas(gcf, save_name);
        close;
    end
end