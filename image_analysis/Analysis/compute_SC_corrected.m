function CC_sig_corr = compute_SC_corrected(dat_in)
%This function will compute the signal correlation according to the 
%'Functional organization and population dynamics in the mouse primary
%auditory cortex' 2010 Nat Neuro Nelken paper
%>>Input>>
%   dat_in - cell array, where each entry is one neuron. 
%       Each cell is RxS matrix, R - # of trials; S - # of stimuli
%
%<<Output<<
%   CC_sig_corr - The corrected signal correlation coefficiens matrix

[R, S] = size(dat_in{1}); %Get the # of trials and stimuli
n_cells = length(dat_in); %Get the # of neurons

%First compute a global mean for each cell (across all stimuli and trials) and subtract it from every trial
%for every stimulus
dat = cellfun(@(x) x - mean(x(:)), dat_in, 'UniformOutput', false);

%Initialize the correlation matrix
CC_sig_corr = zeros(n_cells);

for i = 1:n_cells
    
    for j = 1:n_cells
        
        %Initialize the corr prod sum for ij and uncorrected prod sum for
        %ii and jj
        corrected_prod_stim_sum_ij = zeros(S,1);
        uncorrected_prod_stim_sum_ii = zeros(S,1);
        uncorrected_prod_stim_sum_jj = zeros(S,1);
        
        for s = 1:S
            
            %Take the column vector of responses on all trials for stim s for neurons i and j
            vec_trial_stim_i = dat{i}(:,s); 
            vec_trial_stim_j = dat{j}(:,s); 
            
            %Make a matrix of cross-products for each trial (outer product of the two vectors i and j)
            mat_trial_stim_ij = vec_trial_stim_i*vec_trial_stim_j';
            
            %Do the same procedure for i and j with themselves
            mat_trial_stim_ii = vec_trial_stim_i*vec_trial_stim_i';
            mat_trial_stim_jj = vec_trial_stim_j*vec_trial_stim_j';
            
            %Make a daigonal matrix with the cross-products we want to
            %remove for the ij pair
            mat_diag_trial_stim_ij = zeros(R); mat_diag_trial_stim_ij(1:R+1:end) = diag(mat_trial_stim_ij); 
            
            %Remove the diagonal elements
            mat_trial_stim_ij = mat_trial_stim_ij - mat_diag_trial_stim_ij;
            
            %Compute the corrected product for this particular stimulus
            corrected_prod_stim_ij = sum(mat_trial_stim_ij(:))/(R^2 - R);
            
            %Compute the uncorrected product for this particular stimulus
            %for i and j 
            uncorrected_prod_stim_ii = sum(mat_trial_stim_ii(:))/(R^2);
            uncorrected_prod_stim_jj = sum(mat_trial_stim_jj(:))/(R^2);
            
            %Store the result
            corrected_prod_stim_sum_ij(s) = corrected_prod_stim_ij;
            uncorrected_prod_stim_sum_ii(s) = uncorrected_prod_stim_ii;
            uncorrected_prod_stim_sum_jj(s) = uncorrected_prod_stim_jj;
            
        end
        
        %Compute the corrected covariance across all stimuli for ij pair
        COV_corrected_ij = sum(corrected_prod_stim_sum_ij)/S;
        
        %Compute the uncorrected covariance across all stimuli for ii and
        %jj (= var of i and j)
        COV_uncorrected_ii = sum(uncorrected_prod_stim_sum_ii)/S;
        COV_uncorrected_jj = sum(uncorrected_prod_stim_sum_jj)/S;
        
        %Compute the corrected correlation of neuron i with neuron j
        CC_sig_corr(i,j) = COV_corrected_ij/sqrt(COV_uncorrected_ii*COV_uncorrected_jj);
        
    end
    
end
