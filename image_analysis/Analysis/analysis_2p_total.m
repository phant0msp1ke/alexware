%In this script I will combine the results from the two animals together
%and perform necessary plotting and further analysis. This will be based on
%the function plot_2p_fras which will run for each animal and store the
%output. I probably want some struct array to store all my different
%outputs.

%% Define params
animals = {'Leela', 'Bender'};
save_dir_root = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction/Thesis_chapter';
font_sz = 30;
font_type = 'Liberation Sans';

%Plotting selection
%Figure 1
plot_fig1 = 0;

%Figure 2
plot_fig2 = 0;

%Figure 3
plot_fig3A = 0;
plot_fig3B = 0;

%Figure 4
plot_fig4 = 0;

%Figure 5
plot_fig5 = 0;

%Figure 6
plot_fig6A = 0;
plot_fig6B = 0;

%% Get the data

for j = 1:length(animals)
    animal = animals{j};
    tic;
    fprintf('== Processing animal: %s ==\n', animal);
    Rez.(animal) = analysis_2p(animal);
    fprintf('== Done! This took %0.fs ==\n', toc);
end

%% Plotting setup
freqs = Rez.(animals{1}).freqs;
skip_f = 2;
num_freqs = numel(freqs);
y_labels = cell(0);
for jj = 1:skip_f:numel(freqs)
    y_labels{end+1} = num2str(freqs(jj),'%.1f');
end

%% Figure 1: Plot percentage of tuned neurons

%Fist combine the data together
sel_types = fieldnames(rmfield(Rez.(animal).prop, 'Total'));
cell_types = fieldnames(Rez.(animal).prop.(sel_types{1}));

for i = 1:length(sel_types)
    sel_type = sel_types{i};
    
    for k = 1:length(cell_types)
        cell_type = cell_types{k};
        
        %Get the number of cells in each selection category
        n_cell_1 = Rez.(animals{1}).prop.(sel_type).(cell_type);
        n_cell_2 = Rez.(animals{2}).prop.(sel_type).(cell_type);
        
        %Get the total number of cells in each class
        Total_cell_1 = Rez.(animals{1}).prop.Total.(cell_type);
        Total_cell_2 = Rez.(animals{2}).prop.Total.(cell_type);
        
        %Get the percentages for plotting
        perc.(cell_type).(sel_type) = 100*mean([n_cell_1/Total_cell_1, n_cell_2/Total_cell_2]);
        
        %Get the numbers of cells in each class and total number for
        %Likelihood Ratio Test
        prop.(cell_type).(sel_type) = n_cell_1 + n_cell_2;
        prop.(cell_type).Total = Total_cell_1 + Total_cell_2;
    end
    
end

%Compute the Likelihood Ratio Test with EXC and GAD
prop_gad_all.EXC = prop.EXC; prop_gad_all.GAD = prop.GAD;
rez_gad_all = likelihood_ratio_test(prop_gad_all);

%Compute the Likelihood Ratio Test with EXC, GADother, PV and SOM
prop_subclass = rmfield(prop, 'GAD');
rez_subclass = likelihood_ratio_test(prop_subclass);

%Plot the data comparing EXC and all GAD only
X = categorical({'Responsive OR Selective', 'Responsive','Selective Level', 'Selective Frequency'});
X = reordercats(X,{'Responsive OR Selective', 'Responsive', 'Selective Level', 'Selective Frequency'});
Y = [perc.EXC.is_resp_sel  perc.GAD.is_resp_sel; ... %Responsive or Selective
    perc.EXC.is_resp  perc.GAD.is_resp; ... %Responsive
    perc.EXC.is_sel_level  perc.GAD.is_sel_level; ... %Selective LEVEL
    perc.EXC.is_sel_paper  perc.GAD.is_sel_paper]; %Selective FREQUENCY

params.font_sz = 40;
save_dir = fullfile(save_dir_root, 'Figure_1', '2p');

if ~exist(save_dir, 'dir')
    mkdir(save_dir);
end

if plot_fig1
    
    %Define plotting params
    params.save_dir = save_dir;
    params.colors = {'r', 'b'};
    params.labels = {'EXC', 'GADall'};
    params.title_txt = ['Percentage of tuned neurons by class'];
    params.ylim_val = 80;
    
    plot_percentages(X, Y, params);
    
    %Plot the data comparing EXC and different classes of GAD
    Y = [perc.EXC.is_resp_sel  perc.GADother.is_resp_sel  perc.PV.is_resp_sel  perc.SOM.is_resp_sel; ... %Responsive or Selective
        perc.EXC.is_resp  perc.GADother.is_resp  perc.PV.is_resp  perc.SOM.is_resp; ... %Responsive
        perc.EXC.is_sel_level  perc.GADother.is_sel_level  perc.PV.is_sel_level  perc.SOM.is_sel_level; ... %Selective LEVEL
        perc.EXC.is_sel_paper  perc.GADother.is_sel_paper  perc.PV.is_sel_paper  perc.SOM.is_sel_paper]; %Selective FREQUENCY
    
    %Define plotting params
    params.colors = {'r', [0, 0.6, 0], 'c', 'm'};
    params.labels = {'EXC', 'GADother', 'PV', 'SOM'};
    params.title_txt = ['Percentage of tuned neurons by subclass'];
    
    plot_percentages(X, Y, params);
    
end
%% Figure 3A: Median BF comparison

cell_types = {'EXC', 'GAD'};
median_BF.EXC = []; median_BF.GAD = [];
cdf_BF.EXC = []; cdf_BF.GAD = [];
sz = 40;
lw = 1.5;
lw2 = 4;
font_sz = 55;

BF_all.EXC = []; BF_all.GAD = [];
for j = 1:length(animals)
    animal = animals{j};
    areas = fieldnames(Rez.(animal).BF);
    
    for i = 1:length(areas)
        area = areas{i};
        
        for k = 1:length(cell_types)
            cell_type = cell_types{k};
            
            %Get the data for this animal, area and cell type and compute the median
            temp_data = Rez.(animal).BF.(area).resp_sel.(cell_type);
            temp_data = temp_data(:); %Force a col vector
            temp_median = median(temp_data);
            
            %Compute the distance in octaves between the median frequency
            %of the field and the BF of every neuron
            temp_cdf = abs(log2(temp_median./temp_data));
            
            %Store the results 
            BF_all.(cell_type) = [BF_all.(cell_type); temp_data];
            median_BF.(cell_type)(end+1,1) = temp_median;
            cdf_BF.(cell_type) = [cdf_BF.(cell_type); temp_cdf];
        end
    end
end

%Perform stats to see if BF is different between EXC and GAD between areas
[pval.bf_median, ~, stats.bf_median] = signrank(median_BF.EXC, median_BF.GAD, 'method', 'approximate');

if plot_fig3A
    
    % Plot the data as box plots and individual points connected by lines -
    % I should make this into a function!!!
    n_points = length(median_BF.EXC);
    figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
    title('Comparing the median BF across areas');
    
    ylabel('BF (kHz)');
    set(gcf,'color','w');
    set(gca,'TickDir','out');
    hold on;
    
    %Plot a scatter plot with all the individual points
    scatter(ones(n_points,1), median_BF.EXC, sz, 'k', 'filled');
    scatter(2*ones(n_points,1), median_BF.GAD, sz, 'k', 'filled');
    %Connect them with lines
    for n = 1:n_points
        plot([1,2], [median_BF.EXC(n), median_BF.GAD(n)],'k', 'LineWidth', lw);
    end
    
    %Plot the boxes
    bh = boxplot([median_BF.EXC, median_BF.GAD], 'Notch', 'on', 'Labels', {'EXC', 'GAD'}, 'colors', 'rb', 'medianstyle', 'line');
    set(bh,'LineWidth', lw2);
    
    set(gca, 'YScale', 'log');
    set(gca,'Ytick',[]);
    set(gca,'YminorTick','off');
    yticks(freqs(1:skip_f:num_freqs));
    yticklabels(y_labels);
    xlabel('Cell type');
    ylim([0 24]);
    set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
    hold off;
    
    save_dir = fullfile(save_dir_root, 'Figure_3', '2p');
    if ~exist(save_dir, 'dir')
        mkdir(save_dir);
    end
    
    full_name = fullfile(save_dir,['Median_BF_across_areas','.svg']);
    saveas(gcf, full_name);
    close all;
    
end
%% Figure 2: BF and CF total distribtuions 

font_sz = 55;
% FRA properties 
Q20.EXC = []; Q40.EXC = []; CF.EXC = []; dB_th.EXC = []; 
Q20.GAD = []; Q40.GAD = []; CF.GAD = []; dB_th.GAD = []; 
cell_types = {'EXC', 'GAD'};

for j = 1:length(animals)
    animal = animals{j};
    areas = fieldnames(Rez.(animal).FRA_props.Q20);
    
    for i = 1:length(areas)
        area = areas{i};
        
        for k = 1:length(cell_types)
            cell_type = cell_types{k};
            
            Q20.(cell_type) = [Q20.(cell_type); Rez.(animal).FRA_props.Q20.(area).resp_sel.(cell_type)']; 
            Q40.(cell_type) = [Q40.(cell_type); Rez.(animal).FRA_props.Q40.(area).resp_sel.(cell_type)']; 
            CF.(cell_type) = [CF.(cell_type); Rez.(animal).FRA_props.CF.(area).resp_sel.(cell_type)']; 
            dB_th.(cell_type) = [dB_th.(cell_type); Rez.(animal).FRA_props.dB_th.(area).resp_sel.(cell_type)']; 
        end
        
    end
    
end

%Perform a Wilcoxon test since distribution of BFs is not normal
pval.bf_all = ranksum(BF_all.EXC, BF_all.GAD);
pval.cf_all = ranksum(CF.EXC, CF.GAD);

if plot_fig2
    
    save_dir = fullfile(save_dir_root, 'Figure_2', '2p');
    if ~exist(save_dir, 'dir')
        mkdir(save_dir);
    end
    
    %Plot the distribution of BF as violins
    params_fra_props.title_txt = 'BF distribution across all neurons';
    params_fra_props.save_dir = save_dir;
    params_fra_props.ylabel_txt = 'BF (kHz)';
    params_fra_props.font_sz = font_sz;
    params_fra_props.plot_type = 'bf';
    params_fra_props.freqs = freqs;
    params_fra_props.skip_f = skip_f;
    params_fra_props.y_labels = y_labels;
    fra_props_violinplot(BF_all, params_fra_props);
    
    %Plot the distribution of BF as violins
    params_fra_props.title_txt = 'CF distribution across all neurons';
    params_fra_props.ylabel_txt = 'CF (kHz)';
    fra_props_violinplot(CF, params_fra_props);
    
end
%% Figure 3B: CDF of BF comparison

font_sz = 55;
lw = 4;
n_pixels = 512; mm_um = 10^-3; um_pixel = 0.7812; %This is the normalization factor for a FOV of 16x Objective at Zoom 2
correction_factor = n_pixels*um_pixel*mm_um; %The correction factor we need to divide by
cdf_BF.EXC = cdf_BF.EXC/correction_factor; cdf_BF.GAD = cdf_BF.GAD/correction_factor; %Apply the correction

%Perform a Mann Whitney U test to see if the two distributions are different 
[pval.bf_cdf_mw,~,stats.bf_cdf] = ranksum(cdf_BF.EXC, cdf_BF.GAD);

%Perform a KS test
[~, pval.bf_cdf_ks, stats.bf_cdf_ks] = kstest2(cdf_BF.EXC, cdf_BF.GAD);

save_dir = fullfile(save_dir_root, 'Figure_3', '2p');

if plot_fig3B
    
    %Next, plot the CDF for both cell type distributions
    figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
    
    %EXC cdf plot
    [h_EXC, stats_cdf.EXC] = cdfplot(cdf_BF.EXC); hold on;
    h_EXC.LineWidth = lw; h_EXC.Color = 'r';
    
    %GAD cdf plot
    [h_GAD, stats_cdf.GAD] = cdfplot(cdf_BF.GAD);
    h_GAD.LineWidth = lw; h_GAD.Color = 'b';
    
    xlabel('Distance from median BF (oct/mm)');
    ylabel('Cumulative probability');
    title('CDF of EXC and GAD neurons');
    set(gcf,'color','w');
    set(gca,'TickDir','out');
    labels={'EXC'; 'GAD'};
    legend(labels,'location','northwest');
    set(gca,'FontName', font_type,'FontSize',font_sz,'FontWeight','Normal');
    full_name = fullfile(save_dir,['CDF_BF_across_areas','.svg']);
    saveas(gcf, full_name);
    close all;
    
end
%% Figure 4: Percentage of Single, Double and Complex Neurons and Likelihood Ratio Test

font_sz = 55;
fra_types = {'Single', 'Double', 'Complex'};
cell_types = {'EXC', 'GAD'};

%Get the number of cells in each category across both animals
for j =1:length(cell_types)
    cell_type = cell_types{j};
    
    fra_shape.(cell_type).Total = 0;
    for i = 1:length(fra_types)
        fra_type = fra_types{i};
        
        fra_shape.(cell_type).(fra_type) = Rez.Leela.fra_shape.is_resp_sel.(fra_type).(cell_type) + Rez.Bender.fra_shape.is_resp_sel.(fra_type).(cell_type);
        fra_shape.(cell_type).Total = fra_shape.(cell_type).Total + fra_shape.(cell_type).(fra_type);
    end
    
end

%Perform Likelihood Ratio Test
LR_test = likelihood_ratio_test(fra_shape);

%Plot bar graphs of the results
figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
X = categorical({'Single', 'Double','Complex'});
X = reordercats(X, {'Single', 'Double','Complex'});
EXC_total = fra_shape.EXC.Total; GAD_total = fra_shape.GAD.Total; 

Y = 100*[fra_shape.EXC.Single/EXC_total, fra_shape.GAD.Single/GAD_total; ... %Single neurons
    fra_shape.EXC.Double/EXC_total, fra_shape.GAD.Double/GAD_total; ... %Double neurons
    fra_shape.EXC.Complex/EXC_total, fra_shape.GAD.Complex/GAD_total]; %Complex neurons

hb = bar(X,Y);
hb(1).FaceColor = 'r';
hb(2).FaceColor = 'b';
labels={'EXC'; 'GAD'};
legend(labels,'location','northwest');
set(gcf,'color','w');
set(gca,'TickDir','out'); 
ylabel('% neurons');
title('Proportion of neurons by FRA class for EXC and GAD');
ylim([0 80]);
set(gca,'FontName', font_type, 'FontSize', font_sz, 'FontWeight', 'Normal');
hold on;

save_dir = fullfile(save_dir_root, 'Figure_4', '2p');
if ~exist(save_dir, 'dir')
    mkdir(save_dir);
end

save_name = fullfile(save_dir,['Percentage_FRA_shapes.svg']);
saveas(gcf, save_name);
close;

%% Signal and Noise correlation
%Combine the results from the ferrets together


sel_types = fieldnames(Rez.Leela.CC); %Get the types of selections
data_types = fieldnames(Rez.Leela.CC.all); %Get the data types
int_types = fieldnames(Rez.Leela.CC.all.stim.Noise); %Get the types of interactions

for j = 1:length(sel_types)
    sel_type = sel_types{j};
    
    for i = 1:length(data_types)
        data_type = data_types{i};
        cc_types = fieldnames(Rez.Leela.CC.(sel_type).(data_type));
        
        for k = 1:length(cc_types)
            cc_type = cc_types{k};
            int_types = fieldnames(Rez.Leela.CC.(sel_type).(data_type).(cc_type));
            
            for l = 1:length(int_types)
                int_type = int_types{l};
                
                CC.(sel_type).(data_type).(cc_type).(int_type) = [Rez.Leela.CC.(sel_type).(data_type).(cc_type).(int_type); Rez.Bender.CC.(sel_type).(data_type).(cc_type).(int_type)];
            end
        end
        
    end
end

save_dir = fullfile(save_dir_root, 'Figure_5', '2p');
if ~exist(save_dir, 'dir')
    mkdir(save_dir);
end

%% Figure 5: SC and NC Total Violin plots, KW test
display_results = 'off';
selected_data_NC = 'resp_sel'; % 'sel_paper' OR 'resp_sel' OR 'all'
selected_data_SC = 'resp_sel'; % 'sel_paper' OR 'resp_sel' OR 'all'
method_SC = 'SignalNelken'; % 'SignalPaper' or 'SignalNelken'


%Combine the data for NC
CC_noise.EE = CC.(selected_data_NC).stim.Noise.EE; %EXC - EXC all
CC_noise.EG = [CC.(selected_data_NC).stim.Noise.EG; CC.(selected_data_NC).stim.Noise.EP; CC.(selected_data_NC).stim.Noise.ES]; %EXC - GAD all
CC_noise.GG = [CC.(selected_data_NC).stim.Noise.GG; CC.(selected_data_NC).stim.Noise.GP; CC.(selected_data_NC).stim.Noise.GS;...
    CC.(selected_data_NC).stim.Noise.PP; CC.(selected_data_NC).stim.Noise.PS; CC.(selected_data_NC).stim.Noise.SS;]; %GAD all - GAD all

%Combine the data for SC
CC_signal.EE = CC.(selected_data_SC).stim.(method_SC).EE; %EXC - EXC all
CC_signal.EG = [CC.(selected_data_SC).stim.(method_SC).EG; CC.(selected_data_SC).stim.(method_SC).EP; CC.(selected_data_SC).stim.(method_SC).ES]; %EXC - GAD all
CC_signal.GG = [CC.(selected_data_SC).stim.(method_SC).GG; CC.(selected_data_SC).stim.(method_SC).GP; CC.(selected_data_SC).stim.(method_SC).GS;...
    CC.(selected_data_SC).stim.(method_SC).PP; CC.(selected_data_SC).stim.(method_SC).PS; CC.(selected_data_SC).stim.(method_SC).SS;]; %GAD all - GAD all

%Perform Kruskal-Wallis (KW) test to compare the distributions for NC
data_noise = [CC_noise.EE; CC_noise.EG; CC_noise.GG]; %Make a vector with the data for KW test
group_noise = [repmat({'EE'},length(CC_noise.EE),1); repmat({'EG'},length(CC_noise.EG),1); repmat({'GG'},length(CC_noise.GG),1)]; %Make a vector with the group labels

[pval.NC.global.single, pval.NC.global.table, pval.NC.global.stats] = kruskalwallis(data_noise, group_noise, display_results);
pval.NC.global.multiple = multcompare(pval.NC.global.stats,'estimate', 'kruskalwallis', 'display', display_results);


%Perform Kruskal-Wallis (KW) test to compare the distributions for SC
data_signal = [CC_signal.EE; CC_signal.EG; CC_signal.GG]; %Make a vector with the data for KW test
group_signal = [repmat({'EE'},length(CC_signal.EE),1); repmat({'EG'},length(CC_signal.EG),1); repmat({'GG'},length(CC_signal.GG),1)]; %Make a vector with the group labels

[pval.SC.global.single, pval.SC.global.table, pval.SC.global.stats] = kruskalwallis(data_signal, group_signal, display_results);
pval.SC.global.multiple  = multcompare(pval.SC.global.stats,'estimate', 'kruskalwallis', 'display', display_results);

params.save_dir = save_dir;
params.font_sz = 50;

if plot_fig5
    
    %Plot the violins for NC
    params.title_txt = ['Noise correlations for ', selected_data_NC, ' neurons during stim period'];
    params.int_names = {'EXC-EXC', 'EXC-Gall', 'Gall-Gall'};
    params.colors = {[1 0 0], [1 0 1], [0 0 1]};
    plot_SN_violins2(CC_noise, params);
    
    %Plot the violins for SC
    params.title_txt = ['Signal correlations for ', selected_data_SC ,' ',method_SC,' neurons during stim period'];
    params.int_names = {'EXC-EXC', 'EXC-Gall', 'Gall-Gall'};
    plot_SN_violins2(CC_signal, params);
    
end
%% Figure 6A: SC and NC Matrix

save_dir = fullfile(save_dir_root, 'Figure_6', '2p');
if ~exist(save_dir, 'dir')
    mkdir(save_dir);
end

save_dir2 = fullfile(save_dir, 'A');
if ~exist(save_dir2, 'dir')
    mkdir(save_dir2);
end

params.font_sz = 40;
params.save_dir = save_dir2;

if plot_fig6A
    
    params.save_dir = save_dir3;
    %Plot the Interaction matrix
    plot_SN_matrix(CC, params);
    
end
%% Figure 6B: SC and NC subclasses Violin plots

save_dir3 = fullfile(save_dir, 'B');
if ~exist(save_dir3, 'dir')
    mkdir(save_dir3);
end

display_results = 'on';
multcomp_correction = 'hsd';
%Make the data for a KW test for NC
interactions = fieldnames(CC.all.stim.Noise);
data_noise = [];
group_noise = cell(0);
data_signal = [];
group_signal = cell(0);

for j = 1:length(interactions)
    
    interaction = interactions{j};
    
    data_noise = [data_noise; CC.resp_sel.stim.Noise.(interaction)]; %Make a vector with the data for KW test
    group_noise = [group_noise; repmat({interaction}, length(CC.resp_sel.stim.Noise.(interaction)), 1)]; %Make a vector with the group labels
    
    data_signal = [data_signal; CC.resp_sel.stim.SignalNelken.(interaction)]; %Make a vector with the data for KW test
    group_signal = [group_signal; repmat({interaction}, length(CC.resp_sel.stim.SignalNelken.(interaction)), 1)]; %Make a vector with the group labels
    
end

%Perform KW test for NC
[pval.NC.subclass.single, pval.NC.subclass.table, pval.NC.subclass.stats] = kruskalwallis(data_noise, group_noise, display_results);
pval.NC.subclass.multiple = multcompare(pval.NC.subclass.stats,'estimate', 'kruskalwallis','CType',multcomp_correction, 'display', display_results);

%Perform KW test for SC
[pval.SC.subclass.single, pval.SC.subclass.table, pval.SC.subclass.stats] = kruskalwallis(data_signal, group_signal, display_results);
pval.SC.subclass.multiple = multcompare(pval.SC.subclass.stats,'estimate', 'kruskalwallis','CType',multcomp_correction, 'display', display_results);

params.save_dir = save_dir3;
params.font_sz = 50;

if plot_fig6B
    
    %Plot the violins
    plot_SN_violins(CC, params);
    
end

%% Figure 7: Correlation between SC and NC
marker_size = 15;
lw = 3;
font_sz = 20;
txt_sz = 15;
selection_CC = 'all'; % 'sel_paper' OR 'resp_sel' OR 'all'
method_SC = 'SignalPaper'; % 'SignalPaper' or 'SignalNelken'
corr_type = 'Pearson';

%Get the necessary data
CC_noise = CC.(selection_CC).stim.Noise;
CC_signal = CC.(selection_CC).stim.(method_SC);

interactions = fieldnames(CC_noise);

for j = 1:length(interactions)
    
    interaction = interactions{j};
    
    try
        [rho.(interaction), pval.NC_SC_corr.(interaction)] = corr(CC_noise.(interaction), CC_signal.(interaction), 'rows', 'complete', 'type', corr_type);
    catch
        rho.(interaction) = NaN;
        pval.NC_SC_corr.(interaction) = NaN;
    end
    
end

%Make a matrix of the correlation
CC_mat = zeros(4);
CC_mat(1:4+1:end) = [rho.EE, rho.GG, rho.PP, rho.SS]; %Put the diagonal entries corresponding to the self-interactions
CC_mat(2:4,1) = [rho.EG; rho.EP; rho.ES]; CC_mat(1,2:4) = [rho.EG; rho.EP; rho.ES]; %Put the EXC cross-interactions
CC_mat(3:4,2) = [rho.GP; rho.GS]; CC_mat(2,3:4) = [rho.GP; rho.GS]; %Put the GAD cross-interactions
CC_mat(4,3) = [rho.PS]; CC_mat(3,4) = [rho.PS]; %Put the PV cross-interactions

if plot_fig5D
    
    %Plot the correlations between SC and NC
    figure('units','normalized','outerposition',[0 0 1 1]);
    ixs = [1,5,9,13,6,10,14,11,15,16]; %The index of the plots
    row = 4;
    col = 4;
    edgel = 0.06; edger = 0.015; edgeh = 0.015; edgeb = 0.08; space_h = 0.05; space_v = 0.06;
    [pos]=subplot_pos(row,col,edgel,edger,edgeh,edgeb,space_h,space_v);
    
    for j = 1:length(ixs)
        
        interaction = interactions{j};
        ix = ixs(j);
        subplot('position',pos{ix});
        
        scatter(CC_noise.(interaction), CC_signal.(interaction), marker_size, 'filled', 'k');
        h = lsline;
        h.Color = 'r';
        h.LineWidth = lw;
        set(gcf,'color','w');
        
        if ix==13
            xlabel('Noise correlation');
            ylabel('Signal correlation');
        end
        
        if pval.NC_SC_corr.(interaction)>0.01
            txt = ['r = ',num2str(rho.(interaction),'%0.2f'), ', p = ', num2str(pval.NC_SC_corr.(interaction), '%.2f')];
        else
            txt = ['r = ',num2str(rho.(interaction),'%0.2f'), ', p = ', num2str(pval.NC_SC_corr.(interaction), '%.1e')];
        end
        
        text(0.45, 0.99, txt, 'Units', 'Normalized', 'FontSize', txt_sz);
        
        set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
    end
    
    save_dir5 = fullfile(save_dir, 'D');
    if ~exist(save_dir5, 'dir')
        mkdir(save_dir5);
    end
    
    file_name = ['SC_NC_correlation_',selection_CC, '_', method_SC];
    save_name = fullfile(save_dir5,[file_name,'.svg']);
    saveas(gcf, save_name);
    close;
    
    %Plot the matrix of correlations
    cell_labels = {'EXC','GADother','PV','SOM'};
    figure('units','normalized','outerposition',[0 0 1 1]);
    max_val = max(abs(CC_mat(:)));
    imagesc(CC_mat);
    colormap('redblue');
    caxis([-max_val max_val]);
    colorbar;
    xticks([1:4]);
    xticklabels(cell_labels);
    yticks([1:4]);
    yticklabels(cell_labels);
    title(['Correlation between Signal and Noise ', selection_CC, ' stim_', method_SC]);
    axis square;
    set(gcf,'color','w');
    set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
    file_name = ['SC_NC_correlation_matrix_',selection_CC, '_', method_SC];
    save_name = fullfile(save_dir5,[file_name,'.svg']);
    saveas(gcf, save_name);
    close;
    
end