function CC_sig_corr = compute_SC_corrected_fast(dat_in)
%This function will compute the signal correlation according to the 
%'Functional organization and population dynamics in the mouse primary
%auditory cortex' 2010 Nat Neuro Nelken paper
%>>Input>>
%   dat_in - cell array, where each entry is one neuron. 
%       Each cell is RxS matrix, R - # of trials; S - # of stimuli
%
%<<Output<<
%   CC_sig_corr - The corrected signal correlation coefficiens matrix

[R, S] = size(dat_in{1}); %Get the # of trials and stimuli
n_cells = length(dat_in); %Get the # of neurons

%First compute a global mean for each cell (across all stimuli and trials) and subtract it from every trial
%for every stimulus
dat = cellfun(@(x) x - nanmean(x(:)), dat_in, 'UniformOutput', false);

%Initialize the correlation matrix
CC_sig_corr = zeros(n_cells);

for i = 1:n_cells
    
    for j = 1:n_cells
        
        %Initialize the corr prod sum for ij and uncorrected prod sum for
        %ii and jj
        corrected_prod_stim_sum_ij = zeros(S,1);
        
        %Take the mean responses across trials
        mean_resp_i = nanmean(dat_in{i});
        mean_resp_j = nanmean(dat_in{j});
        
        %Find the covariance of i and j with themselves (varaince)
        COV_uncorrected_ii = ((mean_resp_i - nanmean(mean_resp_i))*(mean_resp_i - nanmean(mean_resp_i))')/S; 
        COV_uncorrected_jj = ((mean_resp_j - nanmean(mean_resp_j))*(mean_resp_j - nanmean(mean_resp_j))')/S; 
        
        for s = 1:S
            
            %Take the column vector of responses on all trials for stim s for neurons i and j
            vec_trial_stim_i = dat{i}(:,s); 
            vec_trial_stim_j = dat{j}(:,s); 
            
            %Make a matrix of cross-products for each trial (outer product of the two vectors i and j)
            mat_trial_stim_ij = vec_trial_stim_i*vec_trial_stim_j';
            
            %Remove the diagonal elements
            mat_trial_stim_ij(1:R+1:end) = 0;
            
            %Compute the corrected product for this particular stimulus
            corrected_prod_stim_sum_ij(s) = nansum(mat_trial_stim_ij(:))/(R^2 - R);
            
        end
        
        %Compute the corrected covariance across all stimuli for ij pair
        COV_corrected_ij = nansum(corrected_prod_stim_sum_ij)/S;
        
        
        %Compute the corrected correlation of neuron i with neuron j
        CC_sig_corr(i,j) = COV_corrected_ij/sqrt(COV_uncorrected_ii*COV_uncorrected_jj);
        
    end
    
end