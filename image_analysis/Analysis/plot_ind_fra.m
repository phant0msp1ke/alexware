function plot_ind_fra(fras, params)

%Get params out
n_fras = length(fras);
is_resp = params.is_resp; 
is_sel = params.is_sel;
BF = params.BF;
fra_shape = params.fr_shape;
freqs = params.freqs;
dB_lvls = flipud(params.dB_lvls);
save_dir = params.save_dir;
cell_type = params.cell_type;
ids = params.ids;
n_freqs  = length(freqs);
n_dB_lvls = length(dB_lvls);
skip_f = 2;
font_sz = 40;

%Make labels
count = 0;
for jj = 1:skip_f:n_freqs
    count = count + 1;
    x_lbl{count} = num2str(freqs(jj),'%.1f');
end

for jj = 1:numel(BF)
    BF_lbl{jj} = num2str(BF(jj),'%.1f');
end

y_lbl = string(dB_lvls);

%Make the plots
for k = 1:n_fras
    figure('units','normalized','outerposition',[0 0 1 1]);
    fra = fras{k};
    imagesc(fra);
    colormap('fake_parula');
    resp = num2str(is_resp(k));
    sel = num2str(is_sel(k));
    shape = fra_shape(k);
    
    switch shape
        case 1
            fra_shape_name = 'Single';
        case 2
            fra_shape_name = 'Double';
        case {3,4}
            fra_shape_name = 'Complex';
    end
    
    bf = BF_lbl{k};
    id = ids(k);
    
    %Labels
    xticks([1:skip_f:n_freqs]);
    xticklabels(x_lbl);
    yticks(1:n_dB_lvls);
    yticklabels(y_lbl);
    set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
    xlabel('Frequency (kHz)','FontSize',font_sz,'FontWeight','normal');
    ylabel('Sound Level (dB)','FontSize',font_sz,'FontWeight','normal');
    title_txt = ['Resp=',resp,', Sel Any=',sel,', BF=',bf,'kHz',', Shape=',fra_shape_name];
    title(title_txt);
    set(gcf,'color','w');
    set(gca,'TickDir','out');
    save_name = fullfile(save_dir,[cell_type,'_',num2str(k),'.svg']);
    saveas(gcf, save_name);
    close;
end