function plot_fra_profile(mean_fra, params)



%Normalize the FRA
norm_sum_fra = mean_fra./sum(mean_fra,2);
norm_max_fra = mean_fra./max(mean_fra,[],2);


%Get params out
n_fras = length(mean_fra);
freqs = params.freqs;
save_dir = params.save_dir;
cell_type = params.cell_type;
n_freqs  = length(freqs);
skip_f = 2;
font_sz = 40;

%Make labels
count = 0;
for jj = 1:skip_f:n_freqs
    count = count + 1;
    x_lbl{count} = num2str(freqs(jj),'%.1f');
end

%Make the plots for sum normalized
figure('units','normalized','outerposition',[0 0 1 1]);
imagesc(norm_sum_fra);
colormap('fake_parula');

%Labels
xticks([1:skip_f:n_freqs]);
xticklabels(x_lbl);
set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
xlabel('Frequency (kHz)','FontSize',font_sz,'FontWeight','normal');
ylabel('ROI #','FontSize',font_sz,'FontWeight','normal');
title(['FRA profile ', cell_type, ' Sum norm']);
set(gcf,'color','w');
set(gca,'TickDir','out');
save_name = fullfile(save_dir,[cell_type,'_FRA_profile_sum_norm.svg']);
saveas(gcf, save_name);
close;


%Make the plots for max normalized
figure('units','normalized','outerposition',[0 0 1 1]);
imagesc(norm_max_fra);
colormap('fake_parula');

%Labels
xticks([1:skip_f:n_freqs]);
xticklabels(x_lbl);
set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
xlabel('Frequency (kHz)','FontSize',font_sz,'FontWeight','normal');
ylabel('ROI #','FontSize',font_sz,'FontWeight','normal');
title(['FRA profile ', cell_type, ' Max norm']);
set(gcf,'color','w');
set(gca,'TickDir','out');
save_name = fullfile(save_dir,[cell_type,'_FRA_profile_max_norm.svg']);
saveas(gcf, save_name);
close;
