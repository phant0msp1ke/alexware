function rez = likelihood_ratio_test(data)
%This function will perform the chi-squared test

cell_types = fieldnames(data); %Get the two cell types
n_cell_types = length(cell_types);
data_types = fieldnames(rmfield(data.(cell_types{1}), 'Total')); %Get the Data types

ix = [1:n_cell_types]; %Make an index for all cell types
perm_mat = nchoosek(ix, 2); %Make a permutation matrix with the nchoosek possible permutations invovlving two elements
n_perm = nchoosek(n_cell_types,2); %Also get the number of possible permutations


% Compute the Likelihood Ratio test for each data type and for every
% possible permutation

for j = 1:n_perm
    %Get the indeces of both cells
    ix_cell1 = perm_mat(j,1); ix_cell2 = perm_mat(j,2);
    
    for i = 1:length(data_types)
        data_type = data_types{i};
        
        %Compute key values for both cell types
        ct1 = cell_types{ix_cell1};  ct2 = cell_types{ix_cell2};
        
        N = data.(ct1).Total; %Total number of neurons in cell type 1
        n = data.(ct1).(data_type); %Number of data of certain type in cell type 1
        
        M = data.(ct2).Total; %Total number of neurons in cell type 2
        m = data.(ct2).(data_type); %Number of data of certain type in cell type 2
        
        p=n/N; %Ratio for cell type 1
        q=m/M; %Ratio for cell type 2
        r=(m+n)/(M+N); %Total ratio
        
        %Compute Chi-squared value
        rez.Chi_sq(j).Interaction = [ct1,'-',ct2];
        Chi_sq = 2*(n*log(p/r) + (N-n)*log((1-p)/(1-r)) + m*log(q/r) + (M-m)*log((1-q)/(1-r)));
        rez.Chi_sq(j).(data_type) = Chi_sq;
        
        
        %Compute p-value for this Chi-squared
        rez.pval(j).Interaction = [ct1,'-',ct2];
        rez.pval(j).(data_type) = 1 - chi2cdf(Chi_sq, 1);
        
        
    end
end