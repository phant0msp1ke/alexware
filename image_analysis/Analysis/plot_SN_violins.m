function plot_SN_violins(CC_rez, params)

sel_types = fieldnames(CC_rez);
data_types = fieldnames(CC_rez.(sel_types{1}));

% int_types = fieldnames(CC_rez.(data_types{1}).(sel_types{1}).(cc_types{1}));
int_names = {'EXC_EXC','EXC_GADother','EXC_PV','EXC_SOM','GADother_GADother','GADother_PV','GADother_SOM','PV_PV','PV_SOM','SOM_SOM'}';

save_dir = params.save_dir;
font_sz = params.font_sz;

for j = 1:length(sel_types)
    sel_type = sel_types{j};
    
    save_dir_sel_type = fullfile(save_dir, sel_type);
    if ~exist(save_dir_sel_type,'dir')
        mkdir(save_dir_sel_type);
    end
    
    for i = 1:length(data_types)
        data_type = data_types{i};
        
        save_dir_data_type = fullfile(save_dir_sel_type, data_type);
        if ~exist(save_dir_data_type,'dir')
            mkdir(save_dir_data_type);
        end
        
        cc_types = fieldnames(CC_rez.(sel_type).(data_type));
        
        for k = 1:length(cc_types)
            cc_type = cc_types{k};
            
            figure('units','normalized','outerposition',[0 0 1 1]);
            transp = 0.35;
            violin_width = 0.45;
            
            violinplot(CC_rez.(sel_type).(data_type).(cc_type), int_names, 'ShowMean', true, 'ViolinAlpha', transp, 'Width', violin_width, 'ShowData', false, 'ShowNotches', false);
            yline(0,'--','LineWidth',2,'Color', 'k','FontSize',font_sz);
            title(strjoin({sel_type, data_type,'CC', cc_type},' '));
            ylabel(['Correlation Coefficient']);
            set(findall(gca, 'Type', 'Line'),'LineWidth',1.25);
            set(gcf,'color','w');
            set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
            save_name = fullfile(save_dir_data_type, strjoin({sel_type, data_type, cc_type,'.svg'},'_'));
            saveas(gcf, save_name);
            close;
            
            %Make another violin plot with the individual points      
            figure('units','normalized','outerposition',[0 0 1 1]);     
            violinplot(CC_rez.(sel_type).(data_type).(cc_type), int_names, 'ShowMean', true, 'ViolinAlpha', transp, 'Width', violin_width, 'ShowData', true, 'ShowNotches', false);
            yline(0,'--','LineWidth',2,'Color', 'k','FontSize',font_sz);
            title(strjoin({sel_type, data_type,'CC', cc_type, 'ind points'},' '));
            ylabel(['Correlation Coefficient']);
            set(findall(gca, 'Type', 'Line'),'LineWidth',1.25);
            set(gcf,'color','w');
            set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
            save_name = fullfile(save_dir_data_type, strjoin({sel_type, data_type, cc_type,'ind_points','.svg'},'_'));
            saveas(gcf, save_name);
            close;
        end
        
    end
end