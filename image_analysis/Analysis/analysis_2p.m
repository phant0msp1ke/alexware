function Rez = analysis_2p(animal)
%% Define params
base_dir = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction';
tone_mat_dir = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction/toneData';
cell_type = cell(0);
cell_type{end+1} = 'EXC';
cell_type{end+1} = 'GAD'; cell_type{end+1} = 'GADother';  cell_type{end+1} = 'PV'; cell_type{end+1} = 'SOM'; 
n_types = length(cell_type);

%% Plots

%FRA
plot_ind = 0;
plot_subplots = 0;

%Frequency profile
plot_profile = 0;
plot_profile_all = 0;
plot_props = 0;

%BF var
plot_bf_var = 0;

%CC
plot_stim_CC = 0;
plot_pre_stim_CC = 0;

plot_CC_mat = 0;
plot_CC_violin = 0;
plot_CC_hist = 0;

%FRA Propoerties - CF, Q20, Q40, BW20, BW40, dB_th
get_fra_prop = 0;

%The isSelective vector of the FRAs is organised like this
%1 - ANOVA for frequency 
%2 - ANOVA for intensity
%3 - ANOVA for interaction

%% Get names and info
rez_dir = dir(fullfile(base_dir, animal, 'results'));
rez_dir = rez_dir(~ismember({rez_dir.name},{'.','..'}));
areas_name = {rez_dir.name};
areas_num = cell2mat(cellfun(@(x) str2num(x(end)), areas_name, 'UniformOutput',false))';
n_areas = length(areas_name);

%% Load the data
tone_data_name = fullfile(tone_mat_dir, strjoin({'toneData',animal,'full.mat'},'_'));
load(tone_data_name);
n_rois = length(toneDataMat);

%Get some key params and metrics 
area_vec =  reach(toneDataMat, 'area');
id_vec =  reach(toneDataMat, 'roiID');

%Get the seelction criteria vectors
is_resp_vec = reach(toneDataMat,'isResponsive'); %Logical vector of Responsive neurons
is_resp_vec = is_resp_vec(:); %Convert to column vector if not
is_sel_vec_temp = reach(toneDataMat,'isSelective'); %Logical vector of Selective neurons, where col1 is freq, col2 dB level, col3 freq-level interaction (vectorized)

%Define the different selection classes
is_sel_mat_temp = reshape(is_sel_vec_temp,3,n_rois)'; %Reshape the selectivity vector into a # ROI X 3 matrix (freq, level, freq-level)
is_sel_any_vec = any(is_sel_mat_temp, 2); %Get the neurons which are selective for ANYTHING
is_sel_paper_vec = any(is_sel_mat_temp(:, [1,3]), 2); %Get the neurons which are selective for FREQUENCY or FREQUENCY-LEVEL combo as per eLife paper
is_sel_level_vec = any(is_sel_mat_temp(:, 2), 2); %Get the neurons which are selective for LEVEL
is_resp_sel_vec = is_resp_vec | is_sel_any_vec; %Get the neurons which are Responsive or Selective for ANYTHING

%Get the pvals
is_sel_pval_temp = reach(toneDataMat, 'isSelectivePVal'); %Logical vector of the pval for Selective neurons, same as above
is_sel_pval_all_vec = reshape(is_sel_pval_temp,3,n_rois)';
is_sel_freq_pval_vec = is_sel_pval_all_vec(:,1); %This will be used for ordering the cell based on their ANOVA Freq pval

%BF and FRA shape 
BF_vec = reach(toneDataMat,'BF');
fra_shape_vec = reach(toneDataMat,'FRAshapeManual');

params.freqs = toneDataMat(1).FRAfrequencies/1000;
params.dB_lvls = toneDataMat(1).FRAlevels;

%% Compute the FRA properties for all Responsive or Selective neurons - CF, Q20 ,Q40, BW20, BW40, dB_th
if get_fra_prop
    params_temp.freqs = toneDataMat(1).FRAfrequencies/1000;
    params_temp.dB_lvls = flipud(toneDataMat(1).FRAlevels);
    
    for j = 1:n_rois
        fra = flipud(toneDataMat(j).FRA);
        base_rate = toneDataMat(j).Baseline_activity;
        fra_prop(j) = compute_fra_prop_2p(fra, base_rate, params_temp);
        
        toneDataMat(j).CF = fra_prop(j).cf;
        toneDataMat(j).Q20 = fra_prop(j).q20;
        toneDataMat(j).Q40 = fra_prop(j).q40;
        toneDataMat(j).BW20 = fra_prop(j).bw_20;
        toneDataMat(j).BW40 = fra_prop(j).bw_40;
        toneDataMat(j).dB_th = fra_prop(j).db_th;
    end
   
    save(tone_data_name, 'toneDataMat');
end

%% Loop over each area and plot the FRAs
plot_dir = fullfile(base_dir, animal, 'Plots');
roi_ix.GAD = zeros(1,n_rois);
roi_ix.GADother = zeros(1,n_rois);
roi_ix.PV = zeros(1,n_rois);
roi_ix.SOM = zeros(1,n_rois);
roi_ix.EXC = zeros(1,n_rois);
count.GAD = 0; count.GADother = 0; count.PV = 0; count.SOM = 0; count.EXC = 0; 
mean_fra_global.GAD = []; mean_fra_global.GADother = []; mean_fra_global.PV = [];
mean_fra_global.SOM = []; mean_fra_global.EXC = [];

sel_names = {'All','Responsive OR Selective', 'Selective paper'};
save_dir_all = fullfile(plot_dir, 'All');

for j = 1:n_areas 
    area = areas_name{j};
    area_n = areas_num(j);

    rez_name = fullfile(base_dir, animal, 'results', area, 'combined', 'rez.mat');
    load(rez_name);
    
    for i = 1:n_types

        %Select the data
        cell_cur = cell_type{i};
        ids = rez.(cell_cur).roi_match;
        count.(cell_cur) = count.(cell_cur) + length(ids);
        ix = ismember(area_vec, area_n) & ismember(id_vec, ids); %Get the index of the neurons which belong to this area and ROI ID based on their class
        pval = is_sel_freq_pval_vec(ix); %Get the pvals for FREQUENCY selectivity
        [pval, ix_pval] = sort(pval,'ascend'); %Sort from smallest to biggest pval
        
        %Get the FRAs
        temp_cell = cellfun(@flipud, {toneDataMat(ix).FRA}, 'UniformOutput', false)';
        fras.(area).(cell_cur) = cellfun(@(x) smoothMat2D(x), temp_cell, 'UniformOutput', false);
        fras.(area).(cell_cur) = fras.(area).(cell_cur)(ix_pval);
        mean_fra = cell2mat(cellfun(@mean , temp_cell, 'UniformOutput', false));
        mean_fra_global.(cell_cur) = [mean_fra_global.(cell_cur); mean_fra];
        
        %Set params
        params.is_resp = is_resp_vec(ix); params.is_resp = params.is_resp(ix_pval); %Get and order the Resposnive Neurons based on the p-value for FREQUENCY
        params.is_sel = is_sel_any_vec(ix); params.is_sel = params.is_sel(ix_pval); %Get and order the Selective ANY Neurons based on the p-value for FREQUENCY
        params.is_sel_paper = is_sel_paper_vec(ix); params.is_sel_paper = params.is_sel_paper(ix_pval); %Get and order the Selective eLife paper (freq or freq-level interaction) Neurons based on the p-value for FREQUENCY
        
        params.BF = BF_vec(ix); params.BF = params.BF(ix_pval)/1000;
        params.fr_shape = fra_shape_vec(ix); params.fr_shape  = params.fr_shape(ix_pval);
        params.ids = id_vec(ix); params.ids = params.ids(ix_pval);
        params.cell_type = cell_cur;
        save_dir = fullfile(plot_dir,area,cell_cur);
        
        %Get the FRA properties
        Q20 = [toneDataMat(ix).Q20]; Q40 = [toneDataMat(ix).Q40]; CF = [toneDataMat(ix).CF]; dB_th = [toneDataMat(ix).dB_th]; 
        Q20 = Q20(ix_pval); Q40 = Q40(ix_pval); CF = CF(ix_pval); dB_th = dB_th(ix_pval);
        
        %All of the FRA props for this area
        FRA_props.Q20.(area).all.(cell_cur) = Q20; FRA_props.Q40.(area).all.(cell_cur) = Q40;
        FRA_props.CF.(area).all.(cell_cur) = CF; FRA_props.dB_th.(area).all.(cell_cur) = dB_th;
        %Responsive or Selective FRA props for this area
        FRA_props.Q20.(area).resp_sel.(cell_cur) = Q20(params.is_sel | params.is_resp); FRA_props.Q40.(area).resp_sel.(cell_cur) = Q40(params.is_sel | params.is_resp);
        FRA_props.CF.(area).resp_sel.(cell_cur) = CF(params.is_sel | params.is_resp); FRA_props.dB_th.(area).resp_sel.(cell_cur) = dB_th(params.is_sel | params.is_resp);
        %Responsive FRA props for this area
        FRA_props.Q20.(area).resp.(cell_cur) = Q20(params.is_resp); FRA_props.Q40.(area).resp.(cell_cur) = Q40(params.is_resp);
        FRA_props.CF.(area).resp.(cell_cur) = CF(params.is_resp); FRA_props.dB_th.(area).resp.(cell_cur) = dB_th(params.is_resp);

        
        %Get the data out, removing the first entry which has silence
        
        %All data with stim presentations
        data.all.(cell_cur) = cellfun(@(x) x(2:end), {toneDataMat(ix).data}' ,'UniformOutput', false);
        data.all.(cell_cur) = data.all.(cell_cur)(ix_pval);
        params_sn.ids.all.(cell_cur) = params.ids';
        
        %All data with pre-stim activity
        data_pre.all.(cell_cur) = cellfun(@(x) x(2:end), {toneDataMat(ix).data_pre}' ,'UniformOutput', false);
        data_pre.all.(cell_cur) = data_pre.all.(cell_cur)(ix_pval);
        
        %Responsive or Selective stim presentations
        data.resp_sel.(cell_cur) = data.all.(cell_cur)(params.is_sel | params.is_resp);
        params_sn.ids.resp_sel.(cell_cur) = params.ids(params.is_sel | params.is_resp)';
        
        %Responsive or Selective pre-stim activity
        data_pre.resp_sel.(cell_cur) = data_pre.all.(cell_cur)(params.is_sel | params.is_resp);
        
        %Selective eLife paper stim presentations
        data.sel_paper.(cell_cur) = data.all.(cell_cur)(params.is_sel_paper);
        params_sn.ids.sel_paper.(cell_cur) = params.ids(params.is_sel_paper)';
        
        %Selective paper pre-stim activity
        data_pre.sel_paper.(cell_cur) = data_pre.all.(cell_cur)(params.is_sel_paper);
        
        
        if ~exist(save_dir, 'dir')
            mkdir(save_dir);
        end
        params.save_dir = save_dir;
        
        %Select the BFs based on the three different options for every area
        %and avery neuronal type
        BFs.(area).all.(cell_cur) = params.BF; %All of the BFs for this area
        BFs.(area).resp_sel.(cell_cur) = params.BF(params.is_sel |  params.is_resp); %Responsice or Selective Any BFs
        BFs.(area).resp.(cell_cur) = params.BF(params.is_resp); %Responsive or Selective Any BFs
        
        %Get the fra
        
        %Keep adding the indices of new cells of each type from every area
        roi_ix.(cell_cur) = roi_ix.(cell_cur) | ix;
        
        %Plot in fras
        if plot_ind
            plot_ind_fra(fras.(area).(cell_cur), params);
        end
        
        %Plot mean fras profile
        if plot_profile
            plot_fra_profile(mean_fra, params);
        end
        
    end
    border.GAD(j) = count.GAD; border.PV(j) = count.PV; border.SOM(j) = count.SOM; border.EXC(j) = count.EXC;
    
    %Compute and plot the signal and noise correlation for every area for
    %stim presentations
    stim_dir = fullfile(plot_dir,area,'Stim');
    if ~exist(stim_dir,'dir') 
        mkdir(stim_dir) 
    end
    params_sn.save_dir = stim_dir;
    params_sn.area_name = area;
    params_sn.sel_names = sel_names;
    params_sn.plot = plot_stim_CC;
    params_sn.data_type = 'stim';
    CC_int.stim.(area) = plot_sig_noise(data, params_sn);
    
    %Compute and plot the signal and noise correlation for every area for
    %pre-stim activity
    pre_stim_dir = fullfile(plot_dir,area,'PreStim');
    if ~exist(pre_stim_dir,'dir')
        mkdir(pre_stim_dir)
    end
    
    params_sn.save_dir = pre_stim_dir;
    params_sn.plot = plot_pre_stim_CC;
    params_sn.data_type = 'pre_stim';
    CC_int.pre_stim.(area) = plot_sig_noise(data_pre, params_sn);
    
    %Plot the BF variability
    if plot_bf_var
        params_bf.freqs = params.freqs;
        params_bf.save_dir = fullfile(plot_dir,area);
        params_bf.save_dir_all = save_dir_all;
        params_bf.area_name = area;
        params_bf.sel_names = sel_names;
        plot_BF_areas(BFs.(area), params_bf);
    end
    
end

%% Combine the correlations together and plot them
data_types = fieldnames(CC_int);
sel_types = fieldnames(CC_int.stim.(area));
cc_types = fieldnames(CC_int.stim.(area).(sel_types{1}));
inter_types = fieldnames(CC_int.stim.(area).(sel_types{1}).(cc_types{1}));

for j = 1:length(data_types)
    dt = data_types{j};
    
    for i = 1:length(sel_types)
        sel_type = sel_types{i};
        
        cc_types = fieldnames(CC_int.(dt).(area).(sel_types{1}));
        
        for k = 1:length(cc_types)
            cc_type = cc_types{k};
            
            for l = 1:length(inter_types)
                inter_type = inter_types{l};
                temp = [];
                
                for m = 1:length(areas_name)
                    area = areas_name{m};
                    temp = [temp; CC_int.(dt).(area).(sel_type).(cc_type).(inter_type)];
                end
                
                CC_rez.(sel_type).(dt).(cc_type).(inter_type) = temp;
            end
        end
        
    end
    
end

cc_save_dir = fullfile(save_dir_all,'CC');
if ~exist(cc_save_dir, 'dir')
    mkdir(cc_save_dir);
end

violin_save_dir = fullfile(cc_save_dir,'Violin');
if ~exist(violin_save_dir, 'dir')
    mkdir(violin_save_dir);
end

hist_save_dir = fullfile(cc_save_dir,'Histogram');
if ~exist(hist_save_dir, 'dir')
    mkdir(hist_save_dir);
end

mat_save_dir = fullfile(cc_save_dir,'Matrix');
if ~exist(mat_save_dir, 'dir')
    mkdir(mat_save_dir);
end


if plot_CC_mat
    %Plot the CC as Matrix
    params_cc.save_dir = mat_save_dir;
    plot_SN_matrix(CC_rez, params_cc);
end

if plot_CC_violin
    %Plot the CC as Violin plots
    params_cc.save_dir = violin_save_dir;
    plot_SN_violins(CC_rez, params_cc);
end

if plot_CC_hist
    %Plot the CC as Histogram plots
    params_cc.save_dir = hist_save_dir;
    plot_SN_histograms(CC_rez, params_cc);
end


%% Get the FRA data and plot them
params.areas_name = areas_name;
params.border = border;
save_dir = fullfile(plot_dir, 'All');

for k = 1:n_types
    
    cell_cur = cell_type{k};
    save_dir_full = fullfile(save_dir, cell_cur);
    if ~exist(save_dir_full,'dir')
        mkdir(save_dir_full);
    end
    
    params.save_dir = save_dir_full;
    params.cell_type = cell_cur;
    
    %This is the logical index for every cell type across all areas
    ix = roi_ix.(cell_cur);
    
    %Get the data based on the selection criteria and store for later use
    pval_freq = is_sel_freq_pval_vec(ix); %Get the pvals for FREQUENCY selectivity
    [pval_freq, ix_pval_freq] = sort(pval_freq); %Sort in ascending order
    %Responsive
    is_resp = is_resp_vec(ix);
    is_resp = is_resp(ix_pval_freq);
    %Selective ANY
    is_sel_any = is_sel_any_vec(ix);
    is_sel_any = is_sel_any(ix_pval_freq);
    %Selective any OR Responsive
    is_resp_sel = is_resp | is_sel_any;
    %Selective PAPER
    is_sel_paper = is_sel_paper_vec(ix);
    is_sel_paper = is_sel_paper(ix_pval_freq);
    %Selective LEVEL
    is_sel_level = is_sel_level_vec(ix);
    is_sel_level = is_sel_level(ix_pval_freq);
    
    %Store in fra_data
    fra_data.(cell_cur).pval_freq = pval_freq;
    fra_data.(cell_cur).is_resp = is_resp;
    fra_data.(cell_cur).is_sel_any = is_sel_any;
    fra_data.(cell_cur).is_resp_sel = is_resp_sel;
    fra_data.(cell_cur).is_sel_paper = is_sel_paper;
    fra_data.(cell_cur).is_sel_level = is_sel_level;
    
    %Get the actual FRA data
    fra_data.(cell_cur).raw = cellfun(@flipud, {toneDataMat(ix).FRA}, 'UniformOutput', false)';
    fra_data.(cell_cur).raw = fra_data.(cell_cur).raw(ix_pval_freq);
    fra_data.(cell_cur).smooth = cellfun(@(x) smoothMat2D(x), fra_data.(cell_cur).raw, 'UniformOutput', false);

    %FRA shape
    fra_data.(cell_cur).fra_shape = fra_shape_vec(ix);
    fra_data.(cell_cur).fra_shape = fra_data.(cell_cur).fra_shape(ix_pval_freq);
    
    %BF
    fra_data.(cell_cur).BF = BF_vec(ix);
    fra_data.(cell_cur).BF = fra_data.(cell_cur).BF(ix_pval_freq);
    
    %Plot the profile of the FRAs for all areas
    if plot_profile_all
        plot_fra_profile_all(mean_fra_global.(cell_cur), params);
    endhttps://www.mathworks.com/help/matlab/creating_plots/bar-chart-with-error-bars.html
    
    %Plot all the FRAs in subplots
    if plot_subplots
        plot_all_2p_fras(fra_data.(cell_cur).smooth, params);
    end
    
end


%% Responsive, Selective, FRA Shape Analysis
%The legend for the FRA shape is the following
%1 - Single
%2 - Double
%3,4 - Complex

for  k = 1:n_types
    
    cell_cur = cell_type{k};
    
    %Get the number of cells in this current class
    n_cell = length(fra_data.(cell_cur).BF);
    prop.Total.(cell_cur) = n_cell;
    
    %Get the proportion of Responsive OR Selective cells
    prop.is_resp_sel.(cell_cur) = sum(fra_data.(cell_cur).is_resp_sel);
    
    %Get the proportion of Single, Double, Complex for Responsive OR Selective cells
    temp = fra_data.(cell_cur).fra_shape(fra_data.(cell_cur).is_resp_sel);
    fra_shape.is_resp_sel.Single.(cell_cur) = sum(temp==1);
    fra_shape.is_resp_sel.Double.(cell_cur) = sum(temp==2);
    fra_shape.is_resp_sel.Complex.(cell_cur) = sum(ismember(temp, [3,4]));
    
    %Get the proportion of Responsive cells
    prop.is_resp.(cell_cur) = sum(fra_data.(cell_cur).is_resp);
    
    %Get the proportion of Single, Double, Complex for Responsive cells
    temp = fra_data.(cell_cur).fra_shape(fra_data.(cell_cur).is_resp);
    fra_shape.is_resp.Single.(cell_cur) = sum(temp==1);
    fra_shape.is_resp.Double.(cell_cur) = sum(temp==2);
    fra_shape.is_resp.Complex.(cell_cur) = sum(ismember(temp, [3,4]));
    
    
    %Get the proportion of selective cells Selective for Level
    prop.is_sel_level.(cell_cur) = sum(fra_data.(cell_cur).is_sel_level);
    
    %Get the proportion of Single, Double, Complex Selective for Level 
    temp = fra_data.(cell_cur).fra_shape(fra_data.(cell_cur).is_sel_level);
    fra_shape.is_sel_level.Single.(cell_cur) = sum(temp==1);
    fra_shape.is_sel_level.Double.(cell_cur) = sum(temp==2);
    fra_shape.is_sel_level.Complex.(cell_cur) = sum(ismember(temp, [3,4]));
    
    %Get the proportion of selective cells for Freqeuncy or
    %Freqeuncy-Level combination, the same as in the eLife paper
    prop.is_sel_paper.(cell_cur) = sum(fra_data.(cell_cur).is_sel_paper);
    
    %Get the proportion of Single, Double, Complex for eLife paper 
    temp = fra_data.(cell_cur).fra_shape(fra_data.(cell_cur).is_sel_paper);
    fra_shape.is_sel_paper.Single.(cell_cur) = sum(temp==1);
    fra_shape.is_sel_paper.Double.(cell_cur) = sum(temp==2);
    fra_shape.is_sel_paper.Complex.(cell_cur) = sum(ismember(temp, [3,4]));
    
end

%% Store the results in a structure
Rez.prop = prop;
Rez.BF = BFs;
Rez.fra_shape = fra_shape;
Rez.FRA_props = FRA_props;
Rez.CC = CC_rez;
Rez.freqs = params.freqs;

%% Plot the Responsive and Selective Proportions
if plot_props

    X = categorical({'Responsive OR Selective', 'Responsive','Selective Level', 'Selective Frequency'});
    X = reordercats(X,{'Responsive OR Selective', 'Responsive', 'Selective Level', 'Selective Frequency'});
    Y = [prop.is_resp_sel.EXC  prop.is_resp_sel.GAD prop.is_resp_sel.GADother  prop.is_resp_sel.PV  prop.is_resp_sel.SOM; ... %Responsive or Selective
        prop.is_resp.EXC  prop.is_resp.GAD prop.is_resp.GADother  prop.is_resp.PV  prop.is_resp.SOM; ... %Responsive
        prop.is_sel_level.EXC  prop.is_sel_level.GAD prop.is_sel_level.GADother  prop.is_sel_level.PV  prop.is_sel_level.SOM; ... %Selective Any
        prop.is_sel_paper.EXC  prop.is_sel_paper.GAD prop.is_sel_paper.GADother  prop.is_sel_paper.PV  prop.is_sel_paper.SOM]; %Selective Paper
    plot_percentages(X,Y);
    
    %% Plot the Single, Double, Complex Proportions for the different ways of selecting the cells
    prop_select{1} = 'resp_sel'; prop_select{2} = 'resp'; prop_select{3} = 'sel_any';  prop_select{4} = 'sel_paper';
    names = {'Responsive OR Selective','Responsive','Selective Any','Selective paper'};
    figure('units','normalized','outerposition',[0 0 1 1]);
    font_sz = 25;
    X = categorical({'Single','Double','Complex'});
    X = reordercats(X,{'Single','Double','Complex'});
    
    row = 2;
    col = 2;
    prop = 0.005;
    edgel = 0.07; edger = prop; edgeh = 0.08; edgeb = 0.08; space_h = 0.05; space_v = 0.11;
    [pos]=subplot_pos(row,col,edgel,edger,edgeh,edgeb,space_h,space_v);
    
    for j = 1:length(prop_select)
        
        property = prop_select{j};
        name = names{j};
        %Combine the data for plotting
        Y = [fra_shape.(property).single.EXC fra_shape.(property).single.GAD fra_shape.(property).single.PV fra_shape.(property).single.SOM; ... %Single
            fra_shape.(property).double.EXC fra_shape.(property).double.GAD fra_shape.(property).double.PV fra_shape.(property).double.SOM; ... %Double
            fra_shape.(property).complex.EXC fra_shape.(property).complex.GAD fra_shape.(property).complex.PV fra_shape.(property).complex.SOM]; %Complex
        
        subplot('position',pos{j});
        
        hb = bar(X,Y);
        
        hb(1).FaceColor = 'r';
        hb(2).FaceColor = 'b';
        hb(3).FaceColor = [0 0.6 0];
        hb(4).FaceColor = 'c';
        hb(5).FaceColor = 'm';
        if j == 2
            labels={'EXC';'GAD'; 'GADother';'PV';'SOM'};
            legend(labels,'location','northeast');
        end
        set(gcf,'color','w');
        
        if ismember(j,[1,3])
            ylabel('neurons(%)');
        end
        
        title(name);
        set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
        ylim([0 0.8])
    end
    
    save_name = fullfile(save_dir,['Percentage_FRA_shapes.svg']);
    saveas(gcf, save_name);
    close;
    
end