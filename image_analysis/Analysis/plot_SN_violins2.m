function plot_SN_violins2(CC_rez, params)


save_dir = params.save_dir;
title_txt = params.title_txt;
int_names = params.int_names;
colors = params.colors;
font_sz = params.font_sz;


figure('units','normalized','outerposition',[0 0 1 1]);
transp = 0.35;
violin_width = 0.40;

violins = violinplot(CC_rez, int_names, 'ShowMean', true, 'ViolinAlpha', transp, 'Width', violin_width, 'ShowData', false, 'ShowNotches', false);

for j = 1:length(colors)
    violins(j).ViolinColor = colors{j};
end

yline(0,'--','LineWidth',2,'Color', 'k','FontSize',font_sz);
title(title_txt);
ylabel(['Correlation Coefficient']);
set(findall(gca, 'Type', 'Line'),'LineWidth',1.25);
set(gcf,'color','w');
set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
save_name = fullfile(save_dir, [title_txt,'.svg']);
saveas(gcf, save_name);
close;

%Make another violin plot with the individual points
figure('units','normalized','outerposition',[0 0 1 1]);
violins = violinplot(CC_rez, int_names, 'ShowMean', true, 'ViolinAlpha', transp, 'Width', violin_width, 'ShowData', true, 'ShowNotches', false);

for j = 1:length(colors)
    violins(j).ViolinColor = colors{j};
end

yline(0,'--','LineWidth',2,'Color', 'k','FontSize',font_sz);
title([title_txt,' ind points']);
ylabel(['Correlation Coefficient']);
set(findall(gca, 'Type', 'Line'),'LineWidth',1.25);
set(gcf,'color','w');
set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
save_name = fullfile(save_dir, [title_txt,'ind_points.svg']);
saveas(gcf, save_name);
close;
