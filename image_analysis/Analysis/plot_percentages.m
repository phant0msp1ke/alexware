function plot_percentages(X, Y, params)
%This function plots the percentges of tuned neurons

font_sz = params.font_sz;
save_dir = params.save_dir;
colors = params.colors;
labels = params.labels;
title_txt = params.title_txt;
ylim_val = params.ylim_val;

figure('units','normalized','outerposition',[0 0 1 1]);
hb = bar(X,Y);

for j = 1:length(colors) 
    hb(j).FaceColor = colors{j};
end

legend(labels,'location','northeast');
set(gcf,'color','w');
set(gca,'TickDir','out'); 
ylabel('% neurons');
ylim([0 ylim_val]);
title(title_txt);
set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
save_name = fullfile(save_dir,[strrep(title_txt,' ', '_'),'.svg']);
saveas(gcf, save_name);
close;