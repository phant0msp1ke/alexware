function plot_sig_noise_time(data, params)

%% Define plotting parameters

sel_type = fieldnames(data);
cell_types = fieldnames(data.(sel_type{1}));

save_dir = params.save_dir;
area_name = params.area_name;
sel_names = params.sel_names;
font_sz = 25;


%% Analyse the data

for j = 1:length(sel_type)
    
    sel_curr = sel_type{j};
    sel_name = sel_names{j};
    
    %Get the indices of PV and SOM and find where they overlap with GAD
    ix_remove = ismember(params.ids.(sel_curr).GAD, [params.ids.(sel_curr).PV; params.ids.(sel_curr).SOM]);
    params.ids.(sel_curr).GAD(ix_remove) = []; %Remove the indices for duplicated GAD also from the params variable
    
    %Remove the PV and SOM ffrom the GAD data
    data.(sel_curr).GAD(ix_remove) = [];
    
    %Define borders of the different cell types
    n_exc = length(params.ids.(sel_curr).EXC); n_gad = length(params.ids.(sel_curr).GAD);
    n_pv = length(params.ids.(sel_curr).PV); n_som = length(params.ids.(sel_curr).SOM);
    border.GAD = n_exc; border.PV = border.GAD + n_gad; border.SOM = border.PV + n_pv;
    
    %Initialise variables
    temp_all = cell(0);
    temp_mean = cell(0);
    ix_rep = [];
    
    for k = 1:length(cell_types)
        cell_curr = cell_types{k};
        temp_mean = cellfun(@(x) cellfun(@(y) mean(y,1),x,'UniformOutput',false),data.(sel_curr).(cell_curr), 'UniformOutput', false); %Take the mean across trials for every cell and stimulus to get averaged time-series
        temp_all = cellfun(@(x) cellfun(@(y) y - mean(y,1),x,'UniformOutput',false),data.(sel_curr).(cell_curr), 'UniformOutput', false); %Subtract the mean time-series response across trials from every trial for every cell and stimulus
        %Get the data for Total CC
        data_total.(cell_curr) = cell2mat(cellfun(@(x) cell2mat(x), temp_mean, 'UniformOutput', false));
        %Get the data for noise CC
        data_noise.(cell_curr) = cellfun(@(x) cell2mat(x), temp_all, 'UniformOutput', false);
        data_noise.(cell_curr) = cell2mat(cellfun(@(x) x(:)', data_noise.(cell_curr), 'UniformOutput', false));
    end
    
    %Find the repeating entries between PV and SOM
    ids = [params.ids.(sel_curr).EXC; params.ids.(sel_curr).GAD; params.ids.(sel_curr).PV; params.ids.(sel_curr).SOM]; %Get all the indices together
    [~,~,ib]=unique(ids,'rows','stable');
    ix_rep = find(hist(ib,unique(ib))>1)';
    
    for i = 1:length(ix_rep)
        temp_ix = find(ids==ids(ix_rep(i)));
        ix_rep(i,2) = temp_ix(end);
    end
    
    data_total_CC = [data_total.EXC; data_total.GAD; data_total.PV; data_total.SOM]; %Gather the data from all cell types together in order for the Total CC
    data_noise_CC = [data_noise.EXC; data_noise.GAD; data_noise.PV; data_noise.SOM]; %Gather the data from all cell types together in order for the Noise CC
    
    %Total correlation
    CC.Total = corr(data_total_CC'); %Compute the correlation matrix for the Total CC
    CC.Total(logical(eye(size(CC.Total)))) = NaN; %Set the diagonal entries to NaN
    
    %Noise correlation
    CC.Noise = corr(data_noise_CC'); %Compute the correlation matrix for the Noise CC
    CC.Noise(logical(eye(size(CC.Noise)))) = NaN; %Set the diagonal entries to NaN
    
    %Signal correlation
    CC.Signal = CC.Total - CC.Noise; %Compute the difference to find the signal correlation
    
    cc_types = fieldnames(CC);
    
    %Set the reepating cells CC to NaN
    for m = 1:size(ix_rep,1)
        for c = 1:length(cc_types)
            CC.(cc_types{c})(ix_rep(m,1),ix_rep(m,2)) = NaN;
            CC.(cc_types{c})(ix_rep(m,2),ix_rep(m,1)) = NaN;
        end
    end
    
    
    %Plot the data
    for c = 1:length(cc_types)
        figure('units','normalized','outerposition',[0 0 1 1]);
        cc_type = cc_types{c};
        imagesc(CC.(cc_type));   
%         axis equal;
        colormap('redblue');
        colorbar;
        max_val = max(abs(CC.(cc_type)(:)));
        caxis([-max_val max_val]);
        names = fieldnames(border);
        
        for l = 1:length(names)
            name = names{l};
            yline(border.(name),'--',{name},'LabelHorizontalAlignment','left','LabelVerticalAlignment','middle','LineWidth',2,'Color', 'k','FontSize',font_sz);
            xline(border.(name),'--',{name},'LabelHorizontalAlignment','center','LineWidth',2,'Color', 'k','FontSize',font_sz);
        end
        
        title([cc_type,' Correlation ', sel_name,' ', area_name]);
        xlabel('ROI #');
        ylabel('ROI #');
        set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
        set(gcf,'color','w');
        save_name = fullfile(save_dir,strjoin({area_name,sel_name,'CC_Time_',cc_type,'.svg'},'_'));
        saveas(gcf,save_name);
        close;
    end
    

end