function plot_SN_matrix(CC_rez, params)

sel_types = fieldnames(CC_rez);
data_types = fieldnames(CC_rez.(sel_types{1}));

cell_names = {'EXC','GADother','PV','SOM'}; %The names of cell types

save_dir = params.save_dir;
font_sz = params.font_sz;

for j = 1:length(sel_types)
    sel_type = sel_types{j};
    
    save_dir_sel_type = fullfile(save_dir, sel_type);
    if ~exist(save_dir_sel_type,'dir')
        mkdir(save_dir_sel_type);
    end
    
    for i = 1:length(data_types)
        data_type = data_types{i};
        
        save_dir_data_type = fullfile(save_dir_sel_type, data_type);
        if ~exist(save_dir_data_type,'dir')
            mkdir(save_dir_data_type);
        end
        
        cc_types = fieldnames(CC_rez.(sel_type).(data_type));
        
        for k = 1:length(cc_types)
            cc_type = cc_types{k};
            
            %Plot a matrix of the interactions
            figure('units','normalized','outerposition',[0 0 1 1]);
            data = structfun(@nanmean, CC_rez.(sel_type).(data_type).(cc_type),'UniformOutput',false);
            CC_mat = zeros(4);
            CC_mat(1:4+1:end) = [data.EE, data.GG, data.PP, data.SS]; %Put the diagonal entries corresponding to the self-interactions
            CC_mat(2:4,1) = [data.EG; data.EP; data.ES]; CC_mat(1,2:4) = [data.EG; data.EP; data.ES]; %Put the EXC cross-interactions
            CC_mat(3:4,2) = [data.GP; data.GS]; CC_mat(2,3:4) = [data.GP; data.GS]; %Put the GAD cross-interactions
            CC_mat(4,3) = [data.PS]; CC_mat(3,4) = [data.PS]; %Put the PV cross-interactions
            
            max_val = max(abs(CC_mat(:)));
            imagesc(CC_mat);
            colormap('redblue');
            caxis([-max_val max_val]);
            colorbar;
            xticks([1:4]);
            xticklabels(cell_names);
            yticks([1:4]);
            yticklabels(cell_names);
            title(strjoin({sel_type, data_type,'CC', cc_type},' '));
            axis square;
            set(gcf,'color','w');
            set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
            save_name = fullfile(save_dir_data_type, strjoin({'CC_matrix',sel_type, data_type, cc_type,'.svg'},'_'));
            saveas(gcf, save_name);
            close;
            
        end
        
    end
end