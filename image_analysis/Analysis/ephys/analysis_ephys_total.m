%In this script I will combine the results from the two animals together
%and perform necessary plotting and further analysis. This will be based on
%the function plot_2p_fras which will run for each animal and store the
%output. I probably want some struct array to store all my different
%outputs.

%% Define params
animals = {'Cork', 'Dery', 'Dory', 'Kilkenny', 'PLP', 'Ronnie'};
save_dir_root = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction/Thesis_chapter';
font_sz = 30;
font_type = 'Liberation Sans';
n_animals = length(animals);

%Plotting selection
%Figure 1
plot_fig1 = 0;

%Figure 2
plot_fig2A = 0;
plot_fig2B = 0;

%Figure 4
plot_fig4 = 0;

%Figure 5
plot_fig5A = 0;
plot_fig5D = 0;

%% Get the data

for j = 1:n_animals
    animal = animals{j};
    tic;
    fprintf('== Processing animal: %s ==\n', animal);
    Rez.(animal) = analysis_ephys(animal);
    fprintf('== Done! This took %0.fs ==\n', toc);
end

%% Plotting setup
freqs = Rez.(animals{1}).freqs;
skip_f = 2;
n_freqs = numel(freqs);
y_labels = cell(0);
for jj = 1:skip_f:numel(freqs)
    y_labels{end+1} = num2str(freqs(jj),'%.1f');
end

%% Figure 1: Plot percentage of tuned neurons

%Fist combine the data together
sel_types = fieldnames(rmfield(Rez.(animal).prop, 'Total'));
cell_types = fieldnames(Rez.(animal).prop.(sel_types{1}));

for i = 1:length(sel_types)
    sel_type = sel_types{i};
    
    for k = 1:length(cell_types)
        cell_type = cell_types{k};
        
        for j = 1:n_animals
            %Get the number of cells in each selection category
            n_cell(j) = Rez.(animals{j}).prop.(sel_type).(cell_type);
         
            %Get the total number of cells in each class
            Total_cell(j) = Rez.(animals{1}).prop.Total.(cell_type);
            
        end
        
        %Get the percentages for plotting
        perc.(cell_type).(sel_type) = 100*mean(n_cell./Total_cell);
        
        %Get the numbers of cells in each class and total number for
        %Likelihood Ratio Test
        prop.(cell_type).(sel_type) = sum(n_cell);
        prop.(cell_type).Total = sum(Total_cell);
    end
    
end

%Compute the Likelihood Ratio Test with EXC and INH
prop_INH_all.EXC = prop.EXC; prop_INH_all.INH = prop.INH;
rez_INH_all = likelihood_ratio_test(prop_INH_all);


%Plot the data comparing EXC and all INH only
X = categorical({'Responsive OR Selective', 'Responsive','Selective Level', 'Selective Frequency'});
X = reordercats(X,{'Responsive OR Selective', 'Responsive', 'Selective Level', 'Selective Frequency'});
Y = [perc.EXC.is_resp_sel  perc.INH.is_resp_sel; ... %Responsive or Selective
    perc.EXC.is_resp  perc.INH.is_resp; ... %Responsive
    perc.EXC.is_sel_level  perc.INH.is_sel_level; ... %Selective LEVEL
    perc.EXC.is_sel_paper  perc.INH.is_sel_paper]; %Selective FREQUENCY

params.font_sz = 40;
save_dir = fullfile(save_dir_root, 'Figure_1', 'ephys');

if ~exist(save_dir, 'dir')
    mkdir(save_dir);
end

if plot_fig1
    
    %Define plotting params
    params.save_dir = save_dir;
    params.colors = {'r', 'b'};
    params.labels = {'EXC', 'INH'};
    params.title_txt = ['Percentage of tuned neurons by class'];
    params.ylim_val = 80;
    
    plot_percentages(X, Y, params);
    
    
end
%% Figure 2A: BF total distribtuions 

font_sz = 55;
cell_types = {'EXC', 'INH'};
median_BF.EXC = []; median_BF.INH = [];
cdf_BF.EXC = []; cdf_BF.INH = [];
sz = 40;
lw = 1.5;
lw2 = 4;

BF_all.EXC = []; BF_all.INH = [];

for j = 1:n_animals
    animal = animals{j};
    areas = fieldnames(Rez.(animal).BF);
    
    for i = 1:length(areas)
        area = areas{i};
        
        for k = 1:length(cell_types)
            cell_type = cell_types{k};
            
            %Get the data for this animal, area and cell type and compute the median
            temp_data = Rez.(animal).BF.(area).resp_sel.(cell_type);
            temp_data = temp_data(:); %Force a col vector
            
            %Store the results
            BF_all.(cell_type) = [BF_all.(cell_type); temp_data];

        end
    end
end

save_dir = fullfile(save_dir_root, 'Figure_2','ephys');
if ~exist(save_dir, 'dir')
    mkdir(save_dir);
end

%Perform a Wilcoxon test since distribution of BFs is not normal
pval.bf_all = ranksum(BF_all.EXC, BF_all.INH);

if plot_fig2A
    
    %Plot the distribution of BF as violins
    params_fra_props.title_txt = 'BF distribution across all neurons';
    params_fra_props.save_dir = save_dir;
    params_fra_props.ylabel_txt = 'BF (kHz)';
    params_fra_props.font_sz = font_sz;
    params_fra_props.plot_type = 'bf';
    params_fra_props.freqs = freqs;
    params_fra_props.skip_f = skip_f;
    params_fra_props.y_labels = y_labels;
    fra_props_violinplot(BF_all, params_fra_props);
    
end

%% Figure 4: Percentage of Single, Double and Complex Neurons and Likelihood Ratio Test

font_sz = 55;
fra_types = {'Single', 'Double', 'Complex'};
cell_types = {'EXC', 'INH'};

%Get the number of cells in each category across both animals
for j =1:length(cell_types)
    cell_type = cell_types{j};
    
    fra_shape.(cell_type).Total = 0;
    for i = 1:length(fra_types)
        fra_type = fra_types{i};
        
        fra_shape.(cell_type).(fra_type) = 0;
        for k = 1:n_animals
            animal = animals{k};
            fra_shape.(cell_type).(fra_type) = fra_shape.(cell_type).(fra_type) + Rez.(animal).fra_shape.is_resp_sel.(fra_type).(cell_type);
        end
        fra_shape.(cell_type).Total = fra_shape.(cell_type).Total + fra_shape.(cell_type).(fra_type);
    end
    
end

%Perform Likelihood Ratio Test
LR_test = likelihood_ratio_test(fra_shape);

%Plot bar graphs of the results
figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
X = categorical({'Single', 'Double','Complex'});
X = reordercats(X, {'Single', 'Double','Complex'});
EXC_total = fra_shape.EXC.Total; INH_total = fra_shape.INH.Total; 

Y = 100*[fra_shape.EXC.Single/EXC_total, fra_shape.INH.Single/INH_total; ... %Single neurons
    fra_shape.EXC.Double/EXC_total, fra_shape.INH.Double/INH_total; ... %Double neurons
    fra_shape.EXC.Complex/EXC_total, fra_shape.INH.Complex/INH_total]; %Complex neurons

if plot_fig4
    hb = bar(X,Y);
    hb(1).FaceColor = 'r';
    hb(2).FaceColor = 'b';
    labels={'EXC'; 'INH'};
    legend(labels,'location','northeast');
    set(gcf,'color','w');
    set(gca,'TickDir','out');
    ylabel('% neurons');
    title('Proportion of neurons by FRA class for EXC and INH');
    ylim([0 80]);
    set(gca,'FontName', font_type, 'FontSize', font_sz, 'FontWeight', 'Normal');
    hold on;
    
    save_dir = fullfile(save_dir_root, 'Figure_4','ephys');
    if ~exist(save_dir, 'dir')
        mkdir(save_dir);
    end
    
    save_name = fullfile(save_dir,['Percentage_FRA_shapes.svg']);
    saveas(gcf, save_name);
    close;
    
end

%% Figure 2B: FRA properties 

font_sz = 55;
Q10.EXC = []; Q30.EXC = []; CF.EXC = []; dB_th.EXC = []; 
Q10.INH = []; Q30.INH = []; CF.INH = []; dB_th.INH = []; 
cell_types = {'EXC', 'INH'};

for j = 1:n_animals
    animal = animals{j};
    areas = fieldnames(Rez.(animal).FRA_props.Q10);
    
    for i = 1:length(areas)
        area = areas{i};
        
        for k = 1:length(cell_types)
            cell_type = cell_types{k};
            
            Q10.(cell_type) = [Q10.(cell_type); Rez.(animal).FRA_props.Q10.(area).resp_sel.(cell_type)']; 
            Q30.(cell_type) = [Q30.(cell_type); Rez.(animal).FRA_props.Q30.(area).resp_sel.(cell_type)']; 
            CF.(cell_type) = [CF.(cell_type); Rez.(animal).FRA_props.CF.(area).resp_sel.(cell_type)']; 
            dB_th.(cell_type) = [dB_th.(cell_type); Rez.(animal).FRA_props.dB_th.(area).resp_sel.(cell_type)']; 
        end
        
    end
    
end

%Perform a Wilcoxon test since distribution of BFs is not normal for Q10
pval.Q10 = ranksum(Q10.EXC, Q10.INH);
pval.Q30 = ranksum(Q30.EXC, Q30.INH);
pval.CF = ranksum(CF.EXC, CF.INH);
pval.dB_th = ranksum(dB_th.EXC, dB_th.INH);

save_dir = fullfile(save_dir_root, 'Figure_2','ephys');
if ~exist(save_dir, 'dir')
    mkdir(save_dir);
end

if plot_fig2B
    
    params_fra_props.ylim_val = 10;
    %Plot the Q10 distribution as violins
    params_fra_props.plot_type = 'other';
    params_fra_props.title_txt = 'Q10 distribution across all neurons';
    params_fra_props.save_dir = save_dir;
    params_fra_props.ylabel_txt = 'Q10';
    params_fra_props.font_sz = font_sz; 
    fra_props_violinplot(Q10, params_fra_props)
    
    %Plot the Q30 distribution as violins
    params_fra_props.title_txt = 'Q30 distribution across all neurons';
    params_fra_props.ylabel_txt = 'Q30';  
    fra_props_violinplot(Q30, params_fra_props);
    
    params_fra_props =  rmfield(params_fra_props, 'ylim_val');
    %Plot the dB_th distribution as violins
    params_fra_props.title_txt = 'Threshold distribution across all neurons';
    params_fra_props.ylabel_txt = 'Threshold (dB SPL)';    
    fra_props_violinplot(dB_th, params_fra_props);
    
    %Plot the CF distribution as violins
    params_fra_props.plot_type = 'bf';
    params_fra_props.title_txt = 'CF distribution across all neurons';
    params_fra_props.ylabel_txt = 'CF (kHz)';
    fra_props_violinplot(CF, params_fra_props);
    
end

%% Figure 5: Signal and Noise correlation
%Combine the results from the ferrets together


sel_types = fieldnames(Rez.(animals{1}).CC); %Get the types of selections
data_types = fieldnames(Rez.(animals{1}).CC.all); %Get the data types
int_types = fieldnames(Rez.(animals{1}).CC.all.stim.Noise); %Get the types of interactions

for j = 1:length(sel_types)
    sel_type = sel_types{j};
    
    for i = 1:length(data_types)
        data_type = data_types{i};
        cc_types = fieldnames(Rez.(animals{1}).CC.(sel_type).(data_type));
        
        for k = 1:length(cc_types)
            cc_type = cc_types{k};
            int_types = fieldnames(Rez.(animals{1}).CC.(sel_type).(data_type).(cc_type));
            
            for l = 1:length(int_types)
                int_type = int_types{l};
                CC.(sel_type).(data_type).(cc_type).(int_type) = [];
                
                for m = 1:n_animals 
                    animal = animals{m};
                    CC.(sel_type).(data_type).(cc_type).(int_type) = [CC.(sel_type).(data_type).(cc_type).(int_type); Rez.(animal).CC.(sel_type).(data_type).(cc_type).(int_type)];
                end
                
            end
        end
        
    end
end

save_dir = fullfile(save_dir_root, 'Figure_5','ephys');
if ~exist(save_dir, 'dir')
    mkdir(save_dir);
end

%% Figure 5A: SC and NC Total Violin plots, KW test
display_results = 'on';
selected_data_NC = 'resp_sel'; % 'sel_paper' OR 'resp_sel' OR 'all'
selected_data_SC = 'resp_sel'; % 'sel_paper' OR 'resp_sel' OR 'all'
method_SC = 'SignalNelken'; % 'SignalPaper' or 'SignalNelken'

%Combine the data for NC
CC_noise.EE = CC.(selected_data_NC).stim.Noise.EE; %EXC - EXC all
CC_noise.EI = CC.(selected_data_NC).stim.Noise.EI; %EXC - INH all
CC_noise.II = CC.(selected_data_NC).stim.Noise.II; %INH all - INH all

%Combine the data for SC
CC_signal.EE = CC.(selected_data_SC).stim.(method_SC).EE; %EXC - EXC all
CC_signal.EI = CC.(selected_data_SC).stim.(method_SC).EI; %EXC - INH all
CC_signal.II = CC.(selected_data_SC).stim.(method_SC).II; %INH all - INH all

%Perform Kruskal-Wallis (KW) test to compare the distributions for NC
data_noise = [CC_noise.EE; CC_noise.EI; CC_noise.II]; %Make a vector with the data for KW test
group_noise = [repmat({'EE'},length(CC_noise.EE),1); repmat({'EI'},length(CC_noise.EI),1); repmat({'II'},length(CC_noise.II),1)]; %Make a vector with the group labels

[pval.NC.global.single, pval.NC.global.table, pval.NC.global.stats] = kruskalwallis(data_noise, group_noise, display_results);
pval.NC.global.multiple = multcompare(pval.NC.global.stats,'estimate', 'kruskalwallis', 'display', display_results);


%Perform Kruskal-Wallis (KW) test to compare the distributions for SC
data_signal = [CC_signal.EE; CC_signal.EI; CC_signal.II]; %Make a vector with the data for KW test
group_signal = [repmat({'EE'},length(CC_signal.EE),1); repmat({'EI'},length(CC_signal.EI),1); repmat({'II'},length(CC_signal.II),1)]; %Make a vector with the group labels

[pval.SC.global.single, pval.SC.global.table, pval.SC.global.stats] = kruskalwallis(data_signal, group_signal, display_results);
pval.SC.global.multiple  = multcompare(pval.SC.global.stats, 'estimate', 'kruskalwallis', 'display', display_results);


save_dir2 = fullfile(save_dir, 'A');
if ~exist(save_dir2, 'dir')
    mkdir(save_dir2);
end
params.save_dir = save_dir2;
params.font_sz = 50;

if plot_fig5A
    
    %Plot the violins for NC
    params.title_txt = ['Noise correlations for ', selected_data_NC, ' neurons during stim period'];
    params.int_names = {'EXC-EXC', 'EXC-INH', 'INH-INH'};
    params.colors = {[1 0 0], [1 0 1], [0 0 1]};
    plot_SN_violins2(CC_noise, params);
    
    %Plot the violins for SC
    params.title_txt = ['Signal correlations for ', selected_data_SC ,' ',method_SC,' neurons during stim period'];
    params.int_names = {'EXC-EXC', 'EXC-INH', 'INH-INH'};
    plot_SN_violins2(CC_signal, params);
    
end

%% Figure 5D: Correlation between SC and NC
marker_size = 15;
lw = 3;
font_sz = 20;
txt_sz = 15;
selection_CC = 'all'; % 'sel_paper' OR 'resp_sel' OR 'all'
method_SC = 'SignalNelken'; % 'SignalPaper' or 'SignalNelken'
corr_type = 'Pearson';

%Get the necessary data
CC_noise = CC.(selection_CC).stim.Noise;
CC_signal = CC.(selection_CC).stim.(method_SC);

interactions = fieldnames(CC_noise);

for j = 1:length(interactions)
    
    interaction = interactions{j};
    [rho.(interaction), pval.NC_SC_corr.(interaction)] = corr(CC_noise.(interaction), CC_signal.(interaction), 'rows', 'complete', 'type', corr_type);
    
end

%Make a matrix of the correlation
CC_mat = zeros(2);   
CC_mat(1,1) = rho.EE; CC_mat(2,2) = rho.II;  %Put the diagonal entries corresponding to the self-interactions
CC_mat(1,2) = rho.EI; CC_mat(2,1) = rho.EI; %Put the EXC-INH cross-interactions


if plot_fig5D
    
    %Plot the correlations between SC and NC
    figure('units','normalized','outerposition',[0 0 1 1]);
    ixs = [1,3,4]; %The index of the plots
    row = 2;
    col = 2;
    edgel = 0.06; edger = 0.015; edgeh = 0.015; edgeb = 0.08; space_h = 0.05; space_v = 0.06;
    [pos]=subplot_pos(row,col,edgel,edger,edgeh,edgeb,space_h,space_v);
    
    for j = 1:length(ixs)
        
        interaction = interactions{j};
        ix = ixs(j);
        subplot('position',pos{ix});
        
        scatter(CC_noise.(interaction), CC_signal.(interaction), marker_size, 'filled', 'k');
        h = lsline;
        h.Color = 'r';
        h.LineWidth = lw;
        set(gcf,'color','w');
        
        if ix==3
            xlabel('Noise correlation');
            ylabel('Signal correlation');
        end
        
        if pval.NC_SC_corr.(interaction)>0.01
            txt = ['r = ',num2str(rho.(interaction),'%0.2f'), ', p = ', num2str(pval.NC_SC_corr.(interaction), '%.2f')];
        else
            txt = ['r = ',num2str(rho.(interaction),'%0.2f'), ', p = ', num2str(pval.NC_SC_corr.(interaction), '%.1e')];
        end
        
        text(0.45, 0.99, txt, 'Units', 'Normalized', 'FontSize', txt_sz);
        
        set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
    end
    
    save_dir5 = fullfile(save_dir, 'D');
    if ~exist(save_dir5, 'dir')
        mkdir(save_dir5);
    end
    
    file_name = ['SC_NC_correlation_',selection_CC,'_', method_SC];
    save_name = fullfile(save_dir5,[file_name,'.svg']);
    saveas(gcf, save_name);
    close;
    
    %Plot the matrix of correlations
    cell_labels = {'EXC','INH'};
    figure('units','normalized','outerposition',[0 0 1 1]);
    max_val = max(abs(CC_mat(:)));
    imagesc(CC_mat);
    colormap('redblue');
    caxis([-max_val max_val]);
    colorbar;
    xticks([1:2]);
    xticklabels(cell_labels);
    yticks([1:2]);
    yticklabels(cell_labels);
    title(['Correlation between Signal and Noise ', selection_CC, ' stim_', method_SC]);
    axis square;
    set(gcf,'color','w');
    set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
    file_name = ['SC_NC_correlation_matrix_',selection_CC,'_', method_SC];
    save_name = fullfile(save_dir5,[file_name,'.svg']);
    saveas(gcf, save_name);
    close;
    
end