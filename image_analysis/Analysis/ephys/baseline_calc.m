function Baseline_activity = baseline_calc(toneDataMat)
%This function will calculate the baseline activity for each neuron

data_pre = cellfun(@(x) x(2:end), {toneDataMat.data_pre}' ,'UniformOutput', false); %Remove the first entry
data_pre = cellfun(@(x) cell2mat(x), data_pre ,'UniformOutput', false); %Convert the cells to arrays
Baseline_activity = cellfun(@(x) nanmean(x(:)), data_pre ,'UniformOutput', false); %Take the mean across all pre-stim periods
