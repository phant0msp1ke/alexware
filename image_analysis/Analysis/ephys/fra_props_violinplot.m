function fra_props_violinplot(data, params)

%Get params out
title_txt = params.title_txt;
save_dir = params.save_dir;
ylabel_txt = params.ylabel_txt;
font_sz = params.font_sz;
plot_type = params.plot_type;

%Plot the dB_th distribution as violins
figure('units','normalized','outerposition',[0 0 1 1]);
transp = 0.25;
violin_width = 0.3;
cell_types = {'EXC', 'INH'};
violins = violinplot(data, cell_types, 'ShowMean', true, 'ViolinAlpha', transp, 'Width', violin_width);
violins(1).ViolinColor = [1, 0, 0];
violins(2).ViolinColor = [0, 0, 1];

switch plot_type
    case 'bf'
        freqs = params.freqs;
        n_freqs = length(freqs);
        skip_f = params.skip_f;
        y_labels = params.y_labels;
        set(gca, 'YScale', 'log');
        set(gca,'Ytick',[]);
        set(gca,'YminorTick','off');
        yticks(freqs(1:skip_f:n_freqs));
        yticklabels(y_labels);
        ylabel(ylabel_txt);
    case 'other'
end

title(title_txt);
set(gcf,'color','w');
set(gca,'TickDir','out')
set(gca,'FontSize',font_sz,'FontWeight','Normal');
if isfield(params, 'ylim_val')
    ylim([0 params.ylim_val]);
end

full_name = fullfile(save_dir,[title_txt,'.svg']);
saveas(gcf, full_name);
close all;