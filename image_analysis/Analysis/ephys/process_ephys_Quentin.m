%This scirpt will add the neuronal type (EXC or INH) to the ephys data from
%Quentin based on the clustering I did

ephys_data_folder = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction/ephys_data/ephys_tones_original'; %This is the data from Quentin
clust_info_depth_folder = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction/ephys_data/clust_infos_depth'; %This is the clust_infos used by Quentin which have the same order as the analysed data
clust_info_folder = '/media/alex/5FC39EAD5A6AA312/EI_tonotopy/Data'; %This is the folder which has the clust_infos with the clustering EXC/INH analysis
save_dir = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction/ephys_data/ephys_tones'; %The dir where we will save the processed toneDataMat
inh_flag = 2; %INH - 2, EXC - 1

% Get a list of all files and folders in this folder
files = dir(clust_info_depth_folder);
files = files(~ismember({files.name},{'.','..'}));

% Get a logical vector that tells which is a directory
dirFlags = [files.isdir];

% Extract the animal names
animals = {files(dirFlags).name};

for j = 1:length(animals)
    
    animal = animals{j};
    fprintf('== Processing animal:%s ==\n', animal);
    pens = dir(fullfile(clust_info_depth_folder,animal)); %Get the pens for this animal
    pens = pens(~ismember({pens.name},{'.','..'}));
    n_pens = length(pens);
    
    toneDataMat_temp = cell(n_pens,1); %Initialise the var to store the results
    
    for i = 1:n_pens
        
        pen = pens(i).name;
        stim = dir(fullfile(clust_info_folder,animal,pen)); %Get the pens for this animal
        stim = stim(~ismember({stim.name},{'.','..','config_dir'}));
        stim = stim(1).name;
        
        depth = load(fullfile(clust_info_depth_folder,animal,pen,'clust_info')); %Load the clust_info used by Quentin
        rez = load(fullfile(clust_info_folder,animal,pen,stim,'clust_info')); %Load the clust_info used for EXC/INH clustering
        
        all_ids = depth.clust_info.clust_id; %Get all the IDs used by quentin, both Good and MUA
        n_clust = length(all_ids); %Find the number of clusters
        good_ids = depth.clust_info.good_units(:,1); %Get the IDs of the good units only used by Quentin
        rez_good_ids = rez.clust_info.clustering.idx(:,1); %Get the IDs of all the good untis used for clustering
        neuron_class = rez.clust_info.clustering.idx(:,2); %The neuronal class asigned by clustering: 1-EXC, 2-INH
        
        
        ix_keep = ismember(rez_good_ids, good_ids); %The indices of the clusters to keep
        keep_ids = rez_good_ids(ix_keep); %Keep only the IDs which are present in both clust_infos
        keep_neuron_class = neuron_class(ix_keep); %Keep also the corresponding neuronal class labels
        
        class_label = NaN(n_clust, 1); %Initialize a vector of NaNs for the class labels to be appended to the toneDataMat structure
        ix_match = ismember(all_ids, keep_ids); %Get the indices of clusters to keep relative to all ids
        class_label(ix_match) = keep_neuron_class; %Assign the class labels to those clusters
        
        %Load the toneDataMat
        load(fullfile(ephys_data_folder, strjoin({'ToneDataMatephys', animal, pen},'_')));
        toneDataMat_temp{i} = allDat.Onset.toneDataMat;
        
        %Load the manual curated FRA shapes and remove NaNs
        load(fullfile(ephys_data_folder, strjoin({animal,pen,'Onset','manualPeaks'},'_'))); %newPeaks are the manually curated FRA shapes, Peaks are the automatic ones 
        manualPeaks = newPeaks;
        manualPeaks(isnan(manualPeaks)) = Peaks(isnan(manualPeaks)); %newPeaks has NaNs so we fill those with the automatic Peaks
        
        %Assign the IDs, class labels, FRA shapes and inh_flag to the toneDataMat
        %structure
        ids = num2cell(all_ids);
        labels = num2cell(class_label);
        manualPeaks = num2cell(manualPeaks);
        [toneDataMat_temp{i}.clust_id] = ids{:};
        [toneDataMat_temp{i}.cell_labels] = labels{:};
        [toneDataMat_temp{i}.FRAshapeManual] = manualPeaks{:};

        
        %Make the isSelective vector for all 3 criteria
        pval = reshape([toneDataMat_temp{i}.isSelectivePVal],3,n_clust)';
        ix_pval = pval<0.05; %Find the entries smaller than alpha = 0.05
        
        %Make frequency and level labels for the data and data_pre
        data_stimID = [];
        dB_lvl = flipud(toneDataMat_temp{i}(1).FRA_level);
        freq = toneDataMat_temp{i}(1).FRA_freq(:);
        n_db = length(dB_lvl);
        n_freq = length(freq);
        data_stimID = repmat(freq, n_db, 1); %Make the freqeuncy labels
        %Make the levels labels
        lvl = [];
        for l = 1:n_db
            lvl = [lvl; dB_lvl(l)*ones(n_freq,1)];
        end
        data_stimID = [data_stimID,lvl];
        data_stimID = [[0, 0]; data_stimID]; %Also add the random entry
        
        %Apply the necessary changes to every cluster
        for k = 1:n_clust       
            toneDataMat_temp{i}(k).isSelective = ix_pval(k,:); %Write the p-value selection
            toneDataMat_temp{i}(k).FRA = flipud(toneDataMat_temp{i}(k).FRA); %Flip the FRA so it is low-to-high dB level
            toneDataMat_temp{i}(k).FRA_level = flipud(toneDataMat_temp{i}(k).FRA_level); %Flip the labels too to keep track
            
            temp_stim = rot90(toneDataMat_temp{i}(k).Stim, -1); %Rotate the FRA stim data clockwise such that smaller freqs and dB levels are first, in order to match the 2p data
            temp_prestim = rot90(toneDataMat_temp{i}(k).StimPre, -1); %Rotate the FRA pre-stim data clockwise such that smaller freqs and dB levels are first, in order to match the 2p data
            
            n_reps = max(max(cellfun(@(x) length(x), temp_stim))); %Find the number of reps        
            mask_stim = cellfun(@(x) length(x)<n_reps, temp_stim); %Make a logical mask for the stimuli where reps are missing
            mask_prestim = cellfun(@(x) length(x)<n_reps, temp_prestim); %Make a logical mask for the pre-stimuli where reps are missing
            
            temp_stim(mask_stim) = cellfun(@(x) [x;NaN],  temp_stim(mask_stim), 'uniform', 0); %Add NaNs where reps are missing
            temp_prestim(mask_prestim) = cellfun(@(x) [x;NaN],  temp_prestim(mask_prestim), 'uniform', 0); %Add NaNs where reps are missing
            
            toneDataMat_temp{i}(k).data = [{1}, temp_stim(:)']; %Unroll as a vector and store under name consistent with 2p data + add a random silence entry to match the 2p data
            toneDataMat_temp{i}(k).data_pre = [{1}, temp_prestim(:)']; %Unroll as a vector and store under name consistent with 2p data + add a random silence entry to match the 2p data
            
            toneDataMat_temp{i}(k).animal = animal; %Save the animal name
            toneDataMat_temp{i}(k).area = str2double(pen(2:3)); %Save the penetration name as number
            
            toneDataMat_temp{i}(k).data_stimID = data_stimID; %Add the ID of the data
        end
        
        toneDataMat_temp{i} = rmfield(toneDataMat_temp{i}, {'Stim', 'StimPre'}); %Remove the extrafields which have the right data but wrong names
        
        %Calculate and assign the Baseline activitiy
        Baseline_activity = baseline_calc(toneDataMat_temp{i});        
        [toneDataMat_temp{i}.Baseline_activity] = Baseline_activity{:};
        
    end
    
    toneDataMat = [toneDataMat_temp{:}]; %Concatenate the results from all the pens together
    toneDataMat(1).inh_flag = inh_flag; %Save the inhibitory flag
    
    %Save the data
    save(fullfile(save_dir, strjoin({'toneData', animal, 'full'},'_')), 'toneDataMat');
end
