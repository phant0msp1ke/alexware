function CC_int = plot_sig_noise_ephys(data, params)

%% Define plotting parameters

sel_type = fieldnames(data);
cell_types = fieldnames(data.(sel_type{1}));

save_dir = params.save_dir;
area_name = params.area_name;
sel_names = params.sel_names;
plot_on = params.plot;
data_type = params.data_type;
font_sz = 25;

colors = {'b'};
%% Analyse the data
    
for j = 1:length(sel_type)
    
    sel_curr = sel_type{j};
    sel_name = sel_names{j};
    save_dir_full = fullfile(save_dir,sel_curr);
    
    if ~exist(save_dir_full,'dir')
        mkdir(save_dir_full)
    end
    
    %Define borders of the different cell types
    n_exc = length(params.ids.(sel_curr).EXC); n_inh = length(params.ids.(sel_curr).INH);
    border.INH = n_exc + 0.5; 
    
    %Initialise variables
    temp_all = cell(0);
    temp_mean = cell(0);
    ix_rep = [];
    data_nelken = cell(0);
    
    for k = 1:length(cell_types)
        cell_curr = cell_types{k};
        temp_all = cellfun(@(x) cellfun(@(y) nanmean(y,2),x,'UniformOutput',false),data.(sel_curr).(cell_curr), 'UniformOutput', false); %Take the mean across time for every trial for every cell
        temp_all = cellfun(@cell2mat, temp_all, 'UniformOutput',false); %Convert the data to a matrix for each ROI which is Trials X Stimuli
        temp_mean = cellfun(@nanmean, temp_all, 'UniformOutput',false); %Compute the mean across trials to get 1 X Stimuli
        
        data_total.(cell_curr) = cell2mat(temp_mean); %Store the data necessary to compute the Total correlation of dimensions nROIS X Stimuli
        data_noise.(cell_curr) = cellfun(@(x) x - nanmean(x), temp_all, 'UniformOutput',false); %Remove the mean response for each stimulus for every trial
        data_noise.(cell_curr)  = cellfun(@(x) x(:)', data_noise.(cell_curr), 'UniformOutput',false); %Vectorize the responses in each cell
        data_noise.(cell_curr) = cell2mat(data_noise.(cell_curr)); %Convert to matrix of dimensions nROIS X (Stimuli*Reps)
        
        %Fitzpatrick NC
        %         data_noise_fitz.(cell_curr) = cellfun(@(x) (x - nanmean(x))./nanstd(x), temp_all, 'UniformOutput',false); %Z-score (remove mean, divide by std) the data to compute NC like Fitzpatrick paper
        %         data_noise_fitz.(cell_curr)  = cellfun(@(x) x(:)', data_noise_fitz.(cell_curr), 'UniformOutput',false); %Vectorize the responses in each cell
        %         data_noise_fitz.(cell_curr) = cell2mat(data_noise_fitz.(cell_curr)); %Convert to matrix of dimensions nROIS X (Stimuli*Reps)
        
        %Nelken Signal correlation data
        data_nelken = [data_nelken; temp_all]; %Store the cells in the order EXC, INH
    end
    
    %Combine the data together
    data_total_CC = [data_total.EXC; data_total.INH]; %Gather the data from all cell types together in order for the Total CC
    data_noise_CC = [data_noise.EXC; data_noise.INH]; %Gather the data from all cell types together in order for the Noise CC
    %     data_noise_fitz_CC = [data_noise_fitz.EXC; data_noise_fitz.INH]; %Gather the data from all cell types together in order for the Noise CC
    
    try
        %Noise correlation
        CC.Noise = corr(data_noise_CC', 'rows', 'complete'); %Compute the correlation matrix for the Noise CC
        CC.Noise(logical(eye(size(CC.Noise)))) = NaN; %Set the diagonal entries to NaN
        
        %Noise correlation Fitzpatrick
        %     CC.Noise_Fitzpatrick = corr(data_noise_fitz_CC', 'rows','complete'); %Compute the correlation matrix for the Noise CC
        %     CC.Noise_Fitzpatrick(logical(eye(size(CC.Noise_Fitzpatrick)))) = NaN; %Set the diagonal entries to NaN
        
        %Compute the Signal Correlations if using the stim period, otherwise
        %not (pre-stim has no signal)
        
        if strcmp(data_type, 'stim')
            %Total correlation
            CC_Total = corr(data_total_CC', 'rows', 'complete'); %Compute the correlation matrix for the Total CC
            CC_Total(logical(eye(size(CC_Total)))) = NaN; %Set the diagonal entries to NaN
            
            %Signal correlation according to eLife paper
            CC.SignalPaper = CC_Total - CC.Noise; %Compute the difference to find the signal correlation
            
            %Signal correlation according to the Nelken paper
            CC.SignalNelken = compute_SC_corrected_fast(data_nelken);
            CC.SignalNelken(logical(eye(size(CC.SignalNelken)))) = NaN; %Set the diagonal entries to NaN
        end
        
    catch
        
        CC.Noise = NaN;
        CC.SignalPaper = NaN;
        CC.SignalNelken = NaN;
    end
    
    cc_types = fieldnames(CC);
    
    %Collect all the different interactions, there are 3 posibilities:
    %E-E, E-I
    %I-I
    
    for c = 1:length(cc_types)
        cc_type = cc_types{c};
        try
        %EXC interactions
        CC_int_EE = CC.(cc_type)(1:n_exc,1:n_exc); %Get the part from CC matrix corresponding to EXC neurons only
        CC_int_EE = CC_int_EE(logical(tril(CC_int_EE,-1))); %Get only the lower triangular part w/o diagonal
        CC_int.(sel_curr).(cc_type).EE = CC_int_EE(:);
        
        CC_int_E_all = CC.(cc_type)(n_exc+1:end,1:n_exc); %Get the part from CC matrix corresponding to the interaction of EXC with GADother,PV and SOM
        CC_int_EI = CC_int_E_all(1:n_inh,:); %Get the interaction EXC-INH
        
        CC_int.(sel_curr).(cc_type).EI = CC_int_EI(:);

        
        %INH interactions
        CC_int_II = CC.(cc_type)(n_exc+1:n_exc+n_inh, n_exc+1:n_exc+n_inh); %Get the part from CC matrix corresponding to GADother neurons only
        CC_int_II = CC_int_II(logical(tril(CC_int_II,-1))); %Get only the lower triangular part w/o diagonal
        CC_int.(sel_curr).(cc_type).II = CC_int_II;
        
        catch
            CC_int.(sel_curr).(cc_type).EE = NaN;
            CC_int.(sel_curr).(cc_type).EI = NaN;
            CC_int.(sel_curr).(cc_type).II = NaN;
            
        end
        
    end
    
    if plot_on
        try
            %Plot the data
            for c = 1:length(cc_types)
                figure('units','normalized','outerposition',[0 0 1 1]);
                cc_type = cc_types{c};
                imagesc(CC.(cc_type));
                colormap('redblue');
                colorbar;
                max_val = max(abs(CC.(cc_type)(:)));
                caxis([-max_val max_val]);
                names = fieldnames(border);
                
                for l = 1:length(names)
                    name = names{l};
                    yline(border.(name),'--',{name},'LabelHorizontalAlignment','left','LabelVerticalAlignment','middle','LineWidth',2,'Color', colors{l},'FontSize',font_sz);
                    xline(border.(name),'--',{name},'LabelHorizontalAlignment','center','LineWidth',2,'Color', colors{l},'FontSize',font_sz);
                end
                
                title([cc_type,' Correlation ', sel_name,' ', area_name]);
                xlabel('ROI #');
                ylabel('ROI #');
                set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
                set(gcf,'color','w');
                axis square;
                save_name = fullfile(save_dir_full,strjoin({area_name,sel_name,'CC',cc_type,'.svg'},'_'));
                saveas(gcf,save_name);
                close;
            end
        catch
            close;
        end
    end

end