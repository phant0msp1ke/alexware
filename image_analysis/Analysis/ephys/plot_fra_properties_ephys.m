function plot_fra_properties_ephys(fra_data, fra_props, params)

%% Define plotting parameters
font_sz = 16;
row = 4;
col = 3;
per = 0.005;
edgel = 0.05; edger = per; edgeh = per; edgeb = 0.08; space_h = 0.015; space_v = 0.015;
[pos]=subplot_pos(row,col,edgel,edger,edgeh,edgeb,space_h,space_v);
num_clust = numel(fra_data);
freqs = params.freqs;
num_freqs = numel(freqs);
skip_f = 2;
x_labels = cell(0);
for jj = 1:skip_f:numel(freqs)
    x_labels{end+1} = num2str(freqs(jj),'%.1f');
end
dB_lvls = flipud(params.dB_lvls);
num_dB_lvls = numel(dB_lvls);
save_dir = params.save_dir;

%% Plot the correct FRAs
clust_spacing = row*col - 1;
num_groups = ceil(num_clust/(clust_spacing+1));
last_spacing = num_clust - (num_groups-1)*(clust_spacing+1) - 1;
first_clust_ix = [1:clust_spacing+1:(num_groups)*(clust_spacing+1)];

cl = 0;
for group = 1:num_groups
    
    first_clust = first_clust_ix(group);
    last_clust = first_clust + clust_spacing;
    
    if group == num_groups
        last_clust = first_clust + last_spacing;
    end
    num_subplots = numel(first_clust:last_clust);
    plot_ix = [first_clust:last_clust];
    
    figure('units','normalized','outerposition',[0 0 1 1]);
    for ii = 1:num_subplots
        cl = cl+1;
        subplot('position',pos{ii});
        imagesc(fra_data{cl});
        colormap('fake_parula');
        if ii == (row - 1)*col + 1
            xticks([1:skip_f:num_freqs]);
            xticklabels(x_labels);
            yticks(1:num_dB_lvls);
            y_labels = string(dB_lvls);
            yticklabels(y_labels);
            set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
            xlabel('Frequency (kHz)','FontSize',font_sz+10,'FontWeight','normal');
            ylabel('Sound Level (dB)','FontSize',font_sz+10,'FontWeight','normal');
        elseif ii > (row - 1)*col + 1 && ii <= row*col
            xticks([1:skip_f:num_freqs]);
            xticklabels(x_labels);
            set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
            xlabel('Frequency (kHz)','FontSize',font_sz+10,'FontWeight','normal');
            set(gca,'ytick',[]);
        else
            set(gca,'ytick',[]);
            set(gca,'xtick',[]);
        end
        %Plot the threshold, Q10, Q30 and CF lines
        hold on;
        line(fra_props.f_plot_ix{cl}, fra_props.lvl_plot_ix{cl},'LineWidth',2,'Color', 'w');
        if ~isempty(fra_props.q10(cl))
            line(fra_props.q10_f_ix{cl}, fra_props.q10_lvl_ix{cl},'LineWidth',2,'Color', 'k');
        end
        if ~isempty(fra_props.q30(cl))
            line(fra_props.q30_f_ix{cl}, fra_props.q30_lvl_ix{cl},'LineWidth',2,'Color', 'k');
        end
        
        if ~isempty(fra_props.cf(cl))
            xline(fra_props.cf_ix(cl), '-', {[num2str(fra_props.cf(cl),'%.1f'),'kHz']},'LabelVerticalAlignment','bottom','LineWidth',2,'Color', 'w', 'FontSize',font_sz);
        end
    end
    if exist('save_dir','var')
        file_name = ['FRAs_props_Clusters_batch',num2str(group),];
        save_name = fullfile(save_dir,[file_name,'.svg']);
        saveas(gcf, save_name);
        close;
    end
end