function Rez = analysis_ephys(animal)
%% Define params
base_dir = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction/ephys_data/animals';
tone_mat_dir = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction/ephys_data/ephys_tones';
cell_types = cell(0);
cell_types{end+1} = 'EXC';
cell_types{end+1} = 'INH'; 
cell_flag.EXC = 1; cell_flag.INH = 2; 
n_types = length(cell_types);
range_start_um = 0; %Where L1-L2/3 border is
range_end_um = -400; %Where L2/3 finishes
%% Plots

%FRA
plot_ind = 0;
plot_subplots = 0;

%Frequency profile
plot_profile = 0;
plot_profile_all = 0;
plot_props = 0;

%BF var
plot_bf_var = 0;

%CC
plot_stim_CC = 0;
plot_pre_stim_CC = 0;

plot_CC_mat = 0;
plot_CC_violin = 0;
plot_CC_hist = 0;

%FRA Propoerties - CF, Q10, Q30, BW10, BW30, dB_th
get_fra_prop = 1;
plot_fra_properties = 0;

%The isSelective vector of the FRAs is organised like this
%1 - ANOVA for frequency 
%2 - ANOVA for intensity
%3 - ANOVA for interaction


%% Load the data
tone_data_name = fullfile(tone_mat_dir, strjoin({'toneData',animal,'full.mat'},'_'));
load(tone_data_name);
n_clust = length(toneDataMat);

%Get some key params and metrics 
area_vec =  reach(toneDataMat, 'area');
clust_id_vec =  reach(toneDataMat, 'clust_id');

%Get the seelction criteria vectors
is_resp_vec = reach(toneDataMat,'isResponsive'); %Logical vector of Responsive neurons
is_resp_vec = is_resp_vec(:); %Convert to column vector if not
is_sel_vec_temp = reach(toneDataMat,'isSelective'); %Logical vector of Selective neurons, where col1 is freq, col2 dB level, col3 freq-level interaction (vectorized)

%Define the different selection classes
is_sel_mat_temp = reshape(is_sel_vec_temp,3,n_clust)'; %Reshape the selectivity vector into a # ROI X 3 matrix (freq, level, freq-level)
is_sel_any_vec = any(is_sel_mat_temp, 2); %Get the neurons which are selective for ANYTHING
is_sel_paper_vec = any(is_sel_mat_temp(:, [1,3]), 2); %Get the neurons which are selective for FREQUENCY or FREQUENCY-LEVEL combo as per eLife paper
is_sel_level_vec = any(is_sel_mat_temp(:, 2), 2); %Get the neurons which are selective for LEVEL
is_resp_sel_vec = is_resp_vec | is_sel_any_vec; %Get the neurons which are Responsive or Selective for ANYTHING

%Get the pvals
is_sel_pval_temp = reach(toneDataMat, 'isSelectivePVal'); %Logical vector of the pval for Selective neurons, same as above
is_sel_pval_all_vec = reshape(is_sel_pval_temp,3,n_clust)';
is_sel_freq_pval_vec = is_sel_pval_all_vec(:,1); %This will be used for ordering the cell based on their ANOVA Freq pval

%BF and FRA shape 
BF_vec = reach(toneDataMat,'BF');
fra_shape_vec = reach(toneDataMat,'FRAshapeManual');

params.freqs = toneDataMat(1).FRA_freq;
params.dB_lvls = toneDataMat(1).FRA_level;

%% Get names and info
areas_num = unique([toneDataMat.area]);
n_areas = length(areas_num);
for k = 1:n_areas
    areas_name{k} = ['area', num2str(areas_num(k),'%02.f')];
end

%% Compute the FRA properties for all Responsive or Selective neurons - CF, Q20 ,Q40, BW20, BW40, dB_th
if get_fra_prop
    params_temp.freqs = toneDataMat(1).FRA_freq;
    params_temp.dB_lvls = flipud(toneDataMat(1).FRA_level);
    
    for j = 1:n_clust
        fra = flipud(toneDataMat(j).FRA);
        base_rate = toneDataMat(j).Baseline_activity;
        fra_prop(j) = compute_fra_prop_ephys(fra, base_rate, params_temp);
        
        toneDataMat(j).CF = fra_prop(j).cf;
        toneDataMat(j).Q10 = fra_prop(j).q10;
        toneDataMat(j).Q30 = fra_prop(j).q30;
        toneDataMat(j).BW10 = fra_prop(j).bw_10;
        toneDataMat(j).BW30 = fra_prop(j).bw_30;
        toneDataMat(j).dB_th = fra_prop(j).db_th;
        toneDataMat(j).q10_f_ix = fra_prop(j).q10_f_ix;
        toneDataMat(j).q10_lvl_ix = fra_prop(j).q10_lvl_ix;
        toneDataMat(j).q30_f_ix = fra_prop(j).q30_f_ix;
        toneDataMat(j).q30_lvl_ix = fra_prop(j).q30_lvl_ix;
        toneDataMat(j).cf_ix = fra_prop(j).cf_ix;
        toneDataMat(j).f_plot_ix = fra_prop(j).f_plot_ix;
        toneDataMat(j).lvl_plot_ix = fra_prop(j).lvl_plot_ix;
    end
   
    save(tone_data_name, 'toneDataMat');
end

%% Loop over each area and plot the FRAs
plot_dir = fullfile(base_dir, animal, 'Plots');
roi_ix.EXC = zeros(1,n_clust);
roi_ix.INH = zeros(1,n_clust);
count.EXC = 0; count.INH = 0;  
mean_fra_global.EXC = []; mean_fra_global.INH = [];

sel_names = {'All','Responsive OR Selective', 'Selective paper'};
save_dir_all = fullfile(plot_dir, 'All');

ix_range = [toneDataMat.correctedDepth] > range_end_um & [toneDataMat.correctedDepth] < range_start_um; %Select only the cells which are between 0 and -400 um corrected depth i.e. those in L2/3 
ix_good = [toneDataMat.Quality]==1; %Select only the good units

for j = 1:n_areas
    area = areas_name{j};
    area_n = areas_num(j);

    for i = 1:n_types

        %Select the data
        cell_type = cell_types{i};
        ix_cell_type = [toneDataMat.cell_labels] == cell_flag.(cell_type); %Get the indices corresponding to the current cell type
        ix_area = ismember(area_vec, area_n); %Get the indices for the current area
        ix = ix_range & ix_good & ix_cell_type & ix_area; %Find the cells which are in range, good, in this area and cell type
        count.(cell_type) = count.(cell_type) + sum(ix); %Keep track of number of neurons in each area
        
        pval = is_sel_freq_pval_vec(ix); %Get the pvals for FREQUENCY selectivity
        [pval, ix_pval] = sort(pval,'ascend'); %Sort from smallest to biggest pval
        
        %Get the FRAs
        temp_cell = cellfun(@flipud, {toneDataMat(ix).FRA}, 'UniformOutput', false)';
        fras.(area).(cell_type) = cellfun(@(x) smoothMat2D(x), temp_cell, 'UniformOutput', false);
        fras.(area).(cell_type) = fras.(area).(cell_type)(ix_pval);
        mean_fra = cell2mat(cellfun(@mean , temp_cell, 'UniformOutput', false));
        mean_fra_global.(cell_type) = [mean_fra_global.(cell_type); mean_fra];
        
        %Set params
        params.is_resp = is_resp_vec(ix); params.is_resp = logical(params.is_resp(ix_pval)); %Get and order the Resposnive Neurons based on the p-value for FREQUENCY
        params.is_sel = is_sel_any_vec(ix); params.is_sel = params.is_sel(ix_pval); %Get and order the Selective ANY Neurons based on the p-value for FREQUENCY
        params.is_sel_paper = is_sel_paper_vec(ix); params.is_sel_paper = params.is_sel_paper(ix_pval); %Get and order the Selective eLife paper (freq or freq-level interaction) Neurons based on the p-value for FREQUENCY
        
        params.BF = BF_vec(ix); params.BF = params.BF(ix_pval);
        params.fr_shape = fra_shape_vec(ix); params.fr_shape  = params.fr_shape(ix_pval);
        params.ids = clust_id_vec(ix); params.ids = params.ids(ix_pval);
        params.cell_type = cell_type;
        save_dir = fullfile(plot_dir,area,cell_type);
        
        %Get the FRA properties
        Q10 = [toneDataMat(ix).Q10]; Q30 = [toneDataMat(ix).Q30]; CF = [toneDataMat(ix).CF]; dB_th = [toneDataMat(ix).dB_th]; 
        Q10 = Q10(ix_pval); Q30 = Q30(ix_pval); CF = CF(ix_pval); dB_th = dB_th(ix_pval);
        
        %All of the FRA props for this area
        FRA_props.Q10.(area).all.(cell_type) = Q10; FRA_props.Q30.(area).all.(cell_type) = Q30;
        FRA_props.CF.(area).all.(cell_type) = CF; FRA_props.dB_th.(area).all.(cell_type) = dB_th;
        %Responsive or Selective FRA props for this area
        FRA_props.Q10.(area).resp_sel.(cell_type) = Q10(params.is_sel | params.is_resp); FRA_props.Q30.(area).resp_sel.(cell_type) = Q30(params.is_sel | params.is_resp);
        FRA_props.CF.(area).resp_sel.(cell_type) = CF(params.is_sel | params.is_resp); FRA_props.dB_th.(area).resp_sel.(cell_type) = dB_th(params.is_sel | params.is_resp);
        %Responsive FRA props for this area
        FRA_props.Q10.(area).resp.(cell_type) = Q10(params.is_resp); FRA_props.Q30.(area).resp.(cell_type) = Q30(params.is_resp);
        FRA_props.CF.(area).resp.(cell_type) = CF(params.is_resp); FRA_props.dB_th.(area).resp.(cell_type) = dB_th(params.is_resp);

        
        %Get the data out, removing the first entry which has silence
        
        %All data with stim presentations
        data.all.(cell_type) = cellfun(@(x) x(2:end), {toneDataMat(ix).data}' ,'UniformOutput', false);
        data.all.(cell_type) = data.all.(cell_type)(ix_pval);
        params_sn.ids.all.(cell_type) = params.ids';
        
        %All data with pre-stim activity
        data_pre.all.(cell_type) = cellfun(@(x) x(2:end), {toneDataMat(ix).data_pre}' ,'UniformOutput', false);
        data_pre.all.(cell_type) = data_pre.all.(cell_type)(ix_pval);
        
        %Responsive or Selective stim presentations
        data.resp_sel.(cell_type) = data.all.(cell_type)(params.is_sel | params.is_resp);
        params_sn.ids.resp_sel.(cell_type) = params.ids(params.is_sel | params.is_resp)';
        
        %Responsive or Selective pre-stim activity
        data_pre.resp_sel.(cell_type) = data_pre.all.(cell_type)(params.is_sel | params.is_resp);
        
        %Selective eLife paper stim presentations
        data.sel_paper.(cell_type) = data.all.(cell_type)(params.is_sel_paper);
        params_sn.ids.sel_paper.(cell_type) = params.ids(params.is_sel_paper)';
        
        %Selective paper pre-stim activity
        data_pre.sel_paper.(cell_type) = data_pre.all.(cell_type)(params.is_sel_paper);
        
        
        if ~exist(save_dir, 'dir')
            mkdir(save_dir);
        end
        params.save_dir = save_dir;
        
        %Select the BFs based on the three different options for every area
        %and avery neuronal type
        BFs.(area).all.(cell_type) = params.BF; %All of the BFs for this area
        BFs.(area).resp_sel.(cell_type) = params.BF(params.is_sel |  params.is_resp); %Responsice or Selective Any BFs
        BFs.(area).resp.(cell_type) = params.BF(params.is_resp); %Responsive or Selective Any BFs
        
        %Get the fra
        
        %Keep adding the indices of new cells of each type from every area
        roi_ix.(cell_type) = roi_ix.(cell_type) | ix;
        
        %Plot indvidual fras
        if plot_ind
            plot_ind_fra(fras.(area).(cell_type), params);
        end
        
        %Plot mean fras profile
        if plot_profile
            plot_fra_profile(mean_fra, params);
        end
        
    end
    border.EXC(j) = count.EXC; border.INH(j) = count.INH; 
    
    %Compute and plot the signal and noise correlation for every area for
    %stim presentations
    stim_dir = fullfile(plot_dir,area,'Stim');
    if ~exist(stim_dir,'dir') 
        mkdir(stim_dir) 
    end
    params_sn.save_dir = stim_dir;
    params_sn.area_name = area;
    params_sn.sel_names = sel_names;
    params_sn.plot = plot_stim_CC;
    params_sn.data_type = 'stim';
    try
        CC_int.stim.(area) = plot_sig_noise_ephys(data, params_sn);
    catch
        CC_int.stim.(area) = NaN;
    end
    
    %Compute and plot the signal and noise correlation for every area for
    %pre-stim activity
    pre_stim_dir = fullfile(plot_dir,area,'PreStim');
    if ~exist(pre_stim_dir,'dir')
        mkdir(pre_stim_dir)
    end
    
    params_sn.save_dir = pre_stim_dir;
    params_sn.plot = plot_pre_stim_CC;
    params_sn.data_type = 'pre_stim';
    try
        CC_int.pre_stim.(area) = plot_sig_noise_ephys(data_pre, params_sn);
    catch
        CC_int.pre_stim.(area) = NaN;
    end
    
    %Plot the BF variability
    if plot_bf_var
        params_bf.analysis_type = 'ephys';
        params_bf.freqs = params.freqs;
        params_bf.save_dir = fullfile(plot_dir,area);
        params_bf.save_dir_all = save_dir_all;
        params_bf.area_name = area;
        params_bf.sel_names = sel_names;
        plot_BF_areas(BFs.(area), params_bf);
    end
    
end

%% Get the FRA data and plot them
params.areas_name = areas_name;
params.border = border;
save_dir = fullfile(plot_dir, 'All');

for k = 1:n_types
    
    cell_type = cell_types{k};
    save_dir_full = fullfile(save_dir, cell_type);
    if ~exist(save_dir_full,'dir')
        mkdir(save_dir_full);
    end
    
    params.save_dir = save_dir_full;
    params.cell_type = cell_type;
    
    %This is the logical index for every cell type across all areas
    ix = roi_ix.(cell_type);
    
    %Get the data based on the selection criteria and store for later use
    pval_freq = is_sel_freq_pval_vec(ix); %Get the pvals for FREQUENCY selectivity
    [pval_freq, ix_pval_freq] = sort(pval_freq); %Sort in ascending order
    %Responsive
    is_resp = is_resp_vec(ix);
    is_resp = is_resp(ix_pval_freq);
    %Selective ANY
    is_sel_any = is_sel_any_vec(ix);
    is_sel_any = is_sel_any(ix_pval_freq);
    %Selective any OR Responsive
    is_resp_sel = is_resp | is_sel_any;
    %Selective PAPER
    is_sel_paper = is_sel_paper_vec(ix);
    is_sel_paper = is_sel_paper(ix_pval_freq);
    %Selective LEVEL
    is_sel_level = is_sel_level_vec(ix);
    is_sel_level = is_sel_level(ix_pval_freq);
    
    %Store in fra_data
    fra_data.(cell_type).pval_freq = pval_freq;
    fra_data.(cell_type).is_resp = logical(is_resp);
    fra_data.(cell_type).is_sel_any = is_sel_any;
    fra_data.(cell_type).is_resp_sel = is_resp_sel;
    fra_data.(cell_type).is_sel_paper = is_sel_paper;
    fra_data.(cell_type).is_sel_level = is_sel_level;
    
    %Store in fra_props_plot
    fra_props_plot.(cell_type).pval_freq = pval_freq;
    fra_props_plot.(cell_type).is_resp = is_resp;
    fra_props_plot.(cell_type).is_sel_any = is_sel_any;
    fra_props_plot.(cell_type).is_resp_sel = is_resp_sel;
    fra_props_plot.(cell_type).is_sel_paper = is_sel_paper;
    fra_props_plot.(cell_type).is_sel_level = is_sel_level;
    
    %Get the actual FRA data
    fra_data.(cell_type).raw = cellfun(@flipud, {toneDataMat(ix).FRA}, 'UniformOutput', false)';
    fra_data.(cell_type).raw = fra_data.(cell_type).raw(ix_pval_freq);
    fra_data.(cell_type).smooth = cellfun(@(x) smoothMat2D(x), fra_data.(cell_type).raw, 'UniformOutput', false);
    
    %Get the actual FRA properties
    fra_props_plot.(cell_type).cf = [toneDataMat(ix).CF]; fra_props_plot.(cell_type).cf = fra_props_plot.(cell_type).cf(ix_pval_freq);
    fra_props_plot.(cell_type).dB_th = [toneDataMat(ix).dB_th]; fra_props_plot.(cell_type).dB_th = fra_props_plot.(cell_type).dB_th(ix_pval_freq);
    fra_props_plot.(cell_type).q10 = [toneDataMat(ix).Q10]; fra_props_plot.(cell_type).q10 = fra_props_plot.(cell_type).q10(ix_pval_freq); 
    fra_props_plot.(cell_type).q30 = [toneDataMat(ix).Q30]; fra_props_plot.(cell_type).q30 = fra_props_plot.(cell_type).q30(ix_pval_freq); 
    
    fra_props_plot.(cell_type).q10_f_ix = {toneDataMat(ix).q10_f_ix};  fra_props_plot.(cell_type).q10_f_ix = fra_props_plot.(cell_type).q10_f_ix(ix_pval_freq);
    fra_props_plot.(cell_type).q10_lvl_ix = {toneDataMat(ix).q10_lvl_ix}; fra_props_plot.(cell_type).q10_lvl_ix = fra_props_plot.(cell_type).q10_lvl_ix(ix_pval_freq);
    fra_props_plot.(cell_type).q30_f_ix = {toneDataMat(ix).q30_f_ix}; fra_props_plot.(cell_type).q30_f_ix = fra_props_plot.(cell_type).q30_f_ix(ix_pval_freq);
    fra_props_plot.(cell_type).q30_lvl_ix = {toneDataMat(ix).q30_lvl_ix}; fra_props_plot.(cell_type).q30_lvl_ix = fra_props_plot.(cell_type).q30_lvl_ix(ix_pval_freq);
    fra_props_plot.(cell_type).cf_ix = [toneDataMat(ix).cf_ix]; fra_props_plot.(cell_type).cf_ix = fra_props_plot.(cell_type).cf_ix(ix_pval_freq);
    fra_props_plot.(cell_type).f_plot_ix = {toneDataMat(ix).f_plot_ix}; 
    fra_props_plot.(cell_type).lvl_plot_ix = {toneDataMat(ix).lvl_plot_ix}; fra_props_plot.(cell_type).lvl_plot_ix = fra_props_plot.(cell_type).lvl_plot_ix(ix_pval_freq);

    %FRA shape
    fra_data.(cell_type).fra_shape = fra_shape_vec(ix);
    fra_data.(cell_type).fra_shape = fra_data.(cell_type).fra_shape(ix_pval_freq);
    
    %BF
    fra_data.(cell_type).BF = BF_vec(ix);
    fra_data.(cell_type).BF = fra_data.(cell_type).BF(ix_pval_freq);
    
    %Plot the profile of the FRAs for all areas
    if plot_profile_all
        plot_fra_profile_all(mean_fra_global.(cell_type), params);
    end
    
    %Plot all the FRAs in subplots
    if plot_subplots
        plot_all_2p_fras(fra_data.(cell_type).smooth, params);
    end
    
    %Plot the FRA properties
    if plot_fra_properties
        plot_fra_properties_ephys(fra_data.(cell_type).smooth, fra_props_plot.(cell_type), params);
    end
    
end

%% Responsive, Selective, FRA Shape Analysis
%The legend for the FRA shape is the following
%1 - Single
%2 - Double
%3,4 - Complex

for  k = 1:n_types
    
    cell_type = cell_types{k};
    
    %Get the number of cells in this current class
    n_cell = length(fra_data.(cell_type).BF);
    prop.Total.(cell_type) = n_cell;
    
    %Get the proportion of Responsive OR Selective cells
    prop.is_resp_sel.(cell_type) = sum(fra_data.(cell_type).is_resp_sel);
    perc.is_resp_sel.(cell_type) = 100*(prop.is_resp_sel.(cell_type)/n_cell);
    
    %Get the proportion of Single, Double, Complex for Responsive OR Selective cells
    temp = fra_data.(cell_type).fra_shape(fra_data.(cell_type).is_resp_sel);
    fra_shape.is_resp_sel.Single.(cell_type) = sum(temp==1);
    fra_shape.is_resp_sel.Double.(cell_type) = sum(temp==2);
    fra_shape.is_resp_sel.Complex.(cell_type) = sum(ismember(temp, [3,4]));
    
    %Get the proportion of Responsive cells
    prop.is_resp.(cell_type) = sum(fra_data.(cell_type).is_resp);
    perc.is_resp.(cell_type) = 100*(prop.is_resp.(cell_type)/n_cell);
    
    %Get the proportion of Single, Double, Complex for Responsive cells
    temp = fra_data.(cell_type).fra_shape(fra_data.(cell_type).is_resp);
    fra_shape.is_resp.Single.(cell_type) = sum(temp==1);
    fra_shape.is_resp.Double.(cell_type) = sum(temp==2);
    fra_shape.is_resp.Complex.(cell_type) = sum(ismember(temp, [3,4]));
    
    
    %Get the proportion of selective cells Selective for Level
    prop.is_sel_level.(cell_type) = sum(fra_data.(cell_type).is_sel_level);
    perc.is_sel_level.(cell_type) = 100*(prop.is_sel_level.(cell_type)/n_cell);
    
    %Get the proportion of Single, Double, Complex Selective for Level 
    temp = fra_data.(cell_type).fra_shape(fra_data.(cell_type).is_sel_level);
    fra_shape.is_sel_level.Single.(cell_type) = sum(temp==1);
    fra_shape.is_sel_level.Double.(cell_type) = sum(temp==2);
    fra_shape.is_sel_level.Complex.(cell_type) = sum(ismember(temp, [3,4]));
    
    %Get the proportion of selective cells for Freqeuncy or
    %Freqeuncy-Level combination, the same as in the eLife paper
    prop.is_sel_paper.(cell_type) = sum(fra_data.(cell_type).is_sel_paper);
    perc.is_sel_paper.(cell_type) = 100*(prop.is_sel_paper.(cell_type)/n_cell);
    
    %Get the proportion of Single, Double, Complex for eLife paper 
    temp = fra_data.(cell_type).fra_shape(fra_data.(cell_type).is_sel_paper);
    fra_shape.is_sel_paper.Single.(cell_type) = sum(temp==1);
    fra_shape.is_sel_paper.Double.(cell_type) = sum(temp==2);
    fra_shape.is_sel_paper.Complex.(cell_type) = sum(ismember(temp, [3,4]));
    
end

%% Plot the Responsive and Selective Proportions
if plot_props
    
    X = categorical({'Responsive OR Selective', 'Responsive','Selective Level', 'Selective Frequency'});
    X = reordercats(X,{'Responsive OR Selective', 'Responsive', 'Selective Level', 'Selective Frequency'});
    Y = [perc.is_resp_sel.EXC  perc.is_resp_sel.INH; ... %Responsive or Selective
        perc.is_resp.EXC  perc.is_resp.INH; ... %Responsive
        perc.is_sel_level.EXC  perc.is_sel_level.INH; ... %Selective Any
        perc.is_sel_paper.EXC  perc.is_sel_paper.INH]; %Selective Paper
    params.font_sz = 30;
    params.save_dir = save_dir;
    params.colors = {'r','b'};
    params.labels = {'EXC','INH'};
    params.title_txt = 'Percentage of tuned neurons by class';
    params.ylim_val = 90;
    plot_percentages(X,Y,params);
    
    %% Plot the Single, Double, Complex Proportions for the different ways of selecting the cells
    prop_select{1} = 'is_resp_sel'; prop_select{2} = 'is_resp'; prop_select{3} = 'is_sel_level';  prop_select{4} = 'is_sel_paper';
    names = {'Responsive OR Selective','Responsive','Selective level','Selective paper'};
    figure('units','normalized','outerposition',[0 0 1 1]);
    font_sz = 25;
    X = categorical({'Single','Double','Complex'});
    X = reordercats(X,{'Single','Double','Complex'});
    
    row = 2;
    col = 2;
    per = 0.005;
    edgel = 0.07; edger = per; edgeh = 0.08; edgeb = 0.08; space_h = 0.05; space_v = 0.11;
    [pos]=subplot_pos(row,col,edgel,edger,edgeh,edgeb,space_h,space_v);
    
    for j = 1:length(prop_select)
        
        property = prop_select{j};
        name = names{j};
        %Combine the data for plotting
        total_exc = fra_shape.(property).Single.EXC + fra_shape.(property).Double.EXC + fra_shape.(property).Complex.EXC;
        total_inh = fra_shape.(property).Single.INH + fra_shape.(property).Double.INH + fra_shape.(property).Complex.INH;
        
        Y = [100*(fra_shape.(property).Single.EXC/total_exc) 100*(fra_shape.(property).Single.INH/total_inh); ... %Single
            100*(fra_shape.(property).Double.EXC/total_exc) 100*(fra_shape.(property).Double.INH/total_inh); ... %Double
            100*(fra_shape.(property).Complex.EXC/total_exc) 100*(fra_shape.(property).Complex.INH/total_inh)]; %Complex
        
        subplot('position',pos{j});
        
        hb = bar(X,Y);
        
        hb(1).FaceColor = 'r';
        hb(2).FaceColor = 'b';
        if j == 2
            labels={'EXC';'INH'};
            legend(labels,'location','northeast');
        end
        set(gcf,'color','w');
        
        if ismember(j,[1,3])
            ylabel('neurons(%)');
        end
        
        title(name);
        set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
        ylim([0 100])
    end
    
    save_name = fullfile(save_dir,['Percentage_FRA_shapes.svg']);
    saveas(gcf, save_name);
    close;
    
end

%% Combine the correlations together and plot them
data_types = fieldnames(CC_int);
sel_types = fieldnames(CC_int.stim.(area));
cc_types = fieldnames(CC_int.stim.(area).(sel_types{1}));
inter_types = fieldnames(CC_int.stim.(area).(sel_types{1}).(cc_types{1}));

for j = 1:length(data_types)
    dt = data_types{j};
    
    for i = 1:length(sel_types)
        sel_type = sel_types{i};
        
        cc_types = fieldnames(CC_int.(dt).(area).(sel_types{1}));
        
        for k = 1:length(cc_types)
            cc_type = cc_types{k};
            
            for l = 1:length(inter_types)
                inter_type = inter_types{l};
                temp = [];
                
                for m = 1:length(areas_name)
                    area = areas_name{m};
                    temp = [temp; CC_int.(dt).(area).(sel_type).(cc_type).(inter_type)];
                end
                
                CC_rez.(sel_type).(dt).(cc_type).(inter_type) = temp;
            end
        end
        
    end
    
end

cc_save_dir = fullfile(save_dir_all,'CC');
if ~exist(cc_save_dir, 'dir')
    mkdir(cc_save_dir);
end

violin_save_dir = fullfile(cc_save_dir,'Violin');
if ~exist(violin_save_dir, 'dir')
    mkdir(violin_save_dir);
end

hist_save_dir = fullfile(cc_save_dir,'Histogram');
if ~exist(hist_save_dir, 'dir')
    mkdir(hist_save_dir);
end

mat_save_dir = fullfile(cc_save_dir,'Matrix');
if ~exist(mat_save_dir, 'dir')
    mkdir(mat_save_dir);
end


if plot_CC_mat
    %Plot the CC as Matrix
    params_cc.save_dir = mat_save_dir;
    plot_SN_matrix(CC_rez, params_cc);
end

if plot_CC_violin
    %Plot the CC as Violin plots
    params_cc.save_dir = violin_save_dir;
    plot_SN_violins(CC_rez, params_cc);
end

if plot_CC_hist
    %Plot the CC as Histogram plots
    params_cc.save_dir = hist_save_dir;
    plot_SN_histograms(CC_rez, params_cc);
end



%% Store the results in a structure
Rez.prop = prop;
Rez.BF = BFs;
Rez.fra_shape = fra_shape;
Rez.FRA_props = FRA_props;
Rez.CC = CC_rez;
Rez.freqs = params.freqs;

