% based on: edit rsbs_ROI_CRSP_immuno_createREF_findexample.m
%
% [fList,pList] = matlab.codetools.requiredFilesAndProducts('tpti_matchingexample.m');
% for i=1:length(fList),
%    fprintf('%s\n',fList{i}); 
% end
% winopen('C:\Jasper\OneDrive\OneDrive - University College London\AlexFerretData\2p2immuno\code\');
% winopen('D:\BehaviourImaging\Matlab\ImagToolbox\2photon_setup\extractdata\');

clear all;
javaaddpath '/home/alex/java/jar/mij.jar'; 
try, MIJ.exit; end
close all;

% tocopy = 1;
% RSG.savedir       = [ROOT,'rsbs_workflow\rsbs_ROI_CRSP\'];
% RSG.rootdir2      = [ROOT,'2photon\'];
% RSG.rootdir3      = [ROOT,'rsbs_workflow\rsbs_preproc_forsegment_nofilt\'];
% RSG.savedirSEG{1} = [ROOT,'rsbs_workflow\rsbs_segmentroi_batch\'];
% RSG.savedirSEG{4} = [ROOT,'rsbs_workflow\rsbs_preproc_forsegment_nofilt\'];
% ImmunoDir = [ROOT,'immuno_registered_convert\'];
ROOTNEW = '/home/alex/Desktop/Code/Jasper_code/2p2immuno/testdata/';

tocopy = 0;
RSG.savedir       = ROOTNEW;
RSG.rootdir2      = ROOTNEW;
RSG.rootdir3      = ROOTNEW;
RSG.savedirSEG{1} = ROOTNEW;
RSG.savedirSEG{4} = ROOTNEW;
ImmunoDir         = ROOTNEW;



%--------------------------------------------------------------------------
% logbook info about recordings

clear info;
ID = 'M71_20141020'; 
info.(ID).site = 'a';
info.(ID).fnamecalib={'20141020_175157__M71_calibeye0','20141020_175218__M71_calibeye1'};
bix=1;info.(ID).block_segment= bix;
info.(ID).fnamePC1{bix}='20141020_175241__M71_B1';
info.(ID).fnamePC2{bix}='20141020_175245__M71_B1';
info.(ID).fnamePC3{bix}='20141020_175244__M71_B1';
info.(ID).task{bix}='SD';
bix=2;
info.(ID).fnamePC1{bix}='20141020_182317__M71_B2';
info.(ID).fnamePC2{bix}='20141020_182325__M71_B2';
info.(ID).fnamePC3{bix}='20141020_182319__M71_B2';
info.(ID).fnamePC4{bix}='20141020_182304__M71_B2_rs_2eyes_retinotopy.mat';
bix=3;
info.(ID).fnamePC1{bix}='20141020_182802__M71_B3';
info.(ID).fnamePC2{bix}='20141020_182811__M71_B3';
info.(ID).fnamePC3{bix}='20141020_182801__M71_B3';
info.(ID).fnamePC4{bix}='20141020_182745__M71_B3_rs_2eyes.mat';
bix=4;
info.(ID).fnamePC1{bix}='20141020_183700__M71_B4';
info.(ID).fnamePC2{bix}='20141020_183724__M71_B4';
info.(ID).fnamePC3{bix}='20141020_183659__M71_B4';
info.(ID).fnamePC4{bix}='20141020_183717__M71_B4_rs_2eyes.mat';

ID = 'M71_20141029';
info.(ID).site = 'a';
info.(ID).fnamecalib={'20141029_161717__M71_calibeye0','20141029_161742__M71_calibeye1'};
bix=1;info.(ID).block_segment= bix;
info.(ID).fnamePC1{bix}='20141029_162853__M71_B1';
info.(ID).fnamePC2{bix}='20141029_162833__M71_B1';
info.(ID).fnamePC3{bix}='20141029_162905__M71_B1';
info.(ID).task{bix}='SD';
bix=2;
info.(ID).fnamePC1{bix}='20141029_171238__M71_B2';
info.(ID).fnamePC2{bix}='20141029_171217__M71_B2';
info.(ID).fnamePC3{bix}='20141029_171246__M71_B2';
info.(ID).fnamePC4{bix}='20141029_171259__M71_B2_rs_2eyes_retinotopy.mat';
bix=3;
info.(ID).fnamePC1{bix}='20141029_171639__M71_B3';
info.(ID).fnamePC2{bix}='20141029_171617__M71_B3';
info.(ID).fnamePC3{bix}='20141029_171648__M71_B3';
info.(ID).fnamePC4{bix}='20141029_171700__M71_B3_rs_2eyes.mat';
bix=4;
info.(ID).fnamePC1{bix}='20141029_172317__M71_B4';
%info.(ID).fnamePC1{bix}='20141029_173935__M71_B4';
info.(ID).fnamePC2{bix}='20141029_172256__M71_B4';
info.(ID).fnamePC3{bix}='20141029_172326__M71_B4';
info.(ID).fnamePC4{bix}='20141029_172337__M71_B4_rs_2eyes.mat';

ID = 'M71_20141101';
info.(ID).site = 'a';
info.(ID).fnamecalib={'20141101_183727__M71_calibeye0','20141101_183734__M71_calibeye1'};
bix=1;info.(ID).block_segment= bix;
info.(ID).fnamePC1{bix}='20141101_185315__M71_B1';
info.(ID).fnamePC2{bix}='20141101_185251__M71_B1';
info.(ID).fnamePC3{bix}='20141101_185330__M71_B1';
info.(ID).task{bix}='SWITCH';


%--------------------------------------------------------------------------

rix = 1;
ROIseslist{rix} = {'M71_20141020'    'M71_20141029'    'M71_20141101'}
ROIsesblks{rix} = {[1]    [1]    [1]};
ROIsitenam{rix} = 'M71a';

ALLERROR={};
for rix=1%[2,13,19,22,27,30,36]%5%:17%2%2%[2,10,19,23,28,36]%18%:length(ROIseslist), still missing 15,31
    for layer=2,
        
        fprintf('%d:%s\n',rix,ROIseslist{rix}{1});
        
        do_review = 1;% leave as 1
        if do_review,
            
            % review correspondence in GUI
            clear global mystackA mystackU;
            
            cfg = [];
            cfg.ROIseslist=ROIseslist{rix};
            cfg.ROIsesblks=ROIsesblks{rix};
            cfg.ROIsitenam=ROIsitenam{rix};
            cfg.rootdir1 = RSG.rootdir2;
            cfg.rootdir2 = RSG.savedir;
            for NID=1:length(cfg.ROIseslist) %NID=10
                RSG.info = info.(cfg.ROIseslist{NID});
                blockix=cfg.ROIsesblks{NID};
                BLK=1;cfg.fnamePC2{NID} = RSG.info.fnamePC2{blockix(BLK)};
                cfg.fname{NID} = sprintf('%s%s_ch-525.bin',RSG.rootdir2,cfg.fnamePC2{NID} );
                [x,y,z]=fileparts(cfg.fname{NID});
                cfg.fname{NID} = sprintf('%s%s_pp4seg_layer%d_nofilt.bin',RSG.savedirSEG{4},y,layer);
                
                if tocopy,
                    of = cfg.fname{NID};
                    [x,y,z]=fileparts(of);
                    nf = [ROOTNEW,y,z];
                    S = copyfile(of,nf);
                    if ~S, error('here'); end
                end
            end
            
            % load correspondence files if any, and celllabels if any
            for NID=1:length(cfg.ROIseslist) %NID=11 i=3
                
                cfg.fnameCRSP{NID} = sprintf('%s%s_CRSPR_layer%d.mat',RSG.savedir,cfg.ROIseslist{NID},layer);
                tmp=load(cfg.fnameCRSP{NID});
                if tocopy,
                    of = cfg.fnameCRSP{NID};
                    [x,y,z]=fileparts(of);
                    nf = [ROOTNEW,y,z];
                    S = copyfile(of,nf);
                    if ~S, error('here'); end
                end
                ix_a=strfind(tmp.CRSPR.segname,'\');
                tmp.CRSPR.segname = [ROOTNEW,tmp.CRSPR.segname(ix_a(end)+1:end)];
                cfg.CRSP{NID}=tmp.CRSPR;
                
                cfg.fnameCL{NID} = sprintf('%s%s_CL_layer%d.mat',RSG.savedir,cfg.ROIseslist{NID},layer);
                tmp=load(cfg.fnameCL{NID});
                cfg.CL{NID}      = tmp.CL;
                if tocopy,
                    of = cfg.fnameCL{NID};
                    [x,y,z]=fileparts(of);
                    nf = [ROOTNEW,y,z];
                    S = copyfile(of,nf);
                    if ~S, error('here'); end
                end
                fprintf('%s\n',cfg.CRSP{NID}.segname);
                [x,y,z]=fileparts( cfg.CRSP{NID}.segname );
                cfg.CRSP{NID}.segname = sprintf('%s%s.mat',RSG.savedir,y);
            end % cfg.ROIseslist
            
            % refresh mask names if necessary
            [x,y,z]=fileparts(RSG.savedirSEG{1});
            [x1,y,z]=fileparts(x);
            for i=1:length(cfg.CRSP),
                if not(isempty(cfg.CRSP{i})),
                    [x,y,z]=fileparts(cfg.CRSP{i}.segname);
                    [x2,y,z]=fileparts(x);
                    if ~isequal(x1,x2),
                        cfg.CRSP{i}.segname=strrep(cfg.CRSP{i}.segname,x2,x1);
                    end
                end
            end
            
            %if W==1,
            COMBI=[[1:length(ROIseslist{rix})-1]', [2:length(ROIseslist{rix})]'];
            COMBIcfg = cell([size(COMBI,1),1]);
            for cmb=1:size(COMBI,1),
                of = sprintf('%s%s_%s_ROICcfg_layer%d.mat',RSG.savedir,ROIseslist{rix}{COMBI(cmb,1)},ROIseslist{rix}{COMBI(cmb,2)},layer);
                tmp=load(of);
                ix=strfind(tmp.cfg.fname,'\');
                tmp.cfg.fname = [ROOTNEW,tmp.cfg.fname(ix(end)+1:end)];
                COMBIcfg{cmb}=tmp.cfg;
                if tocopy,                    
                    [x,y,z]=fileparts(of);
                    nf = [ROOTNEW,y,z];
                    S = copyfile(of,nf);
                    if ~S, error('here'); end
                end
            end
            cfg.COMBI    = COMBI;
            cfg.COMBIcfg = COMBIcfg;
            
            close all;
            
            % find correspondence only for session with empty CRSPR (sessions mentioned in miscrsp)
            if ~isfield(cfg,'miscrsp'),
                cfg.miscrsp=[];
            end
            
            % [cfg]=resscn_ROI_correspondence_automatic_ROIseslist(cfg)
            [cfg]=rsbs_ROI_CRSP_auto_missing(cfg);
            
            clear global mystackA;
            cfg.MI=50;
            cfg.MA=200;
            %cfg.immuno = do_immuno;
            mystackviewer_CRSP(cfg) % edit mystackviewer_correspondence
            
            % info about cell in confocal:
            global mystackA;
            
            nm_confocal_all = {};
            for i=1:length(mystackA.cfg.CL), % i=1; i=2;j=17
                if ~isempty(mystackA.cfg.CL{i}),
                    %i
                    %mystackA.cfg.CL{i}.CELLLABEL{1} % 1=xy 2p 2=label 3=xyz confocal 4=?                    
                    for j=1:length(mystackA.cfg.CL{i}.CELLLABEL),                        
                        dum = mystackA.cfg.CL{i}.CELLLABEL{j}{3};
                        d2p = mystackA.cfg.CL{i}.CELLLABEL{j}{1};
                        nm_confocal = mystackA.cfg.CL{i}.CELLLABEL{j}{4};
                        nm_confocal_all{end+1} = nm_confocal;
                        fprintf('i=%3d j=%3d 2P: x=%8.2f,y=%8.2f, CONFOCAL:%4s x=%8.2f,y=%8.2f,frm=%3d %s\n',i,j,d2p(1),d2p(2),mystackA.cfg.CL{i}.CELLLABEL{j}{2},dum(1),dum(2),dum(3),nm_confocal);
                    end
                end                    
            end
            
            nm_confocal_all = unique(nm_confocal_all);
            if length(nm_confocal_all)>1,
                keyboard;
                [B,I,J]=unique(nm_confocal_all);
                clear N;
                for i=1:length(B),
                    N(i)=length(find(J==i))
                end
                [~,ix]=max(N); nm_confocal_all{1} = nm_confocal_all{ix};
            end
                        
            if 1, % testing part
                ix =strfind(nm_confocal_all{1},'_')
                ix2=strfind(nm_confocal_all{1},'lay')
                nm1=nm_confocal_all{1}(1:3);
                nm2=nm_confocal_all{1}(ix(1)+1:ix2(1)-2);
                
                %------------------------
                % open corresponding immunostacks in matlab viewer
                % mystackviewer_segment
                % winopen(ImmunoDir)

                
                if 0,%filefilter = sprintf('%s%s*%d*_GCAMP.bin',ImmunoDir,ROIsitenam{rix},layer)
                    filefilter = sprintf('%s%s*_GCAMP.bin',ImmunoDir,ROIsitenam{rix}(1:end-1))
                    [filename, pathname] = uigetfile(filefilter, 'Pick a  file');
                else
                    fnameS = dir(sprintf('%s%s*%s*.bin',ImmunoDir,nm1,nm2))
                    filename = fnameS(1).name;
                    pathname = ImmunoDir;
                end
               
                
                % these are the bin files (not yet transformed)
                cfgI=[];
                cfgI.fname{1} = [pathname,filename];
                cfgI.fname{2} = [pathname,strrep(filename,'GCAMP','PV')];
                cfgI.fname{3} = [pathname,strrep(filename,'GCAMP','SOM')];
                cfgI.fname{4} = [pathname,strrep(filename,'GCAMP','VIP')];
                
                if tocopy,             
                     for i=1:length(cfgI.fname),
                         of = cfgI.fname{i};
                         [x,y,z]=fileparts(of);
                         nf = [ROOTNEW,y,z];
                         S = copyfile(of,nf);
                         if ~S, error('here'); end
                     end
                end
                
                %mystackviewer_immunoview(cfgI);
                
                clear global myIview;
                %profile on
                myimmunoview(cfgI);
                %tpti_viewer_confocal(cfgI);
                %profile viewer;
                
                %--------------------------------------------------------------
                % also load the transformed tiff stack
                % edit tpti_demo_alex.m
                fnameS = dir(sprintf('%s%s*%s*.tif',ImmunoDir,nm1,nm2));
                
                pt = strrep(ImmunoDir,'\','/');
                clear fn;
                for i=1:4,
                    fn{i}  = strrep([pt,fnameS(i).name],'\','/');
                    fn0{i} = strrep([fnameS(i).name],'\','/');
                    fprintf('%s\n',fn0{i});
                end
                
                if tocopy,
                    for i=1:length(fn),
                        of = fn{i};
                        [x,y,z]=fileparts(of);
                        nf = [ROOTNEW,y,z];
                        S = copyfile(of,nf);
                        if ~S, error('here'); end
                    end
                end
                
                fid = fopen('/home/alex/Desktop/Code/Jasper_code/2p2immuno/temp/tmp.ijm','wt');
                fprintf(fid,'run("Bio-Formats Macro Extensions");\n');
          
                for i=1:4,
                    fprintf(fid,'Ext.openImagePlus("%s");\n',fn{i});
                end
                ix = [find(cellfun(@(x) ~isempty(strfind(x,'PV')),fn0)),find(cellfun(@(x) ~isempty(strfind(x,'GCAMP')),fn0)),...
                    find(cellfun(@(x) ~isempty(strfind(x,'SOM')),fn0)),find(cellfun(@(x) ~isempty(strfind(x,'VIP')),fn0))];
                
                for i=1:4
                    ix_2=strfind(fn{i},'/');
                    fns{i} = fn{i}(ix_2(end)+1:end);
                end
%                 fprintf(fid,'run("Merge Channels...", "c1=%s c2=%s c3=%s c6=%s create ignore");\n',fn{ix(1)},fn{ix(2)},fn{ix(3)},fn{ix(4)});
%                 fprintf(fid,'run("Merge Channels...", "c1=%s c2=%s c3=%s c6=%s create ignore");\n',fns{ix(1)},fns{ix(2)},fns{ix(3)},fns{ix(4)}); %Alex modified 10/12/18
%                 fprintf(fid,'run("Channels Tool...");\n');
%                 fprintf(fid,'Stack.setDisplayMode("color");\n');
%                 fprintf(fid,'run("Close");\n');
                fclose(fid);
                system('notepad "/home/alex/Desktop/Code/Jasper_code/2p2immuno/temp/tmp.ijm" &')

                Miji;
                
%                 MIJ.run('Install...',['/home/alex/Desktop/Code/Jasper_code/2p2immuno/temp/tmp.ijm']);
%                 Original above
                MIJ.run('Install...','install=/home/alex/Desktop/Code/Jasper_code/2p2immuno/temp/tmp.ijm');
                %Alex modified this 29/01/19
                MIJ.run('tmp'); %Alex modified this 29/01/19
                
                %----------------------------------------------------------
                % load history and try to redo transform
                global myIview;
                myIview.fname = cfgI.fname;
                
                chn=1;
                fname  = myIview.fname{chn};
                fnameS = strrep(fname,'.bin','.tif');
                
                % load the history
                fname = myIview.fname{1};
                fnameS2 = strrep(fname,'.bin','_history.mat');
                
                load(fnameS2,'H');
                                   
                if tocopy,
                    of = fnameS2;
                    [x,y,z]=fileparts(of);
                    nf = [ROOTNEW,y,z];
                    S = copyfile(of,nf);
                    if ~S, error('here'); end
                end
                                
                T1=H.history{1}.T1
                T2=H.history{1}.T2
                T3=H.history{1}.T3
                
                %----------------------------------------------------------
                % edit myimmunoview > call compute
                
                % blob_center = [find(XRNG==myIview.ORI(1)),find(YRNG==myIview.ORI(2)),find(FRNG==myIview.ORI(3))];
                FRNG = H.history{1}.FRNG;
                XRNG = H.history{1}.XRNG;
                YRNG = H.history{1}.YRNG;
                ORI  = [XRNG( T3(4,1) ), YRNG( T3(4,2) ), FRNG( T3(4,3) )] % ORI
                NumZ = FRNG([1,end])-ORI(3) % FRNG = myIview.ORI(3)+myIview.NumZ(1):myIview.ORI(3)+myIview.NumZ(2);
                %isequal(FRNG, ORI(3)+NumZ(1):ORI(3)+NumZ(2))
                rect([1,2]) = XRNG([1,end])-ORI(1); % XRNG=myIview.ORI(1)+[myIview.rect(1):myIview.rect(2)];
                rect([3,4]) = YRNG([1,end])-ORI(2); % YRNG=myIview.ORI(2)+[myIview.rect(3):myIview.rect(4)];
                %isequal(XRNG, ORI(1)+rect(1):ORI(1)+rect(2))
                %isequal(YRNG, ORI(2)+rect(3):ORI(2)+rect(4))
                
                % settings in GUI
                T2
                ORI
                NumZ
                rect
                
                %----------------------------------------------------------
                % update GUI setttings
                global myIview;
                myIview.T = T2;
                str=[];
                for i=1:size(myIview.T,2),
                    str{i}=sprintf('%1.2f  %1.2f  %1.2f  %1.2f',myIview.T(i,:));
                end
                set(myIview.text,'String',str);
                
                myIview.NumZ = NumZ;
                set(myIview.editNumZ,'String',num2str(NumZ))
                
                myIview.ORI = ORI;
                set(myIview.editorigin,'String',num2str(ORI))
                
                myIview.rect = rect;
                set(myIview.editXY,'String',num2str(rect))
                
                try, delete(myIview.rectH); end
                hold on;myIview.rectH=patch(myIview.ORI(1)+myIview.rect([1,2,2,1]),...
                    myIview.ORI(2)+myIview.rect([3,3,4,4]),[1 1 1],'FaceColor','none','EdgeColor',[1 1 1],'LineWidth',2);
                
                %----------------------------------------------------------
                % USER > CLICK COMPUTE IN GUI
                keyboard;
                global myIview;
                
                tform=myIview.tform;
                blob_center = ORI % [663,748,10];
                blob_center(3) = blob_center(3)
                dum1 = tformfwd(blob_center, tform);
                sz=size(myIview.DATT)./2
                sz(1:2) = 0;
                dum1 = dum1 + sz
                dum2 = tforminv(dum1, tform);
                
                round(dum1)
                round(dum2)
                
                
                axes(myIview.ax(1));hold on;t1=plot(blob_center(1),blob_center(2),'gx','MarkerSize',10,'LineWidth',2);
                round(blob_center(3))
                axes(myIview.ax(2));hold on;t2=plot(dum1(1),dum1(2),'gx','MarkerSize',10,'LineWidth',2);
                round(dum1(3))
                
                
                % shortcuts in myimmunoview GUI
                % press p to insert point on the right and see backward transform
                % press l to insert point on the left and see forward transform

                PNT0 = [ 410.4576  577.7767, 1 % points in 2p image
                    566.8136  531.8047, 1
                    381.2203  220.5088, 1
                    573.1695  350.5438, 1
                    357.0678  475.3249, 1
                    290.9661  392.5753, 1]
                
                PNT = [712,1647,18 % points in merged confocal stack
                    982,1628,18
                    854,1063,10
                    1077,1323,14
                    676,1446,15
                    613,1286,12]
                                
                zm = round(mean(PNT,1));
                PNT0=PNT0-repmat(mean(PNT0,1),[size(PNT0,1),1]);
                PNT =PNT -repmat(zm,[size(PNT,1),1]);
                T=compute_transform(PNT',PNT0') % from immuno to 2p
                myIview.T = T';
                
                global myIview
                myIview.MI
                
            end
            
            set(gcf,'CloseRequestFcn','uiresume(gcbf);closereq;');
            interactwithGUI=1; % !! if 0 automatically saves CRSPR files
        
            if interactwithGUI,
                choice = questdlg('Continue?', ...
                    'resscn_ROI_correspondence_sessionlist', ...
                    'Yes','No','No');
            else
                choice = 'Yes';
            end
            % Handle response
            switch choice
                case 'Yes'
                    NID
                case 'No'
                    NID
                    break;
            end
 
        end % if do_review,
        
    end % for layer=1:4,
    try,
        if do_immuno,
            MIJ.run('Close All'); % this crashes matlab if no ImageJ open...(at least on Beast)
            MIJ.exit;
        end
    end
end % for rix=1:length(ROIseslist),
