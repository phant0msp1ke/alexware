function cfg=resscn_ROI_correspondence_automatic_ROIseslist_missing(cfg);
% edit resscn_ROI_correspondence_automatic_ROIseslist_missing2.m
 
CRSP=cfg.CRSP; % 1st column=ROI id in sesion, 2nd=unique ROI id for al sessions

if ~isempty(cfg.miscrsp), % sessions for which CRSPR is missing
    warning(sprintf('sessions for which CRSPR is missing: %s',sprintf('%d ' ,cfg.miscrsp)))    
end

% get max number of cells
maxroi=0;
for i=1:length(CRSP),
    if not(ismember(i,cfg.miscrsp)), % sessions which have CRSPR
        if not(isempty(CRSP{i})),
            maxroi=max([maxroi,max(CRSP{i}.conversion(:,2))]);
        end
    end
end

ix=maxroi;
for i=1:length(CRSP),
    if ismember(i,cfg.miscrsp) % sessions which do not have CRSPR
        if not(isempty(CRSP{i})),
            CRSP{i}.conversion(:,2)=CRSP{i}.conversion(:,2)+ix;
            ix=ix+size(CRSP{i}.conversion,1);
        end
    end
end

% find unique ROIs
ROIu=[];
for i=1:length(CRSP), % for each session
    if not(isempty(CRSP{i})),
        ROIu=[ROIu; repmat(i,[size(CRSP{i}.conversion,1),1]),CRSP{i}.conversion];
    end
end
[B,I,J]=unique(ROIu(:,3));

% find in which session each unique ROI appears
ROIID =NaN([length(B),length(CRSP)]); % ROI x session
for i=1:length(CRSP), % for each session
    if not(isempty(CRSP{i})),
        for j=1:length(B),
            if not(isempty(find(CRSP{i}.conversion(:,2)==B(j)))),
                ROIID(j,i)=1;
            end
        end
    end
end
figure;imagesc([1:length(CRSP)],B,ROIID);
set(gca,'XTick',[1:length(CRSP)]);
ylabel('ROIid');xlabel('session');
title('before automatic correspondence');

%--------------------------------------------------------------------------
% now find correspondence across session

for cmb=1:length(cfg.COMBIcfg),%cfg.COMBI),
    if not(isempty(cfg.COMBIcfg{cmb}))
        F1ix=cfg.COMBIcfg{cmb}.cmb(1);
        F2ix=cfg.COMBIcfg{cmb}.cmb(2);
        [x,y,z]=fileparts(cfg.COMBIcfg{cmb}.fname);
        %y=y(1:end-8);
        ix=strfind(y,'_');
        
        if length(ix)==8,
            F1=y(1:ix(4)-1);
            F2=y(ix(4)+1:ix(8)-1);
            F1ix=strmatch(F1,cfg.ROIseslist);
            F2ix=strmatch(F2,cfg.ROIseslist);
        else
            F1=y(1:ix(2)-1);
            F2=y(ix(2)+1:ix(4)-1);
            F1ix=strmatch(F1,cfg.ROIseslist);
            F2ix=strmatch(F2,cfg.ROIseslist);
        end
        
        if all(ismember([F1ix,F2ix],cfg.miscrsp)), % if both sessions have no CRSPR file yet
                       
        Cnew    = cfg.COMBIcfg{cmb}.C{1};
        Cactual = CRSP{F1ix}.C;
        %if not(isequal(Cnew,Cactual)),error('here');end
        if 0,
            figure;plot(Cnew(:,2),Cnew(:,1),'bo');
            hold on;plot(Cactual(:,2),Cactual(:,1),'rx');
            legend({'COMBIcfg','CRSP'},'Location','Best');
        end
        Cnew2actual =zeros([size(Cnew,1),1]);
        Cnew2actualD=zeros([size(Cnew,1),1]);
        for i=1:size(Cnew,1)
            [Y,ix]=min(abs( sqrt(sum([Cactual-repmat(Cnew(i,:),[size(Cactual,1),1])].^2,2)) ));
            Cnew2actual(i) =ix;
            Cnew2actualD(i)=Y;
        end
        C1=Cnew2actual;
        
        Cnew    = cfg.COMBIcfg{cmb}.C{2};
        Cactual = CRSP{F2ix}.C;
        %if not(isequal(Cnew,Cactual)),error('here');end
        if 0,
            figure;plot(Cnew(:,2),Cnew(:,1),'bo');
            hold on;plot(Cactual(:,2),Cactual(:,1),'rx');
            legend({'COMBIcfg','CRSP'},'Location','Best');
        end
        
        Cnew2actual =zeros([size(Cnew,1),1]);
        Cnew2actualD=zeros([size(Cnew,1),1]);
        for i=1:size(Cnew,1)
            [Y,ix]=min(abs( sqrt(sum([Cactual-repmat(Cnew(i,:),[size(Cactual,1),1])].^2,2)) ));
            Cnew2actual(i) =ix;
            Cnew2actualD(i)=Y;
        end
        C2=Cnew2actual;
        
        % FIX; after replacing bwlabel with bwboundary and not using erode, this
        % should now be true always (so the code could be simplified but
        % not necessary
        if not(isequal(C1,[1:length(C1)]')),error('here');end
        if not(isequal(C2,[1:length(C2)]')),error('here');end
                
        % correction: can still happen occasionally
        % in  resscn_correspondencemasks: if not(isequal(B1,B2)),
        
        for i=1:size(cfg.COMBIcfg{cmb}.C{2},1),
            cnv=cfg.COMBIcfg{cmb}.conversion(i);
            if not(isnan(cnv)),
                CRSP{F2ix}.conversion(C2(i),2)=CRSP{F1ix}.conversion(C1(cnv),2);
            end
        end % for i=1:size(cfg.COMBIcfg{cmb}.C{2},1),
       
        % this should not happen: after FIX above
        dum=CRSP{F2ix}.conversion(:,2);
        dum=dum(~isnan(dum));
        if not( length(unique(dum))==length(dum) ), error('here'); end
        
        dum=CRSP{F1ix}.conversion(:,2);
        dum=dum(~isnan(dum));
        if not( length(unique(dum))==length(dum) ), error('here'); end
        
        if 0
            dum=CRSP{F2ix}.conversion(:,2);
            dum=CRSP{F1ix}.conversion(:,2);
            dum=cfg.COMBIcfg{cmb}.conversion;
            dum=dum(~isnan(dum));
            if not( length(unique(dum))==length(dum) ), error('here'); end
            
            min( CRSP{F2ix}.conversion(:,2) ),max( CRSP{F2ix}.conversion(:,2) )
            min( CRSP{F1ix}.conversion(:,2) ),max( CRSP{F1ix}.conversion(:,2) )            
        end
       
        end % if all(ismember([F1ix,F2ix],cfg.miscrsp)), % if both sessions have no CRSPR file yet
    end % if not(isempty(cfg.COMBI{cmb})),
end % for cmb=1:length(cfg.COMBI),

% find unique ROIs
ROIu=[];
for i=1:length(CRSP), % for each session
    if not(isempty(CRSP{i})),
        ROIu=[ROIu; repmat(i,[size(CRSP{i}.conversion,1),1]),CRSP{i}.conversion];
    end
end
[B,I,J]=unique(ROIu(:,3));

% find in which session each unique ROI appears
ROIID =NaN([length(B),length(CRSP)]); % ROI x session
for i=1:length(CRSP), % for each session
    if not(isempty(CRSP{i})),
        for j=1:length(B),
            if not(isempty(find(CRSP{i}.conversion(:,2)==B(j)))),
                ROIID(j,i)=1;
            end
        end
    end % if not(isempty(CRSP{i})),
end
figure;imagesc([1:length(CRSP)],B,ROIID);
set(gca,'XTick',[1:length(CRSP)]);
ylabel('ROIid');xlabel('session');
title('after automatic correspondence');

cfg.CRSP=CRSP;
