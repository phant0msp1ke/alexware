function [T]=compute_transform(A,B)

% Horn BKP (1987) Closed-form solution of absolute orientation using unit
% quaternions. JOSA A 4:629�642.

%--------------------------------------------------------------------------
% center left and right coordinate systems

LFTc=mean(A,2);  RGTc=mean(B,2);
LFT = A - repmat(LFTc,[1,size(A,2)]);
RGT = B - repmat(RGTc,[1,size(B,2)]);

%--------------------------------------------------------------------------
% compute rotation

M=LFT*transpose(RGT);

% p. 635
Sxx = M(1); 
Syx = M(2); 
Szx = M(3); 
Sxy = M(4); 
Syy = M(5); 
Szy = M(6); 
Sxz = M(7); 
Syz = M(8); 
Szz = M(9); 

N=[(Sxx+Syy+Szz)  (Syz-Szy)      (Szx-Sxz)      (Sxy-Syx);...
    (Syz-Szy)      (Sxx-Syy-Szz)  (Sxy+Syx)      (Szx+Sxz);...
    (Szx-Sxz)      (Sxy+Syx)     (-Sxx+Syy-Szz)  (Syz+Szy);...
    (Sxy-Syx)      (Szx+Sxz)      (Syz+Szy)      (-Sxx-Syy+Szz)];

[V,D]=eig(N);

[~,I]=max(real(  diag(D)  )); I=I(1); % index of max

tmp=V(:,I);tmp=real(tmp); % eigenvector of max eigenvalue

[~,I]=max(abs(tmp)); sgn=sign(tmp(I(1)));
tmp=tmp*sgn;

quaternion=tmp(:);quaternion=quaternion./norm(quaternion);

a=quaternion(1);
b=quaternion(2);
c=quaternion(3);
d=quaternion(4);

% p. 641 (typo in paper R(3,2) is cd + ab not cd + ad 
R = [(a^2+b^2-c^2-d^2), 2*(b*c-a*d),       2*(b*d+a*c);
     2*(b*c+a*d)      , (a^2-b^2+c^2-d^2), 2*(c*d-a*b);
     2*(b*d-a*c)      , 2*(c*d+a*b),       (a^2-b^2-c^2+d^2)];

t=RGTc-R*LFTc; % translation

T=[R,t;[0 0 0 1]];
