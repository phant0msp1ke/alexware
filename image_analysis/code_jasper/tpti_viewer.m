function tpti_viewer(cfg,varargin);

% based on mystackviewer_segment
if 0,
    set(0,'ShowHiddenHandles','on')
    delete(get(0,'Children'));
end

global glb_tpti_viewer;

glb_tpti_viewer.fname = cfg.fname;
glb_tpti_viewer.dataformat=1; % raw data or data that is already shifted

l = dir(glb_tpti_viewer.fname); % see how big the file is and how much datapoints it contains edit FileSize % find total number of frames in data
fid = fopen(glb_tpti_viewer.fname);
glb_tpti_viewer.dim    = fread(fid,2,'int16'); % size of image
glb_tpti_viewer.totfrm = (l.bytes-2*(16/8))./(2*prod(glb_tpti_viewer.dim)); % total number of frames
fclose(fid);

if nargin>1,
    IMG = varargin{1};
else
    IMG = zeros(glb_tpti_viewer.dim');
end

if glb_tpti_viewer.dataformat==2,
    if glb_tpti_viewer.subpixel==1,
        % for using subpixel alignment
        sizX        = glb_tpti_viewer.dim(2);
        sizY        = glb_tpti_viewer.dim(1);
        [X1, X2]    = ndgrid(1:sizX,1:sizY);
        glb_tpti_viewer.sp.sizX = sizX;
        glb_tpti_viewer.sp.sizY = sizY;
        glb_tpti_viewer.sp.X1   = X1;
        glb_tpti_viewer.sp.X2   = X2;
    end
end % if glb_tpti_viewer.dataformat==0,

glb_tpti_viewer.frame = 1;

if isfield(cfg,'navg'),          glb_tpti_viewer.navg  = cfg.navg;                  else glb_tpti_viewer.navg  = 30;        end
if isfield(cfg,'medfilt'),       glb_tpti_viewer.medfilt = cfg.medfilt;             else glb_tpti_viewer.medfilt = 0;       end
if isfield(cfg,'moviestepsize'), glb_tpti_viewer.moviestepsize = cfg.moviestepsize; else glb_tpti_viewer.moviestepsize = 1; end

%--------------------------------------------------------------------------
% get a few frames to determine scaling factors
fid = fopen(glb_tpti_viewer.fname);
ix = round(linspace(1,glb_tpti_viewer.totfrm,30));
if glb_tpti_viewer.dataformat==2,
    IMG = zeros([length(glb_tpti_viewer.patch.y), length(glb_tpti_viewer.patch.x), length(ix)], 'int16');
else
    IMG = NaN([glb_tpti_viewer.dim(2),glb_tpti_viewer.dim(1),length(ix)]);
end
for i=1:length(ix),
    rng=ix(i);
    
    % read in those frames
    fseek(fid, 2*(16/8) + (2*glb_tpti_viewer.dim(1)*glb_tpti_viewer.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
    ndat =fread(fid,[length(rng)*prod(glb_tpti_viewer.dim)],'*int16'); % read single frame
    ndatr=reshape(ndat,[glb_tpti_viewer.dim(2),glb_tpti_viewer.dim(1),length(rng)]); % size(datr)
    
    if glb_tpti_viewer.dataformat==2,
        
        Itmp = squeeze(mean(ndatr,3));
        if glb_tpti_viewer.subpixel
            x0 = glb_tpti_viewer.ops.del1(rng);
            y0 = glb_tpti_viewer.ops.del2(rng);
            F = griddedInterpolant(glb_tpti_viewer.sp.X1, glb_tpti_viewer.sp.X2,  double(Itmp), 'linear');
            [X1n, X2n]  = ndgrid([1:glb_tpti_viewer.sp.sizX]-x0,[1:glb_tpti_viewer.sp.sizY]-y0);
            Itmp = F(X1n(glb_tpti_viewer.patch.y, glb_tpti_viewer.patch.x), X2n(glb_tpti_viewer.patch.y, glb_tpti_viewer.patch.x));
            IMG(:,:,i) = int16(Itmp);
        else
            J = zeros(size(IMG,1), size(IMG,2), 'int16');
            x0 = round(glb_tpti_viewer.ops.del2(rng));
            y0 = round(glb_tpti_viewer.ops.del1(rng));
            cy = (glb_tpti_viewer.patch.y - y0);
            cx = (glb_tpti_viewer.patch.x - x0);
            Iy = cy > 0 & cy < size(Itmp,1);
            Ix = cx > 0 & cx < size(Itmp,2);
            J(Iy, Ix) = Itmp(cy(Iy), cx(Ix));
            IMG(:,:,i) = J;
        end
    else
        IMG(:,:,i) = squeeze(mean(ndatr,3));
    end % if glb_tpti_viewer.dataformat==2,
end
fclose(fid);
if isfield(cfg,'MI'),
    glb_tpti_viewer.MI=cfg.MI;
else
    %if glb_tpti_viewer.dataformat==1,
    glb_tpti_viewer.MI=min(min(nanmean(IMG,3)));
    %else
    %    glb_tpti_viewer.MI=min(min(nanmean(IMG(glb_tpti_viewer.ops.yrange,:,:),3)));
    %end
end
if isfield(cfg,'MA'),
    glb_tpti_viewer.MA=cfg.MA;
else
    %if glb_tpti_viewer.dataformat==1,
    glb_tpti_viewer.MA=max(max(nanmean(IMG,3)));
    %else
    %    glb_tpti_viewer.MA=max(max(nanmean(IMG(glb_tpti_viewer.ops.yrange,:,:),3)));
    %end
end
glb_tpti_viewer.IMG = mean(IMG,3)';

if ~isfield(glb_tpti_viewer,'I'),
    if nargin==1,
        %glb_tpti_viewer.I=logical(zeros([size(IMG,1),size(IMG,2)]));
        glb_tpti_viewer.I=logical(zeros([size(IMG,2),size(IMG,1)]));
    else
        glb_tpti_viewer.I=I;
    end
end
glb_tpti_viewer.alpha_setting = 0.1;
glb_tpti_viewer.brushsize = 4;
glb_tpti_viewer.clicksel.threshold = 0.5;

%--------------------------------------------------------------------------

glb_tpti_viewer.handles.f = figure('Units','normalized');
set(glb_tpti_viewer.handles.f,'Name',cfg.fname);
set(glb_tpti_viewer.handles.f,'CloseRequestFcn',@my_closefcn);

%set(gcf,'menubar','none')
glb_tpti_viewer.menu_extra = uimenu(gcf,'Label','Extra');
glb_tpti_viewer.menu_extra_zoom  = uimenu(glb_tpti_viewer.menu_extra,'Label','Size Threshold',...
    'Callback',@call_menu_extra_sizethreshold);
glb_tpti_viewer.menu_extra_zoom  = uimenu(glb_tpti_viewer.menu_extra,'Label','Fill In Holes',...
    'Callback',@call_menu_extra_fillholes);
glb_tpti_viewer.menu_extra_erode  = uimenu(glb_tpti_viewer.menu_extra,'Label','Erode',...
    'Callback',@call_menu_extra_erode);
glb_tpti_viewer.menu_extra_plot_roi_traces  = uimenu(glb_tpti_viewer.menu_extra,'Label','Show ROI Traces',...
    'Callback',@call_plot_roi_traces);
glb_tpti_viewer.menu_extra_show_roi_number  = uimenu(glb_tpti_viewer.menu_extra,'Label','Show ROI Number',...
    'Callback',@call_show_roi_number);
glb_tpti_viewer.menu_extra_call_maxproj  = uimenu(glb_tpti_viewer.menu_extra,'Label','Max Projection',...
    'Callback',@call_maxproj);
glb_tpti_viewer.menu_extra_call_meanproj  = uimenu(glb_tpti_viewer.menu_extra,'Label','Mean Projection',...
    'Callback',@call_meanproj);
glb_tpti_viewer.menu_extra_call_moviestepsize  = uimenu(glb_tpti_viewer.menu_extra,'Label','Movie Stepsize',...
    'Callback',@call_menu_extra_moviestepsize);
glb_tpti_viewer.menu_extra_call_clicksel  = uimenu(glb_tpti_viewer.menu_extra,'Label','Clicksel',...
    'Callback',@call_menu_extra_clicksel);
glb_tpti_viewer.menu_extra_call_regiongrowing  = uimenu(glb_tpti_viewer.menu_extra,'Label','Region growing');
glb_tpti_viewer.menu_show_red  = uimenu(glb_tpti_viewer.menu_extra,'Label','Show Red Channel (X)','Callback',@call_menu_show_red);
glb_tpti_viewer.menu_clearcount  = uimenu(glb_tpti_viewer.menu_extra,'Label','Clear counting points','Callback',@call_menu_extra_clearcount);

set(gcf,'WindowKeyPressFcn',@call_keypress);
set(gcf,'WindowButtonDownFcn',@call_buttondown);
set(gcf,'WindowButtonUpFcn',@call_buttonup);
set(gcf,'WindowScrollWheelFcn',@call_wheel);

% get(gcf,'Units'), get(gcf,'Position')
set(glb_tpti_viewer.handles.f,'toolbar','figure'); % otherwise disappears when adding uicontrol elements...
if glb_tpti_viewer.totfrm~=1,
    sldstep = [1 100]./(glb_tpti_viewer.totfrm-1);
else
    sldstep = [1 100];
end
uicontrol('Parent',glb_tpti_viewer.handles.f,'Units','normalized','Style', 'slider',...
    'Position', [0.01 0.01 0.2 0.05],...
    'Min',0,'Max',1,'Value',0.5,'SliderStep',sldstep,'Callback', @call_slider);
uicontrol('Parent',glb_tpti_viewer.handles.f,'Units','normalized','Style','edit',...
    'Position', [0.25 0.01 0.1 0.05],...
    'String',num2str(glb_tpti_viewer.navg),'Callback', @call_editnavg);
uicontrol('Parent',glb_tpti_viewer.handles.f,'Units','normalized','Style','edit',...
    'Position', [0.4 0.01 0.1 0.05],...
    'String',num2str(glb_tpti_viewer.MI),'Callback', @call_editcmin);
uicontrol('Parent',glb_tpti_viewer.handles.f,'Units','normalized','Style','edit',...
    'Position', [0.6 0.01 0.1 0.05],...
    'String',num2str(glb_tpti_viewer.MA),'Callback', @call_editcmax);
uicontrol('Parent',glb_tpti_viewer.handles.f,'Units','normalized','Style','edit',...
    'Position', [0.75 0.01 0.1 0.05],...
    'String',num2str(glb_tpti_viewer.medfilt),'Callback', @call_editmedfilt);

glb_tpti_viewer.handles.show_boundary = uicontrol('Parent',glb_tpti_viewer.handles.f,'Units','normalized','Style','checkbox',...
    'Position', [0.01 0.95 0.15 0.05],...
    'String','Boundary','Value',0,'Callback',@call_showboundary);
uicontrol('Parent',glb_tpti_viewer.handles.f,'Units','normalized','Style','pushbutton',...
    'Position', [0.2 0.95 0.1 0.05],...
    'String','<','Callback', @call_frame);
uicontrol('Parent',glb_tpti_viewer.handles.f,'Units','normalized','Style','pushbutton',...
    'Position', [0.3 0.95 0.1 0.05],...
    'String','>','Callback', @call_frame);

uicontrol('Parent',glb_tpti_viewer.handles.f,'Units','normalized','Style','text',...
    'Position', [0 0.2 0.08 0.7],...
    'String',{'M=max projection','S=size threshold','F=fill holes','P=play movie(stop with right mouse click)'},...
    'FontSize',6);

% plot image
glb_tpti_viewer.hI=imagesc(glb_tpti_viewer.IMG);colormap(gray);

MASK=ones(size(glb_tpti_viewer.IMG));
glb_tpti_viewer.MASK=cat(3,MASK,zeros([size(MASK),2]));
hold on;
glb_tpti_viewer.hM=imagesc(glb_tpti_viewer.MASK);
set(glb_tpti_viewer.hM,'AlphaData',glb_tpti_viewer.I.*glb_tpti_viewer.alpha_setting);

xl=get(gca,'XLim')
yl=get(gca,'YLim')
xlims=linspace(xl(1),xl(end),3);
ylims=linspace(yl(1),yl(end),3);
dum=repmat([1:length(ylims)-1],[length(xlims)-1,1]);
pos=[repmat([1:length(xlims)-1]',[length(ylims)-1,1]),dum(:)];
% for i=1:length(xlims)-1,
%     for j=1:length(ylims)-1,
%         set(gca,'XLim',[xlims(i),xlims(i+1)]);
%         set(gca,'YLim',[ylims(i),ylims(i+1)]);
%     end
% end
glb_tpti_viewer.xlims=xlims;
glb_tpti_viewer.ylims=ylims;
glb_tpti_viewer.pos=pos;
glb_tpti_viewer.curp=0;

caxis([glb_tpti_viewer.MI,glb_tpti_viewer.MA]);

function call_menu_extra_clicksel(src, eventdata)

global glb_tpti_viewer;
if strcmp(get(src,'Checked'),'on'),
    set(src,'Checked','off');
    set(gcf,'WindowButtonDownFcn',@call_buttondown);
    set(gcf,'WindowScrollWheelFcn',@call_wheel);
elseif strcmp(get(src,'Checked'),'off'),
    set(src,'Checked','on');
    set(gcf,'WindowButtonDownFcn',@call_buttondown_clicksel);
    set(gcf,'WindowScrollWheelFcn',@call_wheel_clicksel);
    
    if not(isfield(glb_tpti_viewer,'MOV')),
        tic;
        fid = fopen(glb_tpti_viewer.fname);
        ix = 1:glb_tpti_viewer.totfrm; %round(linspace(1,glb_tpti_viewer.totfrm,30));
        IMG = NaN([glb_tpti_viewer.dim(2),glb_tpti_viewer.dim(1),length(ix)]);
        for i=1:length(ix),
            rng=ix(i);
            
            % read in those frames
            fseek(fid, 2*(16/8) + (2*glb_tpti_viewer.dim(1)*glb_tpti_viewer.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
            ndat =fread(fid,[length(rng)*prod(glb_tpti_viewer.dim)],'*int16'); % read single frame
            ndatr=reshape(ndat,[glb_tpti_viewer.dim(2),glb_tpti_viewer.dim(1),length(rng)]); % size(datr)
            
            IMG(:,:,i) = squeeze(mean(ndatr,3))';
        end % for i=1:length(ix),
        glb_tpti_viewer.MOV = IMG;
        t=toc
    end
end

function call_maxproj(src, eventdata)

global glb_tpti_viewer;
if glb_tpti_viewer.dataformat~=2,
    fid = fopen(glb_tpti_viewer.fname);
    ix = 1:glb_tpti_viewer.totfrm; %round(linspace(1,glb_tpti_viewer.totfrm,30));
    IMG = NaN([glb_tpti_viewer.dim(2),glb_tpti_viewer.dim(1),length(ix)]);
    
    for i=1:length(ix),
        rng=ix(i);
        
        % read in those frames
        fseek(fid, 2*(16/8) + (2*glb_tpti_viewer.dim(1)*glb_tpti_viewer.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
        ndat =fread(fid,[length(rng)*prod(glb_tpti_viewer.dim)],'*int16'); % read single frame
        ndatr=reshape(ndat,[glb_tpti_viewer.dim(2),glb_tpti_viewer.dim(1),length(rng)]); % size(datr)
        
        IMG(:,:,i) = squeeze(mean(ndatr,3));
    end
    maxprojection = max(IMG,[],3);
    try, delete(glb_tpti_viewer.hIMG); end
    glb_tpti_viewer.hIMG=imagesc(maxprojection');colormap(gray);
    uistack(glb_tpti_viewer.hM,'top')
end %  if glb_tpti_viewer.dataformat~=2,

function call_meanproj(src, eventdata)

global glb_tpti_viewer;
if glb_tpti_viewer.dataformat~=2,
    fid = fopen(glb_tpti_viewer.fname);
    ix = 1:glb_tpti_viewer.totfrm; %round(linspace(1,glb_tpti_viewer.totfrm,30));
    IMG = NaN([glb_tpti_viewer.dim(2),glb_tpti_viewer.dim(1),length(ix)]);
    
    for i=1:length(ix),
        rng=ix(i);
        
        % read in those frames
        fseek(fid, 2*(16/8) + (2*glb_tpti_viewer.dim(1)*glb_tpti_viewer.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
        ndat =fread(fid,[length(rng)*prod(glb_tpti_viewer.dim)],'*int16'); % read single frame
        ndatr=reshape(ndat,[glb_tpti_viewer.dim(2),glb_tpti_viewer.dim(1),length(rng)]); % size(datr)
        
        IMG(:,:,i) = squeeze(mean(ndatr,3));
    end
    maxprojection = mean(IMG,3);
    try, delete(glb_tpti_viewer.hIMG); end
    glb_tpti_viewer.hIMG=imagesc(maxprojection');colormap(gray);
    uistack(glb_tpti_viewer.hM,'top')
end %  if glb_tpti_viewer.dataformat~=2,

function call_frame(src,eventdata)

global glb_tpti_viewer;
if strcmp('>',get(gcbo,'String')),
    glb_tpti_viewer.curp=glb_tpti_viewer.curp+1;
elseif strcmp('<',get(gcbo,'String')),
    glb_tpti_viewer.curp=glb_tpti_viewer.curp-1;
end
if glb_tpti_viewer.curp<0, glb_tpti_viewer.curp=0; end
if glb_tpti_viewer.curp>size(glb_tpti_viewer.pos,1), glb_tpti_viewer.curp=size(glb_tpti_viewer.pos,1); end

glb_tpti_viewer.curp
%call_plot([],[]);
updateframe=0;
updatemask=0;
updateboundary=0;
call_plot([],[],updateframe,updatemask,updateboundary);

% --------------------------------------------------------------------
function call_plot_roi_traces(src,eventdata)

global glb_tpti_viewer;
BW=glb_tpti_viewer.I';
[B,L,N,A]=bwboundaries(BW,'noholes'); % use with gcamp: only take shell, not inside
uROI = unique(L); uROI(uROI==0)=[]; % unique ROIs

% figure;imagesc(BW);

cfg=[];
cfg.mask    = L; % mask with number for every ROI
if glb_tpti_viewer.totfrm>1000, cfg.rng=30*30; else, cfg.rng=glb_tpti_viewer.totfrm; end
[out]       = resscn_roidat_sel(cfg); % edit resscn_segment


% --------------------------------------------------------------------
function call_show_roi_number(src,eventdata)

if strcmp('off',get(src,'Checked')),
    set(src,'Checked','on');
    
    global glb_tpti_viewer;
    BW=glb_tpti_viewer.I';
    
    if length(unique(BW))>2,
        B=unique(BW);B(B==0)=[];
        cix=1;
        cmap=jet(length(B))
        for i=1:length(B),
            [B1,L1]=bwboundaries(BW==B(i),'noholes');
            for j=1:length(B1),
                hold on;
                glb_tpti_viewer.RN(cix)=plot(B1{j}(:,1),B1{j}(:,2),'r','LineWidth',2,'Color',cmap(i,:));
                cix=cix+1;
                glb_tpti_viewer.RN(cix)=text(mean(B1{j}(:,1)),mean(B1{j}(:,2)),num2str(B(i)),'Color',[1 1 1],'HorizontalAlignment','center');
                cix=cix+1;
            end % for j=1:length(B1),
        end % for i=1:length(B),
    else
        [B,L,N,A]=bwboundaries(BW,'noholes'); % use with gcamp: only take shell, not inside
        uROI = unique(L); uROI(uROI==0)=[]; % unique ROIs
        
        cix=1;
        for i=1:length(B),
            hold on;
            glb_tpti_viewer.RN(cix)=plot(B{i}(:,1),B{i}(:,2),'r','LineWidth',2);
            cix=cix+1;
            glb_tpti_viewer.RN(cix)=text(mean(B{i}(:,1)),mean(B{i}(:,2)),num2str(i),'Color',[1 1 1],'HorizontalAlignment','center');
            cix=cix+1;
        end
    end
else
    set(src,'Checked','off');
    global glb_tpti_viewer;
    try, delete(glb_tpti_viewer.RN); end
end

% figure;imagesc(BW);

function call_showboundary(src,eventdata)
updateframe=0;
updatemask=0;
updateboundary=1;
call_plot([],[],updateframe,updatemask,updateboundary);

function call_menu_extra_erode(src,eventdata)

prompt = {'Set Radius Erode Disk:'};
dlg_title = 'Erode';
num_lines = 1;
def = {'3'};
answer = inputdlg(prompt,dlg_title,num_lines,def);
if ~isempty(answer), % in case user didn't press cancel
    disk_radius=str2num(answer{1}); % T=40
    
    global glb_tpti_viewer;
    BW = glb_tpti_viewer.I;
    
    se = strel('disk',disk_radius);
    BWeroded = imerode(BW,se);
    if 0,
        figure;subplot(2,1,1);imshow(BW);
        subplot(2,1,2);imshow(BWeroded);
    end
    glb_tpti_viewer.I=BWeroded;
    set(glb_tpti_viewer.hM,'AlphaData',glb_tpti_viewer.I.*glb_tpti_viewer.alpha_setting);
end

function call_menu_extra_clearcount(src,eventdata)

if strcmp(get(src,'Checked'),'on'),
    set(src,'Checked','off');
else
    set(src,'Checked','on');
end

function call_menu_extra_sizethreshold(src,eventdata)

prompt = {'Set Min Threshold:'};
dlg_title = 'Size Threshold';
num_lines = 1;
def = {'20'};
answer = inputdlg(prompt,dlg_title,num_lines,def);
if ~isempty(answer), % in case user didn't press cancel
    T=str2num(answer{1}); % T=40
    
    global glb_tpti_viewer;
    
    BW = glb_tpti_viewer.I;
    %figure;imagesc(BW);
    [B,L,N,A] = bwboundaries(BW);
    
    nB=zeros([1,length(B)]);
    for i=1:length(B),
        nB(i)=size(B{i},1);
    end
    L(ismember(L,find(nB<T)))=0;
    L(L>N)=0; % holes
    L=L>0;
    %close all;figure;imagesc(L);
    
    glb_tpti_viewer.I=L;
    set(glb_tpti_viewer.hM,'AlphaData',glb_tpti_viewer.I.*glb_tpti_viewer.alpha_setting);
    
    updateframe=0;
    updatemask=1;
    updateboundary=1;
    call_plot([],[],updateframe,updatemask,updateboundary);
end

function call_menu_extra_moviestepsize(src,eventdata)

global glb_tpti_viewer;

prompt = {'Set Movie Stepsize:'};
dlg_title = 'Stepsize';
num_lines = 1;
def = {num2str(glb_tpti_viewer.moviestepsize)};
answer = inputdlg(prompt,dlg_title,num_lines,def);
if ~isempty(answer), % in case user didn't press cancel
    glb_tpti_viewer.moviestepsize=str2num(answer{1});
end

function call_menu_extra_fillholes(src,eventdata)

global glb_tpti_viewer;
BW = glb_tpti_viewer.I;
[B,L,N,A] = bwboundaries(BW);
L(L>N)=1; % fill holes
L=L>0;
%figure;imagesc(L)
glb_tpti_viewer.I=L;

updateframe=0;
updatemask=1;
updateboundary=1;
call_plot([],[],updateframe,updatemask,updateboundary);

function call_wheel(src,eventdata)

global glb_tpti_viewer;

if eventdata.VerticalScrollCount>0,
    glb_tpti_viewer.brushsize = glb_tpti_viewer.brushsize-1;
else
    glb_tpti_viewer.brushsize = glb_tpti_viewer.brushsize+1;
end
if glb_tpti_viewer.brushsize<1,glb_tpti_viewer.brushsize=1; end

fprintf('brushsize = %d\n',glb_tpti_viewer.brushsize);

function call_wheel_clicksel(src,eventdata)

global glb_tpti_viewer;

if eventdata.VerticalScrollCount>0,
    glb_tpti_viewer.clicksel.threshold = glb_tpti_viewer.clicksel.threshold-0.01;
else
    glb_tpti_viewer.clicksel.threshold = glb_tpti_viewer.clicksel.threshold+0.01;
end
if glb_tpti_viewer.clicksel.threshold<0,glb_tpti_viewer.clicksel.threshold=0; end
if glb_tpti_viewer.clicksel.threshold>1,glb_tpti_viewer.clicksel.threshold=1; end

fprintf('clicksel.threshold = %1.2f\n',glb_tpti_viewer.clicksel.threshold);

function call_buttonup(src,eventdata)

set(gcf,'WindowButtonMotionFcn','');

function call_buttondown_clicksel(src,eventdata)

global glb_tpti_viewer;

selType = get(gcf,'SelectionType')
dum = get(gca,'CurrentPoint');
glb_tpti_viewer.clicksel.pix = 15;
pw=glb_tpti_viewer.clicksel.pix;
switch lower(selType)
    case 'normal' % left
        
        if 0,
            x=round(dum(1,2));y=round(dum(1,1));
            rngx=[x-pw:x+pw];rngy=[y-pw:y+pw];
            %hp=patch(rngx([1,end,end,1]),rngy([1,1,2,2]),[1 1 1],'FaceColor','none','EdgeColor',[1 1 1])
            
            I=mat2gray( glb_tpti_viewer.IMG(rngx,rngy) );
            
            level = glb_tpti_viewer.clicksel.threshold % graythresh(I)
            BW = im2bw(I, level)
            
            [B,L,N,A] = bwboundaries(BW);
            if not(isempty(B)),
                [Y,Ix]=max(cellfun(@length,B));
                L=L==Ix;
                
                [B,L,N,A] = bwboundaries(L);
                L(L>N)=1; % fill holes
                L=L>0;
                
                glb_tpti_viewer.I(rngx,rngy)=L;
            else
                glb_tpti_viewer.I(rngx,rngy)=0;
            end
        else
            
            if 0,
                figure;imagesc(I);
                figure;imagesc(L);
                hold on;patch
            end
            
            x=round(dum(1,2));y=round(dum(1,1));
            rngx=[x-pw:x+pw];rngy=[y-pw:y+pw];
            dum=glb_tpti_viewer.MOV(rngx,rngy,:);
            
            %
            %         %[X,Y]=meshgrid(rngx(1):rngx(2),rngy(1):rngy(2))
            %         [X,Y]=meshgrid(1:size(dum,1),1:size(dum,2));
            %         pixx= reshape(X,[size(X,1)*size(X,2),1]);
            %         pixy= reshape(Y,[size(Y,1)*size(Y,2),1]);
            %         figure;imagesc(X)
            
            % this can be much faster when using reshape etc.
            I=zeros([size(dum,1),size(dum,2)]);
            for i=1:size(dum,1),
                for j=1:size(dum,2),
                    I(i,j)=corr(squeeze(dum(pw+1,pw+1,:)),squeeze(dum(i,j,:)));
                end
            end
            %         dum3=reshape(shiftdim(dum,2),[size(dum,3),size(dum,1)*size(dum,2)]);
            %
            %         figure;plot(squeeze(dum(pw+1,pw+1,:)))
            %
            %         A=corr(dum3);
            %         B=reshape(A(pw+1,:),[size(dum,1),size(dum,2)]);
            %         isequalwithequalnans_tolerance(A(pw+1,:)',A(:,pw+1),0.001)
            %
            %         figure;imagesc(B);colorbar;
            %
            level = glb_tpti_viewer.clicksel.threshold
            %level = graythresh(I)
            BW = im2bw(I, level);
            [B,L,N,A] = bwboundaries(BW);
            if not(isempty(B)),
                [Y,Ix]=max(cellfun(@length,B));
                L=L==Ix;
                
                [B,L,N,A] = bwboundaries(L);
                L(L>N)=1; % fill holes
                L=L>0;
                
                Iorig = glb_tpti_viewer.I(rngx,rngy);
                glb_tpti_viewer.I(rngx,rngy)=Iorig+L;
                if 0,
                    figure;imagesc(nanmean(dum,3));
                    
                    figure;imagesc(glb_tpti_viewer.IMG(rngx,rngy));
                    
                    figure;imagesc(I);colorbar;
                    
                    glb_tpti_viewer.I
                    
                    figure;imagesc(L);
                    hold on;patch
                end
                
                    set(glb_tpti_viewer.hM,'AlphaData',glb_tpti_viewer.I.*glb_tpti_viewer.alpha_setting);
                if get(glb_tpti_viewer.handles.show_boundary,'Value')==1
                    try,
                        uistack(glb_tpti_viewer.BH,'top');
                    end
                end                
                k = waitforbuttonpress;
                if strcmp(get(gcf,'CurrentCharacter'),'c'), % cancel
                    glb_tpti_viewer.I(rngx,rngy)=Iorig;                    
                    set(glb_tpti_viewer.hM,'AlphaData',glb_tpti_viewer.I.*glb_tpti_viewer.alpha_setting);
                    if get(glb_tpti_viewer.handles.show_boundary,'Value')==1
                        try,
                            uistack(glb_tpti_viewer.BH,'top');
                        end
                    end
                end                
            else
                glb_tpti_viewer.I(rngx,rngy)=0;
            end % if not(isempty(B)),
        end   
    case 'alt' % right
        
    case 'extend' %
        
end



%
% % edit resscn_TI_segmentroi_batch_bouton.m
%   % compute local correlation of stack
%                 STK=IMG;
%                 CM=zeros([size(STK,1),size(STK,2)]);
%                 npix=1;
%                 for i=1+npix:size(STK,1)-npix,
%                     if mod(i,10)==0,fprintf('%d\n',i);end
%                     for j=1+npix:size(STK,2)-npix,
%                         dum=STK(i-npix:i+npix,j-npix:j+npix,:);
%
%                         dum3=reshape(shiftdim(dum,2),[size(dum,3),size(dum,1)*size(dum,2)]);
%                         A=corr(dum3);
%                         A(logical(eye(size(A))))=NaN;
%                         CM(i,j)=nanmean(nanmean(A));
%                     end
%                 end


function call_buttondown(src,eventdata)

selType = get(gcf,'SelectionType')

global glb_tpti_viewer;
switch lower(selType)
    case 'normal' % left
        if strcmp(get(glb_tpti_viewer.menu_extra_call_regiongrowing,'Checked'),'on'),
            dum = get(gca,'CurrentPoint');
            glb_tpti_viewer.clicksel.pix = 15;
            pw=glb_tpti_viewer.clicksel.pix;
            
            x=round(dum(1,2));y=round(dum(1,1));
            rngx=[x-pw:x+pw];rngy=[y-pw:y+pw];
            %hp=patch(rngx([1,end,end,1]),rngy([1,1,2,2]),[1 1 1],'FaceColor','none','EdgeColor',[1 1 1])
            
            I=mat2gray( glb_tpti_viewer.IMG(rngx,rngy) ); 
            %figure;imagesc(I);colorbar;pause;close;
            BW=regiongrowing(I,pw+1,pw+1, glb_tpti_viewer.clicksel.threshold); %, figure;imagesc(BW);colorbar;
            [B,L,N,A] = bwboundaries(BW);
            if not(isempty(B)),
                [Y,Ix]=max(cellfun(@length,B));
                L=L==Ix;
                
                [B,L,N,A] = bwboundaries(L);
                L(L>N)=1; % fill holes
                L=L>0;
                
                Iorig=glb_tpti_viewer.I(rngx,rngy);
                glb_tpti_viewer.I(rngx,rngy)=Iorig+L;
                
                set(glb_tpti_viewer.hM,'AlphaData',glb_tpti_viewer.I.*glb_tpti_viewer.alpha_setting);
                if get(glb_tpti_viewer.handles.show_boundary,'Value')==1
                    try,
                        uistack(glb_tpti_viewer.BH,'top');
                    end
                end                
                k = waitforbuttonpress;
                if strcmp(get(gcf,'CurrentCharacter'),'c'), % cancel
                    glb_tpti_viewer.I(rngx,rngy)=Iorig;                    
                    set(glb_tpti_viewer.hM,'AlphaData',glb_tpti_viewer.I.*glb_tpti_viewer.alpha_setting);
                    if get(glb_tpti_viewer.handles.show_boundary,'Value')==1
                        try,
                            uistack(glb_tpti_viewer.BH,'top');
                        end
                    end
                end
            end                   
        else
            set(gcf,'WindowButtonMotionFcn',@call_buttonmotion);
        end
    case 'alt' % right
        set(gcf,'WindowButtonMotionFcn',@call_buttonmotion);
    case 'extend' %
        dum = get(gca,'CurrentPoint');
        global glb_tpti_viewer;
        if strcmp(get(glb_tpti_viewer.menu_clearcount,'Checked'),'off'),
            if isfield(glb_tpti_viewer,'GP'),
                glb_tpti_viewer.GP(end+1)=text(dum(end,1),dum(end,2),num2str(length(glb_tpti_viewer.GP)+1),'Color',[0 1 0],'FontSize',12,'HorizontalAlignment','center');
            else
                %glb_tpti_viewer=rmfield(glb_tpti_viewer,'GP');
                glb_tpti_viewer.GP(1)=text(dum(end,1),dum(end,2),'1','Color',[0 1 0],'FontSize',12,'HorizontalAlignment','center');
            end
        else
            if isfield(glb_tpti_viewer,'GP'),
                if length(glb_tpti_viewer.GP)==1,
                    try,delete(glb_tpti_viewer.GP);end
                    try,glb_tpti_viewer=rmfield(glb_tpti_viewer,'GP');end
                else                   
                    D=NaN([length(glb_tpti_viewer.GP),1]);
                    for i=1:length(glb_tpti_viewer.GP),
                        dum2=get(glb_tpti_viewer.GP(i),'Position')
                        D(i)=sqrt(sum([dum2(1:2)-[dum(end,1),dum(end,2)]].^2))
                    end
                    [Y,ix]=min(D);
                    delete(glb_tpti_viewer.GP(ix));
                    glb_tpti_viewer.GP(ix)=[];
                   for i=1:length(glb_tpti_viewer.GP),
                       set(glb_tpti_viewer.GP(i),'String',num2str(i));
                   end
                end
            end
        end
end

function call_buttonmotion(src,eventdata)

global glb_tpti_viewer;

dum = get(gca,'CurrentPoint');
dum = round(dum(1,1:2));
br = [-glb_tpti_viewer.brushsize:glb_tpti_viewer.brushsize];
add = [br,zeros(size(br));zeros(size(br)),br]'
dum = repmat(dum,[size(add,1),1])+add;

bad=dum(:,1)<1|dum(:,2)<1|dum(:,1)>size(glb_tpti_viewer.I,2)|dum(:,2)>size(glb_tpti_viewer.I,1);
dum(bad,:)=[];

selType = get(gcf,'SelectionType')
switch lower(selType)
    case 'normal' % left
        glb_tpti_viewer.I(dum(:,2),dum(:,1))=1;
    case 'alt' % right
        glb_tpti_viewer.I(dum(:,2),dum(:,1))=0;
    case 'extend' % middle OR left + right
end %switch

set(glb_tpti_viewer.hM,'AlphaData',glb_tpti_viewer.I.*glb_tpti_viewer.alpha_setting);

if get(glb_tpti_viewer.handles.show_boundary,'Value')==1
    try,
        uistack(glb_tpti_viewer.BH,'top');
    end
end

function call_keypress(src,eventdata)
%WindowKeyPressFcn

global glb_tpti_viewer;

if strcmp(eventdata.Key,'leftarrow')==1|strcmp(eventdata.Key,'rightarrow')==1
    global glb_tpti_viewer;
    if strcmp(eventdata.Key,'leftarrow')==1
        glb_tpti_viewer.frame=glb_tpti_viewer.frame-1;
    elseif strcmp(eventdata.Key,'rightarrow')==1
        glb_tpti_viewer.frame=glb_tpti_viewer.frame+1;
    end
    if glb_tpti_viewer.frame<1,
        glb_tpti_viewer.frame=1;
    elseif glb_tpti_viewer.frame>glb_tpti_viewer.totfrm,
        glb_tpti_viewer.frame=glb_tpti_viewer.totfrm;
    end
    updateframe=1;
    updatemask=0;
    updateboundary=0;
    call_plot([],[],updateframe,updatemask,updateboundary);
elseif strcmp(eventdata.Key,'r'),
    if strcmp(get(glb_tpti_viewer.menu_extra_call_regiongrowing,'Checked'),'on'),
        set(glb_tpti_viewer.menu_extra_call_regiongrowing,'Checked','off')
        set(gcf,'WindowScrollWheelFcn',@call_wheel);
        fprintf('stop region growing...\n');
    else
        set(glb_tpti_viewer.menu_extra_call_regiongrowing,'Checked','on')
        set(gcf,'WindowScrollWheelFcn',@call_wheel_clicksel);
        fprintf('start region growing...\n');
    end       
 elseif strcmp(eventdata.Key,'x'),
    if strcmp(get(glb_tpti_viewer.menu_show_red,'Checked'),'on'),
        set(glb_tpti_viewer.menu_show_red,'Checked','off');
    else
        set(glb_tpti_viewer.menu_show_red,'Checked','on');
    end
    updateframe=1;
    updatemask=0;
    updateboundary=0;
    call_plot([],[],updateframe,updatemask,updateboundary);
elseif strcmp(eventdata.Key,'uparrow')==1|strcmp(eventdata.Key,'downarrow')==1,
    if strcmp(eventdata.Key,'uparrow')==1
        glb_tpti_viewer.alpha_setting = glb_tpti_viewer.alpha_setting+0.1;
        fprintf('alpha = %1.2f\n',glb_tpti_viewer.alpha_setting);
    elseif strcmp(eventdata.Key,'downarrow')==1
        glb_tpti_viewer.alpha_setting = glb_tpti_viewer.alpha_setting-0.1;
        fprintf('alpha = %1.2f\n',glb_tpti_viewer.alpha_setting);
    end
    if glb_tpti_viewer.alpha_setting<0,
        glb_tpti_viewer.alpha_setting=0;
    elseif glb_tpti_viewer.alpha_setting>1,
        glb_tpti_viewer.alpha_setting=1;
    end
    set(glb_tpti_viewer.hM,'AlphaData',glb_tpti_viewer.I.*glb_tpti_viewer.alpha_setting);
elseif strcmp(eventdata.Key,'b')
    if get(glb_tpti_viewer.handles.show_boundary,'Value')==1,
        set(glb_tpti_viewer.handles.show_boundary,'Value',0);
    else
        set(glb_tpti_viewer.handles.show_boundary,'Value',1);
    end
    updateframe=0;
    updatemask=0;
    updateboundary=1;
    call_plot([],[],updateframe,updatemask,updateboundary);
elseif strcmp(eventdata.Key,'m')
    call_maxproj([],[]);
elseif strcmp(eventdata.Key,'n')
    call_meanproj([],[]);    
elseif strcmp(eventdata.Key,'p'),
    %stop=0;
    %oldkpf=get(gcf,'KeyPressFcn');
    %set(gcf,'KeyPressFcn','stop=1;')
    for i=1:glb_tpti_viewer.moviestepsize:glb_tpti_viewer.totfrm,
        glb_tpti_viewer.frame = i;
        %eventdata.Key
        updateframe=1;
        updatemask=0;
        updateboundary=0;
        selType = get(gcf,'SelectionType');
        if strcmp(selType,'alt')
            %if strcmp(eventdata.Key,'leftbracket')
            break;
        end
        pause(0.1);
        call_plot([],[],updateframe,updatemask,updateboundary);
    end
    %set(gcf,'KeyPressFcn',oldkpf);
elseif strcmp(eventdata.Key,'f')
    call_menu_extra_fillholes([],[])
elseif strcmp(eventdata.Key,'s')
    T=20;
    global glb_tpti_viewer;
    BW = glb_tpti_viewer.I;
    [B,L,N,A] = bwboundaries(BW);
    
    nB=zeros([1,length(B)]);
    for i=1:length(B),
        nB(i)=size(B{i},1);
    end
    L(ismember(L,find(nB<T)))=0;
    L(L>N)=0; % holes
    L=L>0;
    
    glb_tpti_viewer.I=L;
    set(glb_tpti_viewer.hM,'AlphaData',glb_tpti_viewer.I.*glb_tpti_viewer.alpha_setting);
    
    updateframe=0;
    updatemask=1;
    updateboundary=1;
    call_plot([],[],updateframe,updatemask,updateboundary);
else
    dum = get(gca,'CurrentPoint');
    dum = round(dum(1,1:2)) % dum=[300,200]
    
    a1=get(gca,'XLim')
    a2=get(gca,'YLim')
    
    rngx = [a1(2)-a1(1)];
    rngy = [a2(2)-a2(1)];
    
    Isiz = size(glb_tpti_viewer.IMG);
    eventdata.Key
    % zoom in, centered on current position
    if strcmp(eventdata.Key, 'equal')==1
        rngxnew = rngx.*0.9; rngynew = rngy.*0.9;
    elseif strcmp(eventdata.Key, 'hyphen')==1
        rngxnew = rngx.*1.1; rngynew = rngy.*1.1;
    else
        rngxnew = rngx.*1; rngynew = rngy.*1;
    end
    if strcmp(eventdata.Key,'equal')|strcmp(eventdata.Key, 'hyphen'),
        if rngxnew>size(glb_tpti_viewer.IMG,2), rngxnew = Isiz(2); end
        if rngynew>size(glb_tpti_viewer.IMG,1), rngynew = Isiz(1); end
        a1n=[dum(1)-rngxnew/2,dum(1)+rngxnew/2]; if any(a1n<1), a1n=[1,rngxnew]; elseif any(a1n>Isiz(2)), a1n=[Isiz(2)-rngxnew,Isiz(2)]; end
        a2n=[dum(2)-rngynew/2,dum(2)+rngynew/2]; if any(a2n<1), a2n=[1,rngynew]; elseif any(a2n>Isiz(1)), a2n=[Isiz(1)-rngynew,Isiz(1)]; end
        set(gca,'XLim',a1n,'YLim',a2n);
    end
    glb_tpti_viewer.curp=0;
end

function call_keypress_clicksel(src,eventdata)

global glb_tpti_viewer;

if strcmp(eventdata.Key,'leftarrow')==1|strcmp(eventdata.Key,'rightarrow')==1
    global glb_tpti_viewer;
end

function call_slider(src,eventdata)

global glb_tpti_viewer;
glb_tpti_viewer.frame = round((get(src,'Value').*(glb_tpti_viewer.totfrm-1))+1);
updateframe=1;
updatemask=0;
updateboundary=0;
call_plot([],[],updateframe,updatemask,updateboundary);

function call_editcmin(src,eventdata)

global glb_tpti_viewer;
glb_tpti_viewer.MI = str2num(get(src,'String'));
updateframe=0;
updatemask=0;
updateboundary=0;
%call_plot([],[],updateframe,updatemask,updateboundary);
caxis([glb_tpti_viewer.MI,glb_tpti_viewer.MA])


function call_editcmax(src,eventdata)

global glb_tpti_viewer;
glb_tpti_viewer.MA = str2num(get(src,'String'));
updateframe=0;
updatemask=0;
updateboundary=0;
%call_plot([],[],updateframe,updatemask,updateboundary);
caxis([glb_tpti_viewer.MI,glb_tpti_viewer.MA])


function call_editnavg(src,eventdata)

global glb_tpti_viewer;
glb_tpti_viewer.navg = round(str2num(get(src,'String')));
updateframe=1;
updatemask=0;
updateboundary=0;
call_plot([],[],updateframe,updatemask,updateboundary);

function call_editmedfilt(src,eventdata)

global glb_tpti_viewer;
glb_tpti_viewer.medfilt = round(str2num(get(src,'String')));
updateframe=1;
updatemask=0;
updateboundary=0;
call_plot([],[],updateframe,updatemask,updateboundary);

function call_plot(src,eventdata,updateframe,updatemask,updateboundary)

global glb_tpti_viewer;

if updateframe,
    if strcmp(get(glb_tpti_viewer.menu_show_red,'Checked'),'on')&isfield(glb_tpti_viewer,'REDIMG'),
        glb_tpti_viewer.IMG = glb_tpti_viewer.REDIMG;
    else    
    rng=[glb_tpti_viewer.frame:glb_tpti_viewer.frame+glb_tpti_viewer.navg-1]; rng(rng<1)=[]; rng(rng>glb_tpti_viewer.totfrm)=[];
    % read in those frames
    fid = fopen(glb_tpti_viewer.fname);
    fseek(fid, 2*(16/8) + (2*glb_tpti_viewer.dim(1)*glb_tpti_viewer.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
    ndat =fread(fid,[length(rng)*prod(glb_tpti_viewer.dim)],'*int16'); % read single frame
    ndatr=reshape(ndat,[glb_tpti_viewer.dim(2),glb_tpti_viewer.dim(1),length(rng)]); % size(datr)
    fclose(fid);
    
    if glb_tpti_viewer.dataformat==2,
        IMG = zeros([length(glb_tpti_viewer.patch.y), length(glb_tpti_viewer.patch.x), length(rng)], 'int16');
        
        for i=1:size(ndatr,3),
            I = squeeze(ndatr(:,:,i));
            if glb_tpti_viewer.subpixel
                x0 = glb_tpti_viewer.ops.del1(rng(i));
                y0 = glb_tpti_viewer.ops.del2(rng(i));
                F = griddedInterpolant(glb_tpti_viewer.sp.X1, glb_tpti_viewer.sp.X2,  double(I), 'linear');
                [X1n, X2n]  = ndgrid([1:glb_tpti_viewer.sp.sizX]-x0,[1:glb_tpti_viewer.sp.sizY]-y0);
                I = F(X1n(glb_tpti_viewer.patch.y, glb_tpti_viewer.patch.x), X2n(glb_tpti_viewer.patch.y, glb_tpti_viewer.patch.x));
                IMG(:,:,i) = int16(I);
            else
                J = zeros(size(IMG,1), size(IMG,2), 'int16');
                x0 = round(glb_tpti_viewer.ops.del2(rng(i)));
                y0 = round(glb_tpti_viewer.ops.del1(rng(i)));
                cy = (glb_tpti_viewer.patch.y - y0);
                cx = (glb_tpti_viewer.patch.x - x0);
                Iy = cy > 0 & cy < size(I,1);
                Ix = cx > 0 & cx < size(I,2);
                J(Iy, Ix) = I(cy(Iy), cx(Ix));
                IMG(:,:,i) = J;
            end
        end
        ndatr=IMG;
    end
    
    if glb_tpti_viewer.medfilt~=0,
        for i=1:size(ndatr,3),
            ndatr(:,:,i) = medfilt2(squeeze(ndatr(:,:,i)),[glb_tpti_viewer.medfilt,glb_tpti_viewer.medfilt]);
        end
    end
    glb_tpti_viewer.IMG = squeeze(mean(ndatr,3))';
    title(num2str(glb_tpti_viewer.frame));
    end % if strcmp(get(glb_tpti_viewer.menu_show_red,'Checked'),'on'),
end
% if glb_tpti_viewer.medfilt~=0,
%     glb_tpti_viewer.IMG=medfilt2(glb_tpti_viewer.IMG,[glb_tpti_viewer.medfilt,glb_tpti_viewer.medfilt]);
% end

if updateframe,
    try, delete(glb_tpti_viewer.hIMG); end
    glb_tpti_viewer.hIMG=imagesc(glb_tpti_viewer.IMG);
    uistack(glb_tpti_viewer.hM,'top')
end
if updatemask,
    set(glb_tpti_viewer.hM,'AlphaData',glb_tpti_viewer.I.*glb_tpti_viewer.alpha_setting);
end

if get(glb_tpti_viewer.handles.show_boundary,'Value')==1&updateboundary,
    try,
        delete(glb_tpti_viewer.BH);
    end
    [B] = bwboundaries(glb_tpti_viewer.I);
    clear BH;
    for i=1:length(B),
        hold on;BH(i)=plot(B{i}(:,2),B{i}(:,1),'w','Color',[0.5 0.5 0.5]);
    end
    glb_tpti_viewer.BH=BH;
elseif get(glb_tpti_viewer.handles.show_boundary,'Value')==0&updateboundary,
    try,
        delete(glb_tpti_viewer.BH);
    end
elseif get(glb_tpti_viewer.handles.show_boundary,'Value')==1,
    try,
        uistack(glb_tpti_viewer.BH,'top');
    end
end

if strcmp('on',get(glb_tpti_viewer.menu_extra_show_roi_number,'Checked')),
    try,
        uistack(glb_tpti_viewer.RN,'top');
    end
end

if isfield(glb_tpti_viewer,'GP'),
    uistack(glb_tpti_viewer.GP,'top');
end

if glb_tpti_viewer.curp~=0,
    set(gca,'XLim',[glb_tpti_viewer.xlims(glb_tpti_viewer.pos(glb_tpti_viewer.curp,1)),glb_tpti_viewer.xlims(glb_tpti_viewer.pos(glb_tpti_viewer.curp,1)+1)]);
    set(gca,'YLim',[glb_tpti_viewer.ylims(glb_tpti_viewer.pos(glb_tpti_viewer.curp,2)),glb_tpti_viewer.ylims(glb_tpti_viewer.pos(glb_tpti_viewer.curp,2)+1)]);
end

%glb_tpti_viewer.impixelinfo = impixelinfo(glb_tpti_viewer.handles.f);

function my_closefcn(src,eventdata)
% User-defined close request function
% to display a question dialog box
if 0,
    selection = questdlg('Close This Figure?',...
        'Close Request Function',...
        'Yes','No','Yes');
    switch selection,
        case 'Yes',
            delete(gcf);
        case 'No'
            return
    end
end
delete(gcf);

function out=resscn_roidat_sel(cfg);

global glb_tpti_viewer;

filedat = glb_tpti_viewer.fname;
l = dir(filedat); % see how big the file is and how much datapoints it contains edit FileSize % find total number of frames in data
fid = fopen(filedat);
dim=fread(fid,2,'int16'); % size of image
totfrm = (l.bytes-2*(16/8))./(2*prod(dim)); % total number of frames
fclose(fid);

% check data size of data file matches with ROI
if ~isequal(fliplr(dim'),size(cfg.mask)),
    error('mismatch format binary file and ROI');
end
uROI = unique(cfg.mask); uROI(uROI==0)=[]; % unique ROIs

chnix = 1;
totfrm = cfg.rng;
out.DAT{chnix} = NaN([length(uROI),totfrm]);
frame  = 1;
blksiz = 1000;
fid = fopen(filedat);
for T=1:ceil(totfrm/blksiz), % for every trial
    if mod(T,2)==0, fprintf('data segment %1.2d(of %d):%s\n',T,ceil(totfrm/blksiz),datestr(now)); end
    %end
    rng=[frame:frame+blksiz-1]; rng(rng<1)=[]; rng(rng>totfrm)=[];
    
    % read in those frames
    fseek(fid, 2*(16/8) + (2*dim(1)*dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
    ndat =fread(fid,[length(rng)*prod(dim)],'*int16'); % read single frame
    ndatr=reshape(ndat,[dim(2),dim(1),length(rng)]); % size(ndatr)
    
    % see edit script_test_extractROItimecourse
    p = size(ndatr,3);
    
    ndatr = double(ndatr);
    TC2=NaN([length(uROI),size(ndatr,3)]); % fastest
    for R=1:length(uROI),
        b = cfg.mask==uROI(R);
        TC2(R,:) = b(:).'*reshape(ndatr,[],p)/nnz(b);
    end
    %t=toc;
    %fprintf('trial %d, t=%1.2f\n',T,t);
    out.DAT{chnix}(:,rng) = TC2; % size(DAT),size(TC2)
    
    frame = frame + blksiz;
end % for T=1:length(sel.trl), % for every trial\
fclose(fid);

% Jia, 2011
% 1. take mean F in ROI
% 2. calculate time dependent baseline
SFi = 30;
t0=0.2;
t1=0.75;
t2=3;

nsmp    = round(t1*SFi); if mod(nsmp,2)==0, nsmp=nsmp+1; end
nsmppre = round(t2*SFi);
for i=1:size(out.DAT{chnix},1), %i=6
    data =  squeeze(out.DAT{chnix}(i,:));
    
    F0   = smooth(data,nsmp,'moving');
    Fm=NaN(size(data));
    for s=1:length(data),
        rng  = [s-nsmppre:s-1]; rng(rng<1|rng>length(data))=[];
        if ~isempty(rng), Fm(s)= min(F0(rng)); end
    end
    R = (data-Fm)./Fm;
    Rf = medfilt1(R,3);
    
    if 0,
        figure;plot(frmtim,data,'b');
        hold on;plot(frmtim,F0,'r');
        hold on;plot(frmtim,Fm,'g');
        
        figure;plot(frmtim,R,'b');
        hold on;plot(frmtim,Rf,'r');
    end
    out.dFj{chnix}(i,:)=Rf;
end

% compute SNR of traces
% edit resscn_traces_SNR.m
resscn_traces_SNR;

% plot
cmap=jet(size(out.DAT{chnix},1));
figure;
for i=1:size(out.DAT{chnix},1),
    %hold on;plot([1,size(out.DAT{chnix},2)]./SFi,[i,i],'b--','Color',cmap(i,:));
    %hold on;plot([1:size(out.DAT{chnix},2)]./SFi,i+out.dFj{chnix}(i,:)./max(out.dFj{chnix}(i,:)),'Color',cmap(i,:));
    hold on;plot([1,size(out.DAT{chnix},2)],[i,i],'b--','Color',cmap(i,:));
    hold on;plot([1:size(out.DAT{chnix},2)],i+out.dFj{chnix}(i,:)./max(out.dFj{chnix}(i,:)),'Color',cmap(i,:));
    dum=get(gca,'XLim');
    text(mean(dum),i+0.5,sprintf('snr=%1.2f',SNR.snr1(i)));
end
set(gca,'YLim',[0,size(out.DAT{chnix},1)+1]);

