function mystackdatviewer_CRSP(cfg);

if 0,
    set(0,'ShowHiddenHandles','on')
    delete(get(0,'Children'));
end

global mystackA;
global mystackU;
%mystackA.conversion = cfg.conversion;

if ~isfield(cfg,'firstpreprocframe'),
    cfg.firstpreprocframe = 1;
end

% if ~isfield(cfg,'immuno'),
%    
% end
mystackA.cfg = cfg;
mystackA.celllabel = 'Empty';

% edit mystackviewer_segment.m
if isfield(cfg,'fnameRED'),    
    % do this once: compute max and mean projections for later use
    clear MAXP MEANP;
    for image_number=1:length(mystackA.cfg.fnameRED),
        mystackU.fname = mystackA.cfg.fnameRED{image_number};
        l = dir(mystackU.fname); % see how big the file is and how much datapoints it contains edit FileSize % find total number of frames in data
        fid = fopen(mystackU.fname);
        mystackU.dim    = fread(fid,2,'int16'); % size of image
        mystackU.totfrm = (l.bytes-2*(16/8))./(2*prod(mystackU.dim)); % total number of frames
        fclose(fid);
        rng=[1:mystackU.totfrm];
        fid = fopen(mystackU.fname);
        fseek(fid, 2*(16/8) + (2*mystackU.dim(1)*mystackU.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
        ndat =fread(fid,[length(rng)*prod(mystackU.dim)],'*int16'); % read single frame
        ndatr=reshape(ndat,[mystackU.dim(2),mystackU.dim(1),length(rng)]); % size(datr)
        fclose(fid);
        MAXP{image_number} = squeeze(max(ndatr(:,:,cfg.firstpreprocframe:end),[],3))';
        MEANP{image_number} = squeeze(mean(ndatr(:,:,cfg.firstpreprocframe:end),3))';
    end
    mystackA.REDMAXP  = MAXP;
    mystackA.REDMEANP = MEANP;
end

% do this once: compute max and mean projections for later use
clear MAXP MEANP;
for image_number=1:length(mystackA.cfg.fname),
    mystackU.fname = mystackA.cfg.fname{image_number};
    l = dir(mystackU.fname); % see how big the file is and how much datapoints it contains edit FileSize % find total number of frames in data
    fid = fopen(mystackU.fname);
    mystackU.dim    = fread(fid,2,'int16'); % size of image
    mystackU.totfrm = (l.bytes-2*(16/8))./(2*prod(mystackU.dim)); % total number of frames
    fclose(fid);
    rng=[1:mystackU.totfrm];
    fid = fopen(mystackU.fname);
    fseek(fid, 2*(16/8) + (2*mystackU.dim(1)*mystackU.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
    ndat =fread(fid,[length(rng)*prod(mystackU.dim)],'*int16'); % read single frame
    ndatr=reshape(ndat,[mystackU.dim(2),mystackU.dim(1),length(rng)]); % size(datr)
    fclose(fid);
    %MAXP{image_number} = squeeze(max(ndatr,[],3))';
    %MEANP{image_number} = squeeze(mean(ndatr,3))';
    MAXP{image_number} = squeeze(max(ndatr(:,:,cfg.firstpreprocframe:end),[],3))';
    MEANP{image_number} = squeeze(mean(ndatr(:,:,cfg.firstpreprocframe:end),3))';
end
mystackA.MAXP  = MAXP;
mystackA.MEANP = MEANP;

%--------------------------------------------------------------------------
% make GUI

mystackA.handles.f = figure('Units','normalized','Position',[0.2 0.3 0.7 0.5]);
set(mystackA.handles.f,'CloseRequestFcn',@my_closefcn);

%set(gcf,'menubar','none')
mystackA.menu_file = uimenu(gcf,'Label','File');
mystackA.menu_sessionlist{1}  = uimenu(mystackA.menu_file,'Label','Select Session',...
    'Callback',{@call_menu_Image_sessionlist});
mystackA.menu_file_generatecorrespondencefiles = uimenu(mystackA.menu_file,'Label','Generate Correspondence Files',...
    'Callback',{@call_menu_file_generatecorrespondencefiles});
mystackA.menu_file_generatecorrespondencefiles = uimenu(mystackA.menu_file,'Label','Resort ROIs',...
    'Callback',{@call_menu_file_resortROIs});
mystackA.menu_file_refreshsegfil = uimenu(mystackA.menu_file,'Label','Refresh Segmentation Files',...
    'Callback',{@call_menu_file_refreshsegfil});
mystackA.menu_file_refreshsegfil = uimenu(mystackA.menu_file,'Label','Make All ROI Instances Unique',...
    'Callback',{@call_menu_file_makeallroiinstancesunique});

mystackA.menu_file_refreshsegfil = uimenu(mystackA.menu_file,'Label','Plot ROI across Sessions',...
    'Callback',{@call_menu_file_roisessions});

mystackA.menu_file_maskshiftafterwarp = uimenu(mystackA.menu_file,'Label','Mask Shift After Warp',...
    'Callback',{@call_menu_file_maskshiftafterwarp});

mystackA.menu_image{1} = uimenu(gcf,'Label','Image1');
mystackA.menu_image_clim{1}  = uimenu(mystackA.menu_image{1},'Label','Color Limits',...
    'Callback',{@call_menu_Image_clim,1});
mystackA.menu_image_reviewsegfil{1}  = uimenu(mystackA.menu_image{1},'Label','Review Segmentation',...
    'Callback',{@call_menu_Image_reviewsegfil,1});
mystackA.menu_image_makeuniqueprevious{1}  = uimenu(mystackA.menu_image{1},'Label','create unique ROI linked with previous sessions',...
    'Callback',{@call_menu_Image_makeuniquepreviousnext,1,1});
mystackA.menu_image_makeuniquenext{1}  = uimenu(mystackA.menu_image{1},'Label','create unique ROI linked with next sessions',...
    'Callback',{@call_menu_Image_makeuniquepreviousnext,1,2});

mystackA.menu_image{2} = uimenu(gcf,'Label','Image2');
mystackA.menu_image_clim{2}  = uimenu(mystackA.menu_image{2},'Label','Color Limits',...
    'Callback',{@call_menu_Image_clim,2});
mystackA.menu_image_reviewsegfil{2}  = uimenu(mystackA.menu_image{2},'Label','Review Segmentation',...
    'Callback',{@call_menu_Image_reviewsegfil,2});
mystackA.menu_image_makeuniqueprevious{2}  = uimenu(mystackA.menu_image{2},'Label','create unique ROI linked with previous sessions',...
    'Callback',{@call_menu_Image_makeuniquepreviousnext,2,1});
mystackA.menu_image_makeuniquenext{2}  = uimenu(mystackA.menu_image{2},'Label','create unique ROI linked with next sessions',...
    'Callback',{@call_menu_Image_makeuniquepreviousnext,2,2});

mystackA.menu_label = uimenu(gcf,'Label','Cell labels');
mystackA.menu_labeladdimmuno = uimenu(mystackA.menu_label,'Label','Add immunolabel',...
    'Callback',{@call_menu_labeladdremove,1});
mystackA.menu_labeladd = uimenu(mystackA.menu_label,'Label','Add label',...
    'Callback',{@call_menu_labeladdremove,2});
mystackA.menu_labelremove = uimenu(mystackA.menu_label,'Label','Remove label',...
    'Callback',{@call_menu_labeladdremove,3});
mystackA.menu_labelchange = uimenu(mystackA.menu_label,'Label','Change label',...
    'Callback',{@call_menu_labelchange});
mystackA.menu_labelhide = uimenu(mystackA.menu_label,'Label','Hide numbers',...
    'Callback',{@call_menu_hidenumbers});
mystackA.menu_maxproj = uimenu(mystackA.menu_label,'Label','Use Maxproj',...
    'Callback',{@call_menu_maxproj});
% mystackA.menu_maxproj = uimenu(mystackA.menu_label,'Label','Realign Immunostack',...
%     'Callback',{@call_realign_immunostack});
mystackA.menu_show_red = uimenu(mystackA.menu_label,'Label','Show Red Channel (X)',...
    'Callback',{@call_menu_show_red});

set(gcf,'WindowKeyPressFcn',@call_keypress);
set(gcf,'WindowButtonDownFcn',@call_buttondown);
%set(gcf,'WindowButtonUpFcn',@call_buttonup);
%set(gcf,'WindowScrollWheelFcn',@call_wheel);

% plot image
mystackA.ax(1)=axes('Parent',mystackA.handles.f,'Units','normalized','Position',[0.05 0.1 0.45 0.8]);
title('awsd');
mystackA.ax(2)=axes('Parent',mystackA.handles.f,'Units','normalized','Position',[0.03+0.45+0.03 0.1 0.45 0.8]);
title('jikl');

if 0,
% add slider and edit box for color limits
% dum=[Inf,-Inf];
% for i=1:length(mystackA.MEANP),
%     if min(min(mystackA.MEANP{i}))<dum(1),dum(1)=min(min(mystackA.MEANP{i})); end
%     if max(max(mystackA.MEANP{i}))>dum(2),dum(2)=max(max(mystackA.MEANP{i})); end
% end
mystackA.range
uicontrol('Parent',mystack.handles.f,'Units','normalized','Style', 'slider',...
    'Position', [0.01 0.01 0.2 0.05],...
    'Min',0,'Max',1,'Value',0.5,'SliderStep',[1 100]./(mystack.totfrm-1),'Callback', @call_slider);
uicontrol('Parent',mystack.handles.f,'Units','normalized','Style','edit',...
    'Position', [0.25 0.01 0.1 0.05],...
    'String',num2str(mystack.navg),'Callback', @call_editnavg);
end

% initialize once
mystackA.cmp = [1 2];
% init

for image_number=1:2,
    if image_number<=length(mystackA.cfg.fname),
        %mystackU = mystackA.S{image_number};
        mystackU.fname = mystackA.cfg.fname{mystackA.cmp(image_number)};
        l = dir(mystackU.fname); % see how big the file is and how much datapoints it contains edit FileSize % find total number of frames in data
        fid = fopen(mystackU.fname);
        mystackU.dim    = fread(fid,2,'int16'); % size of image
        mystackU.totfrm = (l.bytes-2*(16/8))./(2*prod(mystackU.dim)); % total number of frames
        fclose(fid);
        
        mystackU.navg  = 1;
        mystackU.frame = 1;
        mystackU.framestep = 30;
        mystackU.medfilt = 0;
        
        mystackA.S{image_number}=mystackU;
    end % if image_number<=length(mystackA.cfg.fname),
end
if length(mystackA.cfg.fname)==1,
    set(mystackA.handles.f,'Name',sprintf('%s',mystackA.cfg.fname{mystackA.cmp(1)}));
else
    set(mystackA.handles.f,'Name',sprintf('%s vs %s',mystackA.cfg.fname{mystackA.cmp(1)},mystackA.cfg.fname{mystackA.cmp(2)}));
end

for image_number=1:2,
    if image_number<=length(mystackA.cfg.fname),
        mystackU=mystackA.S{image_number};
        %--------------------------------------------------------------------------
        % get a few frames to determine scaling factors
        fid = fopen(mystackU.fname);
        ix = round(linspace(1,mystackU.totfrm,30));
        IMG = NaN([mystackU.dim(2),mystackU.dim(1),length(ix)]);
        for i=1:length(ix),
            rng=ix(i);
            % read in those frames
            fseek(fid, 2*(16/8) + (2*mystackU.dim(1)*mystackU.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
            ndat =fread(fid,[length(rng)*prod(mystackU.dim)],'*int16'); % read single frame
            ndatr=reshape(ndat,[mystackU.dim(2),mystackU.dim(1),length(rng)]); % size(datr)
            IMG(:,:,i) = squeeze(mean(ndatr,3));
        end
        fclose(fid);
        if isfield(mystackA.cfg,'MI'),
            mystackU.MI=mystackA.cfg.MI;
        else
            mystackU.MI=min(min(nanmean(IMG,3)));
        end
        if ~isfield(mystackU,'MA'),
            if isfield(mystackA.cfg,'MA'),
                mystackU.MA=mystackA.cfg.MA;
            else
                mystackU.MA=max(max(nanmean(IMG,3)));
            end
        end
        mystackU.IMG = mean(IMG,3)';
        
        axes(mystackA.ax(image_number));
        mystackU.hIMG=imagesc(mystackU.IMG);colormap(gray);
        caxis([mystackU.MI,mystackU.MA]);
        %set(mystackU.hIMG,'ButtonDownFcn',{@call_clickinaxis,image_number});
        
        [x,y,z]=fileparts(mystackU.fname);
        try,str=mystackA.cfg.ROIsitenam;catch,str='';end
        title(sprintf('%s,%s,frm%d',str,y,mystackU.frame),'Interpreter','none');
        %mystackU.M = cfg.M{image_number};
        %mystackU.C = cfg.C{image_number};
        
        xl=get(gca,'XLim');
        yl=get(gca,'YLim');
        xlims=linspace(xl(1),xl(end),3);
        ylims=linspace(yl(1),yl(end),3);
        dum=repmat([1:length(ylims)-1],[length(xlims)-1,1]);
        pos=[repmat([1:length(xlims)-1]',[length(ylims)-1,1]),dum(:)];
        
        mystackU.xlims=xlims;
        mystackU.ylims=ylims;
        mystackU.pos=pos;
        mystackU.curp=0;
        
        mystackA.S{image_number}=mystackU;
        call_plot_boundaries([],[],image_number)
    end % if image_number<=length(mystackA.cfg.fname),
end

for image_number=1:2,
    if image_number<=length(mystackA.cfg.fname),
        if strcmp(get(mystackA.menu_maxproj,'Checked'),'off'),
            call_plot([],[],image_number);
        else
            call_maxproj([],[],image_number);
        end
    end % if image_number<=length(mystackA.cfg.fname),
end

%call_menu_file_resortROIs([],[]);
function call_menu_Image_makeuniquepreviousnext(src,eventdata,image_number,previousornext)

global mystackA;
mystackU=mystackA.S{image_number};
CRSP =mystackA.cfg.CRSP{mystackA.cmp(image_number)};
oldid = CRSP.conversion(mystackU.cix,2);

maxROIid = 0;
for i=1:length(mystackA.cfg.CRSP),
    if max(mystackA.cfg.CRSP{i}.conversion(:,2))>maxROIid,
        maxROIid = max(mystackA.cfg.CRSP{i}.conversion(:,2));
    end
end
newid = maxROIid+1;
set(mystackU.RN(mystackU.cix),'String',num2str(newid));

if previousornext==1, % link with previous
    for i=1:mystackA.cmp(image_number), % for each session
        ix=ismember(mystackA.cfg.CRSP{i}.conversion(:,2),oldid);
        mystackA.cfg.CRSP{i}.conversion(ix,2)=newid;
    end
elseif previousornext==2, % link with next
    for i=mystackA.cmp(image_number):length(mystackA.cfg.CRSP), % for each session
        ix=ismember(mystackA.cfg.CRSP{i}.conversion(:,2),oldid);
        i,find(ix)
        mystackA.cfg.CRSP{i}.conversion(ix,2)=newid;
    end
end
if 1,
    MSG=check_duplicate_ROIs(mystackA.cfg.CRSP);
    if length(MSG)>0, % check if already ROI with this ID exist in this session
        errordlg(['Duplicate ROIs:',MSG],'Duplicate ROI');
    end
end

function call_menu_hidenumbers(src,eventdata)

if strcmp(get(src,'Checked'),'off')
    set(src,'Checked','on');    
else
    set(src,'Checked','off');    
end

for image_number=1:2,
    %call_plot([],[],image_number)
    call_plot_boundaries([],[],image_number)
end

function call_menu_maxproj(src,eventdata)

if strcmp(get(src,'Checked'),'off')
    set(src,'Checked','on');    
else
    set(src,'Checked','off');    
end

%function call_realign_immunostack(src,eventdata)





function call_menu_labeladdremove(src,eventdata,addorremove)

global mystackA;
if addorremove==1, % add immunolabel
    %mystackA.cfg.immuno = 1; 
    if strcmp(get(mystackA.menu_labeladdimmuno,'Checked'),'off')
        set(mystackA.menu_labeladdimmuno,'Checked','on');
        set(mystackA.menu_labeladd,'Checked','off');
        set(mystackA.menu_labelremove,'Checked','off');
    else
        set(mystackA.menu_labeladdimmuno,'Checked','off');
        set(mystackA.menu_labeladd,'Checked','off');
        set(mystackA.menu_labelremove,'Checked','off');
    end
elseif addorremove==2, % add regular label
    %mystackA.cfg.immuno = 0; 
    if strcmp(get(mystackA.menu_labeladd,'Checked'),'off')
        set(mystackA.menu_labeladd,'Checked','on');
        set(mystackA.menu_labeladdimmuno,'Checked','off');
        set(mystackA.menu_labelremove,'Checked','off');
    else
        set(mystackA.menu_labeladd,'Checked','off');
        set(mystackA.menu_labeladdimmuno,'Checked','off');
        set(mystackA.menu_labelremove,'Checked','off');
    end    
elseif addorremove==3, % remove
    mystackA.cfg.immuno = 0; 
    if strcmp(get(mystackA.menu_labelremove,'Checked'),'off')
        set(mystackA.menu_labelremove,'Checked','on');
        set(mystackA.menu_labeladd,'Checked','off');
        set(mystackA.menu_labeladdimmuno,'Checked','off');
    else
        set(mystackA.menu_labelremove,'Checked','off');
        set(mystackA.menu_labeladd,'Checked','off');
        set(mystackA.menu_labeladdimmuno,'Checked','off');
    end
end

function call_menu_labelchange(src,eventdata)

global mystackA;

prompt = {'Change cell label:'};
dlg_title = 'Cell label';
num_lines = 1;
def = {mystackA.celllabel};
answer = inputdlg(prompt,dlg_title,num_lines,def);
if ~isempty(answer), % in case user didn't press cancel
    mystackA.celllabel=answer{1};
end

function call_maxproj(src,eventdata,image_number)

global mystackA;

mystackU=mystackA.S{image_number};
%
% maxmethod=2;
% if maxmethod==1,
%     fid = fopen(mystackU.fname);
%     ix = 1:mystackU.totfrm; %round(linspace(1,mystackU.totfrm,30));
%     IMG = NaN([mystackU.dim(2),mystackU.dim(1),length(ix)]);
%     for i=1:length(ix),
%         rng=ix(i);
%         % read in those frames
%         fseek(fid, 2*(16/8) + (2*mystackU.dim(1)*mystackU.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
%         ndat =fread(fid,[length(rng)*prod(mystackU.dim)],'*int16'); % read single frame
%         ndatr=reshape(ndat,[mystackU.dim(2),mystackU.dim(1),length(rng)]); % size(datr)
%         IMG(:,:,i) = squeeze(mean(ndatr,3));
%     end
%     maxprojection = max(IMG,[],3);
% elseif maxmethod==2,
%     rng=[1:mystackU.totfrm];
%     fid = fopen(mystackU.fname);
%     fseek(fid, 2*(16/8) + (2*mystackU.dim(1)*mystackU.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
%     ndat =fread(fid,[length(rng)*prod(mystackU.dim)],'*int16'); % read single frame
%     ndatr=reshape(ndat,[mystackU.dim(2),mystackU.dim(1),length(rng)]); % size(datr)
%     fclose(fid);
%     maxprojection = squeeze(max(ndatr,[],3));
% end

try, delete(mystackU.hIMG); end
axes(mystackA.ax(image_number));
%mystackU.hIMG=imagesc(maxprojection');colormap(gray);

if strcmp(get(mystackA.menu_show_red,'Checked'),'on'),
    mystackU.hIMG=imagesc(mystackA.REDMAXP{mystackA.cmp(image_number)});colormap(gray);
else
    mystackU.hIMG=imagesc(mystackA.MAXP{mystackA.cmp(image_number)});colormap(gray);
end


[x,y,z]=fileparts(mystackU.fname);
        try,str=mystackA.cfg.ROIsitenam;catch,str='';end
title(sprintf('%s,ses%d:%s,frm%d',str,mystackA.cmp(image_number),y,mystackU.frame),'Interpreter','none');

try, uistack(mystackU.BH,'top'); end
try, uistack(mystackU.RN,'top'); end
try, uistack(mystackU.CELLLABEL,'top'); end
try, uistack(mystackU.CELLLABELpt,'top'); end
mystackA.S{image_number}=mystackU;

function call_menu_file_generatecorrespondencefiles(src,eventdata,image_number)
%mystackA.cfg.COMBIcfg{1}
global mystackA;

TAG=datestr(now,'yyyymmdd_HHMMSS'); % make unique tag to see that CRSP from different files correspond

% ROIseslist = mystackA.cfg.ROIseslist;
% ROIsesblks = mystackA.cfg.ROIsesblks;
%
% % only compute correspondence between successive sessions
% COMBI=[[1:length(ROIseslist)-1]', [2:length(ROIseslist)]'];
% for cmb=1:size(COMBI,1),
%     cfg = [];
%     cfg.C{1}       = mystackA.cfg.CRSP{COMBI(cmb,1)}.C; % centroids
%     cfg.C{2}       = mystackA.cfg.CRSP{COMBI(cmb,2)}.C; % centroids
%     cfg.conversion = NaN([size(cfg.C{2},1),1]);
%     for i=1:size(cfg.C{2},1),
%         ix=find(mystackA.cfg.CRSP{COMBI(cmb,1)}.conversion(:,2)==mystackA.cfg.CRSP{COMBI(cmb,2)}.conversion(i,2));
%         if not(isempty(ix)),
%             cfg.conversion(i)=ix(1);
%         end
%     end
%     cfg.fname    = sprintf('%s%s_%s_ROICcfgR',mystackA.cfg.rootdir2,ROIseslist{COMBI(cmb,1)},ROIseslist{COMBI(cmb,2)});
%     save(cfg.fname,'cfg');
% end % end % for cmb=1:size(COMBI,1),

onlysavechanges=1; % only save to disk if changes

% check that no duplicate ROIs
MSG=check_duplicate_ROIs(mystackA.cfg.CRSP);
if length(MSG)>0, % check if already ROI with this ID exist in this session
    errordlg(['Duplicate ROIs:',MSG],'Duplicate ROI');
end

for i=1:length(mystackA.cfg.fnameCRSP)
    CRSPR=mystackA.cfg.CRSP{i};
    CRSPR.TAG = TAG;
    if ~isempty(strfind(mystackA.cfg.fnameCRSP{i},'_CRSPR')),
        sname=mystackA.cfg.fnameCRSP{i};
    elseif ~isempty(strfind(mystackA.cfg.fnameCRSP{i},'_CRSP')),
        sname=[mystackA.cfg.fnameCRSP{i},'R'];
    end
    if isempty(strfind(sname,'.mat')),sname=[sname,'.mat'];end
    if onlysavechanges==1&not(exist(sname)==0),
        OLD=load(sname,'CRSPR');% load
        if ~isequalwithequalnans(CRSPR.B,OLD.CRSPR.B)|~isequalwithequalnans(CRSPR.conversion,OLD.CRSPR.conversion)|~isequalwithequalnans(CRSPR.L,OLD.CRSPR.L)
            fprintf('%d:saved %s\n',i,sname);
            save(sname,'CRSPR');
        else
            fprintf('%d: no changes %s\n',i,sname);
        end
    else
        save(sname,'CRSPR');
    end
end

function call_menu_Image_reviewsegfil(src,eventdata,image_number)

global mystackA;
cix=mystackA.cmp(image_number);

% always load the file directly from disk in case adjustments were made
segname = mystackA.cfg.CRSP{cix}.segname;
if isempty(strfind(segname,'.mat')),segname=[segname,'.mat'];end
SEGFIL=load(segname);

clear global mystack;
chnix = 1;
cfg       = [];
%cfg.fname = sprintf('%s%s_ch-%s_mr.bin',RSG.rootdir2,idinf{NID}.fnamePC2,mavg.chns{chnix});
cfg.fname=mystackA.cfg.fname{cix};
if isfield(mystackA.cfg,'fnameRED'),
    cfg.fnameRED=mystackA.cfg.fnameRED{cix};
end
% cfg.MI = 50;
% cfg.MA = 200;
cfg.MI = 0; %Alex 17/08/2020 changed this!!!!!!
cfg.MA = 70; %Alex 17/08/2020 changed this!!!!!!
cfg.navg = 1;
mystackviewer_segment(cfg,SEGFIL.SEGMENT.bwcells);
set(gcf,'CloseRequestFcn','uiresume(gcbf);closereq;');
uiwait(gcf);

choice = questdlg('Save Segmentation File?', ...
    'Save Segmentation Menu', ...
    'Yes','No','No');
% Handle response
switch choice
    case 'Yes'
        if exist(segname),
            [x,y,z]=fileparts(segname)
            segnameBackup=[x,'\',y,'_',datestr(now,'yyyymmdd_HHMMSS'),z];
            copyfile(segname,segnameBackup);
            
            % once done, close window
            global mystack; % I = nlines x npixels x 3 (1=automatic segmentation, 2=add to automatic segmentation 3=subtract)
            clear SEGMENT;
            SEGMENT.I = mystack.I;
            SEGMENT.bwcells = mystack.I;
            SEGMENT.celldetectsource = SEGFIL.SEGMENT.celldetectsource;
            save(segname,'SEGMENT');
            
            % update CRSP
            BW=SEGMENT.bwcells';
            [B,L,N,A]=bwboundaries(BW,'noholes'); % use with gcamp: only take shell, not inside
            uROI = unique(L); uROI(uROI==0)=[]; % unique ROIs
            % compute centroids
            clear C;
            for i=1:N
                [r,c]=find(L==i);
                rc=[r,c];
                C(i,:)=round(mean(rc,1));
            end;
            
            clear CRSP;
            CRSP.segname = segname;
            CRSP.B = B;
            CRSP.L = L;
            CRSP.N = N;
            CRSP.A = A;
            CRSP.C = C;
            CRSP.conversion = [[1:length(B)]',zeros(size([1:length(B)]'))];
            %            CRSP.conversion = [[1:length(B)]',zeros(size([1:length(B)]')),zeros(size([1:length(B)]'))];
            
            CRSPold = mystackA.cfg.CRSP{cix};
            
            Cnew    = CRSP.C;
            Cactual = CRSPold.C;
            if 0,
                figure;plot(Cnew(:,2),Cnew(:,1),'bo');
                hold on;plot(Cactual(:,2),Cactual(:,1),'rx');
                legend({'COMBIcfg','CRSP'},'Location','Best');
            end
            Cactual2new =zeros([size(Cnew,1),1]);
            Cactual2newD=zeros([size(Cnew,1),1]);
            for i=1:size(Cnew,1)
                [Y,ix]=min(abs( sqrt(sum([Cactual-repmat(Cnew(i,:),[size(Cactual,1),1])].^2,2)) ));
                Cactual2new(i) =ix;
                Cactual2newD(i)=Y;
            end
            [B,I,J]=unique(Cactual2new);
            if length(B)<length(Cactual2new),
                for i=1:length(B),%i=3
                    sel=find(J==i);
                    [Y,ix]=sort(Cactual2newD(sel));
                    if length(ix)>1,
                        Cactual2new(sel(ix(2:end)))=NaN;
                    end
                end
            end
            ok=find(not(isnan(Cactual2new)));
            CRSP.conversion(ok,2)=CRSPold.conversion(Cactual2new(ok),2);
            %            CRSP.conversion(ok,3)=CRSPold.conversion(Cactual2new(ok),3);
            %[CRSP.conversion(ok,:),CRSPold.conversion(Cactual2new(ok),:)]
            
            % add new number to new ROIs
            maxROIid = 0;
            for i=1:length(mystackA.cfg.CRSP),
                 if not(isempty(mystackA.cfg.CRSP{i})),
                if max(mystackA.cfg.CRSP{i}.conversion(:,2))>maxROIid,
                    maxROIid = max(mystackA.cfg.CRSP{i}.conversion(:,2));
                end
                 end
            end
            ix=find(CRSP.conversion(:,2)==0);
            CRSP.conversion(ix,2)=[maxROIid:maxROIid+length(ix)-1]+1;
            
%             % also keep CELLLABELS!!
%             CRSP.CELLLABEL = CRSPold.CELLLABEL;
            
            if size(CRSP.conversion,2)>2, error('here'); end
            
            mystackA.cfg.CRSP{cix}=CRSP;
            %             call_plot([],[],image_number)
            %             call_plot_boundaries([],[],image_number);
            for image_number=1:2,
                if image_number<=length(mystackA.cfg.fname),
                    if strcmp(get(mystackA.menu_maxproj,'Checked'),'off'),
                        call_plot([],[],image_number);
                    else
                        call_maxproj([],[],image_number);
                    end
                    call_plot_boundaries([],[],image_number)
                end
            end
        end
    case 'No'
        %
end

function call_menu_Image_sessionlist(src,eventdata)

global mystackA;
dum=strcat(cellstr(num2str([1:length(mystackA.cfg.ROIseslist)]')),strcat(':',mystackA.cfg.ROIseslist'));
[s,v] = listdlg('PromptString','Select 2 sessions:',...
    'SelectionMode','multiple',...
    'InitialValue',[mystackA.cmp],...
    'ListString',dum)
if v==1&length(s)>1,
    mystackA.cmp=sort(s(1:2));
    
    update_sessionlist;
end

function update_sessionlist;

global mystackA;
for image_number=1:2,
    if image_number<=length(mystackA.cfg.fname),
        mystackU = mystackA.S{image_number};
        mystackU.fname = mystackA.cfg.fname{mystackA.cmp(image_number)};
        l = dir(mystackU.fname); % see how big the file is and how much datapoints it contains edit FileSize % find total number of frames in data
        fid = fopen(mystackU.fname);
        mystackU.dim    = fread(fid,2,'int16'); % size of image
        mystackU.totfrm = (l.bytes-2*(16/8))./(2*prod(mystackU.dim)); % total number of frames
        fclose(fid);
        
        mystackU.frame = 1;
        
        mystackA.S{image_number}=mystackU;
    end % if image_number<=length(mystackA.cfg.fname),
end

for image_number=1:2,
    if image_number<=length(mystackA.cfg.fname),
        if strcmp(get(mystackA.menu_maxproj,'Checked'),'off'),
            call_plot([],[],image_number);
        else
            call_maxproj([],[],image_number);
        end
        call_plot_boundaries([],[],image_number)
    end
end

function call_menu_Image_clim(src,eventdata,image_number)

image_number
global mystackA;
mystackU = mystackA.S{image_number};

prompt = {'Set Color Limits:'};
dlg_title = 'Size Threshold';
num_lines = 1;
%def = {'40'};
def = {sprintf('%1.2f,%1.2f',mystackU.MI,mystackU.MA)};
answer = inputdlg(prompt,dlg_title,num_lines,def);
if ~isempty(answer), % in case user didn't press cancel
    T=str2num(answer{1}); % T=40
    
    mystackU.MI=T(1);
    mystackU.MA=T(2);
    
    mystackA.S{image_number}=mystackU;
    
    axes(mystackA.ax(image_number));
    caxis([mystackU.MI,mystackU.MA]);
end

function call_wheel(src,eventdata)

function call_buttonup(src,eventdata)

set(gcf,'WindowButtonMotionFcn','');

function call_buttondown(src,eventdata)

%set(gcf,'WindowButtonMotionFcn',@call_buttonmotion);
dum = get(gca,'CurrentPoint');
dum = dum(1,1:2);
%dum = dum(1,[2,1])

global mystackA;
image_number=find(mystackA.ax==gca);

try,
    dim=max(size( mystackA.MAXP{mystackA.cmp(image_number)}));
    pok = all(dum>1&dum<dim); % point in range?
catch
    pok=0;
end

if not(isempty(image_number))&pok,
%     try,
        % find closest centroid
        mystackU=mystackA.S{image_number};
        CRSP =mystackA.cfg.CRSP{mystackA.cmp(image_number)};
        try, CL   =mystackA.cfg.CL{mystackA.cmp(image_number)}; catch, CL=[]; end
        if strcmp(get(mystackA.menu_labeladd,'Checked'),'on')|strcmp(get(mystackA.menu_labeladdimmuno,'Checked'),'on'),
            %if not(isfield(CRSP,'CELLLABEL')), % IF FIRST POINT
            if isempty(CL), % IF FIRST POINT
                if strcmp(get(mystackA.menu_labeladdimmuno,'Checked'),'on'),
                    %RA = MIJ.getResultsTable; % this retrieves table in matlab
                    Slice = MIJ.getColumn('Slice');
                    if isempty(Slice);Slice = MIJ.getColumn('Frame');end
                    RA = [MIJ.getColumn('X'), MIJ.getColumn('Y'), Slice];
                    RA = RA(end,:); % keep only the last measured point
                    RB = char(MIJ.getCurrentTitle);
                    %CRSP.CELLLABEL{1,:}={dum,mystackA.celllabel,RA,RB};
                    CL.CELLLABEL{1,:}={dum,mystackA.celllabel,RA,RB};
                else
                    RA=single(NaN([1,3]));
                    RB=char('');
                    %CRSP.CELLLABEL{1,:}={dum,mystackA.celllabel,RA,RB};
                    CL.CELLLABEL{1,:}={dum,mystackA.celllabel,RA,RB};
                end                
            else % IF NOT FIRST POINT
                if strcmp(get(mystackA.menu_labeladdimmuno,'Checked'),'on'),
                    %RA = MIJ.getResultsTable; % this retrieves table in matlab
                    %RA = RA(end,5:7); % x y z
                    Slice = MIJ.getColumn('Slice');
                    if isempty(Slice);Slice = MIJ.getColumn('Frame');end
                    RA = [MIJ.getColumn('X'), MIJ.getColumn('Y'), Slice];
                    RA = RA(end,:); % keep only the last measured point
                    RB = char(MIJ.getCurrentTitle);
                    
                    % check that point not yet in list to make sure it is
                    % updated
                    duplicate = 0;
                    %                     for i=1:length(CRSP.CELLLABEL),
                    %                         if isequal(CRSP.CELLLABEL{i}{3},RA),
                    %                            f=warndlg(sprintf('MLB:[%d,%d],''%s'', label that refers to same imageJ coordinate already exists!',round(CRSP.CELLLABEL{i}{1}(1)),round(CRSP.CELLLABEL{i}{1}(2)),CRSP.CELLLABEL{i}{2}),'Same IJ coordinate reference');
                    %                            waitfor(f);
                    %                            duplicate=1;
                    %                            %error(sprintf('MLB:[%d,%d],''%s'', label that refers to same imageJ coordinate already exists!',round(CRSP.CELLLABEL{i}{1}(1)),round(CRSP.CELLLABEL{i}{1}(2)),CRSP.CELLLABEL{i}{2}));
                    %                            %close(f);
                    %                         end
                    %                     end
                    for i=1:length(CL.CELLLABEL),
                        if isequal(CL.CELLLABEL{i}{3},RA),
                            f=warndlg(sprintf('MLB:[%d,%d],''%s'', label that refers to same imageJ coordinate already exists!',round(CL.CELLLABEL{i}{1}(1)),round(CL.CELLLABEL{i}{1}(2)),CL.CELLLABEL{i}{2}),'Same IJ coordinate reference');
                            waitfor(f);
                            duplicate=1;
                        end
                    end
                    if ~duplicate
                        %CRSP.CELLLABEL{size(CRSP.CELLLABEL,1)+1,:}={dum,mystackA.celllabel,RA,char(RB)};
                        CL.CELLLABEL{size(CL.CELLLABEL,1)+1,:}={dum,mystackA.celllabel,RA,char(RB)};
                    else
                        RA=single(NaN([1,3]));
                        RB=char('');
                    end
                else
                    RA=single(NaN([1,3]));
                    RB=char('');
                    %CRSP.CELLLABEL{size(CRSP.CELLLABEL,1)+1,:}={dum,mystackA.celllabel,RA,RB};
                    CL.CELLLABEL{size(CL.CELLLABEL,1)+1,:}={dum,mystackA.celllabel,RA,RB};
                end
            end
            str=sprintf('MLB:[x%d,y%d],''%s'',IJ:[%d,%d,%d],''%s''',round(dum(1)),round(dum(2)),mystackA.celllabel,RA(1),RA(2),RA(3),RB);
            % update title
            axes(mystackA.ax(image_number));
            [x,y,z]=fileparts(mystackU.fname);
            try,str2=mystackA.cfg.ROIsitenam;catch,str2='';end
            title({sprintf('%s,ses%d:%s,frm%d',str2,mystackA.cmp(image_number),y,mystackU.frame),str},'Interpreter','none');
            
            try, delete(mystackU.CELLLABEL);delete(mystackU.CELLLABELpt); end
            try, mystackU=rmfield(mystackU,{'CELLLABEL','CELLLABELpt'}); end
            %if isfield(CRSP,'CELLLABEL'),
            if ~isempty(CL),
                cix=1;
                %for i=1:size(CRSP.CELLLABEL,1);
                for i=1:size(CL.CELLLABEL,1);
                    hold on;
                    %mystackU.CELLLABEL(cix)=text(CRSP.CELLLABEL{i}{1}(1),CRSP.CELLLABEL{i}{1}(2),CRSP.CELLLABEL{i}{2},'Color',[1 0 1],'FontSize',12);
                    %mystackU.CELLLABELpt(cix)=plot(CRSP.CELLLABEL{i}{1}(1),CRSP.CELLLABEL{i}{1}(2),'.');
                    mystackU.CELLLABEL(cix)=text(CL.CELLLABEL{i}{1}(1),CL.CELLLABEL{i}{1}(2),CL.CELLLABEL{i}{2},'Color',[1 0 1],'FontSize',12);
                    mystackU.CELLLABELpt(cix)=plot(CL.CELLLABEL{i}{1}(1),CL.CELLLABEL{i}{1}(2),'.');
                    cix=cix+1;
                end
            end           
        elseif strcmp(get(mystackA.menu_labelremove,'Checked'),'on'),
            if ~isempty(CL),% isfield(CRSP,'CELLLABEL'),
                %if size(CRSP.CELLLABEL,1)==1,
                if size(CL.CELLLABEL,1)==1,
                    %CRSP=rmfield(CL,'CELLLABEL');
                    CL=[];
                else
%                     DF=zeros(size(CRSP.CELLLABEL,1),1);
%                     for i=1:size(CRSP.CELLLABEL,1),
%                         DF(i)=sqrt(sum( [CRSP.CELLLABEL{i}{1}(1)-dum(1),CRSP.CELLLABEL{i}{1}(2)-dum(2)].^2,2))
%                     end
%                     [~,ix]=min( DF );
%                     CRSP.CELLLABEL(ix,:)=[];
                    DF=zeros(size(CL.CELLLABEL,1),1);
                    for i=1:size(CL.CELLLABEL,1),
                        DF(i)=sqrt(sum( [CL.CELLLABEL{i}{1}(1)-dum(1),CL.CELLLABEL{i}{1}(2)-dum(2)].^2,2))
                    end
                    [~,ix]=min( DF );
                    CL.CELLLABEL(ix,:)=[];                    
                end
            end
            try, delete(mystackU.CELLLABEL);delete(mystackU.CELLLABELpt); end
            try, mystackU=rmfield(mystackU,{'CELLLABEL','CELLLABELpt'}); end
            %if isfield(CRSP,'CELLLABEL'),
            if ~isempty(CL),
                cix=1;
                %for i=1:size(CRSP.CELLLABEL,1);
                for i=1:size(CL.CELLLABEL,1);
                    %mystackU.CELLLABEL(cix)=text(CRSP.CELLLABEL{i}{1}(1),CRSP.CELLLABEL{i}{1}(2),CRSP.CELLLABEL{i}{2},'Color',[1 0 1],'FontSize',12);
                    %mystackU.CELLLABELpt(cix)=plot(CRSP.CELLLABEL{i}{1}(1),CRSP.CELLLABEL{i}{1}(2),'.');
                    mystackU.CELLLABEL(cix)=text(CL.CELLLABEL{i}{1}(1),CL.CELLLABEL{i}{1}(2),CL.CELLLABEL{i}{2},'Color',[1 0 1],'FontSize',12);
                    mystackU.CELLLABELpt(cix)=plot(CL.CELLLABEL{i}{1}(1),CL.CELLLABEL{i}{1}(2),'.');
                    cix=cix+1;
                end
            end
        else % click in GUI
            [Y,mystackU.cix]=min( sqrt(sum([CRSP.C-repmat(dum,[size(CRSP.C,1),1])].^2,2)) );
            
            selType = get(gcf,'SelectionType');
            switch lower(selType)
                case 'normal' % left: select ROI
                    %mystack.I(dum(:,2),dum(:,1))=1;
                case 'alt' % right: replace ROIid with id of selected ROI in other image
                    if isfield(mystackA.S{2-image_number+1},'cix'),
                        CRSPa=mystackA.cfg.CRSP{mystackA.cmp(2-image_number+1)}; % other image
                        newid = CRSPa.conversion(mystackA.S{2-image_number+1}.cix,2);
                        oldid = CRSP.conversion(mystackU.cix,2);
                        
                        % check for duplicate ROIs:
                        % now rename ID
                        MSG={};
                        for i=1:length(mystackA.cfg.CRSP), % for each session
                            if not(isempty(mystackA.cfg.CRSP{i})),
                                %if ~ismember(i,mystackA.cmp(image_number)) % excluding own session
                                ix=find(ismember(mystackA.cfg.CRSP{i}.conversion(:,2),[newid,oldid]));
                                if length(ix)>1,
                                    str=sprintf('%d,',mystackA.cfg.CRSP{i}.conversion(ix,2));str(end)=[];
                                    MSG=[MSG,sprintf('ses%d:%s',i,str)];
                                end
                            end
                        end
                        
                        %dum=find(CRSP.conversion(:,2)==newid);
                        if length(MSG)>0, % check if already ROI with this ID exist in this session
                            errordlg(['Duplicate ROIs:',MSG],'Duplicate ROI');
                        else
                            % now rename ID
                            for i=1:length(mystackA.cfg.CRSP), % for each session
                                %if ~ismember(i,mystackA.cmp(image_number)) % excluding own session
                                ix=ismember(mystackA.cfg.CRSP{i}.conversion(:,2),oldid);
                                mystackA.cfg.CRSP{i}.conversion(ix,2)=newid;
                                %end
                            end
                            CRSP=mystackA.cfg.CRSP{mystackA.cmp(image_number)};
                            set(mystackU.RN(mystackU.cix),'String',num2str(CRSP.conversion(mystackU.cix,2)));
                        end
                    end % if isfield(mystackA.S{2-image_number+1},'cix'),
                case 'extend' % middle: make unique ROI for selected ROI
                    maxROIid = 0;
                    for i=1:length(mystackA.cfg.CRSP),
                        if not(isempty(mystackA.cfg.CRSP{i})),
                            if max(mystackA.cfg.CRSP{i}.conversion(:,2))>maxROIid,
                                maxROIid = max(mystackA.cfg.CRSP{i}.conversion(:,2));
                            end
                        end
                    end
                    %if image_number==2
                    CRSP.conversion(mystackU.cix,2) = maxROIid+1;
                    set(mystackU.RN(mystackU.cix),'String',num2str(CRSP.conversion(mystackU.cix,2)));
                    %end
            end %switch
            %uniqueROIs([],[]);
        end % if strcmp(get(mystackA.menu_labeladd,'Checked'),'on'),
        
        mystackA.cfg.CRSP{mystackA.cmp(image_number)}=CRSP;
        mystackA.cfg.CL{mystackA.cmp(image_number)}  =CL;
        %
        %         MSG=check_duplicate_ROIs(mystackA.cfg.CRSP);
        %         if length(MSG)>0, % check if already ROI with this ID exist in this session
        %             errordlg(['Duplicate ROIs:',MSG],'Duplicate ROI');
        %         end
        %
        %set(mystackU.BH,'Color',[0.5 0.5 0.5]);
        %set(mystackU.BH(mystackU.cix),'Color',[1 0 0]);
        %set(mystackU.RN,'Color',[0.5 0.5 0.5]);
        if isfield(mystackA,'ZD'), % if Z-DEPTH defined
            %mystackU.RN(cix)=text(rc(1),rc(2),sprintf('%d_d%d',CRSP.conversion(i,2),round(mystackA.ZD.dist{mystackA.LAY}{image_number}(i))),'Color',[0 0 1],'FontSize',12,'FontWeight','bold','Interpreter','none');
            %mystackU.RN(cix)=text(rc(1),rc(2),sprintf('%d_d%d',CRSP.conversion(i,2),round(mystackA.ZD.dist{mystackA.LAY}{image_number}(i))),'Color',[1 0 0],'FontSize',9,'FontWeight','bold','Interpreter','none');
            try, set(mystackU.RN,'Color',[1 0 0],'FontSize',9,'FontWeight','bold'); end
            try, set(mystackU.RN(mystackU.cix),'Color',[1 0 0.5],'FontSize',9,'FontWeight','bold');end
        else
            try, set(mystackU.RN,'Color',[0 0 1],'FontSize',12,'FontWeight','bold'); end
            try, set(mystackU.RN(mystackU.cix),'Color',[1 0 0],'FontSize',12,'FontWeight','bold');end
        end
        mystackA.S{image_number}=mystackU;
%     catch
%         beep;
%         warning(sprintf('error in mystackviewer_CRSP: %s',lasterr))
%         if strcmp(get(mystackA.menu_maxproj,'Checked'),'off'),
%             call_plot([],[],image_number)
%         else
%             call_maxproj([],[],image_number);
%         end
%         call_plot_boundaries([],[],image_number)
%     end
end % if not(isempty(image_number)),

function MSG=check_duplicate_ROIs(CRSP)
% check for duplicate ROIs:
MSG={};
for i=1:length(CRSP), % for each session i=2
    if not(isempty(CRSP{i})),
        [B,I,J]=unique(CRSP{i}.conversion(:,2));
        
        if length(CRSP{i}.conversion(:,2))>length(B),
            NB=zeros(size(B));
            for j=1:length(B),
                NB(j)=length(find(J==j));
            end
            ix = find(NB>1);
            str=sprintf('%d,',B(ix));str(end)=[];
            MSG=[MSG,sprintf('ses%d:%s',i,str)];
        end
    end % if not(isempty(CRSP{i})),
end

function call_menu_file_resortROIs(src,eventdata)

global mystackA;

MSG=check_duplicate_ROIs(mystackA.cfg.CRSP);
if length(MSG)>0, % check if already ROI with this ID exist in this session
    errordlg(['Duplicate ROIs:',MSG],'Duplicate ROI');
end

CRSP=mystackA.cfg.CRSP;

% find in which session each unique ROI appears
ROIu=[]; %clc;ROIu
for i=1:length(CRSP), % for each session
    if not(isempty(CRSP{i})),
        ROIu=[ROIu; repmat(i,[size(CRSP{i}.conversion,1),1]),CRSP{i}.conversion];
    end
end

% resort ROIs
[B,I,J]=unique(ROIu(:,3),'first');

% sort for when ROI first encountered
[Ys,Is]=sort(I); % clc;B(Is)
if 0,
    I(find(B==266))
    B(Is)
end

%
% for i=1:length(B),
%     ix=find(J==i);
%     for j=1:length(ix),
%         CRSP{ROIu(ix(j),1)}.conversion(ROIu(ix(j),2),2)=i;
%     end
% end

for i=1:length(B),
    ix=find(J==Is(i));
    for j=1:length(ix),
        CRSP{ROIu(ix(j),1)}.conversion(ROIu(ix(j),2),2)=i;
    end
end


ROIu=[];
for i=1:length(CRSP), % for each session
    if not(isempty(CRSP{i})),
    ROIu=[ROIu; repmat(i,[size(CRSP{i}.conversion,1),1]),CRSP{i}.conversion];
    end
end

[B,I,J]=unique(ROIu(:,3));
% find in which session each unique ROI appears
ROIID =NaN([length(B),length(CRSP)]); % ROI x session
for i=1:length(CRSP), % for each session
    if not(isempty(CRSP{i})),
    for j=1:length(B),
        if not(isempty(find(CRSP{i}.conversion(:,2)==B(j)))),
            ROIID(j,i)=1;
        end
    end
    end
end
fROI=figure;imagesc([1:length(CRSP)],B,ROIID);
set(gca,'XTick',[1:length(CRSP)]);
ylabel('ROIid');xlabel('session');

dum=get(gca,'YLim');
for i=1:length(CRSP),
    text(i,dum(2),mystackA.cfg.ROIseslist{i},'Color',[1 1 1],'Rotation',90,'Interpreter','none');
end

mystackA.cfg.CRSP=CRSP;

for image_number=1:2,
    if strcmp(get(mystackA.menu_maxproj,'Checked'),'off'),
        call_plot([],[],image_number)
    else
        call_maxproj([],[],image_number);
    end
    call_plot_boundaries([],[],image_number)
end

MSG=check_duplicate_ROIs(mystackA.cfg.CRSP);
if length(MSG)>0, % check if already ROI with this ID exist in this session
    errordlg(['Duplicate ROIs:',MSG],'Duplicate ROI');
end

figure(fROI);

function call_menu_file_maskshiftafterwarp(src,eventdata)

global mystackA;

cix1=mystackA.cmp(1);
imold=imadjust(mat2gray(mystackA.MAXP{cix1}));
Cold = mystackA.cfg.CRSP{cix1}.C; % centroids

cix2=mystackA.cmp(2);
imnew=imadjust(mat2gray(mystackA.MAXP{cix2}));
Cnew = mystackA.cfg.CRSP{cix2}.C; % centroids

%--------------------------------------------------------------------------
% compute warp from old to new
FACT=0.1;
imold_small = imresize(imold, FACT);
imnew_small = imresize(imnew, FACT);

uv = estimate_flow_interface(imold_small, imnew_small, 'classic+nl-fast');
clear UV;
UV(:,:,1) = 1/FACT*imresize(uv(:,:,1), [size(imold)]);
UV(:,:,2) = 1/FACT*imresize(uv(:,:,2), [size(imold)]);
im_warp = imwarp(imnew, UV(:,:,1), UV(:,:,2));

if 0, % overlay old and new (warped) image        
    R=cat(3,imold,imnew);R=cat(3,R,zeros(size(imold)));figure;imshow(R);title('imold & imnew');
    R=cat(3,imold,im_warp);R=cat(3,R,zeros(size(imold)));figure;imshow(R);title('imold & imnew_warp');
end

%--------------------------------------------------------------------------
%shift centroids to get new mask
Mnew=zeros(size(imnew));
Mnew_warp=zeros(size(imnew));
dum0=logical(zeros(size(imold)));
for i=1:size(Cnew,1),
    Mnew(Cnew(i,2),Cnew(i,1))=i;
    dum3=imwarp(Mnew==i, UV(:,:,1), UV(:,:,2));
    Mnew_warp(dum3>0)=i;    
end
Mnew_warp=Mnew_warp';

% find centroids
Cnew_warp=zeros(size(Cnew,1),2);
for i=1:size(Cnew,1),
    [r,c]=find(Mnew_warp==i);
    rc=[r,c];
    Cnew_warp(i,:)=round(mean(rc,1));
end

%--------------------------------------------------------------------------
% compute distances between linked cells and print list
D=NaN(size(mystackA.cfg.CRSP{cix1}.conversion,1),1);
for i=1:size(mystackA.cfg.CRSP{cix1}.conversion,1),
    j=find(mystackA.cfg.CRSP{cix2}.conversion(:,2)==mystackA.cfg.CRSP{cix1}.conversion(i,2));
    if ~isempty(j),
        D(i)=sqrt(sum([Cold(i,:)-Cnew_warp(j,:)].^2));
    end    
end

if 0, % overlay old and new (warped) image
    % close all;
    figure;imshow(imnew);axis on;hold on;
    for i=1:size(Cnew,1),
        text(Cnew(i,1),Cnew(i,2),num2str(i),'HorizontalAlignment','center');
    end
    figure;imshow(im_warp);axis on;hold on;
    for i=1:size(Cnew_warp,1),
        text(Cnew_warp(i,1),Cnew_warp(i,2),num2str(i),'HorizontalAlignment','center');
    end
    
    figure;imshow(imold);axis on;hold on;
    for i=1:size(Cold,1),
        text(Cold(i,1),Cold(i,2),sprintf('%d:%1.2f',mystackA.cfg.CRSP{cix1}.conversion(i,2),D(i)),'HorizontalAlignment','center');
    end
    for i=1:size(Cnew_warp,1),
        text(Cnew_warp(i,1),Cnew_warp(i,2),num2str(mystackA.cfg.CRSP{cix2}.conversion(i,2)),'HorizontalAlignment','center','Color',[1 0 0]);
    end
end

clc;
for i=1:size(Cold,1), %i=3
    %if not(isnan(D(i))),
        fprintf('%3.0d:%5.2f\t',mystackA.cfg.CRSP{cix1}.conversion(i,2),D(i));
        if mod(i,10)==0,
            fprintf('\n');
        end
    %end
end
fprintf('\n');


function call_menu_file_roisessions(src,eventdata)

global mystackA;

image_number=find(mystackA.ax==gca);
mystackU=mystackA.S{image_number};
if isfield(mystackU,'cix'),
    
    oldid = mystackA.cfg.CRSP{mystackA.cmp(image_number)}.conversion(mystackU.cix,2);
    
    global mystackA;
    
    CRSP=mystackA.cfg.CRSP;
    
    %     % do this once: compute max projections for later use
    %     clear MAXP;
    %     for image_number=1:length(mystackA.cfg.fname),
    %         mystackU.fname = mystackA.cfg.fname{image_number};
    %         l = dir(mystackU.fname); % see how big the file is and how much datapoints it contains edit FileSize % find total number of frames in data
    %         fid = fopen(mystackU.fname);
    %         mystackU.dim    = fread(fid,2,'int16'); % size of image
    %         mystackU.totfrm = (l.bytes-2*(16/8))./(2*prod(mystackU.dim)); % total number of frames
    %         fclose(fid);
    %         rng=[1:mystackU.totfrm];
    %         fid = fopen(mystackU.fname);
    %         fseek(fid, 2*(16/8) + (2*mystackU.dim(1)*mystackU.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
    %         ndat =fread(fid,[length(rng)*prod(mystackU.dim)],'*int16'); % read single frame
    %         ndatr=reshape(ndat,[mystackU.dim(2),mystackU.dim(1),length(rng)]); % size(datr)
    %         fclose(fid);
    %         MAXP{image_number} = squeeze(max(ndatr,[],3))';
    %     end
    
    sdim=ceil(sqrt(length(CRSP)));
    clc;clear h;
    
    figure;set(gcf,'Name',sprintf('ROI%d',oldid));six=0;
    for i=1:length(CRSP), % for each session i=3
        ix = find(CRSP{i}.conversion(:,2)==oldid);
        if ~isempty(ix),
            IMG=mat2gray(mystackA.MAXP{i});
            IMG=imadjust(IMG);
            six=six+1;h(six)=subplot(sdim,sdim,i);
            if length(ix)>1, warning('duplicate ROI'); end
            k=1
            % get centroids
            [r,c]=find(CRSP{i}.L==ix(k));rc=[r,c];rc=round(mean(rc,1))
            
            show_entire_figure=1;
            if show_entire_figure,
                %figure;
                imagesc(IMG);colormap(gray);
                hold on;plot(CRSP{i}.B{ix(k)}(:,1),CRSP{i}.B{ix(k)}(:,2),'w','Color',[1 0.5 0.5]);
                axis off;
            else
                SQ=200;
                rng1=rc(1)-SQ:rc(1)+SQ;rng1b=[1:length(rng1)];
                rng2=rc(2)-SQ:rc(2)+SQ;rng2b=[1:length(rng2)];
                A=zeros([length(rng2),length(rng1)]);
                rng1b(rng1<1|rng1>size(IMG,1))=[]; rng1(rng1<1|rng1>size(IMG,1))=[];
                rng2b(rng2<1|rng2>size(IMG,2))=[]; rng2(rng2<1|rng2>size(IMG,2))=[];
                A(rng2b,rng1b)=IMG(rng2,rng1);
                %figure;
                imagesc(A)
                hold on;plot(CRSP{i}.B{ix(k)}(:,1)-rc(1)+SQ+1,CRSP{i}.B{ix(k)}(:,2)-rc(2)+SQ+1,'w','Color',[1 0.5 0.5]);
                colormap(gray);
                axis off;
            end
            set(h(six),'Position',get(h(six),'OuterPosition'));
            dum=axis;text(dum(1)+0.1*(dum(2)-dum(1)),dum(4)-0.2*(dum(4)-dum(3)),num2str(i),'Color',[1 1 1]);
        else
            htmp=subplot(sdim,sdim,i);set(htmp,'Position',get(htmp,'OuterPosition'));
            dum=axis;text(dum(1)+0.1*(dum(2)-dum(1)),dum(4)-0.2*(dum(4)-dum(3)),num2str(i),'Color',[0 0 0]);
            %delete(htmp);
            set(gca,'YDir','reverse');axis off;
        end % if ~isempty(ix),
    end % for i=1:length(CRSP), % for each session
    
end % if isfield(mystackU,'cix'),

function call_menu_file_makeallroiinstancesunique(src,eventdata)

global mystackA;

image_number=find(mystackA.ax==gca);
mystackU=mystackA.S{image_number};
oldid = mystackA.cfg.CRSP{mystackA.cmp(image_number)}.conversion(mystackU.cix,2);

maxROIid = 0;
for i=1:length(mystackA.cfg.CRSP),
    if max(mystackA.cfg.CRSP{i}.conversion(:,2))>maxROIid,
        maxROIid = max(mystackA.cfg.CRSP{i}.conversion(:,2));
    end
end

for i=1:length(mystackA.cfg.CRSP),
    ix=find(ismember(mystackA.cfg.CRSP{i}.conversion(:,2),oldid));
    for j=1:length(ix),
        maxROIid=maxROIid+1;
        mystackA.cfg.CRSP{i}.conversion(ix(j),2)=maxROIid;
    end
end
for image_number=1:2,
    if strcmp(get(mystackA.menu_maxproj,'Checked'),'off'),
        call_plot([],[],image_number)
    else
        call_maxproj([],[],image_number);
    end
    call_plot_boundaries([],[],image_number)
end

function call_menu_file_refreshsegfil(src,eventdata)

global mystackA;

for cix=1:length(mystackA.cfg.CRSP),
    if not(isempty(mystackA.cfg.CRSP{cix})),
        % always load the file directly from disk in case adjustments were made
        segname = mystackA.cfg.CRSP{cix}.segname;
        if isempty(strfind(segname,'.mat')),segname=[segname,'.mat'];end
        if exist(segname),
            SEGFIL=load(segname);
            fprintf('load %s\n',segname);
            
            % once done, close window
            clear SEGMENT;
            %SEGMENT.I = SEGFIL.SEGMENT.I;
            SEGMENT.bwcells = SEGFIL.SEGMENT.bwcells;
            SEGMENT.celldetectsource = SEGFIL.SEGMENT.celldetectsource;
            
            % update CRSP
            BW=SEGMENT.bwcells';
            [B,L,N,A]=bwboundaries(BW,'noholes'); % use with gcamp: only take shell, not inside
            uROI = unique(L); uROI(uROI==0)=[]; % unique ROIs
            % compute centroids
            clear C;
            for i=1:N
                [r,c]=find(L==i);
                rc=[r,c];
                C(i,:)=round(mean(rc,1));
            end;
            
            clear CRSP;
            CRSP.segname = segname;
            CRSP.B = B;
            CRSP.L = L;
            CRSP.N = N;
            CRSP.A = A;
            CRSP.C = C;
            CRSP.conversion = [[1:length(B)]',zeros(size([1:length(B)]'))];
            %        CRSP.conversion = [[1:length(B)]',zeros(size([1:length(B)]')),zeros(size([1:length(B)]'))];
            
            CRSPold = mystackA.cfg.CRSP{cix};
            
            if ~isequal(CRSP.B,CRSPold.B),
                
                Cnew    = CRSP.C;
                Cactual = CRSPold.C;
                if 0,
                    figure;plot(Cnew(:,2),Cnew(:,1),'bo');
                    hold on;plot(Cactual(:,2),Cactual(:,1),'rx');
                    legend({'COMBIcfg','CRSP'},'Location','Best');
                end
                Cactual2new =zeros([size(Cnew,1),1]);
                Cactual2newD=zeros([size(Cnew,1),1]);
                for i=1:size(Cnew,1)
                    [Y,ix]=min(abs( sqrt(sum([Cactual-repmat(Cnew(i,:),[size(Cactual,1),1])].^2,2)) ));
                    Cactual2new(i) =ix;
                    Cactual2newD(i)=Y;
                end
                [B,I,J]=unique(Cactual2new);
                if length(B)<length(Cactual2new),
                    for i=1:length(B),%i=3
                        sel=find(J==i);
                        [Y,ix]=sort(Cactual2newD(sel));
                        if length(ix)>1,
                            Cactual2new(sel(ix(2:end)))=NaN;
                        end
                    end
                end
                ok=find(not(isnan(Cactual2new)));
                CRSP.conversion(ok,2)=CRSPold.conversion(Cactual2new(ok),2);
                %            CRSP.conversion(ok,3)=CRSPold.conversion(Cactual2new(ok),3);
                %[CRSP.conversion(ok,:),CRSPold.conversion(Cactual2new(ok),:)]
                
                % add new number to new ROIs
                maxROIid = 0;
                for i=1:length(mystackA.cfg.CRSP),
                    if not(isempty(mystackA.cfg.CRSP{i})),
                        if max(mystackA.cfg.CRSP{i}.conversion(:,2))>maxROIid,
                            maxROIid = max(mystackA.cfg.CRSP{i}.conversion(:,2));
                        end
                    end % if not(isempty(mystackA.cfg.CRSP{i})),
                end
                ix=find(CRSP.conversion(:,2)==0);
                CRSP.conversion(ix,2)=[maxROIid:maxROIid+length(ix)-1]+1;
                
                if size(CRSP.conversion,2)>2, error('here'); end
                
                mystackA.cfg.CRSP{cix}=CRSP;
                %call_plot([],[],image_number)
                %call_plot_boundaries([],[],image_number);
                
                
            end % if ~isequal(CRSP.B,CRSPold.B),
            if 1,
                BW2=SEGMENT.bwcells'; % reference file
                [B2,L2]=bwboundaries(BW2,'noholes'); % use with gcamp: only take shell, not inside
                [x1,y1,z1]=fileparts(segname);
                [x2,y2,z2]=fileparts(CRSP.segname);
                if ~isequal(CRSP.B,B2)|~isequal(y1,y2),
                    error('different ROIs used for correspondence!');
                end
            end
        end % if exist(segname),
    end % if not(isempty(mystackA.cfg.CRSP{cix})),
end % for cix=1:length(mystackA.cfg.CRSP),

for image_number=1:2,
    call_plot([],[],image_number)
    call_plot_boundaries([],[],image_number)
end

function call_keypress(src,eventdata)
%WindowKeyPressFcn

global mystackA;

if ismember(eventdata.Key,['awsd'])|ismember(eventdata.Key,['jikl']), % image 1
    if ismember(eventdata.Key,['awsd']), % image 1
        mystackU=mystackA.S{1};
        image_number=1;
    elseif ismember(eventdata.Key,['jikl']), % image 2
        mystackU=mystackA.S{2};
        image_number=2;
    end
    if ismember(eventdata.Key,['aj']),
        mystackU.frame=mystackU.frame-mystackU.framestep;
    elseif ismember(eventdata.Key,['dl']),
        mystackU.frame=mystackU.frame+mystackU.framestep;
    elseif ismember(eventdata.Key,['wi']),
        mystackU.framestep=mystackU.framestep+1;
    elseif ismember(eventdata.Key,['sk']),
        mystackU.framestep=mystackU.framestep-1;
    end
    if ismember(eventdata.Key,['ajdl']),
        if mystackU.frame<1,
            mystackU.frame=1;
        elseif mystackU.frame>mystackU.totfrm,
            mystackU.frame=mystackU.totfrm,
        end
        if strcmp(get(mystackA.menu_maxproj,'Checked'),'off'),
            call_plot([],[],image_number)
        else
            call_maxproj([],[],image_number);
        end
        fprintf('frame=%d\n',mystackU.frame);
    elseif ismember(eventdata.Key,['wisk']),
        if mystackU.framestep<1, mystackU.framestep=1; end
        fprintf('framestep=%d\n',mystackU.framestep);
    end
    
    if ismember(eventdata.Key,['awsd']), % image 1
        mystackA.S{1}=mystackU;
    elseif ismember(eventdata.Key,['jikl']), % image 2
        mystackA.S{2}=mystackU;
    end
elseif strcmp(eventdata.Key,'m'),
    for image_number=1:2,
        if image_number<=length(mystackA.cfg.fname),
            if strcmp(get(mystackA.menu_maxproj,'Checked'),'off'),
                call_plot([],[],image_number);
            else
                call_maxproj([],[],image_number);
            end
        end
    end
elseif strcmp(eventdata.Key,'leftbracket'),
    image_number=find(mystackA.ax==gca);
    call_menu_Image_makeuniquepreviousnext([],[],image_number,1);
elseif strcmp(eventdata.Key,'rightbracket'),
    image_number=find(mystackA.ax==gca);
    call_menu_Image_makeuniquepreviousnext([],[],image_number,2);
elseif strcmp(eventdata.Key,'period'),
    image_number=1; % REF image
    mystackU=mystackA.S{image_number};
    id = NaN;
    if isfield(mystackU,'cix'),
        if ~isempty(mystackU.cix),
            id=mystackA.cfg.CRSP{mystackA.cmp(image_number)}.conversion(mystackU.cix,2);
        end
    end
    %     if isnan(id),
    %         mystackU=mystackA.S{2-image_number+1};
    %         if isfield(mystackU,'cix'),
    %             if ~isempty(mystackU.cix),
    %                 id=mystackA.cfg.CRSP{mystackA.cmp(1)}.conversion(mystackU.cix,2);
    %             end
    %         end
    %     end
    
    image_number=find(mystackA.ax==gca);
    if (mystackA.cmp(image_number)<length(mystackA.cfg.CRSP))&(mystackA.cmp(2-image_number+1)~=(mystackA.cmp(image_number)+1)),
        mystackA.cmp(image_number)=mystackA.cmp(image_number)+1;
        update_sessionlist;
    end
    axes(mystackA.ax(image_number));
    %set(gca,'XLim',[1,size(mystackU.IMG,1)],'YLim',[1,size(mystackU.IMG,2)]);
    if ~isnan(id),
        for image_number=1:2,
            if not(isempty(mystackA.cfg.CRSP{mystackA.cmp(image_number)})),
                mystackU     = mystackA.S{image_number};
                mystackU.cix = find(mystackA.cfg.CRSP{mystackA.cmp(image_number)}.conversion(:,2)==id);
                set(mystackU.RN,'Color',[0 0 1],'FontSize',12,'FontWeight','bold');
                set(mystackU.RN(mystackU.cix),'Color',[1 0 0],'FontSize',12,'FontWeight','bold');
                mystackA.S{image_number}=mystackU;
            end % if not(isempty(mystackA.cfg.CRSP{mystackA.cmp(image_number)})),
        end % for image_number=1:2,
    end % if ~isnan(id),
    
    % autoscale axes
    for image_number=1:2,
        if image_number<=length(mystackA.cfg.fname),
            try,
                axes(mystackA.ax(image_number));            
                dum=size(mystackA.cfg.CRSP{mystackA.cmp(image_number)}.L);
                set(gca,'XLim',[1,dum(1)],'YLim',[1,dum(2)]);
            end  
        end
    end
    
elseif strcmp(eventdata.Key,'comma'),
    image_number=1; % REF image
    mystackU=mystackA.S{image_number};
    id = NaN;
    if isfield(mystackU,'cix'),
        if ~isempty(mystackU.cix),
            id=mystackA.cfg.CRSP{mystackA.cmp(image_number)}.conversion(mystackU.cix,2);
        end
    end
    image_number=find(mystackA.ax==gca);
    if (mystackA.cmp(image_number)>1)&(mystackA.cmp(2-image_number+1)~=(mystackA.cmp(image_number)-1)),
        mystackA.cmp(image_number)=mystackA.cmp(image_number)-1;
        update_sessionlist;
    end
    axes(mystackA.ax(image_number));
    
    if ~isnan(id),
        for image_number=1:2,
            if not(isempty(mystackA.cfg.CRSP{mystackA.cmp(image_number)})),
                mystackU     = mystackA.S{image_number};
                mystackU.cix = find(mystackA.cfg.CRSP{mystackA.cmp(image_number)}.conversion(:,2)==id);
                set(mystackU.RN,'Color',[0 0 1],'FontSize',12,'FontWeight','bold');
                set(mystackU.RN(mystackU.cix),'Color',[1 0 0],'FontSize',12,'FontWeight','bold');
                mystackA.S{image_number}=mystackU;
            end % if not(isempty(mystackA.cfg.CRSP{mystackA.cmp(image_number)})),
        end % for image_number=1:2,
    end % if ~isnan(id),
    
    % autoscale axes
    for image_number=1:2,
        try,
            axes(mystackA.ax(image_number));
            dum=size(mystackA.cfg.CRSP{mystackA.cmp(image_number)}.L);
            set(gca,'XLim',[1,dum(1)],'YLim',[1,dum(2)]);
        end
    end
    
elseif strcmp(eventdata.Key,'r'), % review mode
    
    % find if exists same ROI in other image
    image_number=find(mystackA.ax==gca);
    mystackU=mystackA.S{image_number};
    id=mystackA.cfg.CRSP{mystackA.cmp(image_number)}.conversion(mystackU.cix,2);
    
    mystackU=mystackA.S{2-image_number+1};
    mystackU.cix=find(mystackA.cfg.CRSP{mystackA.cmp(2-image_number+1)}.conversion(:,2)==id);
    set(mystackU.RN,'Color',[0 0 1],'FontSize',12,'FontWeight','bold');
    set(mystackU.RN(mystackU.cix),'Color',[1 0 0],'FontSize',12,'FontWeight','bold');
    mystackA.S{2-image_number+1}=mystackU;
elseif strcmp(eventdata.Key,'x'),
    if isfield(mystackA.cfg,'fnameRED'),
        if strcmp(get(mystackA.menu_show_red,'Checked'),'on'),
            set(mystackA.menu_show_red,'Checked','off');
        else
            set(mystackA.menu_show_red,'Checked','on');
        end
        for image_number=1:2,
            if image_number<=length(mystackA.cfg.fname),
                if strcmp(get(mystackA.menu_maxproj,'Checked'),'off'),
                    call_plot([],[],image_number);
                else
                    call_maxproj([],[],image_number);
                end
            end
        end
    end % if isfield(mystackA.cfg,'fnameRED'),
else
    dum = get(gca,'CurrentPoint');
    dum = round(dum(1,1:2)) % dum=[300,200]
    
    image_number=find(mystackA.ax==gca);
    mystackU=mystackA.S{image_number};
    
    a1=get(gca,'XLim')
    a2=get(gca,'YLim')
    
    rngx = [a1(2)-a1(1)];
    rngy = [a2(2)-a2(1)];
    
    Isiz = size(mystackU.IMG);
    eventdata.Key
    % zoom in, centered on current position
    if strcmp(eventdata.Key, 'equal')==1
        rngxnew = rngx.*0.9; rngynew = rngy.*0.9;
    elseif strcmp(eventdata.Key, 'hyphen')==1
        rngxnew = rngx.*1.1; rngynew = rngy.*1.1;
    else
        rngxnew = rngx.*1; rngynew = rngy.*1;
    end
    if strcmp(eventdata.Key,'equal')|strcmp(eventdata.Key, 'hyphen'),
        if rngxnew>size(mystackU.IMG,2), rngxnew = Isiz(2); end
        if rngynew>size(mystackU.IMG,1), rngynew = Isiz(1); end
        a1n=[dum(1)-rngxnew/2,dum(1)+rngxnew/2]; if any(a1n<1), a1n=[1,rngxnew]; elseif any(a1n>Isiz(2)), a1n=[Isiz(2)-rngxnew,Isiz(2)]; end
        a2n=[dum(2)-rngynew/2,dum(2)+rngynew/2]; if any(a2n<1), a2n=[1,rngynew]; elseif any(a2n>Isiz(1)), a2n=[Isiz(1)-rngynew,Isiz(1)]; end
        set(gca,'XLim',a1n,'YLim',a2n);
    end
end

function call_plot(src,eventdata,image_number)

%image_number
global mystackA;
if image_number<=length(mystackA.cfg.fname), % image_number=2
     mystackU=mystackA.S{image_number};
%     mystackU.frame
%     rng=[mystackU.frame:mystackU.frame+mystackU.navg-1]; rng(rng<1)=[]; rng(rng>mystackU.totfrm)=[];
%     % read in those frames
%     fid = fopen(mystackU.fname);
%     fseek(fid, 2*(16/8) + (2*mystackU.dim(1)*mystackU.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
%     ndat =fread(fid,[length(rng)*prod(mystackU.dim)],'*int16'); % read single frame
%     ndatr=reshape(ndat,[mystackU.dim(2),mystackU.dim(1),length(rng)]); % size(datr)
%     fclose(fid);
%     
%     if mystackU.medfilt~=0,
%         for i=1:size(ndatr,3),
%             ndatr(:,:,i) = medfilt2(squeeze(ndatr(:,:,i)),[mystackU.medfilt,mystackU.medfilt]);
%         end
%     end
%     mystackU.IMG = squeeze(mean(ndatr,3))';
    mystackU.IMG = mystackA.MEANP{mystackA.cmp(image_number)};
    
    try, delete(mystackU.hIMG); end
    axes(mystackA.ax(image_number));
    %mystackU.hIMG=imagesc(mystackU.IMG);
    if strcmp(get(mystackA.menu_show_red,'Checked'),'on'),
        mystackU.hIMG=imagesc(mystackA.REDMEANP{mystackA.cmp(image_number)});colormap(gray);
    else
        mystackU.hIMG=imagesc(mystackA.MEANP{mystackA.cmp(image_number)});colormap(gray);
    end

    %set(mystackU.hIMG,'ButtonDownFcn',{@call_clickinaxis,image_number});
    [x,y,z]=fileparts(mystackU.fname);
    %title(sprintf('%s,frm%d',y,mystackU.frame),'Interpreter','none');
    try,str=mystackA.cfg.ROIsitenam;catch,str='';end
    title(sprintf('%s,ses%d:%s,frm%d',str,mystackA.cmp(image_number),y,mystackU.frame),'Interpreter','none');
    
    try, uistack(mystackU.BH,'top'); end
    try, uistack(mystackU.RN,'top'); end
    try, uistack(mystackU.CELLLABEL,'top'); end
    try, uistack(mystackU.CELLLABELpt,'top'); end
    mystackA.S{image_number}=mystackU;
end % if image_number<=length(mystackA.cfg.fname),

function my_closefcn(src,eventdata)
% User-defined close request function
% to display a question dialog box
if 0,
    selection = questdlg('Close This Figure?',...
        'Close Request Function',...
        'Yes','No','Yes');
    switch selection,
        case 'Yes',
            delete(gcf);
        case 'No'
            return
    end
end
delete(gcf);

function call_plot_boundaries(src,eventdata,image_number)

global mystackA;
if image_number<=length(mystackA.cfg.fname),
    mystackU=mystackA.S{image_number};
    try, delete(mystackU.BH); end
    try, delete(mystackU.RN); end
    try, delete(mystackU.CELLLABEL);delete(mystackU.CELLLABELpt); end
    try, mystackU=rmfield(mystackU,{'BH'}); end
    try, mystackU=rmfield(mystackU,{'RN'}); end
    CRSP=mystackA.cfg.CRSP{mystackA.cmp(image_number)};
    try, CL  =mystackA.cfg.CL{mystackA.cmp(image_number)}; catch, CL=[]; end
    
    try, delete(mystackU.CELLLABEL);delete(mystackU.CELLLABELpt); end
    try, mystackU=rmfield(mystackU,{'CELLLABEL','CELLLABELpt'}); end
    if not(isempty(CRSP)),
        if isfield(CRSP,'B'),
        B = CRSP.B;
        %[B] = bwboundaries(mystackU.M);
        clear BH;
        for i=1:length(B),
            hold on;BH(i)=plot(B{i}(:,1),B{i}(:,2),'w','Color',[0.5 0.5 0.5]);
            %hold on;BH(i)=plot(B{i}(:,2),B{i}(:,1),'w','Color',[0.5 0.5 0.5]);
        end
        mystackU.BH=BH;
        
        if strcmp(get(mystackA.menu_labelhide,'Checked'),'off'),
            cix=1;
            for i=1:size(CRSP.conversion,1);
                rc=CRSP.C(i,:);
                %mystackU.RN(cix)=text(rc(1),rc(2),num2str(CRSP.conversion(i,2)),'Color',[1 1 1]);
                if isfield(mystackA,'ZD'), % if Z-DEPTH defined
                    %mystackU.RN(cix)=text(rc(1),rc(2),sprintf('%d_d%d',CRSP.conversion(i,2),round(mystackA.ZD.dist{mystackA.LAY}{image_number}(i))),'Color',[0 0 1],'FontSize',12,'FontWeight','bold','Interpreter','none');                    
                    mystackU.RN(cix)=text(rc(1),rc(2),sprintf('%d_d%d',CRSP.conversion(i,2),round(mystackA.ZD.dist{mystackA.LAY}{image_number}(i))),'Color',[1 0 0],'FontSize',9,'FontWeight','bold','Interpreter','none');                    
                else
                    mystackU.RN(cix)=text(rc(1),rc(2),num2str(CRSP.conversion(i,2)),'Color',[0 0 1],'FontSize',12,'FontWeight','bold');
                end
                %mystackU.RN(cix)=text(rc(1),rc(2),num2str(CRSP.conversion(i,2)),'Color',[1 1 1]);
                cix=cix+1;
            end
        end
        
        if ~isempty(CL), % isfield(CRSP,'CELLLABEL'),
            cix=1;
            for i=1:length(CL.CELLLABEL), % size(CRSP.CELLLABEL,1);
                %mystackU.CELLLABEL(cix)=text(CRSP.CELLLABEL{i}{1}(1),CRSP.CELLLABEL{i}{1}(2),CRSP.CELLLABEL{i}{2},'Color',[1 0 1],'FontSize',12);
                %mystackU.CELLLABELpt(cix)=plot(CRSP.CELLLABEL{i}{1}(1),CRSP.CELLLABEL{i}{1}(2),'.');
                mystackU.CELLLABEL(cix)=text(CL.CELLLABEL{i}{1}(1),CL.CELLLABEL{i}{1}(2),CL.CELLLABEL{i}{2},'Color',[1 0 1],'FontSize',12);
                mystackU.CELLLABELpt(cix)=plot(CL.CELLLABEL{i}{1}(1),CL.CELLLABEL{i}{1}(2),'.');
                cix=cix+1;
            end
        end % if isfield(CRSP,'CELLLABEL'),
        end % if isfield(CRSP,'B'),
    end % if not(isempty(CRSP)),
    
    mystackA.S{image_number}=mystackU;
    if length( mystackA.cfg.CRSP )>1,
        if not( isempty(mystackA.cfg.CRSP{mystackA.cmp(1)})|isempty(mystackA.cfg.CRSP{mystackA.cmp(2)}) ),
            if image_number==2,
                for image_number=1:2,
                    ix=find(ismember(mystackA.cfg.CRSP{mystackA.cmp(image_number)}.conversion(:,2),...
                        mystackA.cfg.CRSP{mystackA.cmp(2-image_number+1)}.conversion(:,2)));
                    set(mystackA.S{image_number}.BH(ix),'Color',[0 1 0]);
                end
            end
        end
    end
end % if image_number<=length(mystackA.cfg.fname),