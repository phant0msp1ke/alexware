javaaddpath '/home/alex/java/jar/mij.jar'; 

if 0, % convert files to binary format:
    
    cfg.rootdir = '/home/alex/Desktop/Data/Reconstruction_data/Example/';
    
    cfg.fname   = 'AVG_reg_area13_tones.tif'; % 2p image
    tsStack = TIFFStack([cfg.rootdir,cfg.fname]); % I=tsStack(:, :, 1);figure;imagesc();colorbar;
    %getImageInfo(tsStack)
    [x,y,z]=fileparts([cfg.rootdir,cfg.fname]);
    filesav = sprintf('%s%s.bin',cfg.rootdir,y);
    fidsave = fopen(filesav,'w');
    dim=[size(tsStack,1),size(tsStack,2)];
    fwrite(fidsave,dim,'int16'); % write header to file
    for i=1:size(tsStack,3),
        I = tsStack(:, :, i);
        fwrite(fidsave,I','int16'); % write to file
    end
    fclose all;
    
    cfg.fname   = 'Slice8.tif'; % 2p image
    tsStack = TIFFStack([cfg.rootdir,cfg.fname]);
    [x,y,z]=fileparts([cfg.rootdir,cfg.fname]);
    filesav = sprintf('%s%s.bin',cfg.rootdir,y);
    fidsave = fopen(filesav,'w');
    dim=[size(tsStack,1),size(tsStack,2)];
    fwrite(fidsave,dim,'int16'); % write header to file
    for i=1:size(tsStack,3),
        I = tsStack(:, :, i);
        fwrite(fidsave,I','int16'); % write to file
    end
    fclose all;
    
end % if 0, % convert files to binary format: only do once

% http://bigwww.epfl.ch/sage/soft/mij/ install
% 1) add to C:\Program Files\MATLAB\R2016a\java\  mij.jar
% 2) add to Matlab startup file:
%   addpath(genpath('C:\Jasper\Fiji.app\scripts\'));
%   javaaddpath 'C:\Program Files\MATLAB\R2016a\java\mij.jar'; 
%   Matlab > Preferences > General > Java Heap memory increase to a few GBs

Miji; % http://bigwww.epfl.ch/sage/soft/mij/

%--------------------------------------------------------------------------
% immuno stack

cfgt.rootdir = '/home/alex/Desktop/Data/Reconstruction_data/Example/';
cfgt.fname   = 'Slice8';
cfgI=[];
cfgI.fname{1} = sprintf('%s%s.tif',cfgt.rootdir,cfgt.fname); % GCaMP channel
MIJ.run('Open...', sprintf('path=[%s]',cfgI.fname{1}));

%--------------------------------------------------------------------------
% open 2p stack used for matching
cfgt.rootdir = '/home/alex/Desktop/Data/Reconstruction_data/Example/';
cfgt.fname   = 'AVG_reg_area13_tones';

% image used for matching
clear global glb_tpti_viewer;
chnix = 1;
cfg       = [];
cfg.fname = sprintf('%s%s.bin',cfgt.rootdir,cfgt.fname);
cfg.MI = 0;
cfg.MA = 29000;
cfg.navg = 1;
tpti_viewer(cfg);

%--------------------------------------------------------------------------
% immuno stack
cfgt.rootdir = '/home/alex/Desktop/Data/Reconstruction_data/Example/';
cfgt.fname   = 'Slice8';

cfgI=[];
cfgI.fname{1} = sprintf('%s%s.bin',cfgt.rootdir,cfgt.fname);
% cfgI.fname{2} = [pathname,strrep(filename,'GCAMP','PV')];
% cfgI.fname{3} = [pathname,strrep(filename,'GCAMP','SOM')];
% cfgI.fname{4} = [pathname,strrep(filename,'GCAMP','VIP')];

clear global glb_tpti_viewer_confocal;
tpti_viewer_confocal(cfgI);


%--------------------------------------------------------------------------
% instructions:

%-------------
% in ImageJ: 
% 1) Analyze > Set Measurements
% - Centroid  (X Y)
% - Stack position (Slice) 
% - Add to overlay (to have number for each point)

% 2) Set Measurements: draw little rectangle centered on point used for matching
% - press 'M' to add point to results table
% - make sure you add matching point in other stack for each point you add in same order
  
%-------------
% in the 2p viewer: 
% - insert matching points with middle mouse button: make sure order
% matches with points in result table
% - if you want to delete points, select 'clear point' menu and click close
% to point you want to delete, to go back to insert mode tick off 'clear
% point' checkbox again

%
% NOT USED ATM: in the confocal viewer: press 'I' to insert new matching
% point: instead insert points in Umage J
   
dbstop if error;

% in the confocal matlab viewer
% - first select area that you want to keep, and press setXY to current zoom 
% - select points in both images (at least 3), and press compute once done
% - press KEEP if you want to keep transform
% if happy, 'export tiff' to save transformed stack as tiff.

%--------------------------------------------------------------------------

test = 1;




