function myIviewviewer_immuno(cfg);

% edit mystackviewer_segment.m
% edit mystackviewer_CRSP.m
global myIview;
if 0,
    set(0,'ShowHiddenmyIview','on')
    delete(get(0,'Children'));
end

myIview=cfg;

myIview.curchn = 1;

n=1;
fname = myIview.fname{n};
l = dir(fname);
fid = fopen(fname);
myIview.dim    = fread(fid,2,'int16'); % size of image> dim(1) = Y dim(2) = X!!
% -for matrices, convention is to organize 1st dim=x, 2nd dim=y>
% -when plotting images needs to transpose because image convention is to have x axis as vertical....
myIview.totfrm = (l.bytes-2*(16/8))./(2*prod(myIview.dim)); % total number of frames
fclose(fid);

for n=2:length(myIview.fname),
    fname = myIview.fname{n};
    l = dir(fname);
    fid = fopen(fname);
    dim    = fread(fid,2,'int16'); % size of image
    totfrm = (l.bytes-2*(16/8))./(2*prod(dim)); % total number of frames
    fclose(fid);
    if ~(isequal(dim,myIview.dim)&isequal(totfrm,myIview.totfrm)),
        error('mismatch dim or nfrm different channels');
    end
end


myIview.curfrm = floor(myIview.totfrm/2);
% myIview.curfrm = round((get(src,'Value').*(myIview.totfrm-1))+1);
%val = (myIview.curfrm-1)/(myIview.totfrm-1)
%val = (myIview.totfrm-1)/(myIview.totfrm-1) > 1
%val = (1-1)/(myIview.totfrm-1) > 0

% read in middle frame
fname = myIview.fname{myIview.curchn};
fid = fopen(fname);
rng=myIview.curfrm;
fseek(fid, 2*(16/8) + (2*myIview.dim(1)*myIview.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
ndat =fread(fid,[length(rng)*prod(myIview.dim)],'*int16'); % read single frame
ndatr=reshape(ndat,[myIview.dim(2),myIview.dim(1),length(rng)]); % size(datr)
fclose(fid);
myIview.IMG=ndatr; % x , y

if isfield(myIview,'MI'),
    myIview.MI=myIview.MI;
else
    myIview.MI=min(min(nanmean(double(myIview.IMG),3)));
end
if isfield(cfg,'MA'),
    myIview.MA=myIview.MA;
else
    myIview.MA=max(max(nanmean(double(myIview.IMG),3)));
    myIview.MA=round(0.25*max(max(max(double(myIview.IMG),3))));
end

%--------------------------------------------------------------------------

myIview.f = figure('Units','normalized');
set(myIview.f,'Name',myIview.fname{myIview.curchn});
set(myIview.f,'CloseRequestFcn',@my_closefcn);

myIview.menu_extra = uimenu(gcf,'Label','Extra');
myIview.menu_extra_transformpoints  = uimenu(myIview.menu_extra,'Label','Compute Transform from Points',...
    'Callback',@call_menu_extra_transformpoints);
myIview.menu_extra_rotation  = uimenu(myIview.menu_extra,'Label','Specify Rotation',...
    'Callback',@call_menu_extra_rotation);
myIview.menu_extra_settransform  = uimenu(myIview.menu_extra,'Label','Set Transform',...
    'Callback',@call_menu_extra_settransform);
myIview.menu_extra_exporttiff  = uimenu(myIview.menu_extra,'Label','Export Tiff',...
    'Callback',@call_menu_extra_exporttiff);

set(gcf,'WindowKeyPressFcn',@call_keypress);

set(myIview.f,'toolbar','figure'); % otherwise disappears when adding uicontrol elements...

myIview.ax(1)=axes('Parent',myIview.f,'Units','normalized','Position',[0.024 0.056+0.02 0.385 1-(2*(0.056+0.02))]);
myIview.ax(2)=axes('Parent',myIview.f,'Units','normalized','Position',[0.435 0.056+0.02 0.385 1-(2*(0.056+0.02))]);
val = (myIview.curfrm-1)/(myIview.totfrm-1);
myIview.slider1=uicontrol('Parent',myIview.f,'Units','normalized','Style', 'slider',...
    'Position', [0.024 0.027-0.01 0.385 0.02],...
    'Min',0,'Max',1,'Value',val,'SliderStep',[1 10]./(myIview.totfrm-1),'Callback', @call_slider1);
myIview.slider2=uicontrol('Parent',myIview.f,'Units','normalized','Style', 'slider',...
    'Position', [0.435 0.027-0.01 0.385 0.02],...
    'Min',0,'Max',1,'Value',0,'SliderStep',[1 10]./(myIview.totfrm-1),'Callback', @call_slider2);

uicontrol('Parent',myIview.f,'Units','normalized','Style','edit',...
    'Position', [0.871 0.113 0.077 0.029],...
    'String',num2str(myIview.MI),'Callback', @call_editcmin);
uicontrol('Parent',myIview.f,'Units','normalized','Style', 'slider',...
    'Position', [0.845 0.084 0.128 0.029],...
    'Min',0,'Max',1,'Value',0,'SliderStep',[1 10]./(myIview.totfrm-1),'Callback', @call_slidermin);
uicontrol('Parent',myIview.f,'Units','normalized','Style','edit',...
    'Position', [0.871 0.056 0.077 0.029],...
    'String',num2str(myIview.MA),'Callback', @call_editcmax); % num2str(myIview.MA)
uicontrol('Parent',myIview.f,'Units','normalized','Style', 'slider',...
    'Position', [0.845 0.027 0.128 0.029],...
    'Min',0,'Max',1,'Value',0,'SliderStep',[1 10]./(myIview.totfrm-1),'Callback', @call_slidermax);

uicontrol('Parent',myIview.f,'Units','normalized','Style', 'pushbutton',...
    'Position', [0.845 0.426 0.128 0.059],...
    'String','Compute','Callback', @call_compute);

uicontrol('Parent',myIview.f,'Units','normalized','Style', 'pushbutton',...
    'Position', [0.845 0.426-0.08 0.128 0.059],...
    'String','Keep','Callback', @call_keep);

myIview.T=eye(4); % default transform matrix
str=[];
for i=1:size(myIview.T,2),
    str{i}=sprintf('%1.2f  %1.2f  %1.2f  %1.2f',myIview.T(i,:));
end
myIview.text = uicontrol('Parent',myIview.f,'Units','normalized','Style','text',...
    'Position', [0.845 0.683 0.129 0.259],...
    'String',str);

myIview.ORI = floor( [myIview.dim([2,1]);myIview.totfrm]./2 )'; % x y z
myIview.editorigin = uicontrol('Parent',myIview.f,'Units','normalized','Style','edit',...
    'Position', [0.845 0.627 0.129 0.029],...
    'String',num2str(myIview.ORI),'Callback', @call_editorigin);
myIview.NumZ = [-myIview.ORI(3)+1,myIview.totfrm-myIview.ORI(3)]; % myIview.ORI(3)+[ myIview.NumZ(1):myIview.NumZ(2)]
myIview.editNumZ = uicontrol('Parent',myIview.f,'Units','normalized','Style','edit',...
    'Position', [0.845 0.57 0.129 0.029],...
    'String',sprintf('%d,%d',myIview.NumZ),'Callback', @call_editNumZ);

% str=sprintf('%d,%d,%d,%d',-round(myIview.dim(2)/4),... % x1
%     round(myIview.dim(2)/4),... % x2
%     -round(myIview.dim(1)/4),... % y1
%     round(myIview.dim(1)/4))    % y2
str=sprintf('%d,%d,%d,%d',-600,600,-600,600);
myIview.editXY = uicontrol('Parent',myIview.f,'Units','normalized','Style','edit',...
    'Position', [0.845 0.513 0.129 0.029],...
    'String',str,'Callback', @call_editXY);
myIview.rect = str2num(str);

% plot image
axes(myIview.ax(1));cla;
myIview.hIMG=imagesc(myIview.IMG');
colormap(gray);
%colormap(morgenstemning);
caxis([myIview.MI,myIview.MA]);

hold on;myIview.rectH=patch(myIview.ORI(1)+myIview.rect([1,2,2,1]),...
    myIview.ORI(2)+myIview.rect([3,3,4,4]),[1 1 1],'FaceColor','none','EdgeColor',[1 1 1],'LineWidth',2);

function call_menu_extra_exporttiff(src,eventdata)

global myIview;

for chn=1:length(myIview.fname),
    
    %fname = myIview.fname{myIview.curchn}
    fname  = myIview.fname{chn};
    fnameS = strrep(fname,'.bin','.tif');
    %if not(exist(fnameS)),
        % convert image with series of transforms
        for T=1:length( myIview.history ),
            FRNG  = myIview.history{T}.FRNG;
            XRNG  = myIview.history{T}.XRNG;
            YRNG  = myIview.history{T}.YRNG;
            tform = myIview.history{T}.tform;
            if T==1,
                DAT = zeros([length(XRNG),length(YRNG),length(FRNG)]);
                fid = fopen(fname);
                l = dir(fname);
                dim    = fread(fid,2,'int16'); % size of image
                totfrm = (l.bytes-2*(16/8))./(2*prod(dim)); % total number of frames
                for n=1:length(FRNG),
                    rng=FRNG(n);
                    fseek(fid, 2*(16/8) + (2*dim(1)*dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
                    ndat =fread(fid,[length(rng)*prod(dim)],'*int16'); % read single frame
                    ndatr=reshape(ndat,[dim(2),dim(1),length(rng)]); % size(datr)
                    DAT(:,:,n)=ndatr(XRNG,YRNG);
                end % for i=1:myIview.totfrm,
                fclose(fid);
            else
                DAT=DATT(XRNG,YRNG,FRNG);
            end
            R = makeresampler('linear', 'fill');
            TDIMS_A = [1 2 3];
            TDIMS_B = [1 2 3];
            TSIZE_B = size(DAT);
            TMAP_B = [];
            F = 0;
            DATT = tformarray(DAT, tform, R, TDIMS_A, TDIMS_B, TSIZE_B, TMAP_B, F);
        end
        if chn==1 & ~isequalwithequalnans( DATT,myIview.DAT0 ),
           error('error transform!'); 
        end
        
        t = Tiff(fnameS,'w');% 'w8');
        tagStruct.Photometric = Tiff.Photometric.MinIsBlack;
        tagStruct.Compression = Tiff.Compression.None;
        tagStruct.BitsPerSample = 16;
        tagStruct.SamplesPerPixel = 1;
        tagStruct.SampleFormat = Tiff.SampleFormat.UInt;
        %tagStruct.ImageLength = size(DATT,1);
        %tagStruct.ImageWidth = size(DATT,2);
        tagStruct.ImageLength = size(DATT,2);
        tagStruct.ImageWidth = size(DATT,1);
        tagStruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
        t.setTag(tagStruct);
        for j=1:size(DATT,3),
            p = uint16(DATT(:,:,j))';
            if j>1,
                t.writeDirectory();
                t.setTag(tagStruct);
            end
            t.write(p);
        end
        t.close();        
    %end    
    fprintf('finished channel %d\n',chn);
end % for chn=1:length(myIview.fname),

% also save the history
fname = myIview.fname{1};
fnameS2 = strrep(fname,'.bin','_history.mat');
clear H;
H.history = myIview.history;
H.fname   = myIview.fname;
save(fnameS2,'H');

function call_menu_extra_transformpoints(src,eventdata)

global myIview;

    
try,
    PNT=NaN(length(myIview.CELLLABEL),3);
    for i=1:length(myIview.CELLLABEL),
        PNT(i,:)=[myIview.CELLLABEL{i}{[1,2,3]}];
    end
    
    % get coordinates from mystackviewer_segment
    global mystack;
    PNT0=NaN(length(myIview.CELLLABEL),3);
    for i=1:length(mystack.GP),
        dum=get(mystack.GP(i),'Position')
        PNT0(i,:)=[dum(1:2),1];
    end
catch 
    PNT  = [1135 793 13; 1114 1450 8; 1618 1205 14];
    PNT0 = [263.7265 198.1626 1.0000; 249.1960 565.0325 1.0000; 548.7489 442.0650 1.0000];
end

zm = round(mean(PNT,1));
PNT0=PNT0-repmat(mean(PNT0,1),[size(PNT0,1),1]);
PNT =PNT -repmat(zm,[size(PNT,1),1]);

T=compute_transform(PNT',PNT0') % from immuno to 2p
myIview.T = T';

str=[];
for i=1:size(myIview.T,2),
    str{i}=sprintf('%1.2f  %1.2f  %1.2f  %1.2f',myIview.T(i,:));
end
set(myIview.text,'String',str);
% set ORI as center of plane
myIview.ORI = zm;
set(myIview.editorigin,'String',num2str(myIview.ORI));

try, delete(myIview.rectH); end
hold on;myIview.rectH=patch(myIview.ORI(1)+myIview.rect([1,2,2,1]),...
    myIview.ORI(2)+myIview.rect([3,3,4,4]),[1 1 1],'FaceColor','none','EdgeColor',[1 1 1],'LineWidth',2);

function call_menu_extra_settransform(src,eventdata)

global myIview;

str=[];
for i=1:size(myIview.T,2),
    str{i}=sprintf('%1.2f  %1.2f  %1.2f  %1.2f',myIview.T(i,:));
end

prompt = {'Set T:'};
dlg_title = 'T';
num_lines = 3;
def = {num2str(myIview.T)};
answer = inputdlg(prompt,dlg_title,num_lines,def);
if ~isempty(answer), % in case user didn't press cancel
    myIview.T = str2num(answer{1});    
    
    str=[];
    for i=1:size(myIview.T,2),
        str{i}=sprintf('%1.2f  %1.2f  %1.2f  %1.2f',myIview.T(i,:));
    end
    set(myIview.text,'String',str);    
end

function call_menu_extra_rotation(src,eventdata)

prompt = {'Set Rotation:'};
dlg_title = 'Rotate';
num_lines = 1;
def = {'0,0,0'};
answer = inputdlg(prompt,dlg_title,num_lines,def);
if ~isempty(answer), % in case user didn't press cancel
    
    global myIview;
    
    rot=str2num(answer{1}); % T=40
    
    % x-rotation
    % x-rotation
    Rx=[1 0 0; 0 cosd(rot(1)) -sind(rot(1)); 0 sind(rot(1)) cosd(rot(1))];
    % y-rotation
    Ry=[cosd(rot(2)) 0 sind(rot(2)); 0 1 0; -sind(rot(2)) 0 cosd(rot(2))];
    % z-rotation
    Rz=[cosd(rot(3)) -sind(rot(3)) 0; sind(rot(3)) cosd(rot(3)) 0; 0 0 1];
    
    Rx = [[Rx,[0 0 0]']; 0 0 0 1];
    Ry = [[Ry,[0 0 0]']; 0 0 0 1];
    Rz = [[Rz,[0 0 0]']; 0 0 0 1];
    
    % The forward mapping is the composition of T1, T2, and T3.
    
    myIview.T = Rx * Ry * Rz;
    str=[];
    for i=1:size(myIview.T,2),
        str{i}=sprintf('%1.2f  %1.2f  %1.2f  %1.2f',myIview.T(i,:));
    end
    set(myIview.text,'String',str);
    
end

function call_keep(src,eventdata)

global myIview;

myIview.DAT0    = myIview.DATT;
if ~isfield(myIview,'history'),
    cix = 1;
else
    cix = length(myIview.history)+1;
end
myIview.history{cix}.FRNG  = myIview.FRNG;
myIview.history{cix}.XRNG  = myIview.XRNG;
myIview.history{cix}.YRNG  = myIview.YRNG;
myIview.history{cix}.tform = myIview.tform;
% just for record
myIview.history{cix}.T1 = myIview.T1;
myIview.history{cix}.T2 = myIview.T2;
myIview.history{cix}.T3 = myIview.T3;

myIview.totfrm = size(myIview.DAT0,3);
myIview.dim    = [size(myIview.DAT0,2),size(myIview.DAT0,1)];
myIview.curfrm = 1;
val = (myIview.curfrm-1)/(myIview.totfrm-1);
set(myIview.slider1,'Value',val);
myIview.ORI = floor( size(myIview.DAT0)/2 ); % x y z
set(myIview.editorigin,'String',num2str(myIview.ORI));
myIview.NumZ = [-myIview.ORI(3)+1,size(myIview.DAT0,3)-myIview.ORI(3)];
set(myIview.editNumZ,'String',sprintf('%d,%d',myIview.NumZ));
xr=[-(myIview.ORI(1)-1),size(myIview.DAT0,1)-myIview.ORI(1)];
yr=[-(myIview.ORI(2)-1),size(myIview.DAT0,2)-myIview.ORI(2)];
%xa=[myIview.ORI(1)+xr(1):myIview.ORI(1)+xr(2)];
str=sprintf('%d,%d,%d,%d',xr(1),... % x1
    xr(2),... % x2
    yr(1),... % y1
    yr(2))    % y2
myIview.editXY = uicontrol('Parent',myIview.f,'Units','normalized','Style','edit',...
    'Position', [0.845 0.513 0.129 0.029],...
    'String',str,'Callback', @call_editXY);
myIview.rect = str2num(str);

% plot image
axes(myIview.ax(1));cla;
imagesc( squeeze(myIview.DAT0(:,:,myIview.curfrm))' );caxis([myIview.MI,myIview.MA]);
%title(sprintf('z=%d',zval));
axis([0.5,size(squeeze(myIview.DAT0(:,:,myIview.curfrm))',1)+0.5,0.5,size(squeeze(myIview.DAT0(:,:,myIview.curfrm))',2)+0.5]);
%set(gca,'YDir','normal');
colormap(gray);
%colormap(morgenstemning);
caxis([myIview.MI,myIview.MA]);

hold on;myIview.rectH=patch(myIview.ORI(1)+myIview.rect([1,2,2,1]),...
    myIview.ORI(2)+myIview.rect([3,3,4,4]),[1 1 1],'FaceColor','none','EdgeColor',[1 1 1],'LineWidth',2);

% also delete all points in CELLLABEL
try
myIview=rmfield(myIview,'CELLLABEL');
end
% function call_drag(src,eventdata)
%
% test= 1;
function call_compute(src,eventdata)

global myIview;

fprintf('started to compute...');

% read in selected part of stack
FRNG = myIview.ORI(3)+myIview.NumZ(1):myIview.ORI(3)+myIview.NumZ(2);
FRNG = FRNG(FRNG>0&FRNG<myIview.totfrm+1);

XRNG=myIview.ORI(1)+[myIview.rect(1):myIview.rect(2)];
YRNG=myIview.ORI(2)+[myIview.rect(3):myIview.rect(4)];

XRNG = XRNG(XRNG>0&XRNG<myIview.dim(2)+1);
YRNG = YRNG(YRNG>0&YRNG<myIview.dim(1)+1);

myIview.FRNG=FRNG;
myIview.XRNG=XRNG;
myIview.YRNG=YRNG;
DAT = zeros(length(XRNG),length(YRNG),length(FRNG));

if ~isfield(myIview,'DAT0'),
    for n=1:length(FRNG),
        % read in frame
        fname = myIview.fname{myIview.curchn};
        fid = fopen(fname);
        rng=FRNG(n);
        fseek(fid, 2*(16/8) + (2*myIview.dim(1)*myIview.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
        ndat =fread(fid,[length(rng)*prod(myIview.dim)],'*int16'); % read single frame
        ndatr=reshape(ndat,[myIview.dim(2),myIview.dim(1),length(rng)]); % size(datr)
        fclose(fid);
        %ndatr=ndatr';
        DAT(:,:,n)=ndatr(XRNG,YRNG);
    end % for i=1:myIview.totfrm,
else
    DAT=myIview.DAT0(XRNG,YRNG,FRNG);
end

% 1. translate the middle to the origin.
%blob_center = (size(DAT) + 1) / 2
blob_center = [find(XRNG==myIview.ORI(1)),find(YRNG==myIview.ORI(2)),find(FRNG==myIview.ORI(3))];
T1 = [1 0 0 0
    0 1 0 0
    0 0 1 0
    -blob_center 1];

% 2. Rotate the blob.

% theta = pi/8;
% T2 = [cos(theta)  0      -sin(theta)   0
%     0             1              0     0
%     sin(theta)    0       cos(theta)   0
%     0             0              0     1]
T2 = myIview.T;

% 3. Translate the rotated blob back to its starting location.

T3 = [1 0 0 0
    0 1 0 0
    0 0 1 0
    blob_center 1];

% The forward mapping is the composition of T1, T2, and T3.

T = T1 * T2 * T3;

tform = maketform('affine', T);
myIview.tform = tform;
myIview.T1 = T1;
myIview.T2 = T2;
myIview.T3 = T3;
% Let's do a quick sanity check: the tform struct should map the blob center to itself.

tformfwd(blob_center, tform)
%
%

% R is a resampler struct produced by the makeresampler function. You tell makeresampler the type of interpolation you want, as well as how to handle array boundaries.
R = makeresampler('linear', 'fill');

% TDIMS_A specifies how the dimensions of the input array correspond to the dimensions of the spatial transformation represented by the tform struct. Here I'll use the simplest form, in which each spatial transformation dimension corresponds to the same input array dimension. (Don't worry about the details here. One of these days I'll write a blog posting showing an example of when you might want to do something different with this dimension mapping.)
TDIMS_A = [1 2 3];

% TDIMS_B specifies how the dimensions of the output array correspond to the dimensions of the spatial transformation.
TDIMS_B = [1 2 3];

% TSIZE_B is the size of the output array.
TSIZE_B = size(DAT);

% TMAP_B is unused when you have a tform struct. Just specify it to be empty.
TMAP_B = [];

% F specifies the values to use outside the boundaries of the input array.
F = 0;

DATT = tformarray(DAT, tform, R, TDIMS_A, TDIMS_B, TSIZE_B, TMAP_B, F);
%DATT = flipdim(DATT,2);
%DATT = flipdim(DATT,1);
myIview.DATT = DATT;

call_plot_DATT(src,[]);
set(gca,'XLim',[0.5,size(DATT,1)+0.5],'YLim',[0.5,size(DATT,2)+0.5]);

beep;pause(0.3);beep;pause(0.1);beep;
fprintf('DONE!\n');

function call_keypress(src,eventdata)
%WindowKeyPressFcn

if ismember(eventdata.Key,{'c','i','p','l'}),
    global myIview;
    if strcmp(eventdata.Key,'c'), % change color channel
        myIview.curchn=myIview.curchn+1;
        myIview.curchn=mod(myIview.curchn-1,length(myIview.fname))+1;
        set(myIview.f,'Name',myIview.fname{myIview.curchn});
        updateframe=1;call_plot(src,eventdata,updateframe);
    elseif strcmp(eventdata.Key,'i'),
        [x, y, button] = ginput(1);
        if button==1,
            x=round(x);
            y=round(y);
            if ~isfield(myIview,'CELLLABEL'),
                myIview.CELLLABEL{1,:} = {x,y,myIview.curfrm,myIview.fname{myIview.curchn}};
            else
                myIview.CELLLABEL{size(myIview.CELLLABEL,1)+1,:} = {x,y,myIview.curfrm,myIview.fname{myIview.curchn}};
            end
        elseif button==3, % delete nearest point
            if isfield(myIview,'CELLLABEL'),
                D=NaN([size(myIview.CELLLABEL,1),1]);
                for i=1:size(myIview.CELLLABEL,1),
                    %if myIview.CELLLABEL{i}{3}==myIview.curfrm,
                    D(i)=sqrt(sum([[x,y]-[myIview.CELLLABEL{i}{1:2}]].^2));
                    %end
                end
                if any(~isnan(D)),
                    [~,ix] = min(D);
                end
                if size(myIview.CELLLABEL,1)==1,
                    myIview=rmfield(myIview,'CELLLABEL');
                else
                    myIview.CELLLABEL(ix,:)=[]
                end
            end
        end
        updateframe=1;call_plot(src,eventdata,updateframe);
    elseif strcmp(eventdata.Key,'p'), % backward transform
        axes( myIview.ax(2) );
        [x, y, button] = ginput(1);
        x=round(x);
        y=round(y);
        
        src=myIview.slider2; zval = round((get(src,'Value').*(size(myIview.DATT,3)-1))+1);
        hold on;plot(x,y,'gx','MarkerSize',10,'LineWidth',2);
        tform = myIview.tform;
        M = tforminv([x,y,zval],tform);
        
        M(1)=M(1)+myIview.XRNG(1)-1;
        M(2)=M(2)+myIview.YRNG(1)-1;
        M(3)=M(3)+myIview.FRNG(1)-1;
        
        myIview.curfrm = round(M(3));
        updateframe=1;call_plot(src,eventdata,updateframe);
        x=round(M(1));
        y=round(M(2));
        hold on;plot(x,y,'gx','MarkerSize',10,'LineWidth',2);
    elseif strcmp(eventdata.Key,'l'), % forward transform
        axes( myIview.ax(1) );
        [x, y, button] = ginput(1);
        x=round(x);
        y=round(y);
        zval = myIview.curfrm
        if ismember(x,myIview.XRNG)&ismember(y,myIview.YRNG)&ismember(zval,myIview.FRNG),
            hold on;plot(x,y,'gx','MarkerSize',10,'LineWidth',2);
            
            tform = myIview.tform;
            M = tformfwd([find(myIview.XRNG==x),find(myIview.YRNG==y),find(myIview.FRNG==zval)],tform);
            M = round(M);
           
            zval2=M(3)
            val = (zval2-1)./(size(myIview.DATT,3)-1);
            %src=myIview.slider2; zval = round((get(src,'Value').*(size(myIview.DATT,3)-1))+1);
            set(myIview.slider2,'Value',val);
            call_plot_DATT(src,eventdata)  
            x2=round(M(1));
            y2=round(M(2));
            hold on;plot(x2,y2,'gx','MarkerSize',10,'LineWidth',2);
        end
    end
    
end

function call_slider1(src,eventdata)

global myIview;
myIview.curfrm = round((get(src,'Value').*(myIview.totfrm-1))+1);
updateframe=1;call_plot(src,eventdata,updateframe);

function call_slider2(src,eventdata)
call_plot_DATT(src,eventdata)


function call_editXY(src,eventdata)

global myIview;
try, delete(myIview.rectH); end
myIview.rect = str2num(get(src,'String'));
hold on;myIview.rectH=patch(myIview.ORI(1)+myIview.rect([1,2,2,1]),...
    myIview.ORI(2)+myIview.rect([3,3,4,4]),[1 1 1],'FaceColor','none','EdgeColor',[1 1 1],'LineWidth',2);

function call_editorigin(src,eventdata)

global myIview;
myIview.ORI = str2num(get(src,'String'));

function call_editNumZ(src,eventdata)

global myIview;
myIview.NumZ = str2num(get(src,'String'));

function call_editcmin(src,eventdata)

global myIview;
myIview.MI = str2num(get(src,'String'));

axes(myIview.ax(1)); 
caxis([myIview.MI,myIview.MA])

axes(myIview.ax(2)); 
caxis([myIview.MI,myIview.MA])

function call_editcmax(src,eventdata)

global myIview;
myIview.MA = str2num(get(src,'String'));

axes(myIview.ax(1)); 
caxis([myIview.MI,myIview.MA])

axes(myIview.ax(2)); 
caxis([myIview.MI,myIview.MA])

function call_slidermin(src,eventdata)

global myIview;

function call_slidermax(src,eventdata)

global myIview;

function call_plot_DATT(src,eventdata)

global myIview;

src=myIview.slider2; zval = round((get(src,'Value').*(size(myIview.DATT,3)-1))+1);

axes(myIview.ax(2));cla;
imagesc( squeeze(myIview.DATT(:,:,zval))' );caxis([myIview.MI,myIview.MA]);title(sprintf('z=%d',zval));

function call_plot(src,eventdata,updateframe)

global myIview;

if updateframe&(~isfield(myIview,'DAT0')),    
    % read in frame
    fname = myIview.fname{myIview.curchn};
    fid = fopen(fname);
    rng=myIview.curfrm;
    fseek(fid, 2*(16/8) + (2*myIview.dim(1)*myIview.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
    ndat =fread(fid,[length(rng)*prod(myIview.dim)],'*int16'); % read single frame
    ndatr=reshape(ndat,[myIview.dim(2),myIview.dim(1),length(rng)]); % size(datr)
    fclose(fid);
    %myIview.IMG=ndatr';
    myIview.IMG=ndatr; % x , y
    title(sprintf('frm%d',myIview.curfrm));
end
if updateframe,
    axes(myIview.ax(1));
    if ~isfield(myIview,'DAT0'),
        myIview.hIMG=imagesc(myIview.IMG');
    else
        imagesc( squeeze(myIview.DAT0(:,:,myIview.curfrm))' );%%caxis([myIview.MI,myIview.MA]);%title(sprintf('z=%d',zval));        
    end
    caxis([myIview.MI,myIview.MA]);
    %uistack(myIview.hM,'top')
    if isfield(myIview,'CELLLABEL'),
        hold on;ax=axis;
        for i=1:size(myIview.CELLLABEL,1),
            if myIview.CELLLABEL{i}{3}==myIview.curfrm,
                myIview.CELLLABELpt(i)=plot(myIview.CELLLABEL{i}{1},myIview.CELLLABEL{i}{2},'gx','MarkerSize',10,'LineWidth',2);
            else
                myIview.CELLLABELpt(i)=plot(myIview.CELLLABEL{i}{1},myIview.CELLLABEL{i}{2},'gs','MarkerSize',10,'LineWidth',2);
            end
        end
        axis(ax);
    end
    
    try, delete(myIview.rectH); end
    hold on;myIview.rectH=patch(myIview.ORI(1)+myIview.rect([1,2,2,1]),...
    myIview.ORI(2)+myIview.rect([3,3,4,4]),[1 1 1],'FaceColor','none','EdgeColor',[1 1 1],'LineWidth',2);

    title(sprintf('frm=%d',myIview.curfrm));
end
if isfield(myIview,'GP'),
    uistack(myIview.GP,'top');
end


function my_closefcn(src,eventdata)
% User-defined close request function
% to display a question dialog box
if 0,
    selection = questdlg('Close This Figure?',...
        'Close Request Function',...
        'Yes','No','Yes');
    switch selection,
        case 'Yes',
            delete(gcf);
        case 'No'
            return
    end
end
delete(gcf);

