function mystackdatviewer(cfg,I);

% MASK is mask (ny * nx * 3 RGB)
% IMG is currently selected frame

if 0,
    set(0,'ShowHiddenHandles','on')
    delete(get(0,'Children'));
end

global mystack;
mystack.fname = cfg.fname;

if ~isempty(strfind(mystack.fname,'_offsets.mat')),
     mystack.chnix = 1;
    % edit resscn_get_offsets
    mystack.dataformat=2; % file that contains offset with which raw data needs to be shifted
    dum=load(mystack.fname);
    mystack.ops=dum.ops;    
    mystack.fname=strrep(mystack.fname,'_offsets.mat','.bin'); % file with raw data
    %mystack.patch.y = 1:mystack.ops.hdr.dim{mystack.chnix}(1);%1:size(mystack.ops.mean_image,1); % mystack.ops.rngpix yrange; % vertical indices to keep
    %mystack.patch.x = 1:mystack.ops.hdr.dim{mystack.chnix}(1);%1:size(mystack.ops.mean_image,2);  % horizontal indices to keep
    mystack.patch.y = 1:mystack.ops.hdr.dim{mystack.chnix}(2);%1:size(mystack.ops.mean_image,1); % mystack.ops.rngpix yrange; % vertical indices to keep
    mystack.patch.x = 1:mystack.ops.hdr.dim{mystack.chnix}(1);%1:size(mystack.ops.mean_image,2);  % horizontal indices to keep
    if isfield(cfg,'subpixel'),mystack.subpixel=cfg.subpixel;else,mystack.subpixel=0;end % do pixel (faster) or subpixel alignment  
    mystack.ops.hdr
   
else
    mystack.dataformat=1; % raw data or data that is already shifted, in that case try to read header
    
    fname = mystack.fname; 
    fname = strrep(fname,'.bin','.ini');
    fname(strfind(fname,'_ch'):end)=[];
    cfghdr=[];    
    cfghdr.fname = fname;
    cfghdr.channel = [1,2];
    hdr          = resscn_info(cfghdr); % edit resscn_info
    hdr          = rsbs_extract_ini(hdr);    
    [x,y,z]=fileparts(mystack.fname);
    mystack.chnix = strmatch(y,hdr.fileini);
    mystack.ops.hdr = hdr;
end

if ~isfield(mystack.ops.hdr,'fields'),
    mystack.ops.hdr.fields{1} = [];
    mystack.ops.hdr.frmN = 1:mystack.ops.hdr.totfrm{1};
end

    
if ~isfield(mystack.ops.hdr.fields{1},'piezo_nbrlayers'),
    mystack.nlayer = 1;
    mystack.layernum = ones(size(mystack.ops.hdr.frmN));
else
    mystack.nlayer = mystack.ops.hdr.fields{1}.piezo_nbrlayers;
    mystack.layernum = mod(mystack.ops.hdr.frmN,mystack.ops.hdr.fields{1}.piezo_nbrlayers) + 1 ; % plane number (1:Nplanes)
end
for layer=1:mystack.nlayer,
    sel = find(mystack.layernum(1:mystack.ops.hdr.totfrm{mystack.chnix})==layer); % sometimes for red chn only first frames recorded (for structural image)
    mystack.totfrmlayer(layer)=length(sel);
end
    
mystack.dim    = mystack.ops.hdr.dim{mystack.chnix}; % size of image
mystack.totfrm = mystack.ops.hdr.totfrm{mystack.chnix}; % size of image

mystack.layer  = 1;
mystack.sel = find(mystack.layernum(1:mystack.totfrm)==mystack.layer); % sometimes for red chn only first frames recorded (for structural image)

if mystack.dataformat==2,
    if mystack.subpixel==1,
        % for using subpixel alignment
        sizX        = mystack.dim(2);
        sizY        = mystack.dim(1);
        [X1, X2]    = ndgrid(1:sizX,1:sizY);
        mystack.sp.sizX = sizX;
        mystack.sp.sizY = sizY;
        mystack.sp.X1   = X1;
        mystack.sp.X2   = X2;
    end
end % if mystack.dataformat==0,

mystack.frame = 1;

if isfield(cfg,'navg'),          mystack.navg  = cfg.navg;                  else mystack.navg  = 30;        end
if isfield(cfg,'medfilt'),       mystack.medfilt = cfg.medfilt;             else mystack.medfilt = 0;       end
if isfield(cfg,'moviestepsize'), mystack.moviestepsize = cfg.moviestepsize; else mystack.moviestepsize = 1; end

%--------------------------------------------------------------------------
% get a few frames to determine scaling factors
fid = fopen(mystack.fname);
ix = round(linspace(1,mystack.totfrmlayer(mystack.layer),30)); % selection of frames from current layer
if mystack.dataformat==2,
    IMG = zeros([length(mystack.patch.y), length(mystack.patch.x), length(ix)], 'int16');
else
    IMG = NaN([mystack.dim(2),mystack.dim(1),length(ix)]);
end
for i=1:length(ix),    
    rng=mystack.sel(ix(i)); % indices to frames from this layer in full stack
    
    % read in those frames
    fseek(fid, 2*(16/8) + (2*mystack.dim(1)*mystack.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
    ndat =fread(fid,[1*prod(mystack.dim)],'*int16'); % read frames
    ndatr=reshape(ndat,[mystack.dim(2),mystack.dim(1),length(rng)]); % size(datr)
    
    if mystack.dataformat==2,        
        Itmp = squeeze(mean(ndatr,3));
        if mystack.subpixel
            x0 = mystack.ops.del1{mystack.layer}(ix(i));
            y0 = mystack.ops.del2{mystack.layer}(ix(i));
            F = griddedInterpolant(mystack.sp.X1, mystack.sp.X2,  double(Itmp), 'linear');
            [X1n, X2n]  = ndgrid([1:mystack.sp.sizX]-x0,[1:mystack.sp.sizY]-y0);
            Itmp = F(X1n(mystack.patch.y, mystack.patch.x), X2n(mystack.patch.y, mystack.patch.x));
            IMG(:,:,i) = int16(Itmp);
        else
            J = zeros(size(IMG,1), size(IMG,2), 'int16');
%             if mystack.nlayer==1,
%                 x0 = round(mystack.ops.del2(ix(i)));
%                 y0 = round(mystack.ops.del1(ix(i)));
%             else
                x0 = round(mystack.ops.del2{mystack.layer}(ix(i)));
                y0 = round(mystack.ops.del1{mystack.layer}(ix(i)));
%             end
            cy = (mystack.patch.y - y0);
            cx = (mystack.patch.x - x0);
            Iy = cy > 0 & cy < size(Itmp,1);
            Ix = cx > 0 & cx < size(Itmp,2);
            J(Iy, Ix) = Itmp(cy(Iy), cx(Ix));
            IMG(:,:,i) = J;
        end
    else
        IMG(:,:,i) = squeeze(mean(ndatr,3));
    end % if mystack.dataformat==2,    
end
fclose(fid);
if isfield(cfg,'MI'),
    mystack.MI=cfg.MI;
else
    %if mystack.dataformat==1,
        mystack.MI=min(min(nanmean(IMG,3)));
    %else
    %    mystack.MI=min(min(nanmean(IMG(mystack.ops.yrange,:,:),3)));
    %end
end
if isfield(cfg,'MA'),
    mystack.MA=cfg.MA;
else
    %if mystack.dataformat==1,
        mystack.MA=max(max(nanmean(IMG,3))); 
    %else
    %    mystack.MA=max(max(nanmean(IMG(mystack.ops.yrange,:,:),3))); 
    %end
end
mystack.IMG = mean(IMG,3)';

if ~isfield(mystack,'I'),
    if nargin==1,
        %mystack.I=logical(zeros([size(IMG,1),size(IMG,2)]));
        mystack.I=logical(zeros([size(IMG,2),size(IMG,1)]));
    else
        mystack.I=I;
    end
end
mystack.alpha_setting = 0.1;
mystack.brushsize = 4;

%--------------------------------------------------------------------------

mystack.handles.f = figure('Units','normalized');
set(mystack.handles.f,'Name',cfg.fname);
set(mystack.handles.f,'CloseRequestFcn',@my_closefcn);

%set(gcf,'menubar','none')
mystack.menu_extra = uimenu(gcf,'Label','Extra');
mystack.menu_extra_zoom  = uimenu(mystack.menu_extra,'Label','Size Threshold',...
    'Callback',@call_menu_extra_sizethreshold);
mystack.menu_extra_zoom  = uimenu(mystack.menu_extra,'Label','Fill In Holes',...
    'Callback',@call_menu_extra_fillholes);
mystack.menu_extra_erode  = uimenu(mystack.menu_extra,'Label','Erode',...
    'Callback',@call_menu_extra_erode);
mystack.menu_extra_plot_roi_traces  = uimenu(mystack.menu_extra,'Label','Show ROI Traces',...
    'Callback',@call_plot_roi_traces);
mystack.menu_extra_show_roi_number  = uimenu(mystack.menu_extra,'Label','Show ROI Number',...
    'Callback',@call_show_roi_number);
mystack.menu_extra_call_maxproj  = uimenu(mystack.menu_extra,'Label','Max Projection',...
    'Callback',@call_maxproj);
mystack.menu_extra_call_moviestepsize  = uimenu(mystack.menu_extra,'Label','Movie Stepsize',...
    'Callback',@call_menu_extra_moviestepsize);

set(gcf,'WindowKeyPressFcn',@call_keypress);
set(gcf,'WindowButtonDownFcn',@call_buttondown);
set(gcf,'WindowButtonUpFcn',@call_buttonup);
set(gcf,'WindowScrollWheelFcn',@call_wheel);

% get(gcf,'Units'), get(gcf,'Position')
uicontrol('Parent',mystack.handles.f,'Units','normalized','Style', 'slider',...
    'Position', [0.01 0.01 0.2 0.05],...
    'Min',0,'Max',1,'Value',0.5,'SliderStep',[1 100]./(mystack.totfrmlayer(mystack.layer)-1),'Callback', @call_slider);
uicontrol('Parent',mystack.handles.f,'Units','normalized','Style','edit',...
    'Position', [0.25 0.01 0.1 0.05],...
    'String',num2str(mystack.navg),'Callback', @call_editnavg);
uicontrol('Parent',mystack.handles.f,'Units','normalized','Style','edit',...
    'Position', [0.4 0.01 0.1 0.05],...
    'String',num2str(mystack.MI),'Callback', @call_editcmin);
uicontrol('Parent',mystack.handles.f,'Units','normalized','Style','edit',...
    'Position', [0.6 0.01 0.1 0.05],...
    'String',num2str(mystack.MA),'Callback', @call_editcmax);
uicontrol('Parent',mystack.handles.f,'Units','normalized','Style','edit',...
    'Position', [0.75 0.01 0.1 0.05],...
    'String',num2str(mystack.medfilt),'Callback', @call_editmedfilt);

clear str; for i=1:mystack.nlayer,str{i}=sprintf('layer%d',i); end
mystack.layerstr = str;
uicontrol('Parent',mystack.handles.f,'Units','normalized','Style','popupmenu',...
    'Position', [0.9 0.01 0.1 0.05],'Value',1,...
    'String',mystack.layerstr,'Callback', @call_editlayer);

mystack.handles.show_boundary = uicontrol('Parent',mystack.handles.f,'Units','normalized','Style','checkbox',...
    'Position', [0.01 0.95 0.15 0.05],...
    'String','Boundary','Value',0,'Callback',@call_showboundary);
uicontrol('Parent',mystack.handles.f,'Units','normalized','Style','pushbutton',...
    'Position', [0.2 0.95 0.1 0.05],...
    'String','<','Callback', @call_frame);
uicontrol('Parent',mystack.handles.f,'Units','normalized','Style','pushbutton',...
    'Position', [0.3 0.95 0.1 0.05],...
    'String','>','Callback', @call_frame);

uicontrol('Parent',mystack.handles.f,'Units','normalized','Style','text',...
    'Position', [0 0.2 0.08 0.7],...
    'String',{'M=max projection','S=size threshold','F=fill holes','P=play movie(stop with right mouse click)'},...
    'FontSize',6);

% plot image
mystack.hI=imagesc(mystack.IMG);colormap(gray);

MASK=ones(size(mystack.IMG));
mystack.MASK=cat(3,MASK,zeros([size(MASK),2]));
hold on;
mystack.hM=imagesc(mystack.MASK);
set(mystack.hM,'AlphaData',mystack.I.*mystack.alpha_setting);

xl=get(gca,'XLim')
yl=get(gca,'YLim')
xlims=linspace(xl(1),xl(end),3);
ylims=linspace(yl(1),yl(end),3);
dum=repmat([1:length(ylims)-1],[length(xlims)-1,1]);
pos=[repmat([1:length(xlims)-1]',[length(ylims)-1,1]),dum(:)];
% for i=1:length(xlims)-1,
%     for j=1:length(ylims)-1,
%         set(gca,'XLim',[xlims(i),xlims(i+1)]);
%         set(gca,'YLim',[ylims(i),ylims(i+1)]);
%     end
% end
mystack.xlims=xlims;
mystack.ylims=ylims;
mystack.pos=pos;
mystack.curp=0;

caxis([mystack.MI,mystack.MA]);

function call_maxproj(src, eventdata)

global mystack;
if mystack.dataformat~=2,    
    fid = fopen(mystack.fname);
    ix = 1:mystack.totfrmlayer(mystack.layer); %round(linspace(1,mystack.totfrmlayer(mystack.layer),30));
    IMG = NaN([mystack.dim(2),mystack.dim(1),length(ix)]);
    
    for i=1:length(ix),
        rng=ix(i);
        
        % read in those frames
        fseek(fid, 2*(16/8) + (2*mystack.dim(1)*mystack.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
        ndat =fread(fid,[length(rng)*prod(mystack.dim)],'*int16'); % read single frame
        ndatr=reshape(ndat,[mystack.dim(2),mystack.dim(1),length(rng)]); % size(datr)
        
        IMG(:,:,i) = squeeze(mean(ndatr,3));
    end
    maxprojection = max(IMG,[],3);
    try, delete(mystack.hIMG); end
    mystack.hIMG=imagesc(maxprojection');colormap(gray);
    uistack(mystack.hM,'top')
end %  if mystack.dataformat~=2,

function call_frame(src,eventdata)

global mystack;
if strcmp('>',get(gcbo,'String')),
    mystack.curp=mystack.curp+1;
elseif strcmp('<',get(gcbo,'String')),
    mystack.curp=mystack.curp-1;
end
if mystack.curp<0, mystack.curp=0; end
if mystack.curp>size(mystack.pos,1), mystack.curp=size(mystack.pos,1); end

mystack.curp
%call_plot([],[]);
updateframe=0;
updatemask=0;
updateboundary=0;
call_plot([],[],updateframe,updatemask,updateboundary);

% --------------------------------------------------------------------
function call_plot_roi_traces(src,eventdata)

global mystack;
BW=mystack.I';
[B,L,N,A]=bwboundaries(BW,'noholes'); % use with gcamp: only take shell, not inside
uROI = unique(L); uROI(uROI==0)=[]; % unique ROIs

% figure;imagesc(BW);

cfg=[];
cfg.mask    = L; % mask with number for every ROI
if mystack.totfrmlayer(mystack.layer)>1000, cfg.rng=30*30; else, cfg.rng=mystack.totfrmlayer(mystack.layer); end
[out]       = resscn_roidat_sel(cfg); % edit resscn_segment


% --------------------------------------------------------------------
function call_show_roi_number(src,eventdata)

if strcmp('off',get(src,'Checked')),
    set(src,'Checked','on');
    
    global mystack;
    BW=mystack.I';
    
    if length(unique(BW))>2, 
        B=unique(BW);B(B==0)=[];
        cix=1;
        cmap=jet(length(B))
        for i=1:length(B),
            [B1,L1]=bwboundaries(BW==B(i),'noholes');
            for j=1:length(B1),
                hold on;
                mystack.RN(cix)=plot(B1{j}(:,1),B1{j}(:,2),'r','LineWidth',2,'Color',cmap(i,:));
                cix=cix+1;
                mystack.RN(cix)=text(mean(B1{j}(:,1)),mean(B1{j}(:,2)),num2str(B(i)),'Color',[1 1 1],'HorizontalAlignment','center');
                cix=cix+1;
            end % for j=1:length(B1),
        end % for i=1:length(B),
    else
    [B,L,N,A]=bwboundaries(BW,'noholes'); % use with gcamp: only take shell, not inside
    uROI = unique(L); uROI(uROI==0)=[]; % unique ROIs
    
    cix=1;
    for i=1:length(B),
        hold on;
        mystack.RN(cix)=plot(B{i}(:,1),B{i}(:,2),'r','LineWidth',2);
        cix=cix+1;
        mystack.RN(cix)=text(mean(B{i}(:,1)),mean(B{i}(:,2)),num2str(i),'Color',[1 1 1],'HorizontalAlignment','center');
        cix=cix+1;
    end 
    end
else
    set(src,'Checked','off');
    global mystack;
    try, delete(mystack.RN); end
end

% figure;imagesc(BW);

function call_showboundary(src,eventdata)
updateframe=0;
updatemask=0;
updateboundary=1;
call_plot([],[],updateframe,updatemask,updateboundary);

function call_menu_extra_erode(src,eventdata)

prompt = {'Set Radius Erode Disk:'};
dlg_title = 'Erode';
num_lines = 1;
def = {'3'};
answer = inputdlg(prompt,dlg_title,num_lines,def);
if ~isempty(answer), % in case user didn't press cancel
    disk_radius=str2num(answer{1}); % T=40
    
    global mystack;
    BW = mystack.I;
    
    se = strel('disk',disk_radius);
    BWeroded = imerode(BW,se);
    if 0,
        figure;subplot(2,1,1);imshow(BW);
        subplot(2,1,2);imshow(BWeroded);
    end
    mystack.I=BWeroded;
    set(mystack.hM,'AlphaData',mystack.I.*mystack.alpha_setting);
end

function call_menu_extra_sizethreshold(src,eventdata)

prompt = {'Set Min Threshold:'};
dlg_title = 'Size Threshold';
num_lines = 1;
def = {'20'};
answer = inputdlg(prompt,dlg_title,num_lines,def);
if ~isempty(answer), % in case user didn't press cancel
    T=str2num(answer{1}); % T=40
    
    global mystack;
    
    BW = mystack.I;
    %figure;imagesc(BW);
    [B,L,N,A] = bwboundaries(BW);
    
    nB=zeros([1,length(B)]);
    for i=1:length(B),
        nB(i)=size(B{i},1);
    end
    L(ismember(L,find(nB<T)))=0;
    L(L>N)=0; % holes
    L=L>0;
    %close all;figure;imagesc(L);
    
    mystack.I=L;
    set(mystack.hM,'AlphaData',mystack.I.*mystack.alpha_setting);
    
    updateframe=0;
    updatemask=1;
    updateboundary=1;
    call_plot([],[],updateframe,updatemask,updateboundary);
end

function call_menu_extra_moviestepsize(src,eventdata)

global mystack;

prompt = {'Set Movie Stepsize:'};
dlg_title = 'Stepsize';
num_lines = 1;
def = {num2str(mystack.moviestepsize)};
answer = inputdlg(prompt,dlg_title,num_lines,def);
if ~isempty(answer), % in case user didn't press cancel
    mystack.moviestepsize=str2num(answer{1});
end

function call_menu_extra_fillholes(src,eventdata)

global mystack;
BW = mystack.I;
[B,L,N,A] = bwboundaries(BW);
L(L>N)=1; % fill holes
L=L>0;
%figure;imagesc(L)
mystack.I=L;

updateframe=0;
updatemask=1;
updateboundary=1;
call_plot([],[],updateframe,updatemask,updateboundary);

function call_wheel(src,eventdata)

global mystack;

if eventdata.VerticalScrollCount>0,
    mystack.brushsize = mystack.brushsize-1;
else
    mystack.brushsize = mystack.brushsize+1;
end
if mystack.brushsize<1,mystack.brushsize=1; end

fprintf('brushsize = %d\n',mystack.brushsize);

function call_buttonup(src,eventdata)

set(gcf,'WindowButtonMotionFcn','');

function call_buttondown(src,eventdata)

selType = get(gcf,'SelectionType')

switch lower(selType)
    case 'normal' % left
        set(gcf,'WindowButtonMotionFcn',@call_buttonmotion);
    case 'alt' % right
        set(gcf,'WindowButtonMotionFcn',@call_buttonmotion);
    case 'extend' %
        dum = get(gca,'CurrentPoint');
        global mystack;
        if isfield(mystack,'GP'),
            mystack.GP(end+1)=text(dum(end,1),dum(end,2),num2str(length(mystack.GP)+1),'Color',[0 1 0],'FontSize',12,'HorizontalAlignment','center');
        else
            %mystack=rmfield(mystack,'GP');
            mystack.GP(1)=text(dum(end,1),dum(end,2),'1','Color',[0 1 0],'FontSize',12,'HorizontalAlignment','center');
        end
end

function call_buttonmotion(src,eventdata)

global mystack;

dum = get(gca,'CurrentPoint');
dum = round(dum(1,1:2));
br = [-mystack.brushsize:mystack.brushsize];
add = [br,zeros(size(br));zeros(size(br)),br]'
dum = repmat(dum,[size(add,1),1])+add;

bad=dum(:,1)<1|dum(:,2)<1|dum(:,1)>size(mystack.I,2)|dum(:,2)>size(mystack.I,1);
dum(bad,:)=[];

selType = get(gcf,'SelectionType')
switch lower(selType)
    case 'normal' % left
        mystack.I(dum(:,2),dum(:,1))=1;
    case 'alt' % right
        mystack.I(dum(:,2),dum(:,1))=0;
    case 'extend' % middle OR left + right
end %switch

set(mystack.hM,'AlphaData',mystack.I.*mystack.alpha_setting);

if get(mystack.handles.show_boundary,'Value')==1
    try,
        uistack(mystack.BH,'top');
    end
end

function call_keypress(src,eventdata)
%WindowKeyPressFcn

global mystack;

if strcmp(eventdata.Key,'leftarrow')==1|strcmp(eventdata.Key,'rightarrow')==1
    global mystack;
    if strcmp(eventdata.Key,'leftarrow')==1
        mystack.frame=mystack.frame-1;
    elseif strcmp(eventdata.Key,'rightarrow')==1
        mystack.frame=mystack.frame+1;
    end
    if mystack.frame<1,
        mystack.frame=1;
    elseif mystack.frame>mystack.totfrmlayer(mystack.layer),
        mystack.frame=mystack.totfrmlayer(mystack.layer);
    end
    updateframe=1;
    updatemask=0;
    updateboundary=0;
    call_plot([],[],updateframe,updatemask,updateboundary);
elseif strcmp(eventdata.Key,'uparrow')==1|strcmp(eventdata.Key,'downarrow')==1,
    if strcmp(eventdata.Key,'uparrow')==1
        mystack.alpha_setting = mystack.alpha_setting+0.1;
        fprintf('alpha = %1.2f\n',mystack.alpha_setting);
    elseif strcmp(eventdata.Key,'downarrow')==1
        mystack.alpha_setting = mystack.alpha_setting-0.1;
        fprintf('alpha = %1.2f\n',mystack.alpha_setting);
    end
    if mystack.alpha_setting<0,
        mystack.alpha_setting=0;
    elseif mystack.alpha_setting>1,
        mystack.alpha_setting=1;
    end
    set(mystack.hM,'AlphaData',mystack.I.*mystack.alpha_setting);
elseif strcmp(eventdata.Key,'b')
    if get(mystack.handles.show_boundary,'Value')==1,
        set(mystack.handles.show_boundary,'Value',0);
    else
        set(mystack.handles.show_boundary,'Value',1);
    end
    updateframe=0;
    updatemask=0;
    updateboundary=1;
    call_plot([],[],updateframe,updatemask,updateboundary);    
elseif strcmp(eventdata.Key,'m')
    call_maxproj([],[]);      
elseif strcmp(eventdata.Key,'p'),        
    %stop=0;
    %oldkpf=get(gcf,'KeyPressFcn');
    %set(gcf,'KeyPressFcn','stop=1;')    
    for i=mystack.frame:mystack.moviestepsize:mystack.totfrmlayer(mystack.layer),
        mystack.frame = i
        %eventdata.Key
        updateframe=1;
        updatemask=0;
        updateboundary=0;        
        selType = get(gcf,'SelectionType');
        if strcmp(selType,'alt')        
        %if strcmp(eventdata.Key,'leftbracket')           
            break;
        end
        pause(0.1);
        call_plot([],[],updateframe,updatemask,updateboundary);
    end    
    %set(gcf,'KeyPressFcn',oldkpf);
  elseif strcmp(eventdata.Key,'f')
      call_menu_extra_fillholes([],[])
 elseif strcmp(eventdata.Key,'s')
     T=20;     
     global mystack;     
     BW = mystack.I;
     [B,L,N,A] = bwboundaries(BW);
     
     nB=zeros([1,length(B)]);
     for i=1:length(B),
         nB(i)=size(B{i},1);
     end
     L(ismember(L,find(nB<T)))=0;
     L(L>N)=0; % holes
     L=L>0;
     
     mystack.I=L;
     set(mystack.hM,'AlphaData',mystack.I.*mystack.alpha_setting);
     
     updateframe=0;
     updatemask=1;
     updateboundary=1;
     call_plot([],[],updateframe,updatemask,updateboundary);        
else
    dum = get(gca,'CurrentPoint');
    dum = round(dum(1,1:2)) % dum=[300,200]
    
    a1=get(gca,'XLim')
    a2=get(gca,'YLim')
    
    rngx = [a1(2)-a1(1)];
    rngy = [a2(2)-a2(1)];
    
    Isiz = size(mystack.IMG);
    eventdata.Key
    % zoom in, centered on current position
    if strcmp(eventdata.Key, 'equal')==1
        rngxnew = rngx.*0.9; rngynew = rngy.*0.9;
    elseif strcmp(eventdata.Key, 'hyphen')==1
        rngxnew = rngx.*1.1; rngynew = rngy.*1.1;
    else
        rngxnew = rngx.*1; rngynew = rngy.*1;
    end
    if strcmp(eventdata.Key,'equal')|strcmp(eventdata.Key, 'hyphen'),
        if rngxnew>size(mystack.IMG,2), rngxnew = Isiz(2); end
        if rngynew>size(mystack.IMG,1), rngynew = Isiz(1); end
        a1n=[dum(1)-rngxnew/2,dum(1)+rngxnew/2]; if any(a1n<1), a1n=[1,rngxnew]; elseif any(a1n>Isiz(2)), a1n=[Isiz(2)-rngxnew,Isiz(2)]; end
        a2n=[dum(2)-rngynew/2,dum(2)+rngynew/2]; if any(a2n<1), a2n=[1,rngynew]; elseif any(a2n>Isiz(1)), a2n=[Isiz(1)-rngynew,Isiz(1)]; end
        set(gca,'XLim',a1n,'YLim',a2n);
    end
    mystack.curp=0;
end

function call_slider(src,eventdata)

global mystack;
mystack.frame = round((get(src,'Value').*(mystack.totfrmlayer(mystack.layer)-1))+1);
updateframe=1;
updatemask=0;
updateboundary=0;
call_plot([],[],updateframe,updatemask,updateboundary);

function call_editcmin(src,eventdata)

global mystack;
mystack.MI = str2num(get(src,'String'));
updateframe=0;
updatemask=0;
updateboundary=0;
%call_plot([],[],updateframe,updatemask,updateboundary);
caxis([mystack.MI,mystack.MA])



function call_editlayer(src,eventdata)

global mystack;
mystack.layer = get(src,'Value');
%mystack.sel = find(mystack.layernum(1:mystack.totfrmlayer(mystack.layer))==mystack.layer); % sometimes for red chn only first frames recorded (for structural image)
mystack.sel = find(mystack.layernum(1:mystack.totfrm)==mystack.layer); % sometimes for red chn only first frames recorded (for structural image)

mystack.sel
mystack.layer

updateframe=1;
updatemask=0;
updateboundary=0;
call_plot([],[],updateframe,updatemask,updateboundary);
caxis([mystack.MI,mystack.MA])

function call_editcmax(src,eventdata)

global mystack;
mystack.MA = str2num(get(src,'String'));
updateframe=0;
updatemask=0;
updateboundary=0;
%call_plot([],[],updateframe,updatemask,updateboundary);
caxis([mystack.MI,mystack.MA])


function call_editnavg(src,eventdata)

global mystack;
mystack.navg = round(str2num(get(src,'String')));
updateframe=1;
updatemask=0;
updateboundary=0;
call_plot([],[],updateframe,updatemask,updateboundary);

function call_editmedfilt(src,eventdata)

global mystack;
mystack.medfilt = round(str2num(get(src,'String')));
updateframe=1;
updatemask=0;
updateboundary=0;
call_plot([],[],updateframe,updatemask,updateboundary);

function call_plot(src,eventdata,updateframe,updatemask,updateboundary)

global mystack;

if updateframe,
    dim = mystack.dim;
%     rng = [mystack.frame:mystack.frame+mystack.navg-1]; rng(rng<1)=[]; rng(rng>mystack.totfrmlayer(mystack.layer))=[];
%     rng = mystack.sel(rng);
    
    ix = [mystack.frame:mystack.frame+mystack.navg-1]; ix(ix<1)=[]; ix(ix>mystack.totfrmlayer(mystack.layer))=[]; % selection of frames from current layer
    rng = mystack.sel(ix); % indices to frames from this layer in full stack
    
    % read in those frames
    fid = fopen(mystack.fname);
    ndatr=int16(zeros([dim(2),dim(1),length(rng)]));
    for i=1:length(rng),
        fseek(fid, 2*(16/8) + (2*dim(1)*dim(2)*( rng(i)-1) ), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
        ndat =fread(fid,[1*prod(dim)],'*int16'); % read frames
        ndatr(:,:,i)=reshape(ndat,[dim(2),dim(1),1]);   % size(ndatr)
    end
    fclose(fid);
    
    if mystack.dataformat==2,        
        IMG = zeros([length(mystack.patch.y), length(mystack.patch.x), length(rng)], 'int16');
        
        for i=1:size(ndatr,3),
            I = squeeze(ndatr(:,:,i));
            if mystack.subpixel
                x0 = mystack.ops.del1{mystack.layer}(i);
                y0 = mystack.ops.del2{mystack.layer}(i);
                F = griddedInterpolant(mystack.sp.X1, mystack.sp.X2,  double(I), 'linear');
                [X1n, X2n]  = ndgrid([1:mystack.sp.sizX]-x0,[1:mystack.sp.sizY]-y0);
                I = F(X1n(mystack.patch.y, mystack.patch.x), X2n(mystack.patch.y, mystack.patch.x));
                IMG(:,:,i) = int16(I);
            else
                J = zeros(size(IMG,1), size(IMG,2), 'int16');
                if mystack.nlayer==1,
                    x0 = round(mystack.ops.del2{mystack.layer}(ix(i)));
                    y0 = round(mystack.ops.del1{mystack.layer}(ix(i)));
                else
                    x0 = round(mystack.ops.del2{mystack.layer}(ix(i)));
                    y0 = round(mystack.ops.del1{mystack.layer}(ix(i)));
                end
%                 x0 = round(mystack.ops.del2{mystack.layer}(i));
%                 y0 = round(mystack.ops.del1{mystack.layer}(i));
                cy = (mystack.patch.y - y0);
                cx = (mystack.patch.x - x0);
                Iy = cy > 0 & cy < size(I,1);
                Ix = cx > 0 & cx < size(I,2);
                J(Iy, Ix) = I(cy(Iy), cx(Ix));
                IMG(:,:,i) = J;
            end
        end
        ndatr=IMG;
    end
    
    if mystack.medfilt~=0,
        for i=1:size(ndatr,3),
            ndatr(:,:,i) = medfilt2(squeeze(ndatr(:,:,i)),[mystack.medfilt,mystack.medfilt]);
        end
    end
    mystack.IMG = squeeze(mean(ndatr,3))';
    title(num2str(mystack.frame));
end
% if mystack.medfilt~=0,
%     mystack.IMG=medfilt2(mystack.IMG,[mystack.medfilt,mystack.medfilt]);
% end

if updateframe,
    try, delete(mystack.hIMG); end
    mystack.hIMG=imagesc(mystack.IMG);
    uistack(mystack.hM,'top')
end
if updatemask,
    set(mystack.hM,'AlphaData',mystack.I.*mystack.alpha_setting);
end

if get(mystack.handles.show_boundary,'Value')==1&updateboundary,
    try,
        delete(mystack.BH);
    end
    [B] = bwboundaries(mystack.I);
    clear BH;
    for i=1:length(B),
        hold on;BH(i)=plot(B{i}(:,2),B{i}(:,1),'w','Color',[0.5 0.5 0.5]);
    end
    mystack.BH=BH;
elseif get(mystack.handles.show_boundary,'Value')==0&updateboundary,
    try,
        delete(mystack.BH);
    end
elseif get(mystack.handles.show_boundary,'Value')==1,
    try,
        uistack(mystack.BH,'top');
    end
end

if strcmp('on',get(mystack.menu_extra_show_roi_number,'Checked')),
    try,
        uistack(mystack.RN,'top');
    end
end

if isfield(mystack,'GP'),
    uistack(mystack.GP,'top');
end

if mystack.curp~=0,
    set(gca,'XLim',[mystack.xlims(mystack.pos(mystack.curp,1)),mystack.xlims(mystack.pos(mystack.curp,1)+1)]);
    set(gca,'YLim',[mystack.ylims(mystack.pos(mystack.curp,2)),mystack.ylims(mystack.pos(mystack.curp,2)+1)]);
end

%mystack.impixelinfo = impixelinfo(mystack.handles.f);

function my_closefcn(src,eventdata)
% User-defined close request function
% to display a question dialog box
if 0,
    selection = questdlg('Close This Figure?',...
        'Close Request Function',...
        'Yes','No','Yes');
    switch selection,
        case 'Yes',
            delete(gcf);
        case 'No'
            return
    end
end
delete(gcf);

function out=resscn_roidat_sel(cfg);

global mystack;

filedat = mystack.fname;
dim     = mystack.dim;

% check data size of data file matches with ROI
if ~isequal(fliplr(dim'),size(cfg.mask)),
    error('mismatch format binary file and ROI');
end
uROI = unique(cfg.mask); uROI(uROI==0)=[]; % unique ROIs


totfrm = cfg.rng;
out.DAT{mystack.chnix} = NaN([length(uROI),totfrm]);
frame  = 1;
blksiz = 1000;
fid = fopen(filedat);
for T=1:ceil(totfrm/blksiz), % for every trial
    if mod(T,2)==0, fprintf('data segment %1.2d(of %d):%s\n',T,ceil(totfrm/blksiz),datestr(now)); end
    %end
    rng=[frame:frame+blksiz-1]; rng(rng<1)=[]; rng(rng>totfrm)=[];
    
    % read in those frames
    fseek(fid, 2*(16/8) + (2*dim(1)*dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
    ndat =fread(fid,[length(rng)*prod(dim)],'*int16'); % read single frame
    ndatr=reshape(ndat,[dim(2),dim(1),length(rng)]); % size(ndatr)
    
    % see edit script_test_extractROItimecourse
    p = size(ndatr,3);
    
    ndatr = double(ndatr);
    TC2=NaN([length(uROI),size(ndatr,3)]); % fastest
    for R=1:length(uROI),
        b = cfg.mask==uROI(R);
        TC2(R,:) = b(:).'*reshape(ndatr,[],p)/nnz(b);
    end
    %t=toc;
    %fprintf('trial %d, t=%1.2f\n',T,t);
    out.DAT{mystack.chnix}(:,rng) = TC2; % size(DAT),size(TC2)
    
    frame = frame + blksiz;
end % for T=1:length(sel.trl), % for every trial\
fclose(fid);

% Jia, 2011
% 1. take mean F in ROI
% 2. calculate time dependent baseline
SFi = 30;
t0=0.2;
t1=0.75;
t2=3;

nsmp    = round(t1*SFi); if mod(nsmp,2)==0, nsmp=nsmp+1; end
nsmppre = round(t2*SFi);
for i=1:size(out.DAT{mystack.chnix},1), %i=6
    data =  squeeze(out.DAT{mystack.chnix}(i,:));
    
    F0   = smooth(data,nsmp,'moving');
    Fm=NaN(size(data));
    for s=1:length(data),
        rng  = [s-nsmppre:s-1]; rng(rng<1|rng>length(data))=[];
        if ~isempty(rng), Fm(s)= min(F0(rng)); end
    end
    R = (data-Fm)./Fm;
    Rf = medfilt1(R,3);
    
    if 0,
        figure;plot(frmtim,data,'b');
        hold on;plot(frmtim,F0,'r');
        hold on;plot(frmtim,Fm,'g');
        
        figure;plot(frmtim,R,'b');
        hold on;plot(frmtim,Rf,'r');
    end
    out.dFj{mystack.chnix}(i,:)=Rf;
end

% compute SNR of traces
% edit resscn_traces_SNR.m
resscn_traces_SNR;

% plot 
cmap=jet(size(out.DAT{mystack.chnix},1));
figure;
for i=1:size(out.DAT{mystack.chnix},1), 
    %hold on;plot([1,size(out.DAT{mystack.chnix},2)]./SFi,[i,i],'b--','Color',cmap(i,:));    
    %hold on;plot([1:size(out.DAT{mystack.chnix},2)]./SFi,i+out.dFj{mystack.chnix}(i,:)./max(out.dFj{mystack.chnix}(i,:)),'Color',cmap(i,:));    
    hold on;plot([1,size(out.DAT{mystack.chnix},2)],[i,i],'b--','Color',cmap(i,:));    
    hold on;plot([1:size(out.DAT{mystack.chnix},2)],i+out.dFj{mystack.chnix}(i,:)./max(out.dFj{mystack.chnix}(i,:)),'Color',cmap(i,:));        
    dum=get(gca,'XLim');
    text(mean(dum),i+0.5,sprintf('snr=%1.2f',SNR.snr1(i)));
end
set(gca,'YLim',[0,size(out.DAT{mystack.chnix},1)+1]);
