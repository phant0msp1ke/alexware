function tpti_viewer_confocal(cfg);

% based on myimmunoview
if 0,
    set(0,'ShowHiddenmyIview','on')
    delete(get(0,'Children'));
end

global glb_tpti_viewer_confocal;

glb_tpti_viewer_confocal=cfg;
glb_tpti_viewer_confocal.curchn = 1;

n=1;
fname = glb_tpti_viewer_confocal.fname{n};
l = dir(fname);
fid = fopen(fname);
glb_tpti_viewer_confocal.dim    = fread(fid,2,'int16'); % size of image> dim(1) = Y dim(2) = X!!
% -for matrices, convention is to organize 1st dim=x, 2nd dim=y>
% -when plotting images needs to transpose because image convention is to have x axis as vertical....
glb_tpti_viewer_confocal.totfrm = (l.bytes-2*(16/8))./(2*prod(glb_tpti_viewer_confocal.dim)); % total number of frames
fclose(fid);

for n=2:length(glb_tpti_viewer_confocal.fname),
    fname = glb_tpti_viewer_confocal.fname{n};
    l = dir(fname);
    fid = fopen(fname);
    dim    = fread(fid,2,'int16'); % size of image
    totfrm = (l.bytes-2*(16/8))./(2*prod(dim)); % total number of frames
    fclose(fid);
    if ~(isequal(dim,glb_tpti_viewer_confocal.dim)&isequal(totfrm,glb_tpti_viewer_confocal.totfrm)),
        error('mismatch dim or nfrm different channels');
    end
end


glb_tpti_viewer_confocal.curfrm = floor(glb_tpti_viewer_confocal.totfrm/2);
% glb_tpti_viewer_confocal.curfrm = round((get(src,'Value').*(glb_tpti_viewer_confocal.totfrm-1))+1);
%val = (glb_tpti_viewer_confocal.curfrm-1)/(glb_tpti_viewer_confocal.totfrm-1)
%val = (glb_tpti_viewer_confocal.totfrm-1)/(glb_tpti_viewer_confocal.totfrm-1) > 1
%val = (1-1)/(glb_tpti_viewer_confocal.totfrm-1) > 0

% read in middle frame
fname = glb_tpti_viewer_confocal.fname{glb_tpti_viewer_confocal.curchn};
fid = fopen(fname);
rng=glb_tpti_viewer_confocal.curfrm;
fseek(fid, 2*(16/8) + (2*glb_tpti_viewer_confocal.dim(1)*glb_tpti_viewer_confocal.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
ndat =fread(fid,[length(rng)*prod(glb_tpti_viewer_confocal.dim)],'*int16'); % read single frame
ndatr=reshape(ndat,[glb_tpti_viewer_confocal.dim(2),glb_tpti_viewer_confocal.dim(1),length(rng)]); % size(datr)
fclose(fid);
glb_tpti_viewer_confocal.IMG=ndatr; % x , y

if isfield(glb_tpti_viewer_confocal,'MI'),
    glb_tpti_viewer_confocal.MI=glb_tpti_viewer_confocal.MI;
else
    glb_tpti_viewer_confocal.MI=min(min(nanmean(double(glb_tpti_viewer_confocal.IMG),3)));
end
if isfield(cfg,'MA'),
    glb_tpti_viewer_confocal.MA=glb_tpti_viewer_confocal.MA;
else
    glb_tpti_viewer_confocal.MA=max(max(nanmean(double(glb_tpti_viewer_confocal.IMG),3)));
    glb_tpti_viewer_confocal.MA=round(0.25*max(max(max(double(glb_tpti_viewer_confocal.IMG),3))));
end

%--------------------------------------------------------------------------

glb_tpti_viewer_confocal.f = figure('Units','normalized');
set(glb_tpti_viewer_confocal.f,'Name',glb_tpti_viewer_confocal.fname{glb_tpti_viewer_confocal.curchn});
set(glb_tpti_viewer_confocal.f,'CloseRequestFcn',@my_closefcn);

glb_tpti_viewer_confocal.menu_extra = uimenu(gcf,'Label','Extra');
glb_tpti_viewer_confocal.menu_extra_transformpoints  = uimenu(glb_tpti_viewer_confocal.menu_extra,'Label','Compute Transform from Points',...
    'Callback',@call_menu_extra_transformpoints);
glb_tpti_viewer_confocal.menu_extra_rotation  = uimenu(glb_tpti_viewer_confocal.menu_extra,'Label','Specify Rotation',...
    'Callback',@call_menu_extra_rotation);
glb_tpti_viewer_confocal.menu_extra_settransform  = uimenu(glb_tpti_viewer_confocal.menu_extra,'Label','Set Transform',...
    'Callback',@call_menu_extra_settransform);
glb_tpti_viewer_confocal.menu_extra_exporttiff  = uimenu(glb_tpti_viewer_confocal.menu_extra,'Label','Export Tiff',...
    'Callback',@call_menu_extra_exporttiff);
glb_tpti_viewer_confocal.menu_extra_setXYascurrentzoom  = uimenu(glb_tpti_viewer_confocal.menu_extra,'Label','set XY to current zoom',...
    'Callback',@call_menu_extra_setXYascurrentzoom);

set(gcf,'WindowKeyPressFcn',@call_keypress);

set(glb_tpti_viewer_confocal.f,'toolbar','figure'); % otherwise disappears when adding uicontrol elements...

glb_tpti_viewer_confocal.ax(1)=axes('Parent',glb_tpti_viewer_confocal.f,'Units','normalized','Position',[0.024 0.056+0.02 0.385 1-(2*(0.056+0.02))]);
glb_tpti_viewer_confocal.ax(2)=axes('Parent',glb_tpti_viewer_confocal.f,'Units','normalized','Position',[0.435 0.056+0.02 0.385 1-(2*(0.056+0.02))]);
val = (glb_tpti_viewer_confocal.curfrm-1)/(glb_tpti_viewer_confocal.totfrm-1);
glb_tpti_viewer_confocal.slider1=uicontrol('Parent',glb_tpti_viewer_confocal.f,'Units','normalized','Style', 'slider',...
    'Position', [0.024 0.027-0.01 0.385 0.02],...
    'Min',0,'Max',1,'Value',val,'SliderStep',[1 10]./(glb_tpti_viewer_confocal.totfrm-1),'Callback', @call_slider1);
glb_tpti_viewer_confocal.slider2=uicontrol('Parent',glb_tpti_viewer_confocal.f,'Units','normalized','Style', 'slider',...
    'Position', [0.435 0.027-0.01 0.385 0.02],...
    'Min',0,'Max',1,'Value',0,'SliderStep',[1 10]./(glb_tpti_viewer_confocal.totfrm-1),'Callback', @call_slider2);

uicontrol('Parent',glb_tpti_viewer_confocal.f,'Units','normalized','Style','edit',...
    'Position', [0.871 0.113 0.077 0.029],...
    'String',num2str(glb_tpti_viewer_confocal.MI),'Callback', @call_editcmin);
uicontrol('Parent',glb_tpti_viewer_confocal.f,'Units','normalized','Style', 'slider',...
    'Position', [0.845 0.084 0.128 0.029],...
    'Min',0,'Max',1,'Value',0,'SliderStep',[1 10]./(glb_tpti_viewer_confocal.totfrm-1),'Callback', @call_slidermin);
uicontrol('Parent',glb_tpti_viewer_confocal.f,'Units','normalized','Style','edit',...
    'Position', [0.871 0.056 0.077 0.029],...
    'String',num2str(glb_tpti_viewer_confocal.MA),'Callback', @call_editcmax);
uicontrol('Parent',glb_tpti_viewer_confocal.f,'Units','normalized','Style', 'slider',...
    'Position', [0.845 0.027 0.128 0.029],...
    'Min',0,'Max',1,'Value',0,'SliderStep',[1 10]./(glb_tpti_viewer_confocal.totfrm-1),'Callback', @call_slidermax);

uicontrol('Parent',glb_tpti_viewer_confocal.f,'Units','normalized','Style', 'pushbutton',...
    'Position', [0.845 0.426 0.128 0.059],...
    'String','Compute','Callback', @call_compute);

uicontrol('Parent',glb_tpti_viewer_confocal.f,'Units','normalized','Style', 'pushbutton',...
    'Position', [0.845 0.426-0.08 0.128 0.059],...
    'String','Keep','Callback', @call_keep);

glb_tpti_viewer_confocal.T=eye(4); % default transform matrix
str=[];
for i=1:size(glb_tpti_viewer_confocal.T,2),
    str{i}=sprintf('%1.2f  %1.2f  %1.2f  %1.2f',glb_tpti_viewer_confocal.T(i,:));
end
glb_tpti_viewer_confocal.text = uicontrol('Parent',glb_tpti_viewer_confocal.f,'Units','normalized','Style','text',...
    'Position', [0.845 0.683 0.129 0.259],...
    'String',str);

glb_tpti_viewer_confocal.ORI = floor( [glb_tpti_viewer_confocal.dim([2,1]);glb_tpti_viewer_confocal.totfrm]./2 )'; % x y z
glb_tpti_viewer_confocal.editorigin = uicontrol('Parent',glb_tpti_viewer_confocal.f,'Units','normalized','Style','edit',...
    'Position', [0.845 0.627 0.129 0.029],...
    'String',num2str(glb_tpti_viewer_confocal.ORI),'Callback', @call_editorigin);
glb_tpti_viewer_confocal.NumZ = [-glb_tpti_viewer_confocal.ORI(3)+1,glb_tpti_viewer_confocal.totfrm-glb_tpti_viewer_confocal.ORI(3)]; % glb_tpti_viewer_confocal.ORI(3)+[ glb_tpti_viewer_confocal.NumZ(1):glb_tpti_viewer_confocal.NumZ(2)]
glb_tpti_viewer_confocal.editNumZ = uicontrol('Parent',glb_tpti_viewer_confocal.f,'Units','normalized','Style','edit',...
    'Position', [0.845 0.57 0.129 0.029],...
    'String',sprintf('%d,%d',glb_tpti_viewer_confocal.NumZ),'Callback', @call_editNumZ);

% str=sprintf('%d,%d,%d,%d',-round(glb_tpti_viewer_confocal.dim(2)/4),... % x1
%     round(glb_tpti_viewer_confocal.dim(2)/4),... % x2
%     -round(glb_tpti_viewer_confocal.dim(1)/4),... % y1
%     round(glb_tpti_viewer_confocal.dim(1)/4))    % y2
str=sprintf('%d,%d,%d,%d',-600,600,-600,600);
glb_tpti_viewer_confocal.editXY = uicontrol('Parent',glb_tpti_viewer_confocal.f,'Units','normalized','Style','edit',...
    'Position', [0.845 0.513 0.129 0.029],...
    'String',str,'Callback', @call_editXY);
glb_tpti_viewer_confocal.rect = str2num(str);

% plot image
axes(glb_tpti_viewer_confocal.ax(1));cla;
glb_tpti_viewer_confocal.hIMG=imagesc(glb_tpti_viewer_confocal.IMG');
colormap(gray);
%colormap(morgenstemning);
caxis([glb_tpti_viewer_confocal.MI,glb_tpti_viewer_confocal.MA]);

hold on;glb_tpti_viewer_confocal.rectH=patch(glb_tpti_viewer_confocal.ORI(1)+glb_tpti_viewer_confocal.rect([1,2,2,1]),...
    glb_tpti_viewer_confocal.ORI(2)+glb_tpti_viewer_confocal.rect([3,3,4,4]),[1 1 1],'FaceColor','none','EdgeColor',[1 1 1],'LineWidth',2);

function call_menu_extra_exporttiff(src,eventdata)

global glb_tpti_viewer_confocal;

for chn=1:length(glb_tpti_viewer_confocal.fname),
    
    %fname = glb_tpti_viewer_confocal.fname{glb_tpti_viewer_confocal.curchn}
    fname  = glb_tpti_viewer_confocal.fname{chn};
    fnameS = strrep(fname,'.bin','.tif');
    %if not(exist(fnameS)),
        % convert image with series of transforms
        for T=1:length( glb_tpti_viewer_confocal.history ),
            FRNG  = glb_tpti_viewer_confocal.history{T}.FRNG;
            XRNG  = glb_tpti_viewer_confocal.history{T}.XRNG;
            YRNG  = glb_tpti_viewer_confocal.history{T}.YRNG;
            tform = glb_tpti_viewer_confocal.history{T}.tform;
            if T==1,
                DAT = zeros([length(XRNG),length(YRNG),length(FRNG)]);
                fid = fopen(fname);
                l = dir(fname);
                dim    = fread(fid,2,'int16'); % size of image
                totfrm = (l.bytes-2*(16/8))./(2*prod(dim)); % total number of frames
                for n=1:length(FRNG),
                    rng=FRNG(n);
                    fseek(fid, 2*(16/8) + (2*dim(1)*dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
                    ndat =fread(fid,[length(rng)*prod(dim)],'*int16'); % read single frame
                    ndatr=reshape(ndat,[dim(2),dim(1),length(rng)]); % size(datr)
                    DAT(:,:,n)=ndatr(XRNG,YRNG);
                end % for i=1:glb_tpti_viewer_confocal.totfrm,
                fclose(fid);
            else
                DAT=DATT(XRNG,YRNG,FRNG);
            end
            R = makeresampler('linear', 'fill');
            TDIMS_A = [1 2 3];
            TDIMS_B = [1 2 3];
            TSIZE_B = size(DAT);
            TMAP_B = [];
            F = 0;
            DATT = tformarray(DAT, tform, R, TDIMS_A, TDIMS_B, TSIZE_B, TMAP_B, F);
        end
        if chn==1 & ~isequalwithequalnans( DATT,glb_tpti_viewer_confocal.DAT0 ),
           error('error transform!'); 
        end
        
        t = Tiff(fnameS,'w');% 'w8');
        tagStruct.Photometric = Tiff.Photometric.MinIsBlack;
        tagStruct.Compression = Tiff.Compression.None;
        tagStruct.BitsPerSample = 16;
        tagStruct.SamplesPerPixel = 1;
        tagStruct.SampleFormat = Tiff.SampleFormat.UInt;
        %tagStruct.ImageLength = size(DATT,1);
        %tagStruct.ImageWidth = size(DATT,2);
        tagStruct.ImageLength = size(DATT,2);
        tagStruct.ImageWidth = size(DATT,1);
        tagStruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
        t.setTag(tagStruct);
        for j=1:size(DATT,3),
            p = uint16(DATT(:,:,j))';
            if j>1,
                t.writeDirectory();
                t.setTag(tagStruct);
            end
            t.write(p);
        end
        t.close();        
    %end    
    fprintf('finished channel %d\n',chn);
end % for chn=1:length(glb_tpti_viewer_confocal.fname),

% also save the history
fname = glb_tpti_viewer_confocal.fname{1};
fnameS2 = strrep(fname,'.bin','_history.mat');
clear H;
H.history = glb_tpti_viewer_confocal.history;
H.fname   = glb_tpti_viewer_confocal.fname;
save(fnameS2,'H');

function call_menu_extra_transformpoints(src,eventdata)

global glb_tpti_viewer_confocal;

    
try,    
    if 0,
        PNT=NaN(length(glb_tpti_viewer_confocal.CELLLABEL),3);
        for i=1:length(glb_tpti_viewer_confocal.CELLLABEL),
            PNT(i,:)=[glb_tpti_viewer_confocal.CELLLABEL{i}{[1,2,3]}];
        end
    else        
        % get coordinates from ImageJ
        Slice = MIJ.getColumn('Slice');
        if isempty(Slice);Slice = MIJ.getColumn('Frame');end
        PNT = [MIJ.getColumn('X'), MIJ.getColumn('Y'), Slice];
        %RA = RA(end,:); % keep only the last measured point
    end
    
    % get coordinates from mystackviewer_segment
    global glb_tpti_viewer;
    PNT0=NaN(size(PNT,1),3);
    for i=1:length(glb_tpti_viewer.GP),
        dum=get(glb_tpti_viewer.GP(i),'Position')
        PNT0(i,:)=[dum(1:2),1];
    end
catch 
    PNT  = [1135 793 13; 1114 1450 8; 1618 1205 14];
    PNT0 = [263.7265 198.1626 1.0000; 249.1960 565.0325 1.0000; 548.7489 442.0650 1.0000];
    error('here');
end

PNT
PNT0

zm = round(mean(PNT,1));
PNT0=PNT0-repmat(mean(PNT0,1),[size(PNT0,1),1]);
PNT =PNT -repmat(zm,[size(PNT,1),1]);


T=compute_transform(PNT',PNT0') % from immuno to 2p
glb_tpti_viewer_confocal.T = T';

if 0, % comparison:
[regParams,Bfit,ErrorStats]=absor(PNT',PNT0') % from immuno to 2p
%rsbs_transform_test2.m arbitrary scaling
glb_tpti_viewer_confocal.T = regParams.M';
end

str=[];
for i=1:size(glb_tpti_viewer_confocal.T,2),
    str{i}=sprintf('%1.2f  %1.2f  %1.2f  %1.2f',glb_tpti_viewer_confocal.T(i,:));
end
set(glb_tpti_viewer_confocal.text,'String',str);
% set ORI as center of plane
glb_tpti_viewer_confocal.ORI = zm;
set(glb_tpti_viewer_confocal.editorigin,'String',num2str(glb_tpti_viewer_confocal.ORI));

try, delete(glb_tpti_viewer_confocal.rectH); end
hold on;glb_tpti_viewer_confocal.rectH=patch(glb_tpti_viewer_confocal.ORI(1)+glb_tpti_viewer_confocal.rect([1,2,2,1]),...
    glb_tpti_viewer_confocal.ORI(2)+glb_tpti_viewer_confocal.rect([3,3,4,4]),[1 1 1],'FaceColor','none','EdgeColor',[1 1 1],'LineWidth',2);

function call_menu_extra_rotation(src,eventdata)

prompt = {'Set Rotation:'};
dlg_title = 'Rotate';
num_lines = 1;
def = {'0,0,0'};
answer = inputdlg(prompt,dlg_title,num_lines,def);
if ~isempty(answer), % in case user didn't press cancel
    
    global glb_tpti_viewer_confocal;
    
    rot=str2num(answer{1}); % T=40
    
    % x-rotation
    % x-rotation
    Rx=[1 0 0; 0 cosd(rot(1)) -sind(rot(1)); 0 sind(rot(1)) cosd(rot(1))];
    % y-rotation
    Ry=[cosd(rot(2)) 0 sind(rot(2)); 0 1 0; -sind(rot(2)) 0 cosd(rot(2))];
    % z-rotation
    Rz=[cosd(rot(3)) -sind(rot(3)) 0; sind(rot(3)) cosd(rot(3)) 0; 0 0 1];
    
    Rx = [[Rx,[0 0 0]']; 0 0 0 1];
    Ry = [[Ry,[0 0 0]']; 0 0 0 1];
    Rz = [[Rz,[0 0 0]']; 0 0 0 1];
    
    % The forward mapping is the composition of T1, T2, and T3.
    
    glb_tpti_viewer_confocal.T = Rx * Ry * Rz;
    str=[];
    for i=1:size(glb_tpti_viewer_confocal.T,2),
        str{i}=sprintf('%1.2f  %1.2f  %1.2f  %1.2f',glb_tpti_viewer_confocal.T(i,:));
    end
    set(glb_tpti_viewer_confocal.text,'String',str);
    
end

function call_keep(src,eventdata)

global glb_tpti_viewer_confocal;

glb_tpti_viewer_confocal.DAT0    = glb_tpti_viewer_confocal.DATT;
if ~isfield(glb_tpti_viewer_confocal,'history'),
    cix = 1;
else
    cix = length(glb_tpti_viewer_confocal.history)+1;
end
glb_tpti_viewer_confocal.history{cix}.FRNG  = glb_tpti_viewer_confocal.FRNG;
glb_tpti_viewer_confocal.history{cix}.XRNG  = glb_tpti_viewer_confocal.XRNG;
glb_tpti_viewer_confocal.history{cix}.YRNG  = glb_tpti_viewer_confocal.YRNG;
glb_tpti_viewer_confocal.history{cix}.tform = glb_tpti_viewer_confocal.tform;
% just for record
glb_tpti_viewer_confocal.history{cix}.T1 = glb_tpti_viewer_confocal.T1;
glb_tpti_viewer_confocal.history{cix}.T2 = glb_tpti_viewer_confocal.T2;
glb_tpti_viewer_confocal.history{cix}.T3 = glb_tpti_viewer_confocal.T3;

glb_tpti_viewer_confocal.totfrm = size(glb_tpti_viewer_confocal.DAT0,3);
glb_tpti_viewer_confocal.dim    = [size(glb_tpti_viewer_confocal.DAT0,2),size(glb_tpti_viewer_confocal.DAT0,1)];
glb_tpti_viewer_confocal.curfrm = 1;
val = (glb_tpti_viewer_confocal.curfrm-1)/(glb_tpti_viewer_confocal.totfrm-1);
set(glb_tpti_viewer_confocal.slider1,'Value',val);
glb_tpti_viewer_confocal.ORI = floor( size(glb_tpti_viewer_confocal.DAT0)/2 ); % x y z
set(glb_tpti_viewer_confocal.editorigin,'String',num2str(glb_tpti_viewer_confocal.ORI));
glb_tpti_viewer_confocal.NumZ = [-glb_tpti_viewer_confocal.ORI(3)+1,size(glb_tpti_viewer_confocal.DAT0,3)-glb_tpti_viewer_confocal.ORI(3)];
set(glb_tpti_viewer_confocal.editNumZ,'String',sprintf('%d,%d',glb_tpti_viewer_confocal.NumZ));
xr=[-(glb_tpti_viewer_confocal.ORI(1)-1),size(glb_tpti_viewer_confocal.DAT0,1)-glb_tpti_viewer_confocal.ORI(1)];
yr=[-(glb_tpti_viewer_confocal.ORI(2)-1),size(glb_tpti_viewer_confocal.DAT0,2)-glb_tpti_viewer_confocal.ORI(2)];
%xa=[glb_tpti_viewer_confocal.ORI(1)+xr(1):glb_tpti_viewer_confocal.ORI(1)+xr(2)];
str=sprintf('%d,%d,%d,%d',xr(1),... % x1
    xr(2),... % x2
    yr(1),... % y1
    yr(2))    % y2
glb_tpti_viewer_confocal.editXY = uicontrol('Parent',glb_tpti_viewer_confocal.f,'Units','normalized','Style','edit',...
    'Position', [0.845 0.513 0.129 0.029],...
    'String',str,'Callback', @call_editXY);
glb_tpti_viewer_confocal.rect = str2num(str);

% plot image
axes(glb_tpti_viewer_confocal.ax(1));cla;
imagesc( squeeze(glb_tpti_viewer_confocal.DAT0(:,:,glb_tpti_viewer_confocal.curfrm))' );caxis([glb_tpti_viewer_confocal.MI,glb_tpti_viewer_confocal.MA]);
%title(sprintf('z=%d',zval));
axis([0.5,size(squeeze(glb_tpti_viewer_confocal.DAT0(:,:,glb_tpti_viewer_confocal.curfrm))',1)+0.5,0.5,size(squeeze(glb_tpti_viewer_confocal.DAT0(:,:,glb_tpti_viewer_confocal.curfrm))',2)+0.5]);
%set(gca,'YDir','normal');
colormap(gray);
%colormap(morgenstemning);
caxis([glb_tpti_viewer_confocal.MI,glb_tpti_viewer_confocal.MA]);

hold on;glb_tpti_viewer_confocal.rectH=patch(glb_tpti_viewer_confocal.ORI(1)+glb_tpti_viewer_confocal.rect([1,2,2,1]),...
    glb_tpti_viewer_confocal.ORI(2)+glb_tpti_viewer_confocal.rect([3,3,4,4]),[1 1 1],'FaceColor','none','EdgeColor',[1 1 1],'LineWidth',2);

% also delete all points in CELLLABEL
try
glb_tpti_viewer_confocal=rmfield(glb_tpti_viewer_confocal,'CELLLABEL');
end
% function call_drag(src,eventdata)
%
% test= 1;
function call_compute(src,eventdata)

global glb_tpti_viewer_confocal;

fprintf('started to compute...');

% read in selected part of stack
FRNG = glb_tpti_viewer_confocal.ORI(3)+glb_tpti_viewer_confocal.NumZ(1):glb_tpti_viewer_confocal.ORI(3)+glb_tpti_viewer_confocal.NumZ(2);
FRNG = FRNG(FRNG>0&FRNG<glb_tpti_viewer_confocal.totfrm+1);

XRNG=glb_tpti_viewer_confocal.ORI(1)+[glb_tpti_viewer_confocal.rect(1):glb_tpti_viewer_confocal.rect(2)];
YRNG=glb_tpti_viewer_confocal.ORI(2)+[glb_tpti_viewer_confocal.rect(3):glb_tpti_viewer_confocal.rect(4)];

XRNG = XRNG(XRNG>0&XRNG<glb_tpti_viewer_confocal.dim(2)+1);
YRNG = YRNG(YRNG>0&YRNG<glb_tpti_viewer_confocal.dim(1)+1);

glb_tpti_viewer_confocal.FRNG=FRNG;
glb_tpti_viewer_confocal.XRNG=XRNG;
glb_tpti_viewer_confocal.YRNG=YRNG;
DAT = zeros(length(XRNG),length(YRNG),length(FRNG));

if ~isfield(glb_tpti_viewer_confocal,'DAT0'),
    for n=1:length(FRNG), % n=1
        % read in frame
        fname = glb_tpti_viewer_confocal.fname{glb_tpti_viewer_confocal.curchn};
        fid = fopen(fname);
        rng=FRNG(n);
        fseek(fid, 2*(16/8) + (2*glb_tpti_viewer_confocal.dim(1)*glb_tpti_viewer_confocal.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
        ndat =fread(fid,[length(rng)*prod(glb_tpti_viewer_confocal.dim)],'*int16'); % read single frame
        ndatr=reshape(ndat,[glb_tpti_viewer_confocal.dim(2),glb_tpti_viewer_confocal.dim(1),length(rng)]); % size(datr)
        fclose(fid);
        %ndatr=ndatr';
        DAT(:,:,n)=ndatr(XRNG,YRNG);
        %figure;imagesc(ndatr);caxis([0,1200]);
    end % for i=1:glb_tpti_viewer_confocal.totfrm,
else
    DAT=glb_tpti_viewer_confocal.DAT0(XRNG,YRNG,FRNG);
end
%figure;imagesc(DAT(:,:,1));caxis([0,1200]);

% 1. translate the middle to the origin.
%blob_center = (size(DAT) + 1) / 2
blob_center = [find(XRNG==glb_tpti_viewer_confocal.ORI(1)),find(YRNG==glb_tpti_viewer_confocal.ORI(2)),find(FRNG==glb_tpti_viewer_confocal.ORI(3))];
T1 = [1 0 0 0
    0 1 0 0
    0 0 1 0
    -blob_center 1];

% 2. Rotate the blob.

% theta = pi/8;
% T2 = [cos(theta)  0      -sin(theta)   0
%     0             1              0     0
%     sin(theta)    0       cos(theta)   0
%     0             0              0     1]
T2 = glb_tpti_viewer_confocal.T;

% 3. Translate the rotated blob back to its starting location.

T3 = [1 0 0 0
    0 1 0 0
    0 0 1 0
    blob_center 1];

% The forward mapping is the composition of T1, T2, and T3.

T = T1 * T2 * T3;

tform = maketform('affine', T);
glb_tpti_viewer_confocal.tform = tform;
glb_tpti_viewer_confocal.T1 = T1;
glb_tpti_viewer_confocal.T2 = T2;
glb_tpti_viewer_confocal.T3 = T3;

% sanity check: the tform struct should map the blob center to itself.
%tformfwd(blob_center, tform)

% R is a resampler struct produced by the makeresampler function. You tell makeresampler the type of interpolation you want, as well as how to handle array boundaries.
R = makeresampler('linear', 'fill');

% TDIMS_A specifies how the dimensions of the input array correspond to the dimensions of the spatial transformation represented by the tform struct. Here I'll use the simplest form, in which each spatial transformation dimension corresponds to the same input array dimension. (Don't worry about the details here. One of these days I'll write a blog posting showing an example of when you might want to do something different with this dimension mapping.)
TDIMS_A = [1 2 3];

% TDIMS_B specifies how the dimensions of the output array correspond to the dimensions of the spatial transformation.
TDIMS_B = [1 2 3];

% TSIZE_B is the size of the output array.
TSIZE_B = size(DAT);

% TMAP_B is unused when you have a tform struct. Just specify it to be empty.
TMAP_B = [];

% F specifies the values to use outside the boundaries of the input array.
F = 0;

DATT = tformarray(DAT, tform, R, TDIMS_A, TDIMS_B, TSIZE_B, TMAP_B, F);
%DATT = flipdim(DATT,2);
%DATT = flipdim(DATT,1);
glb_tpti_viewer_confocal.DATT = DATT;

call_plot_DATT(src,[]);
set(gca,'XLim',[0.5,size(DATT,1)+0.5],'YLim',[0.5,size(DATT,2)+0.5]);

beep;pause(0.3);beep;pause(0.1);beep;
fprintf('DONE!\n');



function call_keypress(src,eventdata)
%WindowKeyPressFcn

if ismember(eventdata.Key,{'c','i','p','l'}),
    global glb_tpti_viewer_confocal;
    if strcmp(eventdata.Key,'c'), % change color channel
        glb_tpti_viewer_confocal.curchn=glb_tpti_viewer_confocal.curchn+1;
        glb_tpti_viewer_confocal.curchn=mod(glb_tpti_viewer_confocal.curchn-1,length(glb_tpti_viewer_confocal.fname))+1;
        set(glb_tpti_viewer_confocal.f,'Name',glb_tpti_viewer_confocal.fname{glb_tpti_viewer_confocal.curchn});
        updateframe=1;call_plot(src,eventdata,updateframe);
    elseif strcmp(eventdata.Key,'i'),
        [x, y, button] = ginput(1);
        if button==1,
            x=round(x);
            y=round(y);
            if ~isfield(glb_tpti_viewer_confocal,'CELLLABEL'),
                glb_tpti_viewer_confocal.CELLLABEL{1,:} = {x,y,glb_tpti_viewer_confocal.curfrm,glb_tpti_viewer_confocal.fname{glb_tpti_viewer_confocal.curchn}};
            else
                glb_tpti_viewer_confocal.CELLLABEL{size(glb_tpti_viewer_confocal.CELLLABEL,1)+1,:} = {x,y,glb_tpti_viewer_confocal.curfrm,glb_tpti_viewer_confocal.fname{glb_tpti_viewer_confocal.curchn}};
            end
        elseif button==3, % delete nearest point
            if isfield(glb_tpti_viewer_confocal,'CELLLABEL'),
                D=NaN([size(glb_tpti_viewer_confocal.CELLLABEL,1),1]);
                for i=1:size(glb_tpti_viewer_confocal.CELLLABEL,1),
                    %if glb_tpti_viewer_confocal.CELLLABEL{i}{3}==glb_tpti_viewer_confocal.curfrm,
                    D(i)=sqrt(sum([[x,y]-[glb_tpti_viewer_confocal.CELLLABEL{i}{1:2}]].^2));
                    %end
                end
                if any(~isnan(D)),
                    [~,ix] = min(D);
                end
                if size(glb_tpti_viewer_confocal.CELLLABEL,1)==1,
                    glb_tpti_viewer_confocal=rmfield(glb_tpti_viewer_confocal,'CELLLABEL');
                else
                    glb_tpti_viewer_confocal.CELLLABEL(ix,:)=[]
                end
            end
        end
        updateframe=1;call_plot(src,eventdata,updateframe);
    elseif strcmp(eventdata.Key,'p'), % backward transform
        axes( glb_tpti_viewer_confocal.ax(2) );
        [x, y, button] = ginput(1);
        x=round(x);
        y=round(y);
        
        src=glb_tpti_viewer_confocal.slider2; zval = round((get(src,'Value').*(size(glb_tpti_viewer_confocal.DATT,3)-1))+1);
        hold on;plot(x,y,'gx','MarkerSize',10,'LineWidth',2);
        tform = glb_tpti_viewer_confocal.tform;
        M = tforminv([x,y,zval],tform);
        
        M(1)=M(1)+glb_tpti_viewer_confocal.XRNG(1)-1;
        M(2)=M(2)+glb_tpti_viewer_confocal.YRNG(1)-1;
        M(3)=M(3)+glb_tpti_viewer_confocal.FRNG(1)-1;
        
        glb_tpti_viewer_confocal.curfrm = round(M(3));
        updateframe=1;call_plot(src,eventdata,updateframe);
        x=round(M(1));
        y=round(M(2));
        hold on;plot(x,y,'gx','MarkerSize',10,'LineWidth',2);
    elseif strcmp(eventdata.Key,'l'), % forward transform
        axes( glb_tpti_viewer_confocal.ax(1) );
        [x, y, button] = ginput(1);
        x=round(x);
        y=round(y);
        zval = glb_tpti_viewer_confocal.curfrm
        if ismember(x,glb_tpti_viewer_confocal.XRNG)&ismember(y,glb_tpti_viewer_confocal.YRNG)&ismember(zval,glb_tpti_viewer_confocal.FRNG),
            hold on;plot(x,y,'gx','MarkerSize',10,'LineWidth',2);
            
            tform = glb_tpti_viewer_confocal.tform;
            M = tformfwd([find(glb_tpti_viewer_confocal.XRNG==x),find(glb_tpti_viewer_confocal.YRNG==y),find(glb_tpti_viewer_confocal.FRNG==zval)],tform);
            M = round(M);
           
            zval2=M(3)
            val = (zval2-1)./(size(glb_tpti_viewer_confocal.DATT,3)-1);
            %src=glb_tpti_viewer_confocal.slider2; zval = round((get(src,'Value').*(size(glb_tpti_viewer_confocal.DATT,3)-1))+1);
            set(glb_tpti_viewer_confocal.slider2,'Value',val);
            call_plot_DATT(src,eventdata)  
            x2=round(M(1));
            y2=round(M(2));
            hold on;plot(x2,y2,'gx','MarkerSize',10,'LineWidth',2);
        end
    end
    
end

function call_slider1(src,eventdata)

global glb_tpti_viewer_confocal;
glb_tpti_viewer_confocal.curfrm = round((get(src,'Value').*(glb_tpti_viewer_confocal.totfrm-1))+1);
updateframe=1;call_plot(src,eventdata,updateframe);

function call_slider2(src,eventdata)
call_plot_DATT(src,eventdata)


function call_editXY(src,eventdata)

global glb_tpti_viewer_confocal;
try, delete(glb_tpti_viewer_confocal.rectH); end
glb_tpti_viewer_confocal.rect = str2num(get(src,'String'));
hold on;glb_tpti_viewer_confocal.rectH=patch(glb_tpti_viewer_confocal.ORI(1)+glb_tpti_viewer_confocal.rect([1,2,2,1]),...
    glb_tpti_viewer_confocal.ORI(2)+glb_tpti_viewer_confocal.rect([3,3,4,4]),[1 1 1],'FaceColor','none','EdgeColor',[1 1 1],'LineWidth',2);


function call_menu_extra_setXYascurrentzoom(src,eventdata)

global glb_tpti_viewer_confocal;

tmp  = [round( get(glb_tpti_viewer_confocal.ax(1),'XLim') ),round( get(glb_tpti_viewer_confocal.ax(1),'YLim') )];
tmp2 = floor([(tmp(1)+tmp(2))/2,(tmp(3)+tmp(4))/2]);
tmp(1:2)=tmp(1:2)-tmp2(1);
tmp(3:4)=tmp(3:4)-tmp2(2);

str=sprintf('%d,%d,%d,%d',tmp(1),tmp(2),tmp(3),tmp(4));
src=glb_tpti_viewer_confocal.editXY;
set(src,'String',str);

tmp3 = glb_tpti_viewer_confocal.ORI;
tmp3(1:2) = tmp2(1:2);
str=sprintf('%d,%d,%d',tmp3(1),tmp3(2),tmp3(3));
set(glb_tpti_viewer_confocal.editorigin,'String',str)

%-----------------------------------------------

try, delete(glb_tpti_viewer_confocal.rectH); end
glb_tpti_viewer_confocal.rect = str2num(get(glb_tpti_viewer_confocal.editXY,'String'));
glb_tpti_viewer_confocal.ORI = str2num(get(glb_tpti_viewer_confocal.editorigin,'String'));
hold on;glb_tpti_viewer_confocal.rectH=patch(glb_tpti_viewer_confocal.ORI(1)+glb_tpti_viewer_confocal.rect([1,2,2,1]),...
    glb_tpti_viewer_confocal.ORI(2)+glb_tpti_viewer_confocal.rect([3,3,4,4]),[1 1 1],'FaceColor','none','EdgeColor',[1 1 1],'LineWidth',2);


function call_editorigin(src,eventdata)

global glb_tpti_viewer_confocal;
glb_tpti_viewer_confocal.ORI = str2num(get(src,'String'));

function call_editNumZ(src,eventdata)

global glb_tpti_viewer_confocal;
glb_tpti_viewer_confocal.NumZ = str2num(get(src,'String'));

function call_editcmin(src,eventdata)

global glb_tpti_viewer_confocal;
glb_tpti_viewer_confocal.MI = str2num(get(src,'String'));

axes(glb_tpti_viewer_confocal.ax(1)); 
caxis([glb_tpti_viewer_confocal.MI,glb_tpti_viewer_confocal.MA])

axes(glb_tpti_viewer_confocal.ax(2)); 
caxis([glb_tpti_viewer_confocal.MI,glb_tpti_viewer_confocal.MA])

function call_editcmax(src,eventdata)

global glb_tpti_viewer_confocal;
glb_tpti_viewer_confocal.MA = str2num(get(src,'String'));

axes(glb_tpti_viewer_confocal.ax(1)); 
caxis([glb_tpti_viewer_confocal.MI,glb_tpti_viewer_confocal.MA])

axes(glb_tpti_viewer_confocal.ax(2)); 
caxis([glb_tpti_viewer_confocal.MI,glb_tpti_viewer_confocal.MA])

function call_slidermin(src,eventdata)

global glb_tpti_viewer_confocal;

function call_slidermax(src,eventdata)

global glb_tpti_viewer_confocal;

function call_plot_DATT(src,eventdata)

global glb_tpti_viewer_confocal;

src=glb_tpti_viewer_confocal.slider2; zval = round((get(src,'Value').*(size(glb_tpti_viewer_confocal.DATT,3)-1))+1);

axes(glb_tpti_viewer_confocal.ax(2));cla;
imagesc( squeeze(glb_tpti_viewer_confocal.DATT(:,:,zval))' );caxis([glb_tpti_viewer_confocal.MI,glb_tpti_viewer_confocal.MA]);title(sprintf('z=%d',zval));

function call_plot(src,eventdata,updateframe)

global glb_tpti_viewer_confocal;

if updateframe&(~isfield(glb_tpti_viewer_confocal,'DAT0')),    
    % read in frame
    fname = glb_tpti_viewer_confocal.fname{glb_tpti_viewer_confocal.curchn};
    fid = fopen(fname);
    rng=glb_tpti_viewer_confocal.curfrm;
    fseek(fid, 2*(16/8) + (2*glb_tpti_viewer_confocal.dim(1)*glb_tpti_viewer_confocal.dim(2)*(rng(1)-1)), 'bof'); % skip header (2*2bytes) + 2bytes * x * y pixels * number of preceding frames
    ndat =fread(fid,[length(rng)*prod(glb_tpti_viewer_confocal.dim)],'*int16'); % read single frame
    ndatr=reshape(ndat,[glb_tpti_viewer_confocal.dim(2),glb_tpti_viewer_confocal.dim(1),length(rng)]); % size(datr)
    fclose(fid);
    %glb_tpti_viewer_confocal.IMG=ndatr';
    glb_tpti_viewer_confocal.IMG=ndatr; % x , y
    title(sprintf('frm%d',glb_tpti_viewer_confocal.curfrm));
end
if updateframe,
    axes(glb_tpti_viewer_confocal.ax(1));
    if ~isfield(glb_tpti_viewer_confocal,'DAT0'),
        glb_tpti_viewer_confocal.hIMG=imagesc(glb_tpti_viewer_confocal.IMG');
    else
        imagesc( squeeze(glb_tpti_viewer_confocal.DAT0(:,:,glb_tpti_viewer_confocal.curfrm))' );%%caxis([glb_tpti_viewer_confocal.MI,glb_tpti_viewer_confocal.MA]);%title(sprintf('z=%d',zval));        
    end
    caxis([glb_tpti_viewer_confocal.MI,glb_tpti_viewer_confocal.MA]);
    %uistack(glb_tpti_viewer_confocal.hM,'top')
    if isfield(glb_tpti_viewer_confocal,'CELLLABEL'),
        hold on;ax=axis;
        for i=1:size(glb_tpti_viewer_confocal.CELLLABEL,1),
            if glb_tpti_viewer_confocal.CELLLABEL{i}{3}==glb_tpti_viewer_confocal.curfrm,
                glb_tpti_viewer_confocal.CELLLABELpt(i)=plot(glb_tpti_viewer_confocal.CELLLABEL{i}{1},glb_tpti_viewer_confocal.CELLLABEL{i}{2},'gx','MarkerSize',10,'LineWidth',2);
            else
                glb_tpti_viewer_confocal.CELLLABELpt(i)=plot(glb_tpti_viewer_confocal.CELLLABEL{i}{1},glb_tpti_viewer_confocal.CELLLABEL{i}{2},'gs','MarkerSize',10,'LineWidth',2);
            end
        end
        axis(ax);
    end
    
    try, delete(glb_tpti_viewer_confocal.rectH); end
    hold on;glb_tpti_viewer_confocal.rectH=patch(glb_tpti_viewer_confocal.ORI(1)+glb_tpti_viewer_confocal.rect([1,2,2,1]),...
    glb_tpti_viewer_confocal.ORI(2)+glb_tpti_viewer_confocal.rect([3,3,4,4]),[1 1 1],'FaceColor','none','EdgeColor',[1 1 1],'LineWidth',2);

    title(sprintf('frm=%d',glb_tpti_viewer_confocal.curfrm));
end
if isfield(glb_tpti_viewer_confocal,'GP'),
    uistack(glb_tpti_viewer_confocal.GP,'top');
end


function my_closefcn(src,eventdata)
% User-defined close request function
% to display a question dialog box
if 0,
    selection = questdlg('Close This Figure?',...
        'Close Request Function',...
        'Yes','No','Yes');
    switch selection,
        case 'Yes',
            delete(gcf);
        case 'No'
            return
    end
end
delete(gcf);


