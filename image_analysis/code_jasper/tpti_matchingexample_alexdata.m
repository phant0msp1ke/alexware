% based on: edit rsbs_ROI_CRSP_immuno_createREF_findexample.m
%
% [fList,pList] = matlab.codetools.requiredFilesAndProducts('tpti_matchingexample.m');
% for i=1:length(fList),
%    fprintf('%s\n',fList{i}); 
% end
% winopen('C:\Jasper\OneDrive\OneDrive - University College London\AlexFerretData\2p2immuno\code\');
% winopen('D:\BehaviourImaging\Matlab\ImagToolbox\2photon_setup\extractdata\');

%clear all;
dbstop if error;
javaaddpath '/home/alex/java/jar/mij.jar'; 
try, MIJ.exit; end
close all;

% tocopy = 1;
% RSG.savedir       = [ROOT,'rsbs_workflow\rsbs_ROI_CRSP\'];
% RSG.rootdir2      = [ROOT,'2photon\'];
% RSG.rootdir3      = [ROOT,'rsbs_workflow\rsbs_preproc_forsegment_nofilt\'];
% RSG.savedirSEG{1} = [ROOT,'rsbs_workflow\rsbs_segmentroi_batch\'];
% RSG.savedirSEG{4} = [ROOT,'rsbs_workflow\rsbs_preproc_forsegment_nofilt\'];
% ImmunoDir = [ROOT,'immuno_registered_convert\'];
ROOTNEW = '/home/alex/Desktop/Code/Jasper_code/2p2immuno/Example_data/';
alexdata = 1;
convert_data = 0; %Logical to decide whether to conver data or not
if alexdata,
    ROOTNEW = '/home/alex/Desktop/Code/Jasper_code/2p2immuno/Example_data/';
end

tocopy = 0;
RSG.savedir       = ROOTNEW;
RSG.rootdir2      = ROOTNEW;
RSG.rootdir3      = ROOTNEW;
RSG.savedirSEG{1} = ROOTNEW;
RSG.savedirSEG{4} = ROOTNEW;
ImmunoDir         = ROOTNEW;


% first write wrapper around Alex data
if convert_data
    % add to mystackviewer_CRSP
   %  if isfield(CRSP,'B'),
    
    
    ROOTNEW = '/home/alex/Desktop/Code/Jasper_code/2p2immuno/Example_data/';
    RSG.savedir = ROOTNEW;
    y = 'Area13';
    
    fname = [ROOTNEW,'Area13_2p_image_green.tif'];
    
    info = imfinfo(fname);
    num_frames = numel(info);
    for z = 1:num_frames
        fprintf('== Loading frame %0.f/%0.f ==\n',z,num_frames);
        img_xyz(:,:,z) = imread(fname, z, 'Info', info);
        % ... Do something with the image ...
    end
    
    img_xyz = img_xyz';
    
    % edit rsbs_preproc_forsegment
    layer = 2
    filesav = sprintf('%s%s_ch-525_pp4seg_layer%d_nofilt.bin',RSG.savedir,y,layer); % Area13_ch-525_pp4seg_layer2_nofilt
    fidsave = fopen(filesav,'w');
    dim=size(img_xyz);
    fwrite(fidsave,dim,'int16'); % write header to file
    for f=1:3, % write a few frames of same frame to avoid problems with code expecting a movie
    fwrite(fidsave,img_xyz,'int16'); % write to file
    end
    fclose(fidsave);

    % edit rsbs_segmentroi_batch
    % dummy file with no ROIs defined yet 
    clear SEGMENT;
    SEGMENT.I       = logical(size( img_xyz ) );
	SEGMENT.bwcells = logical(size( img_xyz ) );
    SEGMENT.celldetectsource = double(img_xyz);
    filesav = sprintf('%s%s.mat',RSG.savedir,y); % Area13_ch-525_pp4seg_layer2_nofilt
    save(filesav,'SEGMENT');
    
    
    % convert confocal to bin file
    % edit rsbs_convert_immuno.m
        
    fname = [ROOTNEW,'Slice8.tif'];    
    filesav = sprintf('%s%s_GCAMP.bin',RSG.savedir,'Slice8');
    fidsave = fopen(filesav,'w');
    info = imfinfo(fname);
    num_frames = numel(info);
    figure;
    for z = 1:num_frames
        fprintf('== Loading frame %0.f/%0.f ==\n',z,num_frames);
        img_xyz = imread(fname, z, 'Info', info);
        if z==1,            
            fwrite(fidsave,size(img_xyz),'int16'); % write header to file        
        end
        fwrite(fidsave,img_xyz,'int16'); % write to file
    end       
    fclose(fidsave);
    
end


%--------------------------------------------------------------------------
% logbook info about recordings

clear info;
ID = 'M71_20141020'; 
info.(ID).site = 'a';
info.(ID).fnamecalib={'20141020_175157__M71_calibeye0','20141020_175218__M71_calibeye1'};
bix=1;info.(ID).block_segment= bix;
info.(ID).fnamePC1{bix}='20141020_175241__M71_B1';
info.(ID).fnamePC2{bix}='20141020_175245__M71_B1';
info.(ID).fnamePC3{bix}='20141020_175244__M71_B1';
info.(ID).task{bix}='SD';
bix=2;
info.(ID).fnamePC1{bix}='20141020_182317__M71_B2';
info.(ID).fnamePC2{bix}='20141020_182325__M71_B2';
info.(ID).fnamePC3{bix}='20141020_182319__M71_B2';
info.(ID).fnamePC4{bix}='20141020_182304__M71_B2_rs_2eyes_retinotopy.mat';
bix=3;
info.(ID).fnamePC1{bix}='20141020_182802__M71_B3';
info.(ID).fnamePC2{bix}='20141020_182811__M71_B3';
info.(ID).fnamePC3{bix}='20141020_182801__M71_B3';
info.(ID).fnamePC4{bix}='20141020_182745__M71_B3_rs_2eyes.mat';
bix=4;
info.(ID).fnamePC1{bix}='20141020_183700__M71_B4';
info.(ID).fnamePC2{bix}='20141020_183724__M71_B4';
info.(ID).fnamePC3{bix}='20141020_183659__M71_B4';
info.(ID).fnamePC4{bix}='20141020_183717__M71_B4_rs_2eyes.mat';

ID = 'M71_20141029';
info.(ID).site = 'a';
info.(ID).fnamecalib={'20141029_161717__M71_calibeye0','20141029_161742__M71_calibeye1'};
bix=1;info.(ID).block_segment= bix;
info.(ID).fnamePC1{bix}='20141029_162853__M71_B1';
info.(ID).fnamePC2{bix}='20141029_162833__M71_B1';
info.(ID).fnamePC3{bix}='20141029_162905__M71_B1';
info.(ID).task{bix}='SD';
bix=2;
info.(ID).fnamePC1{bix}='20141029_171238__M71_B2';
info.(ID).fnamePC2{bix}='20141029_171217__M71_B2';
info.(ID).fnamePC3{bix}='20141029_171246__M71_B2';
info.(ID).fnamePC4{bix}='20141029_171259__M71_B2_rs_2eyes_retinotopy.mat';
bix=3;
info.(ID).fnamePC1{bix}='20141029_171639__M71_B3';
info.(ID).fnamePC2{bix}='20141029_171617__M71_B3';
info.(ID).fnamePC3{bix}='20141029_171648__M71_B3';
info.(ID).fnamePC4{bix}='20141029_171700__M71_B3_rs_2eyes.mat';
bix=4;
info.(ID).fnamePC1{bix}='20141029_172317__M71_B4';
%info.(ID).fnamePC1{bix}='20141029_173935__M71_B4';
info.(ID).fnamePC2{bix}='20141029_172256__M71_B4';
info.(ID).fnamePC3{bix}='20141029_172326__M71_B4';
info.(ID).fnamePC4{bix}='20141029_172337__M71_B4_rs_2eyes.mat';

ID = 'M71_20141101';
info.(ID).site = 'a';
info.(ID).fnamecalib={'20141101_183727__M71_calibeye0','20141101_183734__M71_calibeye1'};
bix=1;info.(ID).block_segment= bix;
info.(ID).fnamePC1{bix}='20141101_185315__M71_B1';
info.(ID).fnamePC2{bix}='20141101_185251__M71_B1';
info.(ID).fnamePC3{bix}='20141101_185330__M71_B1';
info.(ID).task{bix}='SWITCH';

ID = 'M99_20190101'; % Alex data
info.(ID).site = 'a';
%info.(ID).fnamecalib={'20141101_183727__M71_calibeye0','20141101_183734__M71_calibeye1'};
bix=1;info.(ID).block_segment= bix;
info.(ID).fnamePC1{bix}='Area13';
info.(ID).fnamePC2{bix}='Area13';
info.(ID).fnamePC3{bix}='Area13';
%info.(ID).task{bix}='SWITCH';

%--------------------------------------------------------------------------

if alexdata,
    rix = 1;
    ROIseslist{rix} = {'M99_20190101'}
    ROIsesblks{rix} = {[1]    [1]    [1]};
    ROIsitenam{rix} = 'M99a';
%     
%     rix = 1;
%     ROIseslist{rix} = {'M71_20141020'}
%     ROIsesblks{rix} = {[1]};
%     ROIsitenam{rix} = 'M71a';
else
    rix = 1;
    ROIseslist{rix} = {'M71_20141020'    'M71_20141029'    'M71_20141101'}
    ROIsesblks{rix} = {[1]    [1]    [1]};
    ROIsitenam{rix} = 'M71a';
end

ALLERROR={};
for rix=1%[2,13,19,22,27,30,36]%5%:17%2%2%[2,10,19,23,28,36]%18%:length(ROIseslist), still missing 15,31
    for layer=2,
        
        fprintf('%d:%s\n',rix,ROIseslist{rix}{1});
        
        do_review = 1;% leave as 1
        if do_review,
            
            % review correspondence in GUI
            clear global mystackA mystackU;
            
            cfg = [];
            cfg.ROIseslist=ROIseslist{rix};
            cfg.ROIsesblks=ROIsesblks{rix};
            cfg.ROIsitenam=ROIsitenam{rix};
            cfg.rootdir1 = RSG.rootdir2;
            cfg.rootdir2 = RSG.savedir;
            for NID=1:length(cfg.ROIseslist) %NID=10
                RSG.info = info.(cfg.ROIseslist{NID});
                blockix=cfg.ROIsesblks{NID};
                BLK=1;cfg.fnamePC2{NID} = RSG.info.fnamePC2{blockix(BLK)};
                cfg.fname{NID} = sprintf('%s%s_ch-525.bin',RSG.rootdir2,cfg.fnamePC2{NID} );
                [x,y,z]=fileparts(cfg.fname{NID});
                cfg.fname{NID} = sprintf('%s%s_pp4seg_layer%d_nofilt.bin',RSG.savedirSEG{4},y,layer);
                
                if tocopy,
                    of = cfg.fname{NID};
                    [x,y,z]=fileparts(of);
                    nf = [ROOTNEW,y,z];
                    S = copyfile(of,nf);
                    if ~S, error('here'); end
                end
            end
            
            if ~alexdata,
                % load correspondence files if any, and celllabels if any
                for NID=1:length(cfg.ROIseslist) %NID=11 i=3
                    
                    cfg.fnameCRSP{NID} = sprintf('%s%s_CRSPR_layer%d.mat',RSG.savedir,cfg.ROIseslist{NID},layer);
                    tmp=load(cfg.fnameCRSP{NID});
                    if tocopy,
                        of = cfg.fnameCRSP{NID};
                        [x,y,z]=fileparts(of);
                        nf = [ROOTNEW,y,z];
                        S = copyfile(of,nf);
                        if ~S, error('here'); end
                    end
                    cfg.CRSP{NID}=tmp.CRSPR;
                    
                    cfg.fnameCL{NID} = sprintf('%s%s_CL_layer%d.mat',RSG.savedir,cfg.ROIseslist{NID},layer);
                    tmp=load(cfg.fnameCL{NID});
                    cfg.CL{NID}      = tmp.CL;
                    if tocopy,
                        of = cfg.fnameCL{NID};
                        [x,y,z]=fileparts(of);
                        nf = [ROOTNEW,y,z];
                        S = copyfile(of,nf);
                        if ~S, error('here'); end
                    end
                    fprintf('%s\n',cfg.CRSP{NID}.segname);
                    [x,y,z]=fileparts( cfg.CRSP{NID}.segname );
                    cfg.CRSP{NID}.segname = sprintf('%s%s.mat',RSG.savedir,y);
                    if tocopy,
                        cfg.CRSP{NID}.segname
                    end
                end % cfg.ROIseslist
                
            else
                
                for NID=1:length(cfg.ROIseslist) %NID=11 i=3                    
                    cfg.CRSP{NID} = []
                end
                cfg.miscrsp = 1;
                
            end % if ~alexdata,
            
            % refresh mask names if necessary
            [x,y,z]=fileparts(RSG.savedirSEG{1});
            [x1,y,z]=fileparts(x);
            for i=1:length(cfg.CRSP),
                if not(isempty(cfg.CRSP{i})),
                    [x,y,z]=fileparts(cfg.CRSP{i}.segname);
                    [x2,y,z]=fileparts(x);
                    if ~isequal(x1,x2),
                        cfg.CRSP{i}.segname=strrep(cfg.CRSP{i}.segname,x2,x1);
                    end
                end
            end
            
            %if W==1,
            COMBI=[[1:length(ROIseslist{rix})-1]', [2:length(ROIseslist{rix})]'];
            COMBIcfg = cell([size(COMBI,1),1]);
            for cmb=1:size(COMBI,1),
                of = sprintf('%s%s_%s_ROICcfg_layer%d.mat',RSG.savedir,ROIseslist{rix}{COMBI(cmb,1)},ROIseslist{rix}{COMBI(cmb,2)},layer);
                tmp=load(of);
                COMBIcfg{cmb}=tmp.cfg;
                if tocopy,                    
                    [x,y,z]=fileparts(of);
                    nf = [ROOTNEW,y,z];
                    S = copyfile(of,nf);
                    if ~S, error('here'); end
                end
            end
            cfg.COMBI    = COMBI;
            cfg.COMBIcfg = COMBIcfg;
            
            close all;
            
            % find correspondence only for session with empty CRSPR (sessions mentioned in miscrsp)
            if ~isfield(cfg,'miscrsp'),
                cfg.miscrsp=[];
            end
            
            if ~alexdata,
            % [cfg]=resscn_ROI_correspondence_automatic_ROIseslist(cfg)
            [cfg]=rsbs_ROI_CRSP_auto_missing(cfg);
            else
                y = 'Area13';
                cfg.CRSP{1}.segname = sprintf('%s%s.mat',RSG.savedir,y);
                %cfg.CRSP{1}.B = [];
            end
            
            clear global mystackA;
            %cfg.MI=50;
            %cfg.MA=200;
            %cfg.immuno = do_immuno;
            mystackviewer_CRSP(cfg) % edit mystackviewer_correspondence
            
            % info about cell in confocal:
            global mystackA;
                  
            if 1, % testing part
              
                %------------------------
                % open corresponding immunostacks in matlab viewer
                % mystackviewer_segment
                % winopen(ImmunoDir)

                fnameS = dir(sprintf('%s%s*%s*.bin',ImmunoDir,'*_GCAMP.bin'));
                filename = fnameS(1).name;
                pathname = ImmunoDir;
                
                % these are the bin files (not yet transformed)
                cfgI=[];
                cfgI.fname{1} = [pathname,filename];
                %cfgI.fname{2} = [pathname,strrep(filename,'GCAMP','PV')];
                %cfgI.fname{3} = [pathname,strrep(filename,'GCAMP','SOM')];
                %cfgI.fname{4} = [pathname,strrep(filename,'GCAMP','VIP')];
                
                %mystackviewer_immunoview(cfgI);
                
                clear global myIview;
                %profile on
                myimmunoview(cfgI);
                %tpti_viewer_confocal(cfgI);
                %profile viewer;
                
                %--------------------------------------------------------------
                % also load the non transformed tiff stack in image j
              
                clear fn;
                for i=1%:4,
                    fn{i}  = strrep([ImmunoDir,'Slice8.tif'],'\','/');                    
                end
                
                fid = fopen('/home/alex/Desktop/Code/Jasper_code/2p2immuno/temp/tmp.ijm','wt');
                fprintf(fid,'run("Bio-Formats Macro Extensions");\n');                
                for i=1%:4,
                    fprintf(fid,'Ext.openImagePlus("%s");\n',fn{i});
                    %fprintf(fid,'open("%s");\n',fn{i});
                end              
                fprintf(fid,'run("Close");\n');
                fclose(fid);
                system('notepad "/home/alex/Desktop/Code/Jasper_code/2p2immuno/temp/tmp.ijm" &')
                
                Miji;
                
                MIJ.run('Install...', 'install=/home/alex/Desktop/Code/Jasper_code/2p2immuno/temp/tmp.ijm');
                MIJ.run('tmp');
                % edit tpti_matchingexample.m
                
                %open("C:/Jasper/OneDrive/OneDrive - University College London/AlexFerretData/Example/Slice8.tif");
               
                
                
                
            end
            
            set(gcf,'CloseRequestFcn','uiresume(gcbf);closereq;');
            interactwithGUI=1; % !! if 0 automatically saves CRSPR files
        
            if interactwithGUI,
                choice = questdlg('Continue?', ...
                    'resscn_ROI_correspondence_sessionlist', ...
                    'Yes','No','No');
            else
                choice = 'Yes';
            end
            % Handle response
            switch choice
                case 'Yes'
                    NID
                case 'No'
                    NID
                    break;
            end
 
        end % if do_review,
        
    end % for layer=1:4,
    try,
        if do_immuno,
            MIJ.run('Close All'); % this crashes matlab if no ImageJ open...(at least on Beast)
            MIJ.exit;
        end
    end
end % for rix=1:length(ROIseslist),
