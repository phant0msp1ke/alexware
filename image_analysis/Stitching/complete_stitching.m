%% Setup params
tiles_path = '/media/alex/5FC39EAD5A6AA312/Reconstruction_Leela/Tiling/Tiles/'; %The folder with the tiles from ScanImage
areas_path =  '/media/alex/5FC39EAD5A6AA312/Reconstruction_Leela/Tiling/areas/'; %Folder with the areas which will be displayed in the mask. This should contain subfolders called areaxx with one GRABinfo file inside
save_dir = fullfile(tiles_path, 'Stitched'); %Where to save the stitched image and the one with the overlayed mask
register = 1; %Logical flag to register the tiles or not
smooth = 0; %Logical flag to smooth the image or not
divfactor = 0.3; %This controls the size of the image with the mask. This is the biggest stable value possible

%% Stitching and mask overlay

%Stitch all the tiles together
[stitched_image,params] = image_stitcher_center(tiles_path,register,smooth);
close all;

%Make a mask for all the areas
[area_mask, area_names] = make_area_mask(areas_path,params,save_dir);

%Overlay the mask over the stitched images
displayimgnmask2_alex(stitched_image,area_mask,save_dir,area_names,divfactor);