%This funciton will find the z-stack plane that best matches the 2p in vivo
%image
animal = 'Leela';
n_areas = 7;
base_name = '/media/alex/5FC39EAD5A6AA312/Reconstruction';

for i = 1:n_areas
    
    area = ['area0',num2str(i)];
    stack_folder = fullfile(base_name,animal,filesep,'z_stacks/areas_processed/',area,filesep);
    stack_name = fullfile(stack_folder, [area,'_zstack.tif']);
    
    %Load the z-stack
    [stack_xyz, ~] = tiff_loader(stack_name);
    stack_xyz = double(stack_xyz);
    
    %Load the mean image 
    img_name = fullfile(base_name, animal, filesep, 'img_mask/',area, filesep, [animal,'_',area,'_mean.tif']);
    [img, ~] = tiff_loader(img_name);
    img = double(img);
    
    %Compute the correlation between the mean iamge and the z-stack for
    %every plane
    n_stacks = size(stack_xyz,3);
    CCmax = zeros(n_stacks,1);
    fprintf('== Computing CC for Animal: %s, %s ==\n', animal, area);tic;
    for z = 1:n_stacks
        CC = normxcorr2(img, stack_xyz(:,:,z));
        CCmax(z) = max(CC(:));
    end
    fprintf('== Done! This took %0.fs ==\n', toc);
    %Find the image with the highest correlation
    [~, z_best] = max(CCmax);
    
    %Save the resulting image and number
    filename = fullfile(stack_folder, [animal,'_',area,'_plane',num2str(z_best),'_.tif']);
    final_image = uint8(255*mat2gray(stack_xyz(:,:,z_best)));
    imwrite(final_image, filename);
    
end