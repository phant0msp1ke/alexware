function tp_to_bin(img_path, rez_path)
%This function is used to convet .tif in vivo 2p files to binaries for use by the
%reconstruction code. 

%Get the name of the directory and image
[root_dir, full_name] = fileparts(img_path);
ix = strfind(full_name,'_'); ix = ix(2);
ani_area_name = full_name(1:ix-1);

%Get the mask
file_list = dir(fullfile(root_dir, '*Mask.mat'));
file_list = file_list(~ismember({file_list.name},{'.','..'}));
mask_path = fullfile(file_list(1).folder, file_list(1).name);

temp = load(mask_path);
Mask = logical(temp.ROIMask);
savedir = fullfile(root_dir,'Conv_bin_files/');

if ~exist(savedir,'dir')
    mkdir(savedir);
end

[img,~] = tiff_loader(img_path);

img = img'; %Transpose image so that it will be loaded properly by the enxt function

% Save the 2p image as .bin in origianl dir
layer = 2;
filesav = sprintf('%s%s_ch-525_pp4seg_layer%d_nofilt.bin', savedir, ani_area_name, layer); % Area13_ch-525_pp4seg_layer2_nofilt
fidsave = fopen(filesav,'w');
dim=size(img);
fwrite(fidsave,dim,'int16'); % write header to file

for f=1:3 % write a few frames of same frame to avoid problems with code expecting a movie
    fwrite(fidsave,img,'int16'); % write to file
end
fclose(fidsave);

% Save the 2p image as .bin in the res dir
filesav = sprintf('%s%s_ch-525_pp4seg_layer%d_nofilt.bin', rez_path, ani_area_name, layer); % Area13_ch-525_pp4seg_layer2_nofilt
fidsave = fopen(filesav,'w');
dim=size(img);
fwrite(fidsave,dim,'int16'); % write header to file

for f=1:3 % write a few frames of same frame to avoid problems with code expecting a movie
    fwrite(fidsave,img,'int16'); % write to file
end
fclose(fidsave);


% edit rsbs_segmentroi_batch
% dummy file with no ROIs defined yet
SEGMENT.I       = Mask;
SEGMENT.bwcells = Mask;
SEGMENT.celldetectsource = double(img);
filesav = sprintf('%s%s.mat',savedir, ani_area_name); % Area13_ch-525_pp4seg_layer2_nofilt
save(filesav,'SEGMENT');

filesav = sprintf('%s%s.mat',rez_path, ani_area_name); % Area13_ch-525_pp4seg_layer2_nofilt
save(filesav,'SEGMENT');
