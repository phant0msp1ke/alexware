%This function will process the GAD immuno image

%% Add necessary frames
fname = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction/Leela/results/area04/slice5/Leela_area04_slice5_GAD.tif';
n = 2; %How many of the slices before and after to copy

save_name = strrep(fname,'.','_dup.');

[img_xyz, info] = tiff_loader(fname); %Load the GAD z-stack

%Copy the necessary z slices
out_img_xyz = img_xyz(:,:,n+1:end-n);

for z = n:-1:1
    out_img_xyz = cat(3, repmat(img_xyz(:,:,z),1,1,2), out_img_xyz);
    out_img_xyz = cat(3, out_img_xyz, repmat(img_xyz(:,:,(end-z)+1),1,1,2));
end

%Save the resulting image
num_slices = size(out_img_xyz,3);

for z=1:num_slices
   %Write to tif file
   imwrite(out_img_xyz(:,:,z), save_name, 'WriteMode', 'append',  'Compression','none');
end

%% Process the image with CANDLE-J in ImageJ
fprintf('Now process the image with CANDLE-J in ImageJ. This is located in: /home/alex/ImageJ\n');
fprintf('Make sure to launch the icon with the gearwheels otherwise it wouldn''t work\n');
fprintf('The params are:\n');
fprintf('Smoothing parameter: 0.5\n');
fprintf('Patch radius: 1.0\n');
fprintf('Search volume radius: 3.0\n');
pause; %Wait for user to press something

%% Remove the added frames
DEFAULT = 'No';
Button = questdlg('Are you sure you are ready?', ...
    'Next step', ...
    'Yes', 'No',DEFAULT);

switch Button
    case 'Yes'
        [finimg_xyz, ~] = tiff_loader(save_name); %Load the GAD z-stack again
        
        % Remove the necessary slices
        finimg_xyz(:,:,[1:n, (end-n)+1:end]) = [];
        
        % Save the resulting image
        num_slices = size(finimg_xyz,3);
        
        % Change the name appropriately
        fin_name = strrep(save_name,'dup','final');
        max_val = double(intmax('uint8'))*3;
        for z=1:num_slices
            %     Convert to 16 bit image
            current_slice = uint16((double(intmax('uint8')))*3*mat2gray(finimg_xyz(:,:,z)));
            %     Remove snowflakes by setting them to the mean value
            current_slice(current_slice==max_val) = mean(current_slice(:));
            %     Write to tif file
            imwrite(current_slice, fin_name, 'WriteMode', 'append',  'Compression','none');
        end
        
    case 'No'
        disp('Run the cell again when ready');
end