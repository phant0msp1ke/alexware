function ROIMask = get_roi_mask(folder_name)
%This is a small function to get the ROI mask from the Suite2P output

file_name = fullfile(folder_name,'Fall.mat');
load(file_name);
file_name = fullfile(folder_name,'iscell.npy');
iscell = readNPY(file_name);
ROIMask = zeros([512 512]);

%Get the coordinates the coordinates of different cells by converting them
%to [x,y] dimensions
for i = 1:length(stat)
    if iscell(i,1)
        ipix = stat{i}.ypix + (stat{i}.xpix-1) .* 512;
        ROIMask(ipix) = i;
    end
end

ROIMask = rot90(ROIMask); %Rotate by 90 degrees counterclockwise to match the 2p and confocal images