%This script will generate the necessary .bin files for the reconstruction
%from the 2p and cnfocal .tif files

% split = 1; %Logical flag to indicate whether the 2p plane was split between 2 confocal planes
animal_name = 'Leela'; %Name of the animal
area_name = 'area04'; %Name of the area
plane = 'slice5'; %Name of the plane

root_dir = '/media/alex/5FC39EAD5A6AA312/2p_analysis/Reconstruction'; %The root dir with all the files for all animals

rez_dir = fullfile(root_dir, animal_name, 'results', area_name, plane, filesep); %The dir where the results for the reconstruction will be stored

if ~exist(rez_dir,'dir')
    mkdir(rez_dir)
end

mask_dir = fullfile(root_dir,animal_name,'img_mask', area_name);
%% Get the binary confocal files
type{1} = 'GCAMP'; type{2} = 'GAD'; type{3} = 'PV'; type{4} = 'SOM';

for t = 1:length(type)
    conf_path = fullfile(root_dir, strjoin({animal_name, 'confocal_slices', area_name, [animal_name,'_',area_name,'_',plane,'_',type{t},'.tif']}, '/'));
    conf_to_bin(conf_path, rez_dir);
end

%% Get the binary 2p files
tp_path = fullfile(root_dir, animal_name, 'img_mask', area_name, [animal_name,'_',area_name,'_mean.tif']);
tp_to_bin(tp_path, rez_dir);

%% Copy some necessary files

%Copy the GCAMP confocal
conf_path = fullfile(root_dir, animal_name, 'confocal_slices', area_name);
temp1 = dir(fullfile(conf_path, ['*',plane,'_GCAMP.tif']));
gname = strrep(temp1.name, 'GCAMP.tif', 'GCAMP_orig.tif');
name1 = fullfile(rez_dir, gname);
copyfile(fullfile(temp1.folder,temp1.name), name1);

%Copy the registered 2p mean image
temp2 = dir(fullfile(mask_dir, ['*mean_green.tif']));
name2 = fullfile(mask_dir, temp2.name);
copyfile(name2, rez_dir);
