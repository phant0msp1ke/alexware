function conf_to_bin(file_path, rez_path)
%This function is used to convert confocal stack .tif files to binaries for use by the
%reconstruction code. Specifically for myimmunoview. Based on confocal
%conversion part of tpti_matchingexample_alexdata which itself is based on rsbs_convert_immuno.m

[file_name,folder_name] = get_names(file_path);
savedir = fullfile(folder_name,'Conv_bin_files/');

if ~exist(savedir,'dir')
    mkdir(savedir);
end

%Save the confocal file in the original dir
filesav = sprintf([savedir,file_name,'.bin']);
fidsave = fopen(filesav,'w');
info = imfinfo(file_path);
height_img = info(1).Height; %Find out the dimensions of the image
width_img = info(1).Width;
num_frames = numel(info);

for z = 1:num_frames
    fprintf('== Processing frame %0.f/%0.f ==\n',z,num_frames);
    img_xyz = imread(file_path, z, 'Info', info);
    
    %Correct for images which are not perfect squares by filling the
    %smaller dimension with zeros. This is necessary because other
    %functions in the analysis require square images
    if height_img > width_img
        img_xyz(:,width_img+1:height_img) = 0;
    elseif width_img > height_img
        img_xyz(height_img+1:width_img,:) = 0;
    end
    
    img_xyz = img_xyz'; %image^T so it is saved in the right orientation for the immuno viewer
    if z==1
        fwrite(fidsave,size(img_xyz),'int16'); % write header to file
    end
    fwrite(fidsave,img_xyz,'int16'); % write to file
end
fclose(fidsave);

%Save the confocal file in results directory
filesav = sprintf([rez_path,file_name,'.bin']);
fidsave = fopen(filesav,'w');


for z = 1:num_frames
    fprintf('== Processing frame %0.f/%0.f ==\n',z,num_frames);
    img_xyz = imread(file_path, z, 'Info', info);
    
    %Correct for images which are not perfect squares by filling the
    %smaller dimension with zeros. This is necessary because other
    %functions in the analysis require square images
    if height_img > width_img
        img_xyz(:,width_img+1:height_img) = 0;
    elseif width_img > height_img
        img_xyz(height_img+1:width_img,:) = 0;
    end
    
    img_xyz = img_xyz'; %image^T so it is saved in the right orientation for the immuno viewer
    if z==1
        fwrite(fidsave,size(img_xyz),'int16'); % write header to file
    end
    fwrite(fidsave,img_xyz,'int16'); % write to file
end
fclose(fidsave);