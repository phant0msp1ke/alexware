function imgs = get_reg_img(folder_name)

file_name = fullfile(folder_name,'Fall.mat');
load(file_name);

%Get the mean, mean enhanced and reference images
imgs.mean = rot90(ops.meanImg);
imgs.enhc = rot90(ops.meanImgE);
imgs.ref = rot90(ops.refImg);

imgs.names{1} = 'mean';
imgs.names{2} = 'enhc';
imgs.names{3} = 'ref';
