%% Setup
fpath = '/media/alex/5FC39EAD5A6AA312/Reconstruction/Bender/z_stacks/areas_raw/area01/(20181008_23_45_17)-_area01_zstack3';
save_dir = '/media/alex/5FC39EAD5A6AA312/Reconstruction/Bender/z_stacks/areas_processed';
invert = 0; %Logical flag whether to invert the order of the z-stack when saving

%Make the necessary names
ix = strfind(fpath,'_');
area = fpath(ix(end-1)+1:ix(end)-1);
save_name = fpath(ix(end-1)+1:end);

area_dir = fullfile(save_dir, area);
if ~exist(area_dir, 'dir')
    mkdir(area_dir);
end

[fname,pname,tag]=uigetfile({'*.tif'},'Select File',fpath);
% grabfname=[fname(1:end-8),'_GRABinfo.mat'];
grabfname=strrep(fname, '.tif', '_GRABinfo.mat');

load(fullfile(pname, grabfname));
% Stimulus variables
nChannels=GRABinfo.channelsSave;

imWidth=GRABinfo.scanPixelsPerLine;
imHeight=GRABinfo.scanLinesPerFrame;
stackNumSlices = GRABinfo.stackNumSlices;
z_step_um = GRABinfo.stackZStepSize;
frames_per_z = GRABinfo.acqNumFrames;
total_frames = nChannels*stackNumSlices*frames_per_z;
frames_per_file = 900;


imList = dir([pname fname(1:end-8) '*.tif']);
nIm = length(imList);

frames_last_file = total_frames - frames_per_file*(nIm - 1);

im=zeros(imHeight,imWidth,total_frames,'uint16');
%% Open raw images
fprintf('Extracting images...\n');tic;

count=0;
for i = 1:nIm
    fprintf('== Opening image %0.f/%0.f ==\n',i,nIm);
    args(i).filename=[pname imList(i).name];
    args(i).info=imfinfo([pname imList(i).name]);
    args(i).pixelregion=[];
    
    if i < nIm
        nFrames = frames_per_file;
    else
        nFrames = frames_last_file;
    end
    
    for ii=1:nFrames
        count=count+1;
        args(i).index=ii;
        args(i).offset=args(i).info(ii).Offset;
        [im(:,:,count),~,~]=rtifc(args(i));
    end
end
fprintf('== Done! Extraction took %0.fs ==\n',toc);

%% Store in cell aray according to z-plane
ix_z = [1:frames_per_z:total_frames];
img_mat = cell(stackNumSlices,1);

for slice = 1:stackNumSlices
    img_mat{slice} = im(:,:,ix_z(slice):ix_z(slice)+(frames_per_z-1));
end
%% Register
fprintf('Registering images...\n');tic;
parfor slice = 1:stackNumSlices
    fprintf('== Processing slice %0.f/%0.f ==\n',slice,stackNumSlices);
    img_mat{slice} = register_img(img_mat{slice});
    img_mat{slice} = mean(img_mat{slice},3);
    %Rotate by 90 degrees counter-clockwise
    img_mat{slice} = rot90(img_mat{slice});
end
fprintf('== Done! Registration took %0.fs ==\n',toc);

%% Optionally fix the scanlines if they are misaligned
ix = round(stackNumSlices/2); %Get an image from the middle of the stack which is likely to have cells
test_shift = 2; %How many pixels to shift +/-

scanfix_dir = fullfile(area_dir,'fix_scanlines');
if ~exist(scanfix_dir, 'dir')
    mkdir(scanfix_dir)
end

%Generate images with different shift to see which ones works best
temp_img = img_mat{ix};
fix_scanlines_test(temp_img,test_shift, scanfix_dir);

%Ask the user if they want to fix scanlines based on these images
prompt = 'Do you want to fix scanlines? y/n [n]: ';
resp = input(prompt,'s');
if isempty(resp)
    resp = 'n';
end

new_img_mat = img_mat;

if strcmp(resp,'y')
    prompt = 'Do you want to use cols or rows? [cols]: ';
    dir_fix = input(prompt,'s');
    if isempty(dir_fix)
        dir_fix = 'cols';
    end
    prompt = 'How many pixels do you want to shift?: ';
    shift_pix = input(prompt);
    
    %Apply the selected shift to the images
    for s = 1:stackNumSlices
        switch dir_fix
            case 'cols'
                new_img_mat{s}(:,1:2:end) = circshift(new_img_mat{s}(:,1:2:end),shift_pix);
            case 'rows'
                new_img_mat{s}(1:2:end,:) = circshift(new_img_mat{s}(1:2:end,:),shift_pix);
        end
    end
end



%% Save the results as a .tif
fprintf('Writing Tiff file...\n');tic;
outputFileName = fullfile(area_dir,[save_name,'.tif']);

if invert
    ixs = [stackNumSlices:-1:1];
else
    ixs = [1:stackNumSlices];
end

for slice = ixs
   %Convert to 16 bit image
   current_slice = uint16(double(intmax('uint16'))*mat2gray(new_img_mat{slice}));
   %Write to tif file
   imwrite(current_slice, outputFileName, 'WriteMode', 'append',  'Compression','none');
end
fprintf('== Done! Writing took %0.fs ==\n',toc);

%% Also save using ImageJ so it can be green
javaaddpath '/home/alex/java/jar/mij.jar'; %Add this so we can run Miji
try
    MIJ.exit;
catch
    fprintf('IMageJ already closed\n');
end
tmp_folder = '/home/alex/Desktop/Data/ImageJ_test/temp/'; %Folder where the macro is stored

fiji_name = fullfile(area_dir,[save_name,'_green.tif']);
fid = fopen([tmp_folder,'/tmp.ijm'],'wt'); %Open a pointer to the .ijm file that will be saved
fprintf(fid,['open("',outputFileName,'");\n']);
fprintf(fid,['run("Channels Tool...");\n']);
fprintf(fid,['run("Green");\n']);
fprintf(fid,['saveAs("Tiff", "',fiji_name,'");\n']);
fprintf(fid,['run("Close");\n']);
fprintf(fid,['close();\n']);

fclose(fid); %Close the pointer

Miji; %Launch Miji
MIJ.run('Install...', ['install=',tmp_folder,'/tmp.ijm']); %Install the Macro
MIJ.run('tmp'); %Run the Macro
MIJ.exit; %Close Miji
clc;
clear all;
close;