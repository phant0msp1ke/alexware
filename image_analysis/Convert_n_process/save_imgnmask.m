%This script loads the mean image used for ROI selection and the ROI mask
n_areas = 1;
animal = 'Unity';
for i = 1:n_areas
    
    area = ['area0',num2str(i)];
    base_name = '/media/alex/5FC39EAD5A6AA312/Reconstruction/';
    
    save_folder = fullfile(base_name, animal, 'img_mask/', area, filesep);
    transperancy = 0.3; %The transperancy value of the mask
    
    %Make the folders
    if ~exist(save_folder,'dir')
        mkdir(save_folder);
    end
    
    folder_name = fullfile(base_name, animal,filesep, 'suite2p_data/', area, filesep, 'plane0');
    save_name = [animal,'_',area];
    
    
    %Initialize Fiji stuff
    javaaddpath '/home/alex/java/jar/mij.jar'; %Add this so we can run Miji
    try
        MIJ.exit;
    catch
        fprintf('IMageJ already closed\n');
    end
    tmp_folder = '/media/alex/5FC39EAD5A6AA312/Reconstruction/Unity/temp'; %Folder where the macro is stored
    file_list = dir(fullfile(tmp_folder,'*.ijm'));
    file_list = file_list(~ismember({file_list.name},{'.','..'}));
    if ~isempty(file_list)
        fname = fullfile(file_list.folder, file_list.name);
        delete(fname);
    end
    fid = fopen([tmp_folder,'/tmp.ijm'],'wt'); %Open a pointer to the .ijm file that will be saved
    %% Get the images and mask
    ROIMask = get_roi_mask(folder_name);
    imgs = get_reg_img(folder_name);
    n_imgs = length(imgs.names);
    
    %Save the mask
    mask_path = fullfile(save_folder,'ROIMask.mat');
    save(mask_path,'ROIMask');
    
    fiji_name = cell(n_imgs,1);
    img_name = cell(n_imgs,1);
    
    %Save the three images
    for j = 1:n_imgs
        name = imgs.names{j};
        img_name{j} = [save_name,'_',name];
        filename = fullfile(save_folder,[img_name{j},'.tif']);
        final_image = uint8(255*mat2gray(imgs.(name)));
        imwrite(final_image, filename);
        
        %Make them green and enhance contrast using Fiji
        fiji_name{j} = fullfile(save_folder,[img_name{j},'_green.tif']);
        fprintf(fid,['open("',filename,'");\n']);
        fprintf(fid, ['run("Brightness/Contrast...");\n','run("Enhance Contrast", "saturated=0.35");\n','run("Apply LUT");\n']);
        fprintf(fid,['run("Channels Tool...");\n']);
        fprintf(fid,['run("Green");\n']);
        fprintf(fid,['saveAs("Tiff", "',fiji_name{j},'");\n']);
        fprintf(fid,['run("Close");\n']);
        fprintf(fid,['run("Close");\n']);
%         fprintf(fid,['run("Close");\n']);
        
    end
    
    %% Run Fiji
    fclose(fid); %Close the pointer
    Miji; %Launch Miji
    MIJ.run('Install...', ['install=',tmp_folder,'/tmp.ijm']); %Install the Macro
    MIJ.run('tmp'); %Run the Macro
    MIJ.exit; %Close Miji
    
    %% Make the images with the masks
    
    for j = 1:n_imgs
        img_mask_path = fullfile(save_folder, [img_name{j},'_mask']);
        imfuseA(fiji_name{j}, mask_path, img_mask_path, transperancy);
    end
    
end

clc;
clear all;
close all;