function imfuseA(img_path, mask_path, save_name, transperancy)
%This function will load the mean_img used for the ROI extraction and
%overlay the ROIs on top so I know on which areas to focus my search and
%reconstruction accuracy effort

if ~exist('transperancy', 'var') || isempty(transperancy)
    transperancy = 0.3;
end


[img,~] = tiff_loader(img_path); %Load the mean image
img = double(img); %Convert to double so it is displayed properly
temp = load(mask_path); %Load the ROI mask
mask = temp.ROIMask;
mask = double(logical(mask)); %Convert to 0s and 1s 
red_mask = cat(3, ones(size(img)), zeros(size(img)), zeros(size(img))); %Make a red mask

figure('units','normalized','outerposition',[0 0 1 1]);
set(gcf,'color','w');

%Fuse the two images
transp_mask = transperancy*mask; %Make a transperancy mask
image(img);
colormap(gray(256));
hold on;
image(red_mask, 'AlphaData', transp_mask);
axis equal;
axis off;
fname = [save_name,'.jpg'];
saveas(gcf, fname);
close all;
