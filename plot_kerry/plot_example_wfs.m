clear all; close all;
%% Define params
fpath = '/media/alex/5FC39EAD5A6AA312/EI_tonotopy/Secondary/Ronnie/P05/P05-quning/clust_info.mat'; % abs path to the clust_info file to use
save_dir = '/home/alex/Desktop/Code/alexware/plot_kerry/Plots';
font_axis = 50;
wf_choose_exc = 13; %Take the jth longest waveform
wf_choose_inh = 13; %Take the kth shortest waveform
n_wfs = 300; %How many waveforms to use
wf_th = 0.3; % Threshold for the waveform removal
n_cross = 10; % How many times it's OK to cross the threshold
t_start_ms = -0.1; %The start and end times beyind which to look for violations
t_end_ms = 0.15;
plot_raw = 0; %Logical flag whether ot plot the raw waveforms
%% Load and process the data
load(fpath);
t_ms = clust_info.wf_time;
ix_time = t_ms<t_start_ms | t_ms>t_end_ms;

%Order the waveforms according to length of spike waveforms (trough-to-peak)
[spike_width_ms_sort, ix] = sort(clust_info.results.spike_width_ms, 'ascend');

%Find the max and min length of spike waveforms (trough-to-peak)
ix_max = ix(end-wf_choose_exc);
ix_min = ix(wf_choose_inh);

%Get the corresponding cluster ids
id_exc = clust_info.results.normal_cluster_id(ix_max);
id_inh = clust_info.results.normal_cluster_id(ix_min);

%Find the right ix to acess the raw wfs
ix_exc = find(clust_info.clust_id==id_exc);
ix_inh = find(clust_info.clust_id==id_inh);

%Select random waveforms to plot
n_exc = length(clust_info.raw_wfs{ix_exc});
n_inh = length(clust_info.raw_wfs{ix_inh});
ix_perm_exc = randperm(n_exc, n_wfs);
ix_perm_inh = randperm(n_inh, n_wfs);

%Get the waveforms
wfs_exc = clust_info.raw_wfs{ix_exc}(:,ix_perm_exc);
wfs_inh = clust_info.raw_wfs{ix_inh}(:,ix_perm_inh);

%Find the mean and max values
mean_exc = mean(wfs_exc,2);
max_exc = abs(min(mean_exc));
mean_inh = mean(wfs_inh,2);
max_inh = abs(min(mean_inh));

%Remove outlier wfs
wf_cross_exc = abs(wfs_exc(ix_time,:))>max_exc*wf_th;
wf_cross_inh = abs(wfs_inh(ix_time,:))>max_inh*wf_th;

ix_keep_exc = sum(wf_cross_exc)<n_cross;
ix_keep_inh = sum(wf_cross_inh)<n_cross;

wfs_exc = wfs_exc(:,ix_keep_exc);
wfs_inh = wfs_inh(:,ix_keep_inh);

%Scale the waveforms
mean_exc = mean(wfs_exc,2);
max_exc = abs(min(mean_exc));
mean_inh = mean(wfs_inh,2);
max_inh = abs(min(mean_inh));

wfs_exc = wfs_exc/max_exc;
wfs_inh = wfs_inh/max_inh;
%% Plot the waveforms
if plot_raw
    max_exc = max(abs(wfs_exc(:)));
    wfs_exc = wfs_exc/max_exc;
    max_inh = max(abs(wfs_inh(:)));
    wfs_inh = wfs_inh/max_inh;
    figure('units','normalized','outerposition',[0 0 1 1]);  hold on;
    plot(t_ms, wfs_exc, 'r');
    plot(t_ms, wfs_inh, 'b');
    xlabel('Time from trough (ms)');
    ylabel('Normalized amplitude');
    hold off;
    xlim([-0.4, 0.9]);
    ylim([-1 0.5]);
    set(gca,'TickDir','out');
    set(gca,'FontName','Arial','FontSize',font_axis,'FontWeight','Normal');
    set(gcf,'color','w');
end

%% Plot the waveforms with mean and sem
lw = 4;
transparent = 0;
wfs_exc = clust_info.raw_wfs{ix_exc}(:,ix_perm_exc);
max_exc = max(abs(wfs_exc(:)));
wfs_exc = wfs_exc/max_exc;
wfs_inh = clust_info.raw_wfs{ix_inh}(:,ix_perm_inh);
max_inh = max(abs(wfs_inh(:)));
wfs_inh = wfs_inh/max_inh;

mean_exc = mean(wfs_exc,2);
% max_exc = max(abs(mean_exc));
% mean_exc = mean_exc/max_exc;
mean_inh = mean(wfs_inh,2);
% max_inh = max(abs(mean_inh));
% mean_inh = mean_inh/max_inh;
std_exc = std(wfs_exc,[],2);
std_inh = std(wfs_inh,[],2);
figure('units','normalized','outerposition',[0 0 1 1]);  hold on;
shadedErrorBar(t_ms, mean_inh, std_inh, {'LineWidth',lw,'Color', 'b'}, transparent);
shadedErrorBar(t_ms, mean_exc, std_exc, {'LineWidth',lw,'Color', 'r'}, transparent);
xlabel('Time from trough (ms)');
ylabel('Normalized amplitude');
hold off;
xlim([-0.4, 0.9]);
ylim([-0.7 0.4]);
set(gca,'TickDir','out');
set(gca,'FontName','Arial','FontSize',font_axis,'FontWeight','Normal');
set(gcf,'color','w');
save_name = fullfile(save_dir, 'Mean_std_wfs.svg');
saveas(gcf, save_name);
% close all;