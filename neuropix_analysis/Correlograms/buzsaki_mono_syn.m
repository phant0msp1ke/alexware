function labels = buzsaki_mono_syn(spikeTimes, clust_id_list)
%function buzsaki_mono_syn(spikeTimes, clust_id_list)
%This function is a wrapper function for the Buzsaki code function
%bz_MonoSynConvClick which computes cross-correlograms and uses them to
%estimate excitatory/inhibitory monosynaptic interactions
%>>INPUT>>
%   spikeTimes - This var is the direct output from Kilosort2 
%       1st col - contains the time stapmps of all spikes. 2nd
%       2nd col - contains the clust_id for every spike
%   clust_id_list - list of all the clusters I want to use for CCG
%                   computation
%<<OUTPUT<<
%   ???

%% Define params
binSize_ms = 0.5;
duration_ms = 200;
window_ms = 10;
alpha = 0.001;
plot_check = false;

binSize_s = binSize_ms/1000;
duration_s = duration_ms/1000;
window_s = window_ms/1000;
conv_w = window_s/binSize_s; 

%% Allocate data
spike_times = spikeTimes(:,1); % Spike time stamps in s
clust_id = spikeTimes(:,2); % clust_id for every spike
ix = ismember(clust_id,clust_id_list); % get the index of only the clusters I need
clust_id = clust_id(ix); % remove the ones I don't need
spike_times = spike_times(ix); %remove also their corresponding spikes 
n_clust = length(clust_id_list);

%% Prepare data

% Convert cluster_id to indices from 1...n_clust
clust_ix = clust_id; %Make a copy
for c = 1:n_clust
    curr_id = clust_id_list(c);
    clust_ix(clust_id==curr_id,1) = c;
end

n_spikes = length(spike_times);
shank_id = ones(n_spikes,1); % Create shankID because function won't run w/o
    
spikeIDs = [shank_id, clust_id, clust_ix];

%% Run Buzsaki CCG code
fprintf('== Computing Connections ==\n');tic;
mono_syn_res = bz_MonoSynConvClick(spikeIDs, spike_times, 'binsize', binSize_s, 'duration', duration_s, 'conv_w', conv_w, 'alpha', alpha, 'plot', plot_check);
fprintf('== Done! This took %.1fs ==\n',toc);

%% Assign labels based on the correlograms and save some other info
labels =  buzsaki_assign_labels(mono_syn_res);
