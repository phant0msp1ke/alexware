function labels =  buzsaki_assign_labels(rez)
%function labels =  buzsaki_assign_labels(rez)
%This function is a helper function to assign labels based on the output of
%the buzsaki_mono_syn function.
%>>INPUT>>
%   mono_syn_res - This var is the direct output from Kilosort2
%<<OUTPUT<<
%   labels - this will contain cluster_id and E/I labels
%       col1 - clust_id labels
%       col2 - E/I labels based on the buzsaki_mono_syn function

%Get some basic variables
labels = [];
if ~isempty(rez.sig_con)
    ref_ix_all = [rez.sig_con(:,1); rez.sig_con(:,2)]; %Get all cluster indices of clusters which cause some ineraction - ref clusters
    rec_ix_all = [rez.sig_con(:,2); rez.sig_con(:,1)]; %Get all cluster indices of clusters which receive some ineraction - rec clusters
    ref_ix = unique(ref_ix_all); %Get the unique contributions
    n_sig_clusters = length(ref_ix); %Get number of unique clusters
    
    bin_size_s = rez.binSize; % The bin size in s for the CCGs
    anal_win_s = 0.0045; % The analysis window length in s (one-sided)
    halfBins = round(rez.duration/bin_size_s/2);
    t = (-halfBins:halfBins)'*(bin_size_s*1000); %Get a vector of time points
    
    %Get CCG window mask
    window_right = false(size(rez.ccgR,1),1);
    zero_bin_ix = ceil(length(window_right)/2);
    anal_win_ix = round(anal_win_s/bin_size_s);
    r_bin_ix = zero_bin_ix + anal_win_ix; %The index of the right window i.e. +ve time shifts
    l_bin_ix = zero_bin_ix - anal_win_ix; %The index of the left window i.e. -ve time shifts
    window_right(zero_bin_ix+1:r_bin_ix) = true;
    
    
    
    for r = 1:n_sig_clusters
        
        cur_ref_ix = ref_ix(r); %Get the index of the current ref cluster
        cur_ref_clust_id = rez.completeIndex(cur_ref_ix,2); %Get the cluster id for this cluster
        partners_ix = rec_ix_all(ref_ix_all==cur_ref_ix); %Get the indices of rec neurons affected by ref neuron
        n_partners = length(partners_ix); %Find the number of neurons affected by ref neuron
        ei_id_ind = [];
        
        for p = 1:n_partners
            cur_par_ix = partners_ix(p);
            cur_par_clust_id = rez.completeIndex(cur_par_ix,2); %Get the cluster id for this cluster
            %Get the ccgR
            ccgR = rez.ccgR(:, cur_ref_ix, cur_par_ix);
            %Get the excitatory bins
            exc = ccgR;
            exc(exc<rez.Bounds(:, cur_ref_ix, cur_par_ix, 1) | ~window_right) = 0;
            exc_bin_count = sum(exc~=0);
            
            %Get the inhibitory bins
            inh = ccgR;
            inh(inh>rez.Bounds(:, cur_ref_ix, cur_par_ix, 2) | ~window_right) = 0;
            inh_bin_count = sum(inh~=0);
            
            if exc_bin_count>0 && inh_bin_count==0
                label = 1;
                
            elseif exc_bin_count==0 && inh_bin_count>0
                label = 2;
                
            elseif exc_bin_count>0 && inh_bin_count>0
                label = 3;
                
            else
                label = 0;
                
            end
            ei_id_ind(p) = label;
            labels(r).ref_clust_id = cur_ref_clust_id;
            labels(r).par_clust_id(p) = cur_par_clust_id;
            labels(r).ei_id_ind = ei_id_ind;
            labels(r).exc{p} = exc;
            labels(r).inh{p} = inh;
            labels(r).ccgR{p} = ccgR;
            labels(r).up_bounds{p} = rez.Bounds(:, cur_ref_ix, cur_par_ix, 1);
            labels(r).lower_bounds{p} = rez.Bounds(:, cur_ref_ix, cur_par_ix, 2);
            labels(r).pred{p} = rez.Pred(:, cur_ref_ix, cur_par_ix);
            
            
        end
        
        no_exc = sum(ei_id_ind == 1); no_inh = sum(ei_id_ind == 2);
        
        if no_exc>no_inh
            label_global = 1;
            
        elseif no_inh>no_exc
            label_global = 2;
            
        elseif no_exc==no_inh && no_exc~=0
            label_global = 3;
            
        else
            label_global = 0;
            
        end
        labels(r).ei_id_global = label_global;
        
    end
    
    %Remove the clusters which are labeled with 0 globally
    
    labels(reach(labels,'ei_id_global')==0) = [];
    
    labels(1).t = t;
    labels(1).bin_size_s = rez.binSize;
    labels(1).duration_s = rez.duration;
    labels(1).conv_w_ms = rez.conv_w;
    
end