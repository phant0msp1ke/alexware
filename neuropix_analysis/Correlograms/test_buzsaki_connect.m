%Define params
data_path = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/For_analysis/sahani_done/Ronnie/P05/P05-alex_reverb_wtih_noise_same/clust_info.mat';
binSize_ms = 1;
duration_ms = 100;
window_ms = 10;
alpha = 0.001;

binSize_s = binSize_ms/1000;
duration_s = duration_ms/1000;
window_s = window_ms/1000;
conv_w = window_s/binSize_s; 

%Load the data
load(data_path);
spike_times = clust_info.spikeTimes(:,1);
spike_times_ms = spike_times; %Convert to ms
clust_id = clust_info.spikeTimes(:,2);
clust_id_list = unique(clust_id);
n_clust = 10;

%Convert cluster_id to indices from 1...n_clust
clust_id_ix = clust_id; %Make a copy
for j = 1:n_clust
    curr_id = clust_id_list(j);
    clust_id_ix(clust_id==curr_id,1) = j;
end

spiketimes = [];
unit_id = [];
clu_id = [];
for j = 1:n_clust
    ix = clust_id_ix == j;
    spiketimes = [spiketimes; spike_times_ms(ix,1)];
    unit_id = [unit_id; clust_id_ix(ix)];
    clu_id = [clu_id; clust_id(ix)];
end

% [ccgR1,tR] = CCG(spiketimes,unit_id,'binSize',binSize_s,'duration',duration_s);

n_spikes = length(spiketimes);
shank_id = ones(n_spikes,1);
    
spikeIDs = [shank_id, clu_id, unit_id];
%Run Buzsaki CCG code
fprintf('== Computing Connections ==\n');tic;
% mono_res = bz_MonoSynConvClick(spikeIDs, spiketimes, 'binsize', binSize_s,'duration', duration_s, 'alpha', p_val);
mono_res = bz_MonoSynConvClick(spikeIDs, spiketimes, 'binsize', binSize_s, 'duration', duration_s, 'conv_w', conv_w, 'alpha', alpha, 'plot', true);
fprintf('== Done! This took %.1fs ==\n',toc);
