function plot_ccgs(labels, save_dir, x_lim_val)
%Define params

if ~exist('x_lim_val','var') || isempty(x_lim_val)
    x_lim_val = 20;
end

lw = 7;
font_type = 'Liberation Sans';
axis_sz = 70;
%Get vars out
n_ref = length(labels);
t = labels(1).t;

%% Plot CCGs

for k = 1:n_ref
    n_par = length(labels(k).ei_id_ind);
    ref_id = labels(k).ref_clust_id;
    switch labels(k).ei_id_global
        case 1
            global_lbl = 'exc';
        case 2
            global_lbl = 'inh';
    end
    
    for p = 1:n_par
        par_id = labels(k).par_clust_id(p);
        figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type); 
        % Plot the CCG
        bar(t, labels(k).ccgR{p},'k','BarWidth',1); hold on;
        % Plot the excitatory and inhibitory bins
        bar(t, labels(k).exc{p},'r','BarWidth',1); 
        bar(t, labels(k).inh{p},'FaceColor',[0.0745 0.6235 1.0000],'BarWidth',1);
        % Plot the Upper and Lower bounds
        plot(t,labels(k).up_bounds{p},'g--', 'LineWidth', lw);
        plot(t,labels(k).lower_bounds{p},'g--', 'LineWidth', lw);
        plot(t,labels(k).pred{p},'g', 'LineWidth', lw);
        %Plot ref-line through 0
        xline(0,'k','LineWidth', lw);
        
        set(gcf,'color','w');
        set(gca,'TickDir','out');
        set(gca,'FontSize',axis_sz,'FontWeight','Normal');
        xlim([-x_lim_val x_lim_val]);
        xticks([-20:5:20]);
        hold off;
        switch labels(k).ei_id_ind(p)
            case 1
                ind_lbl = 'exc';
            case 2
                ind_lbl = 'inh';
            case 3
                ind_lbl = 'both';
            case 0
                ind_lbl = 'none';
        end
        save_name = fullfile(save_dir,strjoin({num2str(ref_id), num2str(par_id), global_lbl, ind_lbl, '.svg'},'_'));
        saveas(gcf, save_name);
        close;
    end
end