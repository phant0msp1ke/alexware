%Define params
data_path = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/For_analysis/sahani_done/Ronnie/P05/P05-alex_reverb_wtih_noise_same/clust_info.mat';
binSize_ms = 1;
duration_ms = 100;
norm = 'rate';

%Load the data
load(data_path);
spike_times = clust_info.spikeTimes(:,1);
clust_id = clust_info.spikeTimes(:,2);
clust_id_list = unique(clust_id);
n_clust = length(clust_id_list);

%Convert cluster_id to indices from 1...n_clust
clust_id_ix = clust_id; %Make a copy
for j = 1:n_clust
    curr_id = clust_id_list(j);
    clust_id_ix(clust_id==curr_id,1) = j;
end


%Run Buzsaki CCG code
binSize_s = binSize_ms/1000;
duration_s = duration_ms/1000;
fprintf('== Computing CCGs ==\n');tic;
[ccgR1,tR] = CCG(spike_times,clust_id_ix,'binSize',binSize_s,'duration',duration_s);
fprintf('== Done! This took %.1fs ==\n',toc);

figure;
count = 0;
for i = 1:5
    for j = 1:5
        count = count+1;
        subplot(5,5,count);
        bar(tR,squeeze(ccgR1(:,i,j)));
        xlabel('Time (ms)');
        ylabel('Spike rate (Hz)');
    end
end