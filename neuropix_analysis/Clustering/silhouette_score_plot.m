function Rez = silhouette_score_plot(clust_info, features, save_dir)
%function silhouette_score(X)
%This function does k-means clustering with different number of clusters
%and performs silhouette scoring for the results. It returns the results in
%a struct

font_axis = 55;
font_sz_lbl = 60;
count = 0;
for f = 1:length(features)
    
    count = count + 1;
    
    switch features{f}(1)
        case 1
            x_feature = clust_info.results.spike_width_ms;
            x_lab = ('Spike width [ms]');
        case 2
            x_feature = clust_info.results.width_hh_trough_ms;
            x_lab = ('Width at ^{1}/_{2}height trough [ms]');
        case 3
            x_feature = clust_info.results.end_slope;
            x_lab = ('End slope [^{uV}/_{ms}]');
        case 4
            x_feature = clust_info.results.peak_trough_ratio;
            x_lab = ('^{Peak}/_{Trough} [AU]');
        case 5
            x_feature = clust_info.results.width_hh_peak_ms;
            x_lab = ('Width at ^{1}/_{2}height peak [ms]');
    end
    
    switch features{f}(2)
        case 1
            y_feature = clust_info.results.spike_width_ms;
            y_lab = ('Spike width [ms]');
        case 2
            y_feature = clust_info.results.width_hh_trough_ms;
            y_lab = ('Width at ^{1}/_{2}height trough [ms]');
        case 3
            y_feature = clust_info.results.end_slope;
            y_lab = ('End slope [^{uV}/_{ms}]');
        case 4
            y_feature = clust_info.results.peak_trough_ratio;
            y_lab = ('^{Peak}/_{Trough} [AU]');
        case 5
            y_feature = clust_info.results.width_hh_peak_ms;
            y_lab = ('Width at ^{1}/_{2}height peak [ms]');
    end
    
    %% First standardise the data before clustering
    X = [x_feature,y_feature];
    mean_X = mean(X);
    std_X = std(X);
    X_norm = (X - mean_X)./std_X;
    
    %% Try different numbers of clusters
    
    %Cluster using k-means
    [Rez(count).idx, ~] = kmeans(X_norm, 2, 'Replicates', 100, 'Distance', 'sqeuclidean');
    
    [~,ix_min_width] = min(X(:,1));
    inh_flag = Rez(count).idx(ix_min_width);
    idx_copy = Rez(count).idx; %Here we want to assign ix to exc and inh and always the same
    Rez(count).idx(idx_copy~=inh_flag) = 1; %These are excitatory
    Rez(count).idx(idx_copy==inh_flag) = 2; %These are inhibitory
    
    %Compute the Silhouette score
    figure('units','normalized','outerposition',[0 0 1 1]);
    [Rez(count).silh, h] = silhouette(X_norm, Rez(count).idx, 'sqeuclidean');
    ytickLabels{1} = 'EXC';
    ytickLabels{2} = 'INH';
    yticklabels(ytickLabels);
    
    %Save the mean score
    Rez(count).mean_silh = mean(Rez(count).silh);
    
    %Save the features
    Rez(count).features = features{f};
    
    xlim([-1 1]);
    xline(Rez(count).mean_silh,'--',{['Mean=',num2str(Rez(count).mean_silh,'%.2f')]},'LabelHorizontalAlignment','left','LabelVerticalAlignment','bottom','LineWidth',4,'Color', 'r', 'FontSize', font_sz_lbl);
    set(gcf,'color','w');
    set(gca,'TickDir','out');
    set(gca,'FontName','Arial','FontSize',font_axis,'FontWeight','Normal');
    save_name = fullfile(save_dir,['silhouette_features_', num2str(features{f}),'.svg']);
    saveas(gcf, save_name);
    close;
    
end