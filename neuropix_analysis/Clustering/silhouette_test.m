
function Rez = silhouette_test(clust_info, features, save_dir)
%function silhouette_score(X)
%This function does k-means clustering with different number of clusters
%and performs silhouette scoring for the results. It returns the results in
%a struct

font_axis = 50;
font_sz_lbl = 30;
count = 0;
for f = 1:length(features)
    
    count = count + 1;
    
    switch features{f}(1)
        case 1
            x_feature = clust_info.results.spike_width_ms;
            x_lab = ('Spike width [ms]');
        case 2
            x_feature = clust_info.results.width_hh_trough_ms;
            x_lab = ('Width at ^{1}/_{2}height trough [ms]');
        case 3
            x_feature = clust_info.results.end_slope;
            x_lab = ('End slope [^{uV}/_{ms}]');
        case 4
            x_feature = clust_info.results.peak_trough_ratio;
            x_lab = ('^{Peak}/_{Trough} [AU]');
        case 5
            x_feature = clust_info.results.width_hh_peak_ms;
            x_lab = ('Width at ^{1}/_{2}height peak [ms]');
    end
    
    switch features{f}(2)
        case 1
            y_feature = clust_info.results.spike_width_ms;
            y_lab = ('Spike width [ms]');
        case 2
            y_feature = clust_info.results.width_hh_trough_ms;
            y_lab = ('Width at ^{1}/_{2}height trough [ms]');
        case 3
            y_feature = clust_info.results.end_slope;
            y_lab = ('End slope [^{uV}/_{ms}]');
        case 4
            y_feature = clust_info.results.peak_trough_ratio;
            y_lab = ('^{Peak}/_{Trough} [AU]');
        case 5
            y_feature = clust_info.results.width_hh_peak_ms;
            y_lab = ('Width at ^{1}/_{2}height peak [ms]');
    end
    
    %% First standardise the data before clustering
    X = [x_feature,y_feature];
    mean_X = mean(X);
    std_X = std(X);
    X_norm = (X - mean_X)./std_X;
    
    %% Try different numbers of clusters
    
    %Cluster using k-means
    [Rez(count).idx, ~] = kmeans(X_norm, 2, 'Replicates', 100, 'Distance', 'sqeuclidean');
    
    [~,ix_min_width] = min(X(:,1));
    inh_flag = Rez(count).idx(ix_min_width);
    idx_copy = Rez(count).idx; %Here we want to assign ix to exc and inh and always the same
    Rez(count).idx(idx_copy~=inh_flag) = 1; %These are excitatory
    Rez(count).idx(idx_copy==inh_flag) = 2; %These are inhibitory
    
    [Rez(count).silh, ~] = silhouette(X_norm, Rez(count).idx, 'sqeuclidean');
    close;
    
    %Save the mean score
    Rez(count).mean_silh = mean(Rez(count).silh);
    
    %Save the features
    Rez(count).features = features{f};
    
    figure('units','normalized','outerposition',[0 0 1 1]);
    
    N = size(X,1);
    
    %# cluster and compute silhouette score
    K = 2;
    
    %# plot
    [~,ord] = sortrows([Rez(count).idx Rez(count).silh],[1 -2]);
    indices = accumarray(Rez(count).idx, 1:N, [K 1], @(x){sort(x)});
    ytick(1) = length(indices{1})/2;
    ytick(2) = length(indices{1}) + length(indices{2})/2;
    %     ytickLabels = num2str((1:K)','%d');
    ytickLabels{1} = 'EXC';
    ytickLabels{2} = 'INH';
    
    h = barh(1:N, Rez(count).silh(ord),'hist');
    set(h, 'EdgeColor','none', 'CData', Rez(count).idx(ord))
    set(gca, 'CLim',[1 K], 'CLimMode','manual')
    set(gca, 'YDir','reverse', 'YTick',ytick, 'YTickLabel',ytickLabels)
    xlabel('Silhouette Value'), ylabel('Cluster');
    xlim([-1 1]);
    xline(Rez(count).mean_silh,'--',{['Mean=',num2str(Rez(count).mean_silh,'%.2f')]},'LabelHorizontalAlignment','left','LabelVerticalAlignment','bottom','LineWidth',4,'Color', 'k', 'FontSize', font_sz_lbl);
    set(gcf,'color','w');
    set(gca,'TickDir','out');
    set(gca,'FontName','Arial','FontSize',font_axis,'FontWeight','Normal');
    save_name = fullfile(save_dir,['silhouette_features_', num2str(features{f}),'.svg']);
    saveas(gcf, save_name);
    close;

end