function Rez = silhouette_score(clust_info, features, n_clust)
%function silhouette_score(X)
%This function does k-means clustering with different number of clusters
%and performs silhouette scoring for the results. It returns the results in
%a struct

count = 0;
figure;
for f = 1:length(features)
    
    switch features{f}(1)
        case 1
            x_feature = clust_info.results.spike_width_ms;
            x_lab = ('Spike width [ms]');
        case 2
            x_feature = clust_info.results.width_hh_trough_ms;
            x_lab = ('Width at ^{1}/_{2}height trough [ms]');
        case 3
            x_feature = clust_info.results.end_slope;
            x_lab = ('End slope [^{uV}/_{ms}]');
        case 4
            x_feature = clust_info.results.peak_trough_ratio;
            x_lab = ('^{Peak}/_{Trough} [AU]');
        case 5
            x_feature = clust_info.results.width_hh_peak_ms;
            x_lab = ('Width at ^{1}/_{2}height peak [ms]');
    end
    
    switch features{f}(2)
        case 1
            y_feature = clust_info.results.spike_width_ms;
            y_lab = ('Spike width [ms]');
        case 2
            y_feature = clust_info.results.width_hh_trough_ms;
            y_lab = ('Width at ^{1}/_{2}height trough [ms]');
        case 3
            y_feature = clust_info.results.end_slope;
            y_lab = ('End slope [^{uV}/_{ms}]');
        case 4
            y_feature = clust_info.results.peak_trough_ratio;
            y_lab = ('^{Peak}/_{Trough} [AU]');
        case 5
            y_feature = clust_info.results.width_hh_peak_ms;
            y_lab = ('Width at ^{1}/_{2}height peak [ms]');
    end
    
    %% First standardise the data before clustering
    X = [x_feature,y_feature];
    mean_X = mean(X);
    std_X = std(X);
    X_norm = (X - mean_X)./std_X;
    
    %% Try different numbers of clusters
    
    for k = 2:n_clust
        count = count + 1;
        %Cluster using k-means
        subplot(3,4,count);
        [Rez(count).idx, Rez(count).C_norm] = kmeans(X_norm, k, 'Replicates', 100, 'Distance', 'sqeuclidean');
        
        if ismember(count, [9:12])
        xlabel('Silhouette Value');
        end
        
        if ismember(count, [1,5,9])
            ylabel('Cluster');
        end
        Rez(count).n_clust = k;
        
        %Compute the Silhouette score
        [Rez(count).silh, ~] = silhouette(X_norm, Rez(count).idx, 'sqeuclidean');
        %Save the mean score
        Rez(count).mean_silh = mean(Rez(count).silh);
        %Save the features
        Rez(count).features = features{f};
    end
    
end