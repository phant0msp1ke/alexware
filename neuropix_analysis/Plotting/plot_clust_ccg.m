function plot_clust_ccg(clust_info, norm)

%Get vars out
if ~exist('norm', 'var') || isempty(norm)
    norm = 'none';
end

switch norm
    case 'none'
        X = clust_info.clustering.X;
        C = clust_info.clustering.C;
        x_lab = clust_info.clustering.x_lab;
        y_lab = clust_info.clustering.y_lab;
        
    case 'zscore'
        X = clust_info.clustering.X_norm;
        C = clust_info.clustering.C_norm;
        x_lab = clust_info.clustering.x_lab_norm;
        y_lab = clust_info.clustering.y_lab_norm;
end

kmeans_idx = clust_info.clustering.idx(:,2); %Get the ei label from k-means
ccg_idx = clust_info.clustering.idx(:,3); %Get the ei label from CCGs
inh_flag = clust_info.clustering.inh_flag;
x_lab_norm = clust_info.clustering.x_lab_norm;
y_lab_norm = clust_info.clustering.y_lab_norm;
font_axis = 50;
exc_kmeans_color = [0.9294    0.5843    0.7020];
exc_ccg_color = [0.8 0 0];
inh_kmeans_color = [0.5176    0.6902    0.8902];
inh_ccg_color = 'b';
%% Plot data (Z-scored)
figure('units','normalized','outerposition',[0 0 1 1]);
%Plot neuron ei identity according to kmeans clustering
plot(X(kmeans_idx~=inh_flag,1), X(kmeans_idx~=inh_flag,2),'^','MarkerSize',8, 'MarkerFaceColor', exc_kmeans_color, 'MarkerEdgeColor', exc_kmeans_color);
hold on;
plot(X(kmeans_idx==inh_flag,1), X(kmeans_idx==inh_flag,2),'.','MarkerSize',25, 'MarkerFaceColor', inh_kmeans_color, 'MarkerEdgeColor', inh_kmeans_color)
%Plot neuron ei identity according to CCGs
plot(X(ccg_idx~=inh_flag & ccg_idx~=0, 1), X(ccg_idx~=inh_flag & ccg_idx~=0, 2),'^','MarkerSize',12, 'MarkerFaceColor', exc_ccg_color, 'MarkerEdgeColor', exc_ccg_color);
plot(X(ccg_idx==inh_flag,1), X(ccg_idx==inh_flag,2),'o','MarkerSize',9, 'MarkerFaceColor', inh_ccg_color, 'MarkerEdgeColor', inh_ccg_color);
%This is to plot specific clusters for the Figure in the paper
% plot(X(181, 1), X(181, 2),'^','MarkerSize',10, 'MarkerFaceColor', 'k', 'MarkerEdgeColor', 'k'); %Alex tempp!!!!
% plot(X(190, 1), X(190, 2),'o','MarkerSize',10, 'MarkerFaceColor', 'k', 'MarkerEdgeColor', 'k'); %Alex tempp!!!!
%Plot centroids
% plot(C_norm(:,1),C_norm(:,2),'kx',...
%     'MarkerSize',15,'LineWidth',3)
% legend('kmeans excitatory','kmeans inhibitory','CCG excitatory','CCG inhibitory','Centroids',...
%     'Location','NE');
% title('Inhibitory/Excitatory cluster assignment and centroids [Normalized]');
% xlabel(x_lab);
% ylabel(y_lab);
set(gca,'TickDir','out');
set(gca,'FontName','Arial','FontSize',font_axis,'FontWeight','Normal');
axis equal;
xlim([0 1.1]);
ylim([0 1.15]);
hold off;
set(gcf,'color','w');