%% Enter all the peentrations to be processed
extract = 0;
extract_folders = 0;
plot_hist = 0;
features = [1 2];
all_pen_dir = '/media/alex/5FC39EAD5A6AA312/EI_tonotopy/Data/All_pens';

if extract_folders
    input_dir = '/media/alex/5FC39EAD5A6AA312/EI_tonotopy/Data';
    file_type = 'ap.bin';
    get_folders(input_dir, file_type, all_pen_dir)
end

load(fullfile(all_pen_dir,'folder_list.mat'));
pen = folder_list;

%% Extract the waveforms, cluster, plot histograms and scatter plots
files_error = cell(0);
count = 0;
if extract
    for p = 1:length(pen)
        fprintf('== Processing pen %0.f/%0.f ==\n',p,length(pen));
        try
            [clust_info] = process_wfs(pen{p},features);
        catch
            count = count+1;
            files_error{count} = pen{p};
        end
        if plot_hist
            plot_histograms(pen{p});
            plot_corr(pen{p});
        end
    end
end
%% Combine the penetrations together
fprintf('== Getting the data ==\n');tic;
clust_info_temp.results = struct([]);
clust_info_temp.animal_name = cell(1);
clust_info_temp.pen_name = cell(1);

ccg_label = [];
for p = 1:length(pen)
    %Get the animal and pen name
    [temp,~] = fileparts(pen{p});
    [temp,pen_name] = fileparts(temp);
    [temp,animal_name] = fileparts(temp);
    
    fprintf('== Loading pen %0.f/%0.f ==\n',p,length(pen));
    load(fullfile(pen{p},'clust_info'));
    n_clust = length(clust_info.results.normal_cluster_id);
    ccg_label = [ccg_label; clust_info.clustering.idx(:,3)];
    clust_info_temp.animal_name(end+1:end+n_clust,1) = {animal_name};
    clust_info_temp.pen_name(end+1:end+n_clust,1) = {pen_name};
    clust_info_temp.results = [clust_info_temp.results;clust_info.results];
    
end

clust_info_temp.animal_name(1) = [];
clust_info_temp.pen_name(1) = [];

clear clust_info;
names = fieldnames(clust_info_temp.results);

for n = 1:length(names)
    clust_info.results.(names{n}) = reach(clust_info_temp.results,names{n});
end

clust_info.cluster_id = clust_info.results.normal_cluster_id;
clust_info.animal_name = clust_info_temp.animal_name;
clust_info.pen_name = clust_info_temp.pen_name;

%% Cluster and record metrics for all of the pens together
plot_on = 0;

file_path = fullfile(all_pen_dir,'clust_info.mat');
% load(file_path);
slash_ix1 = find(file_path == '/', 1, 'last');
var_name = file_path(slash_ix1+1:end);
var_name = strrep(var_name,'_',' ');

[clust_info] = run_kmeans(clust_info,features,plot_on);
clust_info.clustering.idx(:,3) = ccg_label;
clust_info.kmeans_label = clust_info.clustering.idx(:,2);
clust_info.ccg_label = clust_info.clustering.idx(:,3);
clust_info.inh_flag = clust_info.clustering.inh_flag;

save_dir = fullfile(all_pen_dir,['/Plots/Clustering/','feature_',num2str(features(1)),'_',num2str(features(2))]);
if ~exist(save_dir, 'dir')
    mkdir(save_dir);
end

%Plot the results
plot_clust_ccg(clust_info);
clust_info_name = fullfile(all_pen_dir,'clust_info.mat');
save(clust_info_name,'clust_info'); %Save the updates clust info
fig_handles = findobj('Type', 'figure');
save_name = fullfile(save_dir,[var_name,'_IE_Clustering_norm.svg']);
saveas(fig_handles(1),save_name);
close all;

%% Plot histograms and scatter plots for all pens together
if plot_hist
    plot_histograms(all_pen_dir);
    plot_corr(all_pen_dir);
end
