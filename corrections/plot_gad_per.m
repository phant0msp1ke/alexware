%This script will plot the percentage of GAD neurons, which also have the
%mDlx enhancer
clear all; close all;

%% Input the data

save_dir = '/mnt/40086D4C086D41D0/Thesis_corrections';
gad_per.Bender = [92.31, 89.66, 89.19, 87.5, 94.29]; %The proportion of GAD neurons that had mDlx labelling in Areas 1, 2, 5, 6, 9  
gad_per.Leela = [90, 86.11, 90.63]; %The proportion of GAD neurons that had mDlx labelling in Areas 1, 2, 4

%% Plot the data

%Get the mean and sem
data.mean = structfun(@mean,gad_per);
data.sem = structfun(@(x) std(x)/sqrt(length(x)),gad_per);
errhigh = data.sem;
errlow  = data.sem;

%Plot the data comparing EXC and all GAD only
X{1} = categorical({'Animal 1'});
X{2} = categorical({'Animal 2'});
X_final = categorical({'Animal 1', 'Animal 2'});
X_final = reordercats(X_final,{'Animal 1', 'Animal 2'});
font_sz = 40;
colors = {'r', 'b'};
ylim_vals = [70 100];

figure('units','normalized','outerposition',[0 0 1 1]);


hold on;

for i = 1:2
    hb(i) = bar(X{i}, data.mean(i));
    hb(i).FaceColor = colors{i};
end

er = errorbar(X_final, data.mean, errlow, errhigh);
er.Color = [0 0 0];
er.LineStyle = 'none';

set(gcf,'color','w');
set(gca,'TickDir','out'); 
% ylabel('% GAD neurons expressing mDlx');
ylim(ylim_vals);
set(gca,'FontName','Arial','FontSize',font_sz,'FontWeight','Normal');
save_name = fullfile(save_dir,['GAD_mDlx_percentage','.svg']);
saveas(gcf, save_name);
close;