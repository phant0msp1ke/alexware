%% Neuronal data
model = 'ridge';
normz = 'perfreq_noneuro';
kfold = 1;
dt_ms = 10;
h_max_ms = 200;
n_cores = 20;
save_dir = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/Kernel_fits/kfolds/LNP_model_sim_noneuron_norm_anech';

make_lnp_model_anech(model, h_max_ms, dt_ms, normz, save_dir, kfold, n_cores);


