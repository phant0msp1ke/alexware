%% Neuronal data

h_max_ms = 200;
dt_ms = 10;
normz = 'perfreq_noneuro';
save_dir = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/Kernel_fits/kfolds/NRF_model_sim_noneuron_norm';
n_cores = 20;

params_nrf.minibatch_number = 25; %The number of examples in each minibacth
params_nrf.regtype = 'sq'; % Regularisation type, 'sq' = 'ridge'/L2, 'abs' = 'lasso'/L1 
params_nrf.lam=logspace(log10(1e-8),log10(1e-3),6); %The regularisation params lambda to try
params_nrf.crossvalidation_fold = 4; %Number of cross-validation folds
params_nrf.net_str = {10,1}; %Number of hidden units in each layer, e.g. {20,1} = 1HL, 20 HUs, 1 output unit
params_nrf.num_pass = 20; %How many times to pass through the data

make_nrf_model(h_max_ms, dt_ms, normz, save_dir, n_cores, params_nrf);