
%% Model 1
model = 'lasso';
normz = 'perfreq_noneuro';
kfold = 1;
dt_ms = 10;
h_max_ms = 300;
n_cores = 20;
save_dir = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/Reconstruction/Reconstruction_kernels';

make_decoding_models(model, h_max_ms, dt_ms, normz, save_dir, kfold, n_cores);
