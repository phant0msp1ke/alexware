%% Define params
clc; clear all; close all;
%Define params and paths
cluster_dir = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/For_analysis/All_data/';
save_dir = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/Reconstruction/PSTHs/';

%This selects the neurons to be used for reconstruction
    %Options are:
        %from_anech - the anechoic responses are used to generate the rec
            %kernels. This will exclude animals Derekah and Noah as they
            %had no anechoic condition
        %from_both - this will use both cochleagrams to make a single rec
            %filter for both small and large conditions
        %from_sep - this will make a different reconstruction filter for
            %each of the small and large conditions

rec_type = 'from_anech';
exclude_animals = ["Noah","Derekah"];

% coch_file = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/Cochleagrams/Cochleagrams/New_th/Ronnie_PLP/specpower/10ms/coch_all_conditions_specpower.mat';
NPSP_th = 40;
n_stim = 6;
n_rep = 10;
dt_ms = 10;
t_end_s = 36;

%% Select the clusters to use
load(fullfile(cluster_dir,'info.mat'),'info'); %Load the info file with the meta data

if strcmp(rec_type,'from_anech')
    ix = info.NPSP<NPSP_th & ~ismember(info.animal_name,exclude_animals); %Find only the neurons that had the anechoic condition and are below the NPSP th
else
    ix = info.NPSP<NPSP_th; %Find only the neurons that are below the NPSP th
end

%Get the necessary fields
cluster_ids = info.cluster_id(ix);
pen_names = info.pen_name(ix);
animal_names = info.animal_name(ix);
qualities = info.quality(ix);
NPSPs = info.NPSP(ix);
n_clusters = sum(ix);



%Load the cochleagram
% coch = load(coch_file,'coch');
% t_edges_s = coch.coch(1).t;

%Get the PSTHs for the stimuli and compute FF and CC
dt_s = dt_ms/1000;
t_edges_s = [0:dt_s:t_end_s];

fprintf('== Generating PSTHs ==\n');
tic;

for j = 1:n_clusters
    
    temp = load(fullfile(cluster_dir,strjoin({animal_names{j},pen_names{j},num2str(cluster_ids(j))},'_')),'data');
    y = temp.data;
    
    for s = 1:n_stim
        psth_temp = [];
        for r = 1:n_rep
            psth_temp(r,:) = histc(y.stim(s).repeat(r).spiketimes,t_edges_s);
        end
        y_temp(s,:) = mean(psth_temp);
    end
    
    y_t_anech1 = y_temp(1,:);
    y_t_anech2 = y_temp(2,:);
    y_t_small1 = y_temp(3,:);
    y_t_small2 = y_temp(4,:);
    y_t_large1 = y_temp(5,:);  
    y_t_large2 = y_temp(6,:);
    
    Y_nt.anech1(j,:) = y_t_anech1;
    Y_nt.anech2(j,:) = y_t_anech2;
    Y_nt.small1(j,:) = y_t_small1;
    Y_nt.small2(j,:) = y_t_small2;
    Y_nt.large1(j,:) = y_t_large1;
    Y_nt.large2(j,:) = y_t_large2;
    
end

fprintf('== Done! This took %0.fs ==\n',toc);

%% Save the data and other important info
Y_nt.info.rec_type = rec_type;
Y_nt.info.dt_ms = dt_ms;
Y_nt.info.NPSP_th = NPSP_th;
Y_nt.info.cluster_ids = cluster_ids;
Y_nt.info.pen_names = pen_names;
Y_nt.info.animal_names = animal_names;
Y_nt.info.qualities = qualities;
Y_nt.info.NPSPs = NPSPs;
Y_nt.info.n_clusters = n_clusters;

save_dir_full = fullfile(save_dir, rec_type);
save_name = fullfile(save_dir_full,'Y_nt');

save(save_name, 'Y_nt');