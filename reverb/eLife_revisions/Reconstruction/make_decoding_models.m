function make_decoding_models(model,h_max_ms,dt_ms,normz,save_dir,kfold,n_cores)
%% Input params
%NPSP selection criteria
NPSP_th = 40;
coch_type = 'specpower';

if ~exist('model','var') || isempty(model)
    model = 'lasso';
end

if ~exist('normz','var') || isempty(normz)
    normz = 'none';
end

if ~exist('h_max_ms','var') || isempty(h_max_ms)
    h_max_ms = 50;
end

if ~exist('dt_ms','var') || isempty(dt_ms)
    dt_ms = 10;
end

if ~exist('kfold','var') || isempty(n_cores)
    kfold = true;
end

if ~exist('n_cores','var') || isempty(n_cores)
    n_cores = 20;
end

%% Define the params for all possible models
coch_file = fullfile(fullfile('/mnt/40086D4C086D41D0/Reverb_neuronal_data/Cochleagrams/Cochleagrams/New_th/Derry_Kilkenny_Cork/',coch_type),['/',num2str(dt_ms),'ms','/coch_all_conditions_',coch_type]);
psth_file = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/Reconstruction/PSTHs/from_anech/Y_nt.mat'; %The file with all the PSTHs Y_nt

%% Load the cochleagram and PSTHs
coch = load(coch_file,'coch');
load(psth_file,'Y_nt');
n_f = coch.coch(1).params.n_f;

anech1_X_ft = coch.coch(1).X_ft;
anech2_X_ft = coch.coch(2).X_ft;

anech1_Y_nt = Y_nt.anech1;
anech2_Y_nt = Y_nt.anech2;
%% Optionally, normalize the cochleagram and PSTHs

switch normz
    case 'perfreq_noneuro'
        mean_anech = mean([anech1_X_ft, anech2_X_ft],2); std_anech = std([anech1_X_ft, anech2_X_ft],[],2);
        anech1_X_ft = (anech1_X_ft - mean_anech)./std_anech; anech2_X_ft = (anech2_X_ft - mean_anech)./std_anech;
        
    case 'perfreq'
        mean_anech = mean([anech1_X_ft, anech2_X_ft],2); std_anech = std([anech1_X_ft, anech2_X_ft],[],2);
        anech1_X_ft = (anech1_X_ft - mean_anech)./std_anech; anech2_X_ft = (anech2_X_ft - mean_anech)./std_anech;
        
        mean_anech_neuro = mean([anech1_Y_nt, anech2_Y_nt],2); std_anech_neuro = std([anech1_Y_nt, anech2_Y_nt],[],2);
        anech1_Y_nt = (anech1_Y_nt - mean_anech_neuro)./std_anech_neuro; anech2_Y_nt = (anech2_Y_nt - mean_anech_neuro)./std_anech_neuro;
        
    case 'none'
end

%Combine the two stimuli together
anech_X_ft = cat(2, anech1_X_ft, anech2_X_ft);

%% Tensorize the PSTHs

dt_ms = coch.coch(1).params.dt_sec*1000; %Convert the dt used for the cochlea into ms
n_h = round(h_max_ms/dt_ms); %Find the number of history steps necessary given the bin size and max history

fprintf('== Tensorizing the PSTHs ==\n');tic;
anech1_Y_nht = tensorize(anech1_Y_nt,n_h);
anech2_Y_nht = tensorize(anech2_Y_nt,n_h);

%Combine the two stimuli together
anech_Y_fht = cat(3,anech1_Y_nht,anech2_Y_nht);
fprintf('== Done! This took %0.fs ==\n',toc);

%% Fit the model

delete(gcp('nocreate'));
parpool('local',n_cores);

sprintf('== Fitting decoding kernels ==\n');tic;
kernels = cell(n_f,1);

if kfold
    parfor f = 1:n_f
        kernels{f} = run_model_kfold_dec(anech_Y_fht, anech_X_ft(f,:), model);
    end
end

fprintf('== Done! Fitting all kernels took %0.fs ==\n',toc);

dec_kernels.kernels = kernels;
dec_kernels.info = Y_nt.info;
dec_kernels.coch_params = coch.coch(1).params;

sprintf('== Saving the results ==\n');tic;

norm_dir = fullfile(save_dir,normz);
if ~exist(norm_dir, 'dir')
    mkdir(norm_dir);
end

model_dir = fullfile(norm_dir,model);
if ~exist(model_dir, 'dir')
    mkdir(model_dir);
end

bin_dir = fullfile(model_dir,[num2str(dt_ms,'%1.0f'),'ms']);
if ~exist(bin_dir, 'dir')
    mkdir(bin_dir);
end

save_dir_full = fullfile(bin_dir,[num2str(h_max_ms,'%1.0f'),'ms']);
if ~exist(save_dir_full, 'dir')
    mkdir(save_dir_full);
end

save(fullfile(save_dir_full,'dec_kernels'),'dec_kernels','-v7.3');

fprintf('== Done! Saving took %0.fs ==\n',toc);