%% Define params
psth_file = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/Reconstruction/PSTHs/from_anech/Y_nt.mat';
rec_kernel_dir = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/Reconstruction/Reconstruction_kernels';
coch_file = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/Cochleagrams/Cochleagrams/New_th/Derry_Kilkenny_Cork/specpower/10ms/coch_all_conditions_specpower.mat';

% Select which kernels to load
% nomz = {'none','perfreq_noneuro'}
normz = 'perfreq_noneuro'; 
%model = {'ridge', 'lasso'}
model = 'lasso';
dt_ms = '10ms';
%h_max_ms = '50ms','100ms','200ms' 
h_max_ms = '200ms';

norm_pred = 0;

kernel_dir = fullfile(rec_kernel_dir,normz,model,dt_ms,h_max_ms);
kernel_file = fullfile(kernel_dir,'dec_kernels.mat');

%% Load the data
load(psth_file,'Y_nt');
load(kernel_file, 'dec_kernels');
load(coch_file);

%Get the cochleagrams
anech1_X_ft = coch(1).X_ft;
anech2_X_ft = coch(2).X_ft;
small1_X_ft = coch(3).X_ft;
small2_X_ft = coch(4).X_ft;
large1_X_ft = coch(5).X_ft;
large2_X_ft = coch(6).X_ft;

%% Normalize the cochleagrams
%Combine the two stimuli together
anech_X_ft = cat(2,anech1_X_ft, anech2_X_ft);
small_X_ft = cat(2,small1_X_ft, small2_X_ft);
large_X_ft = cat(2,large1_X_ft, large2_X_ft);

switch normz
    case 'perfreq_noneuro'
        mean_anech = mean(anech_X_ft,2); std_anech = std(anech_X_ft,[],2);
%         mean_small = mean(small_X_ft,2); std_small = std(small_X_ft,[],2);
%         mean_large = mean(large_X_ft,2); std_large = std(large_X_ft,[],2);
        
%         anech_X_ft = (anech_X_ft - mean_anech)./std_anech;
%         small_X_ft = (small_X_ft - mean_anech)./std_anech;
%         large_X_ft = (large_X_ft - mean_anech)./std_anech;
    case 'none'
        
end



%Get the kernels
n_ker = length(dec_kernels.kernels);
for k = 1:n_ker
    kernels{k} = dec_kernels.kernels{k}.main{end};
end
n_h = size(dec_kernels.kernels{1}.main{end}.k_fh,2);


anech1_Y_nt = Y_nt.anech1;
anech2_Y_nt = Y_nt.anech2;
small1_Y_nt = Y_nt.small1;
small2_Y_nt = Y_nt.small2;
large1_Y_nt = Y_nt.large1;
large2_Y_nt = Y_nt.large2;


%% Tensorize
fprintf('== Tensorizing the PSTHs ==\n');tic;

anech1_Y_nht = tensorize(anech1_Y_nt,n_h);
anech2_Y_nht = tensorize(anech2_Y_nt,n_h);
small1_Y_nht = tensorize(small1_Y_nt,n_h);
small2_Y_nht = tensorize(small2_Y_nt,n_h);
large1_Y_nht = tensorize(large1_Y_nt,n_h);
large2_Y_nht = tensorize(large2_Y_nt,n_h);

%Combine the two stimuli together
anech_Y_fht = cat(3,anech1_Y_nht, anech2_Y_nht);
small_Y_fht = cat(3,small1_Y_nht, small2_Y_nht);
large_Y_fht = cat(3,large1_Y_nht, large2_Y_nht);

n_t = size(anech_Y_fht,3);

fprintf('== Done! This took %0.fs ==\n',toc);

%% Make predictions
fprintf('== Making predicted cochleagrams ==\n');tic;
anech_anech_X_ft_hat = zeros(n_ker,n_t);
small_anech_X_ft_hat = zeros(n_ker,n_t);
large_anech_X_ft_hat = zeros(n_ker,n_t);

for k = 1:n_ker
    anech_anech_X_ft_hat(k,:) = kernelconv(anech_Y_fht, kernels{k});
    small_anech_X_ft_hat(k,:) = kernelconv(small_Y_fht, kernels{k});
    large_anech_X_ft_hat(k,:) = kernelconv(large_Y_fht, kernels{k});
end
fprintf('== Done! This took %0.fs ==\n',toc);

switch normz
    case 'perfreq_noneuro'
        anech_anech_X_ft_hat = (anech_anech_X_ft_hat.*std_anech) + mean_anech;
        small_anech_X_ft_hat = (small_anech_X_ft_hat.*std_anech) + mean_anech;
        large_anech_X_ft_hat = (large_anech_X_ft_hat.*std_anech) + mean_anech;
    case 'none'
        
end


X_ft_hat_final.anech_orig = anech_X_ft; X_ft_hat_final.small_orig = small_X_ft; X_ft_hat_final.large_orig = large_X_ft;
X_ft_hat_final.anech_decoded = anech_anech_X_ft_hat; X_ft_hat_final.small_decoded = small_anech_X_ft_hat; X_ft_hat_final.large_decoded = large_anech_X_ft_hat;

%% Plot original and predictions
room_names{1} = 'anech_orig'; room_names{2} = 'small_orig'; room_names{3} = 'large_orig';
room_names{4} = 'anech_decoded'; room_names{5} = 'small_decoded'; room_names{6} = 'large_decoded';
start_s_list = [0:2:68];
%Find the max value
max_val = 40;

for j = 1:length(start_s_list)
    start_s = start_s_list(j);
    end_s = start_s+2;
    t = coch(1).t;
    [~,ix_start] = min(abs(t - start_s));
    [~,ix_end] = min(abs(t - end_s));
    t = t(ix_start:ix_end);
    t = t - t(1);
    color_map = 'inferno';
    freqs = fliplr(coch(1).params.freqs);
    n_f = length(freqs);
    n_tlab = 8;
    skip_f = 2;
    skip_t = round(length(t)/n_tlab);
    row = 2;
    col = 3;
    per = 0.03;
    edgel = per; edger = 0.02; edgeh = 0.05; edgeb = 0.05; space_h = 0.08; space_v = 0.08;
    [pos]=subplot_pos(row,col,edgel,edger,edgeh,edgeb,space_h,space_v);
    
    for f = 1:numel(freqs)
        y_labels{f} = num2str(freqs(f)./1000,'%.1f');
    end
    
    for tm = 1:n_tlab
        x_labels{tm} = num2str(t((tm-1)*skip_t +1),'%.1f');
    end
    
    figure('units','normalized','outerposition',[0 0 1 1]);
    
    for r = 1:6
        room = room_names{r};
        subplot('position',pos{r});
        imagesc(X_ft_hat_final.(room)(:,ix_start:ix_end)-max_val);
        colorbar;
        colormap(color_map);
        %     caxis([-80 0]);
        yticks([1:skip_f:n_f]);
        yticklabels(y_labels);
        xticks([1:skip_t:length(coch(1).t)]);
        xticklabels(x_labels);
        title(room);
        set(gca,'FontName','Arial','FontSize',15,'FontWeight','Normal');
        %     xlabel('Time [s]','FontSize',14,'FontWeight','Normal');
        %     ylabel('Freqeuncy [kHz]','FontSize',14,'FontWeight','Normal');
    end
    set(gcf,'color','w');
    set(gca,'TickDir','out');
    
    save_name = fullfile(kernel_dir,['Original_vs_predcited_cochleagrams',num2str(start_s),'s_to_',num2str(end_s),'s.svg']);
    saveas(gcf,save_name);
    close all;
end
%% Compute the CC and MSE between the original cochleagram and the anechoic
kfold = 36; %How many folds to use for the testing set

%Find out how many folds need to be ran
n_t = size(X_ft_hat_final.anech_orig,2);
chunk_sz = floor(n_t/kfold);

orig_rooms = {'anech_orig','small_orig','large_orig'};
pred_rooms = {'anech_decoded','small_decoded','large_decoded'};

%Run the model k times
for k = 1:kfold
    
    start_ix = (k-1)*chunk_sz + 1;
    end_ix = start_ix + chunk_sz -1;
    val_ix = [start_ix:end_ix];
    
    c = 0;
    
    for j = 1:3
        orig_room = orig_rooms{j};
        for i = 1:3
            pred_room = pred_rooms{i};
            %Compare original anechoic and reverberant cochleagrams
            c = c+1;
            A = []; B = []; C = [];
            A = X_ft_hat_final.(orig_room)(:,val_ix); B = X_ft_hat_final.(pred_room)(:,val_ix);
            r = corrcoef(A(:), B(:));
            pred.CC(k,c) = r(1,2);
            mse = mean((A(:) - B(:)).^2);
            pred.MSE(k,c) = mse;
            pred.comp{c} = [orig_room,'_',pred_room];
            
        end
    end
    
end

pred.CC_mean = nanmean(pred.CC);
pred.MSE_mean = nanmean(pred.MSE);

%% Compute the CC and MSE between the original cochleagram and the anechoic
n_f = 30; %How many folds to use for the testing set

%Find out how many folds need to be ran

orig_rooms = {'anech_orig','small_orig','large_orig'};
pred_rooms = {'anech_decoded','small_decoded','large_decoded'};

%Run the model k times
for f = 1:n_f
    
    c = 0;
    
    for j = 1:3
        orig_room = orig_rooms{j};
        for i = 1:3
            pred_room = pred_rooms{i};
            %Compare original anechoic and reverberant cochleagrams
            c = c+1;
            A = []; B = []; C = [];
            A = X_ft_hat_final.(orig_room)(f,:); B = X_ft_hat_final.(pred_room)(f,:);
            r = corrcoef(A(:), B(:));
            pred_f.CC(f,c) = r(1,2);
            mse = mean((A(:) - B(:)).^2);
            pred_f.MSE(f,c) = mse;
            pred_f.comp{c} = [orig_room,'_',pred_room];
            
        end
    end
    
end

pred_f.CC_mean = nanmean(pred_f.CC);
pred_f.MSE_mean = nanmean(pred_f.MSE);
%% Compute the CC and MSE between the original cochleagram and the anechoic


%Compare original anechoic and predicted anech cochleagrams
% pred(1).comparisons = 'Anech-Anech Pred';
% A = X_ft_hat_final.anech_orig(:); B = X_ft_hat_final.anech_decoded(:);
% if norm_pred
%     A = (A - mean(A))./std(A);
%     B = (B - mean(B))./std(B);
% end
% r = corrcoef(A, B);
% pred(1).CC = r(1,2);
% mse = mean((A - B).^2);
% pred(1).MSE = mse;
% 
% %Compare original anechoic and predicted small cochleagrams
% pred(2).comparisons = 'Anech-Small Pred';
% A = X_ft_hat_final.anech_orig(:); B = X_ft_hat_final.small_decoded(:);
% if norm_pred
%     A = (A - mean(A))./std(A);
%     B = (B - mean(B))./std(B);
% end
% r = corrcoef(A, B);
% pred(2).CC = r(1,2);
% mse = mean((A - B).^2);
% pred(2).MSE = mse;
% 
% %Compare original anechoic and predicted large cochleagrams
% pred(3).comparisons = 'Anech-Large Pred';
% A = X_ft_hat_final.anech_orig(:); B = X_ft_hat_final.large_decoded(:);
% if norm_pred
%     A = (A - mean(A))./std(A);
%     B = (B - mean(B))./std(B);
% end
% r = corrcoef(A, B);
% pred(3).CC = r(1,2);
% mse = mean((A - B).^2);
% pred(3).MSE = mse;
% 
% %Compare original small and predicted small cochleagrams
% pred(4).comparisons = 'Small-Small Pred';
% A = X_ft_hat_final.small_orig(:); B = X_ft_hat_final.small_decoded(:);
% if norm_pred
%     A = (A - mean(A))./std(A);
%     B = (B - mean(B))./std(B);
% end
% r = corrcoef(A, B);
% pred(4).CC = r(1,2);
% mse = mean((A - B).^2);
% pred(4).MSE = mse;
% 
% %Compare original large and predicted small cochleagrams
% pred(5).comparisons = 'Large-Small Pred';
% A = X_ft_hat_final.large_orig(:); B = X_ft_hat_final.small_decoded(:);
% if norm_pred
%     A = (A - mean(A))./std(A);
%     B = (B - mean(B))./std(B);
% end
% r = corrcoef(A, B);
% pred(5).CC = r(1,2);
% mse = mean((A - B).^2);
% pred(5).MSE = mse;
% 
% %Compare original large and predicted large cochleagrams
% pred(6).comparisons = 'Large-Large Pred';
% A = X_ft_hat_final.large_orig(:); B = X_ft_hat_final.large_decoded(:);
% if norm_pred
%     A = (A - mean(A))./std(A);
%     B = (B - mean(B))./std(B);
% end
% r = corrcoef(A, B);
% pred(6).CC = r(1,2);
% mse = mean((A - B).^2);
% pred(6).MSE = mse;
% 
% %Compare original small and predicted large cochleagrams
% pred(7).comparisons = 'Small-Large Pred';
% A = X_ft_hat_final.small_orig(:); B = X_ft_hat_final.large_decoded(:);
% if norm_pred
%     A = (A - mean(A))./std(A);
%     B = (B - mean(B))./std(B);
% end
% r = corrcoef(A, B);
% pred(7).CC = r(1,2);
% mse = mean((A - B).^2);
% pred(7).MSE = mse;


%% Compute pvals for the comparisons

% Small room
% pval.CC.small = signrank(pred.CC(:,1), pred.CC(:,2));
% pval.MSE.small = signrank(pred.MSE(:,1), pred.MSE(:,2));
% 
% Medium room
% pval.CC.med = signrank(pred.CC(:,3), pred.CC(:,4));
% pval.MSE.med = signrank(pred.MSE(:,3), pred.MSE(:,4));
% 
% Large room
% pval.CC.large = signrank(pred.CC(:,5), pred.CC(:,6));
% pval.MSE.large = signrank(pred.MSE(:,5), pred.MSE(:,6));