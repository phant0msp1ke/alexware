%% First, we get the cortical data and make psths to be able to compute CC for every neuron
clc; clear all; close all;
%Define params and paths
cluster_dir = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/For_analysis/All_data/';
lnp_dir = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/Kernel_fits/kfolds/LNP_model_noneuron_norm/perfreq_noneuro/ridge/10ms/Predicted_PSTHs_noneuro_norm';
lnp_data = '/mnt/40086D4C086D41D0/Reverb_paper/fig3_sup2/LNP_correct/lnp_data.mat';
neural_data_path = '/mnt/40086D4C086D41D0/Reverb_paper/fig3_sup2/Neuronal_data/neuronal_data.mat';
save_dir = '/mnt/40086D4C086D41D0/Reverb_paper/Revisions/Point_1';
save_dir_specific = '/mnt/40086D4C086D41D0/Reverb_paper/Revisions/Point_1/Spike_count';

neurons = load(neural_data_path);
lnp = load(lnp_data);

% coch_file = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/Cochleagrams/Cochleagrams/New_th/Ronnie_PLP/specpower/10ms/coch_all_conditions_specpower.mat';
NPSP_th = 40;
n_stim = 6;
n_rep = 10;
dt_ms = 10;
t_end_s = 36;

% Select the clusters to fit
load(fullfile(cluster_dir,'info.mat'),'info'); %Load the info file with the meta data
ix = info.NPSP<NPSP_th; %Find only the neurons that are below the NPSP th
%Get the necessary fields
cluster_ids = info.cluster_id(ix);
pen_names = info.pen_name(ix);
animal_names = info.animal_name(ix);
qualities = info.quality(ix);
NPSPs = info.NPSP(ix);
n_clusters = sum(ix);

rez.neurons.CC = zeros(n_clusters,1);
rez.neurons.FF.small.mean = zeros(n_clusters,1);
rez.neurons.FF.small.std = zeros(n_clusters,1);
rez.neurons.FF.large.mean = zeros(n_clusters,1);
rez.neurons.FF.large.std = zeros(n_clusters,1);
rez.neurons.FF.mean = zeros(n_clusters,1);

%Load the cochleagram
% coch = load(coch_file,'coch');
% t_edges_s = coch.coch(1).t;

%Get the PSTHs for the stimuli and compute FF and CC
dt_s = dt_ms/1000;
t_edges_s = [0:dt_s:t_end_s];

fprintf('== Computing CC and FF ==\n');
tic;
for j = 1:n_clusters

    temp = load(fullfile(cluster_dir,strjoin({animal_names{j},pen_names{j},num2str(cluster_ids(j))},'_')),'data');
    animal_name = temp.data.params.animal_name;
    y = temp.data;

    for s = 1:n_stim
        psth_temp = [];
        for r = 1:n_rep
            psth_temp(r,:) = histc(y.stim(s).repeat(r).spiketimes,t_edges_s);
            isi_temp{r} = diff(y.stim(s).repeat(r).spiketimes);
        end
        y_temp{s} = psth_temp;
        isi{s} = isi_temp;
    end

    if ismember(animal_name,["Ronnie","PLP","Cork","Kilkenny","Derry"])
        y_t_small = [y_temp{3}, y_temp{4}];
        isi_small = [isi{3}, isi{4}];
    elseif ismember(animal_name,["Noah","Derekah"])
        y_t_small = [y_temp{1}, y_temp{2}];
        isi_small = [isi{1}, isi{2}];
    else
        error('Unrecognized animal name');

    end
    y_t_large = [y_temp{5}, y_temp{6}];
    isi_large = [isi{5}, isi{6}];
    
    %Calculate the mean responses
    y_t_small_mean = mean(y_t_small);
    y_t_large_mean = mean(y_t_large);
    
    %Save for later plotting
    y_plot.neurons.small(j,:) = y_t_small_mean;
    y_plot.neurons.large(j,:) = y_t_large_mean;

    %Calculate FF using ISI
    ff_isi_small_temp = cell2mat(cellfun(@(x) var(x,'omitnan')/mean(x,'omitnan'),isi_small,'UniformOutput',false));
    ff_isi_large_temp = cell2mat(cellfun(@(x) var(x,'omitnan')/mean(x,'omitnan'),isi_large,'UniformOutput',false));
    rez.neurons.FF_ISI.small.mean(j) = mean(ff_isi_small_temp, 'omitnan');
    rez.neurons.FF_ISI.large.mean(j) = mean(ff_isi_large_temp, 'omitnan');
    rez.neurons.FF_ISI.small.std(j) = std(ff_isi_small_temp, 'omitnan');
    rez.neurons.FF_ISI.large.std(j) = std(ff_isi_large_temp, 'omitnan');

    rez.neurons.FF_ISI.mean(j) = mean([rez.neurons.FF_ISI.small.mean(j), rez.neurons.FF_ISI.large.mean(j)]);

    %Calculate the fano factor for each stimulus and together
    ff_small_temp = var(y_t_small,'omitnan')./mean(y_t_small,'omitnan');
    ff_large_temp = var(y_t_large,'omitnan')./mean(y_t_large,'omitnan');

    rez.neurons.FF.small.mean(j) = mean(ff_small_temp,'omitnan');
    rez.neurons.FF.small.std(j) = std(ff_small_temp,'omitnan');
    rez.neurons.FF.large.mean(j) = mean(ff_large_temp,'omitnan');
    rez.neurons.FF.large.std(j) = std(ff_large_temp,'omitnan');

    rez.neurons.FF.mean(j) = mean([rez.neurons.FF.small.mean(j), rez.neurons.FF.large.mean(j)]);

    %Calculate the CC
    cc = corrcoef(y_t_small_mean, y_t_large_mean);
    rez.neurons.CC(j,1) = cc(1,2);

end

fprintf('== Done! This took %0.fs ==\n',toc);

%% Compute CC for the LNP neurons

% Select the clusters to fit
% load(fullfile(lnp_dir,'info.mat'),'info'); %Load the info file with the meta data
% ix_lnp = info.NPSP<NPSP_th; %Find only the neurons that are below the NPSP th
% %Get the necessary fields
% cluster_ids_lnp = info.cluster_id(ix_lnp);
% pen_names_lnp = info.pen_name(ix_lnp);
% animal_names_lnp = info.animal_name(ix_lnp);
% qualities_lnp = info.quality(ix_lnp);
% NPSPs_lnp = info.NPSP(ix_lnp);
% n_clusters_lnp = sum(ix_lnp);

for j = 1:n_clusters

    temp = load(fullfile(lnp_dir,strjoin({animal_names{j},pen_names{j},num2str(cluster_ids(j))},'_')),'psth');

    y_t_small_mean = temp.psth.y_t_small_mean;
    y_t_large_mean = temp.psth.y_t_big_mean;
    
    %Save for later plotting
    y_plot.lnp.small(j,:) = y_t_small_mean;
    y_plot.lnp.large(j,:) = y_t_large_mean;
    
    %Calculate the CC
    cc = corrcoef(y_t_small_mean, y_t_large_mean);
    rez.LNP.CC(j,1) = cc(1,2);

end

%% Compute stats
[p_val,~,stats_cc] = signrank(rez.neurons.CC, rez.LNP.CC);

%% Plot the firing rates for neurons and lno, small vs large room

% row = 5;
% col = 5;
% per = 0.03;
% edgel = per; edger = 0.02; edgeh = 0.05; edgeb = 0.05; space_h = 0.06; space_v = 0.09;
% [pos]=subplot_pos(row,col,edgel,edger,edgeh,edgeb,space_h,space_v);
% clust_per_batch = row*col;
% axis_sz = 15;
% sz = 10;
% 
% num_batch = ceil(n_clusters/clust_per_batch);
% curr_n_clust = clust_per_batch;
% 
% count = 0;
% 
% 
% for b = 1:num_batch
%     figure('units','normalized','outerposition',[0 0 1 1]);
%     save_name = fullfile(save_dir_specific,['batch_',num2str(b),'_neurons_lnp_spike_count.svg']);
%     
%     if b == num_batch
%         curr_n_clust = n_clust - (num_batch-1)*clust_per_batch;
%     end
%     
%     for j = 1:curr_n_clust
%         count = count+1;
%         subplot('position',pos{j});
%         scatter(y_plot.neurons.small(count,:), y_plot.neurons.large(count,:),sz,'filled','MarkerEdgeColor','k','MarkerFaceColor','k'); hold on; 
%         scatter(y_plot.lnp.small(count,:), y_plot.lnp.large(count,:),sz,'filled','MarkerEdgeColor','m','MarkerFaceColor','k');
%         max_val = max([y_plot.neurons.small(count,:), y_plot.neurons.large(count,:), y_plot.lnp.small(count,:), y_plot.lnp.large(count,:)]);
%         h1 = refline([1,0]);
%         h1.Color = 'k';
%         h1.LineWidth = 2;
%         axis square;
% %         ylim([-max_val, max_val]);
% %         xlim([-max_val, max_val]);
%         xlabel('Small');
%         ylabel('Large');
%         set(gcf,'color','w');
%         hold off;
%         set(gca,'FontSize',axis_sz,'FontWeight','Normal');
%     end
%     saveas(gcf,save_name);
%     close;
% end

%% Plot the histograms for the Fano Factor from spike count
font_type = 'Liberation Sans';
hist_type = 'bar';
BW = 0.01;
small_col = [0.592, 0.737, 0.384]; large_col = [0.173, 0.373, 0.176];
x_font_sz = 55; y_font_sz = x_font_sz; all_font_sz = 40; axis_sz = 40;
lw1 = 5;
figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
hold on;
histogram(rez.neurons.FF.small.mean,'FaceColor',small_col,'EdgeColor',small_col,'DisplayStyle',hist_type, 'BinWidth',BW);
histogram(rez.neurons.FF.large.mean,'FaceColor',large_col,'EdgeColor',large_col,'DisplayStyle',hist_type, 'BinWidth',BW);
annotation('textbox',[0.8 0.72 0.1 0.1],'String', sprintf('Large'),'LineStyle','none','Color',large_col,'FontSize',axis_sz,'FontWeight','bold');
annotation('textbox',[0.8 0.64 0.1 0.1],'String', sprintf('Small'),'LineStyle','none','Color',small_col,'FontSize',axis_sz,'FontWeight','bold');
xlabel('Fano Factor','FontSize',x_font_sz,'FontWeight','bold');
ylabel('Number of neurons','FontSize',y_font_sz,'FontWeight','bold');
title(['Fano Factor distribution based on spike count in time bin ',num2str(dt_ms),'ms']);
set(gca,'FontSize',all_font_sz,'FontWeight','Normal');
xline(1,'k','LineWidth',lw1);
xline(mean(rez.neurons.FF.mean),'r','LineWidth',lw1);
set(gcf,'color','w');
set(gca,'TickDir','out');
% xlim([-lim_val, lim_val]);
hold off;
save_name = fullfile(save_dir,['Fano factor distirbution from spike count data time bin',num2str(dt_ms),'ms.svg']);
saveas(gcf,save_name)
close;

%% Plot the histograms for the Fano Factor from ISI
font_type = 'Liberation Sans';
hist_type = 'bar';
BW = 0.1;
small_col = [0.592, 0.737, 0.384]; large_col = [0.173, 0.373, 0.176];
x_font_sz = 55; y_font_sz = x_font_sz; all_font_sz = 40; axis_sz = 40;
lw1 = 5;
figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
hold on;
histogram(rez.neurons.FF_ISI.small.mean,'FaceColor',small_col,'EdgeColor',small_col,'DisplayStyle',hist_type, 'BinWidth',BW);
histogram(rez.neurons.FF_ISI.large.mean,'FaceColor',large_col,'EdgeColor',large_col,'DisplayStyle',hist_type, 'BinWidth',BW);
annotation('textbox',[0.8 0.72 0.1 0.1],'String', sprintf('Large'),'LineStyle','none','Color',large_col,'FontSize',axis_sz,'FontWeight','bold');
annotation('textbox',[0.8 0.64 0.1 0.1],'String', sprintf('Small'),'LineStyle','none','Color',small_col,'FontSize',axis_sz,'FontWeight','bold');
xlabel('Fano Factor','FontSize',x_font_sz,'FontWeight','bold');
ylabel('Number of neurons','FontSize',y_font_sz,'FontWeight','bold');
title(['Fano Factor distribution based on ISI']);
set(gca,'FontSize',all_font_sz,'FontWeight','Normal');
xline(1,'k','LineWidth',lw1);
xline(mean(rez.neurons.FF_ISI.mean),'r','LineWidth',lw1);
set(gcf,'color','w');
set(gca,'TickDir','out');
% xlim([-lim_val, lim_val]);
hold off;
save_name = fullfile(save_dir,['Fano factor distirbution from ISI.svg']);
saveas(gcf,save_name)
close;

%% Plot a scatter plot of the FF vs difference in COM for LNP

figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
sz = 30;
ylim_val = 25;
scatter(rez.neurons.FF.mean, lnp.diff_vec.com.neg ,sz,'filled','MarkerEdgeColor','b','MarkerFaceColor','b'); 
yline(0,'k--','LineWidth',lw1);
xlabel('Fano Factor','FontSize',x_font_sz,'FontWeight','bold');
ylabel('COM inhibitory difference [ms]','FontSize',y_font_sz,'FontWeight','bold');
title(['Inhibitory COM difference vs FF LNP']);
% annotation('textbox',[0.65 0.2 0.1 0.1],'String', sprintf('n=%0.f',n_clust),'LineStyle','none','Color','k','FontSize',all_font_sz,'FontWeight','bold');
% annotation('textbox',[0.8 0.86 0.1 0.1],'String', sprintf('p=%s',p_star(p_val)),'LineStyle','none','Color','k','FontSize',all_font_sz,'FontWeight','bold');
% axis equal;
set(gca,'FontSize',all_font_sz,'FontWeight','Bold');
set(gcf,'color','w');
set(gca,'TickDir','out');
% xlim(l);
ylim([-ylim_val, ylim_val]);
% hline = refline(1,0);
% hline.Color = 'k';
% hline.LineWidth = lw;
% hline.LineStyle = '--';
save_name = fullfile(save_dir,['Inhibitory COM vs FF LNP.svg']);
saveas(gcf,save_name)
close;

%% Plot a scatter plot of the FF vs difference in PT for LNP
figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
sz = 30;
ylim_val = 50;
scatter(rez.neurons.FF.mean, lnp.diff_vec.pt.neg ,sz,'filled','MarkerEdgeColor','b','MarkerFaceColor','b'); 
yline(0,'k--','LineWidth',lw1);
xlabel('Fano Factor','FontSize',x_font_sz,'FontWeight','bold');
ylabel('PT inhibitory difference [ms]','FontSize',y_font_sz,'FontWeight','bold');
title(['Inhibitory PT difference vs FF LNP']);
% annotation('textbox',[0.65 0.2 0.1 0.1],'String', sprintf('n=%0.f',n_clust),'LineStyle','none','Color','k','FontSize',all_font_sz,'FontWeight','bold');
% annotation('textbox',[0.8 0.86 0.1 0.1],'String', sprintf('p=%s',p_star(p_val)),'LineStyle','none','Color','k','FontSize',all_font_sz,'FontWeight','bold');
% axis equal;
set(gca,'FontSize',all_font_sz,'FontWeight','Bold');
set(gcf,'color','w');
set(gca,'TickDir','out');
% xlim(l);
ylim([-ylim_val, ylim_val]);
% hline = refline(1,0);
% hline.Color = 'k';
% hline.LineWidth = lw;
% hline.LineStyle = '--';
save_name = fullfile(save_dir,['Inhibitory PT vs FF LNP.svg']);
saveas(gcf,save_name)
close;

%% Plot a scatter plot of the FF vs difference in COM for neurons

figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
sz = 30;
ylim_val = 25;
scatter(rez.neurons.FF.mean, neurons.diff_vec.com.neg ,sz,'filled','MarkerEdgeColor','b','MarkerFaceColor','b'); 
yline(0,'k--','LineWidth',lw1);
xlabel('Fano Factor','FontSize',x_font_sz,'FontWeight','bold');
ylabel('COM inhibitory difference [ms]','FontSize',y_font_sz,'FontWeight','bold');
title(['Inhibitory COM difference vs FF neurons']);
% annotation('textbox',[0.65 0.2 0.1 0.1],'String', sprintf('n=%0.f',n_clust),'LineStyle','none','Color','k','FontSize',all_font_sz,'FontWeight','bold');
% annotation('textbox',[0.8 0.86 0.1 0.1],'String', sprintf('p=%s',p_star(p_val)),'LineStyle','none','Color','k','FontSize',all_font_sz,'FontWeight','bold');
% axis equal;
set(gca,'FontSize',all_font_sz,'FontWeight','Bold');
set(gcf,'color','w');
set(gca,'TickDir','out');
% xlim(l);
ylim([-ylim_val, ylim_val]);
% hline = refline(1,0);
% hline.Color = 'k';
% hline.LineWidth = lw;
% hline.LineStyle = '--';
save_name = fullfile(save_dir,['Inhibitory COM vs FF neurons.svg']);
saveas(gcf,save_name)
close;

%% Plot a scatter plot of the FF vs difference in PT for neurons
figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
sz = 30;
ylim_val = 50;
scatter(rez.neurons.FF.mean, neurons.diff_vec.pt.neg ,sz,'filled','MarkerEdgeColor','b','MarkerFaceColor','b'); 
yline(0,'k--','LineWidth',lw1);
xlabel('Fano Factor','FontSize',x_font_sz,'FontWeight','bold');
ylabel('PT inhibitory difference [ms]','FontSize',y_font_sz,'FontWeight','bold');
title(['Inhibitory PT difference vs FF neurons']);
% annotation('textbox',[0.65 0.2 0.1 0.1],'String', sprintf('n=%0.f',n_clust),'LineStyle','none','Color','k','FontSize',all_font_sz,'FontWeight','bold');
% annotation('textbox',[0.8 0.86 0.1 0.1],'String', sprintf('p=%s',p_star(p_val)),'LineStyle','none','Color','k','FontSize',all_font_sz,'FontWeight','bold');
% axis equal;
set(gca,'FontSize',all_font_sz,'FontWeight','Bold');
set(gcf,'color','w');
set(gca,'TickDir','out');
% xlim(l);
ylim([-ylim_val, ylim_val]);
% hline = refline(1,0);
% hline.Color = 'k';
% hline.LineWidth = lw;
% hline.LineStyle = '--';
save_name = fullfile(save_dir,['Inhibitory PT vs FF neurons.svg']);
saveas(gcf,save_name)
close;

%% Compute stats for COM LNP - neurons difference
diff_com_neg = lnp.diff_vec.com.neg(:) - neurons.diff_vec.com.neg(:);
X_com_neg = [ones(length(diff_com_neg),1), diff_com_neg];
[b.com.neg,~,~,~,stats.com.neg] = regress(rez.neurons.FF.mean(:), X_com_neg);

r_val.com.neg =  sign(b.com.neg(2))*sqrt(stats.com.neg(1)); p.com.neg = stats.com.neg(3);

%% Plot a scatter plot of the FF vs difference in COM for LNP vs neurons

figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
sz = 30;
ylim_val = 30;
scatter(rez.neurons.FF.mean, diff_com_neg,sz,'filled','MarkerEdgeColor','b','MarkerFaceColor','b'); hold on
yline(0,'k--','LineWidth',lw1);
% h1 = refline([b.com.neg(2),b.com.neg(1)]);
% h1.Color = 'k';
% h1.LineWidth = 3;
xlabel('Fano Factor','FontSize',x_font_sz,'FontWeight','bold');
ylabel('COM inhibitory difference [ms]','FontSize',y_font_sz,'FontWeight','bold');
title(['Inhibitory COM LNP - neurons difference vs FF neurons']);
% annotation('textbox', [0.75, 0.72, 0.3, 0.2], 'String', sprintf('\\beta_{1}=%.3f\n',b.com.neg(2)),'LineStyle','none','Color','k','FontSize',all_font_sz,'FontWeight','Normal');
% annotation('textbox', [0.75, 0.65, 0.3, 0.2], 'String', sprintf('r_{big}=%.2f\n',r_val.com.neg),'LineStyle','none','Color','k','FontSize',all_font_sz,'FontWeight','Normal');
% annotation('textbox', [0.75, 0.58, 0.3, 0.2], 'String', sprintf('p=%.5f\n',p.com.neg),'LineStyle','none','Color','k','FontSize',all_font_sz,'FontWeight','Normal');
% axis equal;
set(gca,'FontSize',all_font_sz,'FontWeight','Bold');
set(gcf,'color','w');
set(gca,'TickDir','out');
% xlim(l);
ylim([-ylim_val, ylim_val]);
% hline = refline(1,0);
% hline.Color = 'k';
% hline.LineWidth = lw;
% hline.LineStyle = '--';
hold off;
save_name = fullfile(save_dir,['Inhibitory COM LNP - neurons vs FF.svg']);
saveas(gcf,save_name)
close;



%% Plot a scatter plot of the FF vs difference in PT for LNP vs neurons
figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
sz = 30;
ylim_val = 50;
scatter(rez.neurons.FF.mean,  lnp.diff_vec.pt.neg - neurons.diff_vec.pt.neg ,sz,'filled','MarkerEdgeColor','b','MarkerFaceColor','b'); 
yline(0,'k--','LineWidth',lw1);
xlabel('Fano Factor','FontSize',x_font_sz,'FontWeight','bold');
ylabel('PT inhibitory difference [ms]','FontSize',y_font_sz,'FontWeight','bold');
title(['Inhibitory PT LNP - neurons difference vs FF neurons']);
% annotation('textbox',[0.65 0.2 0.1 0.1],'String', sprintf('n=%0.f',n_clust),'LineStyle','none','Color','k','FontSize',all_font_sz,'FontWeight','bold');
% annotation('textbox',[0.8 0.86 0.1 0.1],'String', sprintf('p=%s',p_star(p_val)),'LineStyle','none','Color','k','FontSize',all_font_sz,'FontWeight','bold');
% axis equal;
set(gca,'FontSize',all_font_sz,'FontWeight','Bold');
set(gcf,'color','w');
set(gca,'TickDir','out');
% xlim(l);
ylim([-ylim_val, ylim_val]);
% hline = refline(1,0);
% hline.Color = 'k';
% hline.LineWidth = lw;
% hline.LineStyle = '--';
save_name = fullfile(save_dir,['Inhibitory PT LNP - neurons vs FF.svg']);
saveas(gcf,save_name)
close;

%% Plot CC

% Define params for histogram plot
hist_type = 'bar'; %The type of the histogram
font_type = 'Liberation Sans';
model = 'CC_compare';
all_font_sz = 55;
lw1 = 7; lw2 = 3;
%Plot general properties
params_norm_his.fit = 'neurons'; params_norm_his.calc_method = 'bf';
params_norm_his.hist_type = hist_type; params_norm_his.get_neurons = 'all'; params_norm_his.NPSP_th = NPSP_th; params_norm_his.model = 'psth'; params_norm_his.font_type = font_type;
%Plot colours
params_norm_his.his_color = 'k';
%Plot size
params_norm_his.all_font_sz = all_font_sz; params_norm_his.lw1 = lw1; params_norm_his.lw2 = lw2;
%Save dir
params_norm_his.save_dir =  save_dir;

params_norm_his.specific_name = [' CC histogram plot comparing cortical neurons and LNP sim neurons new '];
params_norm_his.units = 'normal';
params_norm_his.big_val = rez.neurons.CC; params_norm_his.small_val = rez.LNP.CC;
params_norm_his.p_val = p_val;
params_norm_his.his_spacing = 0.01; params_norm_his.lim_val = 0.35;
plot_norm_histogram(params_norm_his);