%% First, we get the cortical data and make psths to be able to compute CC for every neuron
clc; clear all; close all;
%Define params and paths
cluster_dir = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/For_analysis/All_data/';
lnp_dir = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/Kernel_fits/kfolds/LNP_model_sim_noneuron_norm_anech/LNP_model_noneuron_norm/perfreq_noneuro/ridge/10ms/Predicted_PSTHs_noneuro_norm';
% lnp_data = '/mnt/40086D4C086D41D0/Reverb_paper/fig3_sup2/LNP_correct/lnp_data.mat';
% neural_data_path = '/mnt/40086D4C086D41D0/Reverb_paper/fig3_sup2/Neuronal_data/neuronal_data.mat';
save_dir = '/mnt/40086D4C086D41D0/Reverb_paper/eLife_Revisions/Point_1';
save_dir_specific = '/mnt/40086D4C086D41D0/Reverb_paper/Revisions/Point_1/Spike_count';

% neurons = load(neural_data_path);
% lnp = load(lnp_data);

% coch_file = '/mnt/40086D4C086D41D0/Reverb_neuronal_data/Cochleagrams/Cochleagrams/New_th/Ronnie_PLP/specpower/10ms/coch_all_conditions_specpower.mat';
NPSP_th = 40;
n_stim = 6;
n_rep = 10;
dt_ms = 10;
t_end_s = 36;

% Select the clusters to fit
load(fullfile(lnp_dir,'info.mat'),'info'); %Load the info file with the meta data
ix = info.NPSP<NPSP_th; %Find only the neurons that are below the NPSP th
%Get the necessary fields
cluster_ids = info.cluster_id(ix);
pen_names = info.pen_name(ix);
animal_names = info.animal_name(ix);
qualities = info.quality(ix);
NPSPs = info.NPSP(ix);
n_clusters = sum(ix);

% rez.neurons.CC = zeros(n_clusters,1);
% rez.neurons.FF.small.mean = zeros(n_clusters,1);
% rez.neurons.FF.small.std = zeros(n_clusters,1);
% rez.neurons.FF.large.mean = zeros(n_clusters,1);
% rez.neurons.FF.large.std = zeros(n_clusters,1);
% rez.neurons.FF.mean = zeros(n_clusters,1);

%Load the cochleagram
% coch = load(coch_file,'coch');
% t_edges_s = coch.coch(1).t;

%Get the PSTHs for the stimuli and compute FF and CC
dt_s = dt_ms/1000;
t_edges_s = [0:dt_s:t_end_s];

fprintf('== Computing CC and FF ==\n');
tic;
for j = 1:n_clusters

    temp = load(fullfile(cluster_dir,strjoin({animal_names{j},pen_names{j},num2str(cluster_ids(j))},'_')),'data');
    animal_name = temp.data.params.animal_name;
    y = temp.data;

    for s = 1:n_stim
        psth_temp = [];
        for r = 1:n_rep
            psth_temp(r,:) = histc(y.stim(s).repeat(r).spiketimes,t_edges_s);
            isi_temp{r} = diff(y.stim(s).repeat(r).spiketimes);
        end
        y_temp{s} = psth_temp;
        isi{s} = isi_temp;
    end
    
    y_t_anech = [y_temp{1}, y_temp{2}];
    isi_anech = [isi{1}, isi{2}];
    
    y_t_small = [y_temp{3}, y_temp{4}];
    isi_small = [isi{3}, isi{4}];
    
    y_t_large = [y_temp{5}, y_temp{6}];
    isi_large = [isi{5}, isi{6}];
    
    %Calculate the mean responses
    y_t_anech_mean = mean(y_t_anech);
    y_t_small_mean = mean(y_t_small);
    y_t_large_mean = mean(y_t_large);
    
    %Save for later plotting
    y_plot.neurons.anech(j,:) = y_t_anech_mean;
    y_plot.neurons.small(j,:) = y_t_small_mean;
    y_plot.neurons.large(j,:) = y_t_large_mean;

    %Calculate FF using ISI
    ff_isi_anech_temp = cell2mat(cellfun(@(x) var(x,'omitnan')/mean(x,'omitnan'),isi_anech,'UniformOutput',false));
    ff_isi_small_temp = cell2mat(cellfun(@(x) var(x,'omitnan')/mean(x,'omitnan'),isi_small,'UniformOutput',false));
    ff_isi_large_temp = cell2mat(cellfun(@(x) var(x,'omitnan')/mean(x,'omitnan'),isi_large,'UniformOutput',false));
    rez.neurons.FF_ISI.anech.mean(j) = mean(ff_isi_anech_temp, 'omitnan');
    rez.neurons.FF_ISI.small.mean(j) = mean(ff_isi_small_temp, 'omitnan');
    rez.neurons.FF_ISI.large.mean(j) = mean(ff_isi_large_temp, 'omitnan');
    rez.neurons.FF_ISI.anech.std(j) = std(ff_isi_anech_temp, 'omitnan');
    rez.neurons.FF_ISI.small.std(j) = std(ff_isi_small_temp, 'omitnan');
    rez.neurons.FF_ISI.large.std(j) = std(ff_isi_large_temp, 'omitnan');

    rez.neurons.FF_ISI.mean(j) = mean([rez.neurons.FF_ISI.anech.mean(j), rez.neurons.FF_ISI.small.mean(j), rez.neurons.FF_ISI.large.mean(j)]);

    %Calculate the fano factor for each stimulus and together
    ff_anech_temp = var(y_t_anech,'omitnan')./mean(y_t_anech,'omitnan');
    ff_small_temp = var(y_t_small,'omitnan')./mean(y_t_small,'omitnan');
    ff_large_temp = var(y_t_large,'omitnan')./mean(y_t_large,'omitnan');
    
    rez.neurons.FF.anech.mean(j) = mean(ff_anech_temp,'omitnan');
    rez.neurons.FF.anech.std(j) = std(ff_anech_temp,'omitnan');
    rez.neurons.FF.small.mean(j) = mean(ff_small_temp,'omitnan');
    rez.neurons.FF.small.std(j) = std(ff_small_temp,'omitnan');
    rez.neurons.FF.large.mean(j) = mean(ff_large_temp,'omitnan');
    rez.neurons.FF.large.std(j) = std(ff_large_temp,'omitnan');

    rez.neurons.FF.mean(j) = mean([rez.neurons.FF.anech.mean(j), rez.neurons.FF.small.mean(j), rez.neurons.FF.large.mean(j)]);

    %Calculate the CC
    cc = corrcoef(y_t_anech_mean, y_t_small_mean);
    rez.neurons.CC.small_anech(j,1) = cc(1,2);
    cc = corrcoef(y_t_anech_mean, y_t_large_mean);
    rez.neurons.CC.large_anech(j,1) = cc(1,2);
    cc = corrcoef(y_t_small_mean, y_t_large_mean);
    rez.neurons.CC.large_small(j,1) = cc(1,2);
    
end

fprintf('== Done! This took %0.fs ==\n',toc);

%% Plot the histograms for the Fano Factor from spike count
font_type = 'Liberation Sans';
hist_type = 'bar';
BW = 0.01;
anech_col = [0.7400, 0.9213, 0.4800]; small_col = [0.592, 0.737, 0.384]; large_col = [0.173, 0.373, 0.176];
x_font_sz = 55; y_font_sz = x_font_sz; all_font_sz = 40; axis_sz = 40;
lw1 = 5;
figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
hold on;
histogram(rez.neurons.FF.anech.mean,'FaceColor',anech_col,'EdgeColor',anech_col,'DisplayStyle',hist_type, 'BinWidth',BW);
histogram(rez.neurons.FF.small.mean,'FaceColor',small_col,'EdgeColor',small_col,'DisplayStyle',hist_type, 'BinWidth',BW);
histogram(rez.neurons.FF.large.mean,'FaceColor',large_col,'EdgeColor',large_col,'DisplayStyle',hist_type, 'BinWidth',BW);
annotation('textbox',[0.8 0.72 0.1 0.1],'String', sprintf('Large'),'LineStyle','none','Color',large_col,'FontSize',axis_sz,'FontWeight','bold');
annotation('textbox',[0.8 0.64 0.1 0.1],'String', sprintf('Small'),'LineStyle','none','Color',small_col,'FontSize',axis_sz,'FontWeight','bold');
annotation('textbox',[0.8 0.56 0.1 0.1],'String', sprintf('Anech'),'LineStyle','none','Color',anech_col,'FontSize',axis_sz,'FontWeight','bold');
xlabel('Fano Factor','FontSize',x_font_sz,'FontWeight','bold');
ylabel('Number of neurons','FontSize',y_font_sz,'FontWeight','bold');
title(['Fano Factor distribution based on spike count in time bin ',num2str(dt_ms),'ms']);
set(gca,'FontSize',all_font_sz,'FontWeight','Normal');
xline(1,'k','LineWidth',lw1);
xline(mean(rez.neurons.FF.mean),'r','LineWidth',lw1);
set(gcf,'color','w');
set(gca,'TickDir','out');
% xlim([-lim_val, lim_val]);
hold off;
save_name = fullfile(save_dir,['Fano factor distirbution from spike count data time bin',num2str(dt_ms),'ms.svg']);
saveas(gcf,save_name)
close;

%% Plot the histograms for the Fano Factor from ISI
font_type = 'Liberation Sans';
hist_type = 'bar';
BW = 0.1;
x_font_sz = 55; y_font_sz = x_font_sz; all_font_sz = 40; axis_sz = 40;
lw1 = 5;
figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
hold on;
histogram(rez.neurons.FF_ISI.anech.mean,'FaceColor',anech_col,'EdgeColor',anech_col,'DisplayStyle',hist_type, 'BinWidth',BW);
histogram(rez.neurons.FF_ISI.small.mean,'FaceColor',small_col,'EdgeColor',small_col,'DisplayStyle',hist_type, 'BinWidth',BW);
histogram(rez.neurons.FF_ISI.large.mean,'FaceColor',large_col,'EdgeColor',large_col,'DisplayStyle',hist_type, 'BinWidth',BW);
annotation('textbox',[0.8 0.72 0.1 0.1],'String', sprintf('Large'),'LineStyle','none','Color',large_col,'FontSize',axis_sz,'FontWeight','bold');
annotation('textbox',[0.8 0.64 0.1 0.1],'String', sprintf('Small'),'LineStyle','none','Color',small_col,'FontSize',axis_sz,'FontWeight','bold');
annotation('textbox',[0.8 0.56 0.1 0.1],'String', sprintf('Anech'),'LineStyle','none','Color',small_col,'FontSize',axis_sz,'FontWeight','bold');
xlabel('Fano Factor','FontSize',x_font_sz,'FontWeight','bold');
ylabel('Number of neurons','FontSize',y_font_sz,'FontWeight','bold');
title(['Fano Factor distribution based on ISI']);
set(gca,'FontSize',all_font_sz,'FontWeight','Normal');
xline(1,'k','LineWidth',lw1);
xline(mean(rez.neurons.FF_ISI.mean),'r','LineWidth',lw1);
set(gcf,'color','w');
set(gca,'TickDir','out');
% xlim([-lim_val, lim_val]);
hold off;
save_name = fullfile(save_dir,['Fano factor distirbution from ISI.svg']);
saveas(gcf,save_name)
close;

%% Compute CC for the LNP neurons

for j = 1:n_clusters

    temp = load(fullfile(lnp_dir,strjoin({animal_names{j},pen_names{j},num2str(cluster_ids(j))},'_')),'psth');

    y_t_anech_mean = temp.psth.y_t_anech_mean;
    y_t_small_mean = temp.psth.y_t_small_mean;
    y_t_large_mean = temp.psth.y_t_big_mean;
    
    %Save for later plotting
    y_plot.lnp.anech(j,:) = y_t_anech_mean;
    y_plot.lnp.small(j,:) = y_t_small_mean;
    y_plot.lnp.large(j,:) = y_t_large_mean;
    
    %Calculate the CC
    cc = corrcoef(y_t_anech_mean, y_t_small_mean);
    rez.LNP.CC.small_anech(j,1) = cc(1,2);
    cc = corrcoef(y_t_anech_mean, y_t_large_mean);
    rez.LNP.CC.large_anech(j,1) = cc(1,2);
    cc = corrcoef(y_t_small_mean, y_t_large_mean);
    rez.LNP.CC.large_small(j,1) = cc(1,2);

end

%% Compute stats
[pval.small_anech,~,stats_cc.small_anech] = signrank(rez.neurons.CC.small_anech, rez.LNP.CC.small_anech);
[pval.large_anech,~,stats_cc.large_anech] = signrank(rez.neurons.CC.large_anech, rez.LNP.CC.large_anech);
[pval.large_small,~,stats_cc.large_small] = signrank(rez.neurons.CC.large_small, rez.LNP.CC.large_small);

diff_CC.median.small_anech = nanmedian(rez.neurons.CC.small_anech - rez.LNP.CC.small_anech);
diff_CC.median.large_anech = nanmedian(rez.neurons.CC.large_anech - rez.LNP.CC.large_anech);
diff_CC.median.large_small = nanmedian(rez.neurons.CC.large_small - rez.LNP.CC.large_small);

diff_CC.mean.small_anech = nanmean(rez.neurons.CC.small_anech - rez.LNP.CC.small_anech);
diff_CC.mean.large_anech = nanmean(rez.neurons.CC.large_anech - rez.LNP.CC.large_anech);
diff_CC.mean.large_small = nanmean(rez.neurons.CC.large_small - rez.LNP.CC.large_small);

%% Plot the firing rates for neurons and lno, small vs large room

% row = 5;
% col = 5;
% per = 0.03;
% edgel = per; edger = 0.02; edgeh = 0.05; edgeb = 0.05; space_h = 0.06; space_v = 0.09;
% [pos]=subplot_pos(row,col,edgel,edger,edgeh,edgeb,space_h,space_v);
% clust_per_batch = row*col;
% axis_sz = 15;
% sz = 10;
% 
% num_batch = ceil(n_clusters/clust_per_batch);
% curr_n_clust = clust_per_batch;
% 
% count = 0;
% 
% 
% for b = 1:num_batch
%     figure('units','normalized','outerposition',[0 0 1 1]);
%     save_name = fullfile(save_dir_specific,['batch_',num2str(b),'_neurons_lnp_spike_count.svg']);
%     
%     if b == num_batch
%         curr_n_clust = n_clust - (num_batch-1)*clust_per_batch;
%     end
%     
%     for j = 1:curr_n_clust
%         count = count+1;
%         subplot('position',pos{j});
%         scatter(y_plot.neurons.small(count,:), y_plot.neurons.large(count,:),sz,'filled','MarkerEdgeColor','k','MarkerFaceColor','k'); hold on; 
%         scatter(y_plot.lnp.small(count,:), y_plot.lnp.large(count,:),sz,'filled','MarkerEdgeColor','m','MarkerFaceColor','k');
%         max_val = max([y_plot.neurons.small(count,:), y_plot.neurons.large(count,:), y_plot.lnp.small(count,:), y_plot.lnp.large(count,:)]);
%         h1 = refline([1,0]);
%         h1.Color = 'k';
%         h1.LineWidth = 2;
%         axis square;
% %         ylim([-max_val, max_val]);
% %         xlim([-max_val, max_val]);
%         xlabel('Small');
%         ylabel('Large');
%         set(gcf,'color','w');
%         hold off;
%         set(gca,'FontSize',axis_sz,'FontWeight','Normal');
%     end
%     saveas(gcf,save_name);
%     close;
% end


%% Plot CC Small-Anech

% Define params for histogram plot
hist_type = 'bar'; %The type of the histogram
font_type = 'Liberation Sans';
model = 'CC_compare';
all_font_sz = 55;
lw1 = 7; lw2 = 3;
%Plot general properties
params_norm_his.fit = 'neurons'; params_norm_his.calc_method = 'bf';
params_norm_his.hist_type = hist_type; params_norm_his.get_neurons = 'all'; params_norm_his.NPSP_th = NPSP_th; params_norm_his.model = 'psth'; params_norm_his.font_type = font_type;
%Plot colours
params_norm_his.his_color = 'k';
%Plot size
params_norm_his.all_font_sz = all_font_sz; params_norm_his.lw1 = lw1; params_norm_his.lw2 = lw2;
%Save dir
params_norm_his.save_dir =  save_dir;

params_norm_his.specific_name = [' CC Small-Anech histogram plot comparing cortical neurons and LNP sim neurons new '];
params_norm_his.units = 'normal';
params_norm_his.big_val = rez.neurons.CC.small_anech; params_norm_his.small_val = rez.LNP.CC.small_anech;
params_norm_his.p_val = pval.small_anech;
params_norm_his.his_spacing = 0.01; params_norm_his.lim_val = 0.3;
plot_norm_histogram(params_norm_his);

%% Plot CC Large-Anech

% Define params for histogram plot
hist_type = 'bar'; %The type of the histogram
font_type = 'Liberation Sans';
model = 'CC_compare';
all_font_sz = 55;
lw1 = 7; lw2 = 3;
%Plot general properties
params_norm_his.fit = 'neurons'; params_norm_his.calc_method = 'bf';
params_norm_his.hist_type = hist_type; params_norm_his.get_neurons = 'all'; params_norm_his.NPSP_th = NPSP_th; params_norm_his.model = 'psth'; params_norm_his.font_type = font_type;
%Plot colours
params_norm_his.his_color = 'k';
%Plot size
params_norm_his.all_font_sz = all_font_sz; params_norm_his.lw1 = lw1; params_norm_his.lw2 = lw2;
%Save dir
params_norm_his.save_dir =  save_dir;

params_norm_his.specific_name = [' CC Large-Anech histogram plot comparing cortical neurons and LNP sim neurons new '];
params_norm_his.units = 'normal';
params_norm_his.big_val = rez.neurons.CC.large_anech; params_norm_his.small_val = rez.LNP.CC.large_anech;
params_norm_his.p_val = pval.large_anech;
params_norm_his.his_spacing = 0.01; params_norm_his.lim_val = 0.3;
plot_norm_histogram(params_norm_his);

%% Plot CC Large-Small

% Define params for histogram plot
hist_type = 'bar'; %The type of the histogram
font_type = 'Liberation Sans';
model = 'CC_compare';
all_font_sz = 55;
lw1 = 7; lw2 = 3;
%Plot general properties
params_norm_his.fit = 'neurons'; params_norm_his.calc_method = 'bf';
params_norm_his.hist_type = hist_type; params_norm_his.get_neurons = 'all'; params_norm_his.NPSP_th = NPSP_th; params_norm_his.model = 'psth'; params_norm_his.font_type = font_type;
%Plot colours
params_norm_his.his_color = 'k';
%Plot size
params_norm_his.all_font_sz = all_font_sz; params_norm_his.lw1 = lw1; params_norm_his.lw2 = lw2;
%Save dir
params_norm_his.save_dir =  save_dir;

params_norm_his.specific_name = [' CC Large-Small histogram plot comparing cortical neurons and LNP sim neurons new '];
params_norm_his.units = 'normal';
params_norm_his.big_val = rez.neurons.CC.large_small; params_norm_his.small_val = rez.LNP.CC.large_small;
params_norm_his.p_val = pval.large_small;
params_norm_his.his_spacing = 0.01; params_norm_his.lim_val = 0.3;
plot_norm_histogram(params_norm_his);