%% Define and load vars
kernel_path = '/mnt/40086D4C086D41D0/Reverb_normative/Model_fits/Current/600s_exp_nojap_400_19k_specpower_kfolds/closed/single/chopped/perfreq/ridge/10ms/200ms/kernel.mat'; %Absolute path to the kernel used for the prediction
coch_path_train = '/mnt/40086D4C086D41D0/Reverb_normative/Cochleagrams/600s_exp_nojap_400_19k_specpower/closed/single/chopped/10ms/specpower/coch_all_conditions_closed_singlepos_specpower.mat'; %Absolute path to the cochleagram used for the prediction
coch_path_test = coch_path_train;
save_dir = '/mnt/40086D4C086D41D0/Reverb_paper/eLife_Revisions/Point_1';

norm = 0; %Optionally normalize the cohcleagrams by removing the mean and dividing by the std

fprintf('== Loading the data ==\n');tic;
load(kernel_path);
coch_train = load(coch_path_train);
% room_names{1} = 'anech_orig'; room_names{2} = 'small_orig'; room_names{3} = 'med_orig'; room_names{4} = 'big_orig';
% room_names{5} = 'anech'; room_names{6} = 'small'; room_names{7} = 'med'; room_names{8} = 'big';

%Get the cochleagrams of the training data
anech_X_ft_train = coch_train.coch(1).X_ft;
small_X_ft_train = coch_train.coch(2).X_ft;
med_X_ft_train = coch_train.coch(3).X_ft;
large_X_ft_train = coch_train.coch(4).X_ft;

%Get the cochleagrams of the test data
coch_test = load(coch_path_test);

anech_X_ft_test = coch_test.coch(1).X_ft;
small_X_ft_test = coch_test.coch(2).X_ft;
med_X_ft_test = coch_test.coch(3).X_ft;
large_X_ft_test = coch_test.coch(4).X_ft;

%Get the kernels
n_ker = length(kernels.small);
for k = 1:n_ker
    kernel_small{k} = kernels.small{k}.main{end};
    kernel_med{k} = kernels.med{k}.main{end};
    kernel_large{k} = kernels.big{k}.main{end};
end
n_h = kernels.n_h;
n_t = size(small_X_ft_test,2);
fprintf('== Done! This took %0.fs ==\n',toc);

%% Normalize the cochleagrams
mean_anech = mean(anech_X_ft_train,2); std_anech = std(anech_X_ft_train,[],2);
mean_small = mean(small_X_ft_train,2); std_small = std(small_X_ft_train,[],2);
mean_med = mean(med_X_ft_train,2); std_med = std(med_X_ft_train,[],2);
mean_large = mean(large_X_ft_train,2); std_large = std(large_X_ft_train,[],2);

anech_X_ft_norm = (anech_X_ft_test - mean_anech)./std_anech;
small_X_ft_norm = (small_X_ft_test - mean_small)./std_small;
med_X_ft_norm = (med_X_ft_test - mean_med)./std_med;
large_X_ft_norm = (large_X_ft_test - mean_large)./std_large;

%% Tensorize
fprintf('== Tensorizing the cochleagrams ==\n');tic;
small_X_fht = tensorize(small_X_ft_norm,n_h);
med_X_fht = tensorize(med_X_ft_norm,n_h);
large_X_fht = tensorize(large_X_ft_norm,n_h);
fprintf('== Done! This took %0.fs ==\n',toc);

%% Make predictions
fprintf('== Making predicted cochleagrams ==\n');tic;
small_anech_X_ft_hat = zeros(n_ker,n_t);
med_anech_X_ft_hat = zeros(n_ker,n_t);
large_anech_X_ft_hat = zeros(n_ker,n_t);

for k = 1:n_ker
    small_anech_X_ft_hat(k,:) = kernelconv(small_X_fht, kernel_small{k});
    med_anech_X_ft_hat(k,:) = kernelconv(med_X_fht, kernel_med{k});
    large_anech_X_ft_hat(k,:) = kernelconv(large_X_fht, kernel_large{k});
end
fprintf('== Done! This took %0.fs ==\n',toc);

%% Undo normalization to convert back to original values
X_ft_hat.small = (small_anech_X_ft_hat.*std_small) + mean_small;
X_ft_hat.med = (med_anech_X_ft_hat.*std_med) + mean_med;
X_ft_hat.large = (large_anech_X_ft_hat.*std_large) + mean_large;

X_ft_orig.anech = anech_X_ft_test;
X_ft_orig.small = small_X_ft_test;
X_ft_orig.med = med_X_ft_test;
X_ft_orig.large = large_X_ft_test;

pred_rooms = fieldnames(X_ft_hat);
orig_rooms = fieldnames(X_ft_orig);

if norm
    
    for j = 1:length(pred_rooms)
        room = pred_rooms{j};
        X_ft_hat.(room) = (X_ft_hat.(room) - mean(X_ft_hat.(room)(:)))./std(X_ft_hat.(room)(:));
    end
    
    for i = 1:length(orig_rooms)
        room = orig_rooms{i};
        X_ft_orig.(room) = (X_ft_orig.(room) - mean(X_ft_orig.(room)(:)))./std( X_ft_orig.(room)(:));
    end
    
end

%% Compute the CC and MSE between original cochleagrams and predcited ones

%Total original
r = corrcoef(X_ft_orig.small(:), X_ft_orig.large(:));
CC.total.orig = r(1,2);
mse = mean((X_ft_orig.small(:) - X_ft_orig.large(:)).^2);
MSE.total.orig = mse;

%Total predicted
r = corrcoef(X_ft_hat.small(:), X_ft_hat.large(:));
CC.total.pred = r(1,2);
mse = mean((X_ft_hat.small(:) - X_ft_hat.large(:)).^2);
MSE.total.pred = mse;

%Individual frequencies
for f = 1:n_ker
    %Original
    r_f = corrcoef(X_ft_orig.small(f,:), X_ft_orig.large(f,:));
    CC.perfreq.orig(f) = r_f(1,2);
    mse_f = mean((X_ft_orig.small(f,:) - X_ft_orig.large(f,:)).^2);
    MSE.perfreq.orig(f) = mse_f;

    %Predicted
    r_f = corrcoef(X_ft_hat.small(f,:), X_ft_hat.large(f,:));
    CC.perfreq.pred(f) = r_f(1,2);
    mse_f = mean((X_ft_hat.small(f,:) - X_ft_hat.large(f,:)).^2);
    MSE.perfreq.pred(f) = mse_f;

end

%% Compute stats for the comparisons


[pval.CC,~,stats.CC] = signrank(CC.perfreq.pred, CC.perfreq.orig);
[pval.MSE,~,stats.MSE] = signrank(MSE.perfreq.pred, MSE.perfreq.orig);

%% Plot the histograms of difference for CC

% Define params for histogram plot
hist_type = 'bar'; %The type of the histogram
font_type = 'Liberation Sans';
model = 'CC_compare';
all_font_sz = 55;
lw1 = 7; lw2 = 3;
%Plot general properties
params_norm_his.fit = 'normative'; params_norm_his.calc_method = 'bf';
params_norm_his.hist_type = hist_type;  params_norm_his.model = 'normative'; params_norm_his.font_type = font_type;
%Plot colours
params_norm_his.his_color = 'k';
%Plot size
params_norm_his.all_font_sz = all_font_sz; params_norm_his.lw1 = lw1; params_norm_his.lw2 = lw2;
%Save dir
params_norm_his.save_dir =  save_dir;

params_norm_his.specific_name = [' CC histogram plot comparing original and predicted cochleagram '];
params_norm_his.units = 'normal';
params_norm_his.big_val = CC.perfreq.pred; params_norm_his.small_val = CC.perfreq.orig;
params_norm_his.p_val = pval.CC;
params_norm_his.his_spacing = 0.01; params_norm_his.lim_val = 0.20;
plot_norm_histogram(params_norm_his);

%% Plot the histograms of difference fro MSE

% Define params for histogram plot
hist_type = 'bar'; %The type of the histogram
font_type = 'Liberation Sans';
model = 'MSE_compare';
all_font_sz = 55;
lw1 = 7; lw2 = 3;
%Plot general properties
params_norm_his.fit = 'normative'; params_norm_his.calc_method = 'bf';
params_norm_his.hist_type = hist_type;  params_norm_his.model = 'normative'; params_norm_his.font_type = font_type;
%Plot colours
params_norm_his.his_color = 'k';
%Plot size
params_norm_his.all_font_sz = all_font_sz; params_norm_his.lw1 = lw1; params_norm_his.lw2 = lw2;
%Save dir
params_norm_his.save_dir =  save_dir;

params_norm_his.specific_name = [' MSE histogram plot comparing original and predicted cochleagram '];
params_norm_his.units = 'normal';
params_norm_his.big_val = MSE.perfreq.pred; params_norm_his.small_val = MSE.perfreq.orig;
params_norm_his.p_val = pval.MSE;
params_norm_his.his_spacing = 4; params_norm_his.lim_val = 50;
plot_norm_histogram(params_norm_his);

%% Plot original and predictions
%Find the max value

if norm
    max_val = 3; %How much to subtract from the cochleagram for normalization
    up_lim_dB = 0; %The upper limit in dB
    low_lim_dB = -5; %The lower limit in dB
else
    max_val = 40; %How much to subtract from the cochleagram for normalization
    up_lim_dB = 0; %The upper limit in dB
    low_lim_dB = -80; %The lower limit in dB
end
start_s = 50;
end_s = 54;
t = coch_test.coch(1).t;
[~,ix_start] = min(abs(t - start_s));
[~,ix_end] = min(abs(t - end_s));
t = t(ix_start:ix_end);
t = t - t(1);

dt_s = 1;
dt = mean(diff(t));
dt_samples = round(dt_s/dt);

color_map = 'inferno';
freqs = fliplr(coch_test.coch(1).params.freqs); 
n_f = length(freqs);
n_tlab = 8;
skip_f = 2;
skip_t = round(length(t)/n_tlab);

for f = 1:numel(freqs)
    y_labels{f} = num2str(freqs(f)./1000,'%.1f');
end

for tm = 1:n_tlab
    x_labels{tm} = num2str(t((tm-1)*skip_t +1),'%.1f');
end

%% Plot individual cochleagrams
sz = 40;
all_font_sz = sz;
x_font_sz = sz;
y_font_sz = sz;
font_type = 'Liberation Sans';


%1 - Small orginal
figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
imagesc(X_ft_orig.small(:,ix_start:ix_end)-max_val);
colormap(color_map);
caxis([low_lim_dB up_lim_dB]);
yticks([1:skip_f:n_f]);
yticklabels(y_labels);
xticks([1:dt_samples:length(t)]);
xticklabels(x_labels);
xlabel('Time (s)','FontSize',x_font_sz,'FontWeight','Normal');
ylabel('Freqeuncy (kHz)','FontSize',y_font_sz,'FontWeight','Normal');
set(gca,'FontSize',all_font_sz,'FontWeight','Normal');
set(gcf,'color','w');
set(gca,'TickDir','out');
save_name = fullfile(save_dir,'Small_coch_original.svg');
saveas(gcf,save_name);
close all;

%2 - Large original
figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
imagesc(X_ft_orig.large(:,ix_start:ix_end)-max_val);
colormap(color_map);
caxis([low_lim_dB up_lim_dB]);
yticks([1:skip_f:n_f]);
yticklabels(y_labels);
xticks([1:dt_samples:length(t)]);
xticklabels(x_labels);
xlabel('Time (s)','FontSize',x_font_sz,'FontWeight','Normal');
ylabel('Freqeuncy (kHz)','FontSize',y_font_sz,'FontWeight','Normal');
set(gca,'FontSize',all_font_sz,'FontWeight','Normal');
set(gcf,'color','w');
set(gca,'TickDir','out');
save_name = fullfile(save_dir,'Large_coch_original.svg');
saveas(gcf,save_name);
close all;

%3 - Small predicted
figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
imagesc(X_ft_hat.small(:,ix_start:ix_end)-max_val);
colormap(color_map);
caxis([low_lim_dB up_lim_dB]);
yticks([1:skip_f:n_f]);
yticklabels(y_labels);
xticks([1:dt_samples:length(t)]);
xticklabels(x_labels);
xlabel('Time (s)','FontSize',x_font_sz,'FontWeight','Normal');
ylabel('Freqeuncy (kHz)','FontSize',y_font_sz,'FontWeight','Normal');
set(gca,'FontSize',all_font_sz,'FontWeight','Normal');
set(gcf,'color','w');
set(gca,'TickDir','out');
save_name = fullfile(save_dir,'Small_coch_predicted.svg');
saveas(gcf,save_name);
close all;

%4 - Large predcited
figure('units','normalized','outerposition',[0 0 1 1],'DefaultTextFontName', font_type, 'DefaultAxesFontName',font_type);
imagesc(X_ft_hat.large(:,ix_start:ix_end)-max_val);
colormap(color_map);
caxis([low_lim_dB up_lim_dB]);
yticks([1:skip_f:n_f]);
yticklabels(y_labels);
xticks([1:dt_samples:length(t)]);
xticklabels(x_labels);
xlabel('Time (s)','FontSize',x_font_sz,'FontWeight','Normal');
ylabel('Freqeuncy (kHz)','FontSize',y_font_sz,'FontWeight','Normal');
set(gca,'FontSize',all_font_sz,'FontWeight','Normal');
set(gcf,'color','w');
set(gca,'TickDir','out');
save_name = fullfile(save_dir,'Large_coch_predcited.svg');
saveas(gcf,save_name);
close all;
