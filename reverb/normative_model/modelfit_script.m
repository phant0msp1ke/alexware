
coch_file = '/mnt/40086D4C086D41D0/Reverb_normative/Cochleagrams/600s_exp_nojap_400_19k_specpower/closed/single/chopped/10ms/bez_fiber_200/coch_all_conditions_closed_singlepos_bez.mat';
save_dir = '/mnt/40086D4C086D41D0/Reverb_normative/Model_fits/Current/600s_exp_nojap_400_19k_bez_200_kfolds';
model = 'ridge';
normz = 'perfreq';
kfolds = 1;
h_max_ms = 200;
n_cores = 20;

make_model(coch_file,h_max_ms,model,normz,n_cores,save_dir,kfolds);
