function get_folders(input_dir, file_type, save_dir)
%This function will get the folder names of all folders containing files of
%certain type and save them as a varaible for latter use

%Get the folder and files names of a all the files in all subfolders of a
%certain type
file_list = subdir(fullfile(input_dir,['*', file_type]));

%Save the folders only as cell array
folder_list = {file_list.folder};

%Save as a .mat file
save(fullfile(save_dir,'folder_list.mat'),'folder_list')