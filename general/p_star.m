function stars = p_star(p_val)
%function stars = p_star(p_val)
%This function takes as an input p_val and returns str with the apropriate
%number of stars

if p_val < 0 || p_val>1
    error('p value is non-sensical')
end

if p_val>=0.05
    stars = '';
elseif p_val<0.05 && p_val>=0.01
    stars = '*';
elseif p_val<0.01 && p_val>=0.001
    stars = '**';
elseif p_val<0.001 && p_val>=0.0001
    stars = '***';
else
    stars = '****';
end
